﻿Imports System.IO
Imports System.Text.RegularExpressions
Public Class SQLObject
    Inherits DataTypes


    Public ReadOnly TableList As List(Of Table) = New List(Of Table)
    Public ReadOnly TypeList As List(Of Type) = New List(Of Type)
    Public ReadOnly StoredList As List(Of StoredProc) = New List(Of StoredProc)
    Public CommandList As List(Of OtherCommands) = New List(Of OtherCommands)


    Dim path As String = ""
    Dim sqlscriptFilePath As StreamWriter = New StreamWriter("D:\sqlscript.sql")

    Dim sqlscript As String
    Sub New(ByVal path As String, ByRef dstDB As String)
        Me.path = path
        sqlscriptFilePath.WriteLine("USE [" + dstDB + "]" + Environment.NewLine + "GO" + Environment.NewLine)
        sqlscriptFilePath.WriteLine("ALTER DATABASE [" + dstDB + "] SET TRUSTWORTHY ON" + Environment.NewLine + "GO" + Environment.NewLine)
    End Sub

    Public Sub Fill()

        Dim TableObj As Table
        Dim TypeObj As Type
        Dim StoredObj As StoredProc
        Dim Command As OtherCommands
        Dim column As Tuple(Of String, String, String, String)
        Dim vissle As String = String.Empty
        Dim vissle2 As String = String.Empty
        Dim vissle3 As String = String.Empty
        Dim match As Match
        Dim StartIndex, EndIndex As Integer


        Dim sr As StreamReader = New StreamReader(path)
        Dim SQLscript As String = sr.ReadToEnd
        Dim myDelims As String() = New String(0) {"GO"}
        Dim Commands As String()
        Dim lines As String()
        Commands = SQLscript.Split(myDelims, StringSplitOptions.RemoveEmptyEntries)


        For Each Com In Commands
            If Com.Contains("CREATE TYPE") Then
                TypeObj.Body = Com
                match = Regex.Match(Com, "\[([A-Za-z0-9\-_]+)\]", RegexOptions.IgnoreCase)
                match = match.NextMatch()
                TypeObj.Name = match.Value.Substring(1, match.Value.Length - 2)
                TypeList.Add(TypeObj)


            ElseIf Com.Contains("CREATE PROCEDURE") Then
                StoredObj.Body = Com.Replace("CREATE", "ALTER")
                match = Regex.Match(Com, "\[([A-Za-z0-9\-_]+)\]", RegexOptions.IgnoreCase)
                match = match.NextMatch()
                StoredObj.Name = match.Value.Substring(1, match.Value.Length - 2)
                StoredList.Add(StoredObj)


            ElseIf Com.Contains("CREATE TABLE") Then
                TableObj.Body = Com

                match = Regex.Match(Com, "\[([A-Za-z0-9\-_]+)\]", RegexOptions.IgnoreCase)
                match = match.NextMatch()
                TableObj.Name = match.Value.Substring(1, match.Value.Length - 2)
                StartIndex = Com.IndexOf("(") + 1
                If Com.IndexOf("CONSTRAINT") > -1 Then
                    EndIndex = Com.IndexOf("CONSTRAINT") - 1
                ElseIf Com.IndexOf("ON") > -1 Then
                    EndIndex = Com.IndexOf("ON") - 1
                Else
                    EndIndex = Com.IndexOf(")") - 1
                End If
                'EndIndex = If(Com.IndexOf("CONSTRAINT") = -1, Com.IndexOf(")") - 1, Com.IndexOf("CONSTRAINT") - 1)
                lines = Com.Substring(StartIndex, EndIndex - StartIndex + 1).Split(Environment.NewLine)
                TableObj.Column = New List(Of Tuple(Of String, String, String, String))
                For Each line In lines
                    vissle = String.Empty
                    vissle2 = String.Empty
                    vissle3 = String.Empty
                    match = Regex.Match(line, "\[([A-Za-z0-9\-_]+)\]", RegexOptions.IgnoreCase)

                    While match.Success
                        vissle = match.Value.Substring(1, match.Value.Length - 2)
                        match = match.NextMatch()
                        vissle2 = match.Value.Substring(1, match.Value.Length - 2)
                        match = match.NextMatch()
                    End While
                    If line.IndexOf("IDENTITY") > -1 Then

                    End If
                    match = Regex.Match(line, "\(([0-9\-,_]+)\)", RegexOptions.IgnoreCase)

                    While match.Success
                        vissle3 = If(line.IndexOf("IDENTITY") > -1, "IDENTITY" + match.Value, match.Value)
                        match = match.NextMatch()

                    End While

                    If line.IndexOf("NOT NULL") > 0 And vissle IsNot String.Empty Then
                        column = New Tuple(Of String, String, String, String)(vissle, vissle2, vissle3, "NOT NULL")
                        'vissle = vissle2 = vissle3 = String.Empty
                    ElseIf line.IndexOf("NULL") > 0 And vissle IsNot String.Empty Then
                        column = New Tuple(Of String, String, String, String)(vissle, vissle2, vissle3, "NULL")
                        'vissle = vissle2 = vissle3 = String.Empty
                    ElseIf vissle IsNot String.Empty AndAlso vissle <> "False" AndAlso vissle <> "True" Then
                        column = New Tuple(Of String, String, String, String)(vissle, vissle2, vissle3, "")
                        'vissle = vissle2 = vissle3 = String.Empty
                    Else
                        Continue For
                    End If
                    TableObj.Column.Add(column)
                Next


                TableList.Add(TableObj)
            Else
                'If Not Com.Contains("CREATE CLUSTERED INDEX") Then
                '    Command.Body = Com
                '    CommandList.Add(Command)
                'End If

                If Com.Contains("CREATE ASSEMBLY") Then
                    Command.Body = Com '.Replace("UNSAFE", "SAFE")
                    CommandList.Add(Command)
                End If

            End If
        Next
        CommandList = CommandList.Distinct.ToList

        For Each item In CommandList
            match = Regex.Match(item.Body, "([\*]+)\/", RegexOptions.IgnoreCase)
            If match.Success Then
                item.Body = item.Body.Replace(match.Value, "")
            End If
            match = Regex.Match(item.Body, "\/([\*]+)", RegexOptions.IgnoreCase)
            If match.Success Then
                item.Body = item.Body.Replace(match.Value, "")
            End If


        Next
    End Sub
    Sub AddQuery(ByRef query As String)
        sqlscriptFilePath = New StreamWriter("D:\sqlscript2.sql")
        sqlscriptFilePath.WriteLine(query)
        sqlscriptFilePath.Close()

    End Sub
    Sub Build()
        For i = 0 To CommandList.Count - 1
            sqlscriptFilePath.WriteLine(CommandList(i).Body)
        Next
        sqlscriptFilePath.AutoFlush = True
        BuildTypeBody(TypeList)
        BuildTableBody(TableList)
        BuildStoredBody(StoredList)

        sqlscriptFilePath.Close()
    End Sub



    Sub BuildTypeBody(ByRef _types As List(Of Type))
        For i As Integer = 0 To _types.Count - 1
            sqlscript = " IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'" & _types(i).Name & "' AND ss.name = N'dbo')" & _types(i).Body
            sqlscriptFilePath.WriteLine(sqlscript)

        Next
    End Sub



    Sub BuildStoredBody(ByRef _stords As List(Of StoredProc))
        For i As Integer = 0 To _stords.Count - 1
            sqlscript = Environment.NewLine & "GO" & Environment.NewLine & "Print N'Creating [dbo].[" & _stords(i).Name & "]'" & Environment.NewLine & "GO" & Environment.NewLine
            sqlscript = sqlscript & "SET ANSI_NULLS ON" & Environment.NewLine & "GO" & Environment.NewLine & "SET QUOTED_IDENTIFIER ON" & Environment.NewLine & "GO" & Environment.NewLine & "IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[" & _stords(i).Name & "]') AND type in (N'P', N'PC'))" & Environment.NewLine & "BEGIN" & Environment.NewLine & "EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[" & _stords(i).Name & "] AS'" & Environment.NewLine & "End" & Environment.NewLine & "GO"
            sqlscript = sqlscript & _stords(i).Body
            sqlscriptFilePath.WriteLine(sqlscript)
        Next
    End Sub
    Sub BuildTableBody(ByRef _tables As List(Of Table))
        For i As Integer = 0 To _tables.Count - 1
            sqlscript = "SET ANSI_NULLS ON" & Environment.NewLine & "GO" & Environment.NewLine & "SET QUOTED_IDENTIFIER ON" & Environment.NewLine & "GO" & Environment.NewLine & "IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[" & _tables(i).Name & "]') AND type in (N'U'))" & Environment.NewLine & "BEGIN" & _tables(i).Body & "END" & Environment.NewLine & "GO"
            sqlscriptFilePath.WriteLine(sqlscript)
        Next
    End Sub


    'Sub BuildStoredBody(ByRef _stords As List(Of StoredProc))
    '    For i As Integer = 0 To _stords.Count - 1
    '        sqlscript = "GO" & Environment.NewLine & "IF @@ERROR <> 0 SET NOEXEC ON " & Environment.NewLine & "GO" & Environment.NewLine & "Print N'Creating [dbo].[" & _stords(i).Name & "]'" & Environment.NewLine & "GO" & Environment.NewLine
    '        sqlscript = sqlscript & "SET ANSI_NULLS ON" & Environment.NewLine & "GO" & Environment.NewLine & "SET QUOTED_IDENTIFIER ON" & Environment.NewLine & "GO" & Environment.NewLine & "IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[" & _stords(i).Name & "]') AND type in (N'P', N'PC'))" & Environment.NewLine & "BEGIN" & Environment.NewLine & "EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[" & _stords(i).Name & "] AS'" & Environment.NewLine & "End" & Environment.NewLine & "GO"
    '        sqlscript = sqlscript & _stords(i).Body
    '        sqlscriptFilePath.WriteLine(sqlscript)
    '    Next
    'End Sub


End Class

