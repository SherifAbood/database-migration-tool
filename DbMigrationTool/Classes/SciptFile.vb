﻿Public Class SciptFile
    Public scriptMCSProject As String = "USE [master]
GO
/****** Object:  Database [MCSProjects5]    Script Date: 10/4/2018 04:12:14 PM ******/

CREATE DATABASE [MCSProjects5]
-- CONTAINMENT = NONE
-- ON  PRIMARY 
--( NAME = N'MCSProjects5.10', FILENAME = N'C:\AbdullahDatabases\MCSProjects5' , SIZE = 289792KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
-- LOG ON 


GO
ALTER DATABASE [MCSProjects5] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [MCSProjects5].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [MCSProjects5] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [MCSProjects5] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [MCSProjects5] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [MCSProjects5] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [MCSProjects5] SET ARITHABORT OFF 
GO
ALTER DATABASE [MCSProjects5] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [MCSProjects5] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [MCSProjects5] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [MCSProjects5] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [MCSProjects5] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [MCSProjects5] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [MCSProjects5] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [MCSProjects5] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [MCSProjects5] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [MCSProjects5] SET  DISABLE_BROKER 
GO
ALTER DATABASE [MCSProjects5] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [MCSProjects5] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [MCSProjects5] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [MCSProjects5] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [MCSProjects5] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [MCSProjects5] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [MCSProjects5] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [MCSProjects5] SET RECOVERY FULL 
GO
ALTER DATABASE [MCSProjects5] SET  MULTI_USER 
GO
ALTER DATABASE [MCSProjects5] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [MCSProjects5] SET DB_CHAINING OFF 
GO
ALTER DATABASE [MCSProjects5] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [MCSProjects5] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [MCSProjects5] SET DELAYED_DURABILITY = DISABLED 
GO
USE [MCSProjects5]
GO
/****** Object:  Table [dbo].[Patchs]    Script Date: 10/4/2018 04:12:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Patchs]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Patchs](
	[FK_ProjectID] [int] NULL,
	[FK_PipeID] [int] NULL,
	[PK_PatchID] [int] NULL,
	[PatchStart] [datetime] NULL,
	[PathEnd] [datetime] NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tblAIMSComponentLocation]    Script Date: 10/4/2018 04:12:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblAIMSComponentLocation]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblAIMSComponentLocation](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Component_Location] [nvarchar](4000) NULL,
	[Component_Type] [nvarchar](4000) NULL,
	[AssetID] [int] NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tblAIMSHashTable]    Script Date: 10/4/2018 04:12:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblAIMSHashTable]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblAIMSHashTable](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Pcode] [nvarchar](50) NULL,
	[Scode] [nvarchar](50) NULL,
	[Primary_Event_Category] [nvarchar](100) NULL,
	[Secondary_Event_Category] [nvarchar](100) NULL,
	[Range_Point] [nvarchar](100) NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tblAutoBackup]    Script Date: 10/4/2018 04:12:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblAutoBackup]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblAutoBackup](
	[ID] [int] NOT NULL,
	[Name] [varchar](255) NOT NULL,
	[XMLSettings] [xml] NOT NULL,
	[Period] [int] NULL,
	[DayName] [int] NULL,
	[DayNumber] [int] NULL,
	[MonthNumber] [int] NULL,
	[DateTime] [datetime] NULL,
	[Type] [int] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblCoordinateSystem]    Script Date: 10/4/2018 04:12:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblCoordinateSystem]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblCoordinateSystem](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PipeID] [int] NULL,
	[ProjectID] [int] NULL,
	[CoordinateSystemSettings] [xml] NULL,
	[StartKP] [float] NULL,
	[EndKP] [float] NULL,
	[StartDTime] [datetime] NULL,
	[EndDTime] [datetime] NULL,
	[SpheroidName] [nvarchar](50) NULL,
	[OriginParametersName] [nvarchar](50) NULL,
 CONSTRAINT [PK_tblCoordinateSystem] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tblDesignRoutes]    Script Date: 10/4/2018 04:12:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblDesignRoutes]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblDesignRoutes](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FK_ProjectID] [int] NOT NULL,
	[Route_Data] [nvarchar](50) NULL,
	[East] [decimal](16, 6) NULL,
	[North] [decimal](16, 6) NULL,
	[KP] [decimal](16, 6) NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tblDives]    Script Date: 10/4/2018 04:12:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblDives]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblDives](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FK_ProjectID] [int] NOT NULL,
	[Dive_Number] [int] NOT NULL,
	[StartKP] [decimal](16, 6) NULL,
	[EndKP] [decimal](16, 6) NULL,
	[StartDTime] [datetime] NULL,
	[EndDTime] [datetime] NULL,
	[DiveType] [int] NULL,
	[ROVID] [int] NOT NULL,
 CONSTRAINT [PK_tblDives] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tblDiveTypes]    Script Date: 10/4/2018 04:12:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblDiveTypes]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblDiveTypes](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FK_ProjectID] [int] NOT NULL,
	[DiveType] [text] NOT NULL,
	[DiveType_Code] [nvarchar](50) NULL,
	[Method_Used] [nvarchar](255) NULL,
 CONSTRAINT [PK_tblDiveTypes] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tblIBISHashTable]    Script Date: 10/4/2018 04:12:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblIBISHashTable]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblIBISHashTable](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Pcode] [text] NULL,
	[Scode] [text] NULL,
	[IBIS_Code] [int] NULL,
	[Status] [text] NULL,
	[Primary_Description] [text] NULL,
	[Secondary_Description] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tblOperationCodes]    Script Date: 10/4/2018 04:12:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblOperationCodes]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblOperationCodes](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FK_ProjectID] [int] NOT NULL,
	[OperationType] [nvarchar](255) NOT NULL,
	[Operation_Code] [nvarchar](50) NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tblPCode]    Script Date: 10/4/2018 04:12:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblPCode]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblPCode](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [nvarchar](15) NULL,
	[Name] [nvarchar](50) NULL,
	[Picture] [image] NULL,
	[PShortcut] [int] NULL CONSTRAINT [DF_tblPCode_PShortcut]  DEFAULT ((0)),
	[Type] [int] NULL,
	[Layername] [nvarchar](50) NULL,
	[ProjectID] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tblPipeRadii]    Script Date: 10/4/2018 04:12:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblPipeRadii]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblPipeRadii](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FK_ProjectID] [int] NULL,
	[FK_PipeID] [int] NOT NULL,
	[FromKP] [decimal](16, 6) NULL,
	[ToKP] [decimal](16, 6) NULL,
	[FromDTime] [datetime] NULL,
	[ToDTime] [datetime] NULL,
	[Radius] [float] NULL,
	[Value] [float] NULL,
	[IntervalType] [int] NULL,
 CONSTRAINT [PK_PipeRadii] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tblPipes]    Script Date: 10/4/2018 04:12:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblPipes]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblPipes](
	[PK_PipeID] [int] NOT NULL,
	[FK_ProjectID] [int] NULL,
	[Name] [nvarchar](255) NULL,
	[LoggingDB] [text] NULL,
	[ProcessingDB] [text] NULL,
	[VideoFilePath1] [varchar](255) NULL,
	[VideoFilePath2] [varchar](255) NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[Duration] [real] NULL,
	[PatchSize] [int] NULL,
	[NumberOfPatchs] [int] NULL,
	[PipeType] [int] NULL,
	[LeftVideoFilePath] [varchar](255) NULL,
	[CenterVideoFilePath] [varchar](255) NULL,
	[RightVideoFilePath] [varchar](255) NULL,
	[CombinedVideoFilePath] [varchar](255) NULL,
	[StartKP] [decimal](16, 6) NULL,
	[EndKP] [decimal](16, 6) NULL,
	[StartDTime] [datetime] NULL,
	[EndDTime] [datetime] NULL,
	[SurveyType] [int] NULL,
	[KPUnit] [int] NULL,
	[UTCShift] [real] NULL,
	[Area] [nvarchar](50) NULL,
	[OfflineKP] [decimal](16, 6) NULL,
	[ChecksKP] [decimal](16, 6) NULL,
	[SmoothingKP] [decimal](16, 6) NULL,
	[EventsKP] [decimal](16, 6) NULL,
	[ChartingKP] [decimal](16, 6) NULL,
	[Reported] [bit] NULL,
	[PipeStatus] [xml] NULL,
	[StoredProcedureSettings] [xml] NULL,
	[PipeDirection] [nchar](15) NULL,
	[FK_ProjectYearID] [int] NULL,
	[DBVersion] [nvarchar](50) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblPipeStatus]    Script Date: 10/4/2018 04:12:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblPipeStatus]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblPipeStatus](
	[PK_PipeStatus] [int] IDENTITY(1,1) NOT NULL,
	[FK_ProjectID] [int] NOT NULL,
	[FK_PipeID] [int] NOT NULL,
	[FK_ProjectYearID] [int] NULL,
	[GeneralDetails_StartDtime] [datetime] NULL,
	[GeneralDetails_EndDtime] [datetime] NULL,
	[GeneralDetails_StartKp] [float] NULL,
	[GeneralDetails_EndKp] [float] NULL,
	[Importing_FromLogDBToProcDB] [float] NULL,
	[TrackProcessing_ImportFromPoslogToPosProc] [float] NULL,
	[TrackProcessing_DeSpikingNavigation] [float] NULL,
	[TrackProcessing_RouteDigitizing] [float] NULL,
	[TrackProcessing_KpCalc] [float] NULL,
	[TrackProcessing_HeadingDevCalc] [float] NULL,
	[TrackProcessing_DesignKpCalc] [float] NULL,
	[TrackProcessing_DccCalc] [float] NULL,
	[DepthProcessing_DepthAndTideCalc] [float] NULL,
	[ScansProcessing_ImportFromScanLogToScanProc] [float] NULL,
	[ScansProcessing_HPRInterpolation] [float] NULL,
	[ScansProcessing_KpCalc] [float] NULL,
	[ScansProcessing_HeadingDevCalc] [float] NULL,
	[ScansProcessing_PositionCalc] [float] NULL,
	[ScansProcessing_DesignKpCalc] [float] NULL,
	[ScansProcessing_DccCalc] [float] NULL,
	[ScansProcessing_DepthCalc] [float] NULL,
	[ScansProcessing_UpdateKpInterval] [float] NULL,
	[ScansProcessing_PipeAndSeaBedDetection] [float] NULL,
	[ScansProcessing_GapsChecks] [float] NULL,
	[ScansProcessing_ProfileSmoothing] [float] NULL,
	[ScansProcessing_SeabedSmoothing] [float] NULL,
	[PipeTrackerProcessing_ImportFromPTrackerLogToPTrackerProc] [float] NULL,
	[PipeTrackerProcessing_HPRInterpolation] [float] NULL,
	[PipeTrackerProcessing_PositionCalc] [float] NULL,
	[PipeTrackerProcessing_KpCalc] [float] NULL,
	[PipeTrackerProcessing_DepthCalc] [float] NULL,
	[PipeTrackerProcessing_UpdateKpInterval] [float] NULL,
	[PipeTrackerProcessing_ImplementingPipeTrackerInScanProc] [float] NULL,
	[EventsProcessing_VideoCombining] [float] NULL,
	[EventsProcessing_ImportFromEventLogToEventProc] [float] NULL,
	[EventsProcessing_VideoReview] [float] NULL,
	[EventsProcessing_KpCalc] [float] NULL,
	[EventsProcessing_EventChecks] [float] NULL,
	[EventsProcessing_PositionCalc] [float] NULL,
	[EventsProcessing_DepthCalc] [float] NULL,
	[EventsProcessing_DesignKpCalc] [float] NULL,
	[EventsProcessing_DccCalc] [float] NULL,
	[Deliverables_PreparingEventList] [float] NULL,
	[Deliverables_EventListQC] [float] NULL,
	[Deliverables_PreparingCharts] [float] NULL,
	[Deliverables_ChartsQC] [float] NULL,
	[Deliverables_PreparingReport] [float] NULL,
	[Deliverables_ReportQC] [float] NULL,
	[FinishedOnline] [bit] NULL,
	[FinishedProcessing] [bit] NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tblPipeWorkScope]    Script Date: 10/4/2018 04:12:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblPipeWorkScope]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblPipeWorkScope](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FK_ProjectID] [int] NOT NULL,
	[FK_PipeID] [int] NULL,
	[Line_Code] [nvarchar](200) NOT NULL,
	[FromKP] [decimal](16, 6) NOT NULL,
	[ToKP] [decimal](16, 6) NOT NULL,
	[Length] [decimal](16, 6) NULL,
	[Exclude] [bit] NULL,
	[IBIS_Number] [int] NULL,
	[Plan_Number] [text] NULL,
	[Base_Plan] [text] NULL,
	[Comments] [text] NULL,
	[Profiles_Required] [bit] NULL,
	[CP_Required] [bit] NULL,
	[Database_Column] [text] NULL,
	[Database_Complete] [bit] NULL,
	[Complete] [bit] NULL,
	[IN_Workplan] [bit] NULL,
	[Structure] [bit] NULL,
	[Open_Water] [bit] NULL,
	[Where_Exactly] [text] NULL,
	[Area] [text] NULL,
	[Route_Data] [text] NULL,
 CONSTRAINT [PK_tblPipeWorkScope] PRIMARY KEY CLUSTERED 
(
	[Line_Code] ASC,
	[FromKP] ASC,
	[ToKP] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tblProjectLogBook]    Script Date: 10/4/2018 04:12:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblProjectLogBook]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblProjectLogBook](
	[FK_ProjectID] [int] NULL,
	[FK_PipeID] [int] NULL,
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DTime] [datetime] NOT NULL,
	[UserID] [int] NULL,
	[Description] [text] NULL,
	[Activity] [int] NULL,
	[App] [int] NULL,
 CONSTRAINT [PK_LogBook] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tblProjects]    Script Date: 10/4/2018 04:12:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblProjects]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblProjects](
	[PK_ProjectID] [int] NOT NULL,
	[Name] [nvarchar](255) NULL,
	[Comment] [text] NULL,
	[ProjectCode] [nvarchar](50) NULL,
	[JobFolder] [text] NULL,
	[TimeShift] [decimal](16, 6) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tblReportRegister]    Script Date: 10/4/2018 04:12:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblReportRegister]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblReportRegister](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FK_ProjectID] [int] NOT NULL,
	[FK_SurveySectionID] [int] NULL,
	[StartKP] [decimal](16, 6) NULL,
	[EndKP] [decimal](16, 6) NULL,
	[Length] [decimal](16, 6) NULL,
	[ReportNo] [text] NULL,
	[Client_ReportNo] [text] NULL,
	[Start_DTime] [datetime] NULL,
	[End_DTime] [datetime] NULL,
	[Comment] [text] NULL,
	[RevisionNo] [int] NULL,
	[Submitted] [bit] NULL,
	[Submitted_Date] [datetime] NULL,
	[Returned] [bit] NULL,
	[Returned_Date] [datetime] NULL,
	[Corrected] [bit] NULL,
	[Corected_Date] [datetime] NULL,
	[Accepted] [bit] NULL,
	[Accepted_Date] [datetime] NULL,
	[Canceled] [bit] NULL,
	[Canceled_Date] [datetime] NULL,
	[IBIS_SequenceNumber] [int] NULL,
	[IBIS_IncidentFile_Revision] [int] NULL,
	[IBIS_IncidentFile_Submitted] [bit] NULL,
	[IBIS_IncidentFile_Submitted_Date] [datetime] NULL,
	[IBIS_IncidentFile_Corrected] [bit] NULL,
	[IBIS_IncidentFile_Corrected_Date] [datetime] NULL,
	[IBIS_IncidentFile_Returned] [bit] NULL,
	[IBIS_IncidentFile_Returned_Date] [datetime] NULL,
	[IBIS_IncidentFile_Accepted] [bit] NULL,
	[IBIS_IncidentFile_Accepted_Date] [datetime] NULL,
	[IBIS_IncidentFile_Canceled] [bit] NULL,
	[IBIS_IncidentFile_Canceled_Date] [datetime] NULL,
	[IBIS_LogFile_Revision] [int] NULL,
	[IBIS_LogFile_Submitted] [bit] NULL,
	[IBIS_LogFile_Submitted_Date] [datetime] NULL,
	[IBIS_LogFile_Corrected] [bit] NULL,
	[IBIS_LogFile_Corrected_Date] [datetime] NULL,
	[IBIS_LogFile_Returned] [bit] NULL,
	[IBIS_LogFile_Returned_Date] [datetime] NULL,
	[IBIS_LogFile_Accepted] [bit] NULL,
	[IBIS_LogFile_Accepted_Date] [datetime] NULL,
	[IBIS_LogFile_Canceled] [bit] NULL,
	[IBIS_LogFile_Canceled_Date] [datetime] NULL,
	[IBIS_DiveFile_Revision] [int] NULL,
	[IBIS_DiveFile_Submitted] [bit] NULL,
	[IBIS_DiveFile_Submitted_Date] [datetime] NULL,
	[IBIS_DiveFile_Corrected] [bit] NULL,
	[IBIS_DiveFile_Corrected_Date] [datetime] NULL,
	[IBIS_DiveFile_Returned] [bit] NULL,
	[IBIS_DiveFile_Returned_Date] [datetime] NULL,
	[IBIS_DiveFile_Accepted] [bit] NULL,
	[IBIS_DiveFile_Accepted_Date] [datetime] NULL,
	[IBIS_DiveFile_Canceled] [bit] NULL,
	[IBIS_DiveFile_Canceled_Date] [datetime] NULL,
	[IBIS_ImageFile_Revision] [int] NULL,
	[IBIS_ImageFile_Submitted] [bit] NULL,
	[IBIS_ImageFile_Submitted_Date] [datetime] NULL,
	[IBIS_ImageFile_Corrected] [bit] NULL,
	[IBIS_ImageFile_Corrected_Date] [datetime] NULL,
	[IBIS_ImageFile_Returned] [bit] NULL,
	[IBIS_ImageFile_Returned_Date] [datetime] NULL,
	[IBIS_ImageFile_Accepted] [bit] NULL,
	[IBIS_ImageFile_Accepted_Date] [datetime] NULL,
	[IBIS_ImageFile_Canceled] [bit] NULL,
	[IBIS_ImageFile_Canceled_Date] [datetime] NULL,
	[IBIS_ProfileFile_Revision] [int] NULL,
	[IBIS_ProfileFile_Submitted] [bit] NULL,
	[IBIS_ProfileFile_Submitted_Date] [datetime] NULL,
	[IBIS_ProfileFile_Corrected] [bit] NULL,
	[IBIS_ProfileFile_Corrected_Date] [datetime] NULL,
	[IBIS_ProfileFile_Returned] [bit] NULL,
	[IBIS_ProfileFile_Returned_Date] [datetime] NULL,
	[IBIS_ProfileFile_Accepted] [bit] NULL,
	[IBIS_ProfileFile_Accepted_Date] [datetime] NULL,
	[IBIS_ProfileFile_Canceled] [bit] NULL,
	[IBIS_ProfileFile_Canceled_Date] [datetime] NULL,
	[IncidentFile_Submit] [bit] NULL,
	[LogFile_Submit] [bit] NULL,
	[ImageFile_Submit] [bit] NULL,
	[DiveFile_Submit] [bit] NULL,
	[ProfileFile_Submit] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tblROVs]    Script Date: 10/4/2018 04:12:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblROVs]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblROVs](
	[FK_ProjectID] [int] NOT NULL,
	[PK_ROVID] [int] NOT NULL,
	[Name] [nvarchar](255) NULL,
	[Description] [text] NULL,
	[Image] [image] NULL,
	[Settings] [xml] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tblROVSettings]    Script Date: 10/4/2018 04:12:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblROVSettings]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblROVSettings](
	[FK_ProjectID] [int] NOT NULL,
	[PK_ROVID] [int] NOT NULL,
	[PosX] [real] NULL,
	[PosY] [real] NULL,
	[PosZ] [real] NULL,
	[ScanX] [real] NULL,
	[ScanY] [real] NULL,
	[ScanZ] [real] NULL,
	[AltX] [real] NULL,
	[AltY] [real] NULL,
	[AltZ] [real] NULL,
	[EventX] [real] NULL,
	[EventY] [real] NULL,
	[EventZ] [real] NULL,
	[DepthX] [real] NULL,
	[DepthY] [real] NULL,
	[DepthZ] [real] NULL,
	[PTrackerX] [real] NULL,
	[PTrackerY] [real] NULL,
	[PTrackerZ] [real] NULL,
	[DopplerX] [real] NULL,
	[DopplerY] [real] NULL,
	[DopplerZ] [real] NULL,
	[MScanOffsetX] [real] NULL,
	[MScanOffsetY] [real] NULL,
	[MScanOffsetZ] [real] NULL,
	[SScanOffsetX] [real] NULL,
	[SScanOffsetY] [real] NULL,
	[SScanOffsetZ] [real] NULL,
	[MRX] [real] NULL,
	[MRY] [real] NULL,
	[MRZ] [real] NULL,
	[SRX] [real] NULL,
	[SRY] [real] NULL,
	[SRZ] [real] NULL,
	[DMSRX] [real] NULL,
	[DMSRY] [real] NULL,
	[DMSRZ] [real] NULL,
	[Distance] [real] NULL,
	[DatumHeight] [real] NULL,
	[PTrackerAltX] [real] NULL,
	[PTrackerAltY] [real] NULL,
	[PTrackerAltZ] [real] NULL,
	[Width] [real] NULL,
	[Height] [real] NULL,
	[Length] [real] NULL,
	[SettingsType] [int] NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tblSettingsFolders]    Script Date: 10/4/2018 04:12:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblSettingsFolders]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblSettingsFolders](
	[FK_ProjectID] [int] NOT NULL,
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[OnlineProfilePath] [nvarchar](255) NULL,
	[OfflineProfilePath] [nvarchar](255) NULL,
	[StoredProceduresPath] [nvarchar](255) NULL,
	[AutoProcessingSettingsPath] [nvarchar](255) NULL,
	[ProcessGraphicalPath] [nvarchar](255) NULL,
	[MultipleScansPath] [nvarchar](255) NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tblSTCode]    Script Date: 10/4/2018 04:12:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblSTCode]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblSTCode](
	[PrimaryID] [int] NOT NULL,
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [nvarchar](15) NOT NULL,
	[Type] [int] NULL,
	[LayerName] [nvarchar](50) NOT NULL,
	[LayerName_Side] [nvarchar](50) NOT NULL,
	[Description] [text] NULL,
	[Comment] [text] NULL,
	[ProfileEventFlag] [bit] NULL,
	[PlanEventFlag] [bit] NULL,
	[AddPlanTextFlag] [bit] NULL,
	[AddProfileTextFlag] [bit] NULL,
	[Discard] [bit] NULL,
	[imagecap] [bit] NULL,
	[AnomalyCode] [nvarchar](15) NULL,
	[GenerateAOC] [bit] NULL,
	[EndCode] [nvarchar](15) NULL,
	[AutoGeneratedComment] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tblStructure]    Script Date: 10/4/2018 04:12:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblStructure]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblStructure](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FK_ProjectID] [int] NOT NULL,
	[Installation] [nvarchar](255) NULL,
	[Workpack] [nvarchar](255) NULL,
	[WorkpackYear] [int] NULL,
	[DBConnection] [nvarchar](255) NULL,
	[LeftVideoFilePath] [varchar](255) NULL,
	[CenterVideoFilePath] [varchar](255) NULL,
	[RightVideoFilePath] [varchar](255) NULL,
	[CombinedVideoFilePath] [varchar](255) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblSurveySections]    Script Date: 10/4/2018 04:12:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblSurveySections]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblSurveySections](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FK_ProjectID] [int] NOT NULL,
	[FK_PipeID] [int] NOT NULL,
	[Task_Number] [int] NULL,
	[Dive_Number] [int] NULL,
	[Plan_Number] [text] NULL,
	[Start_WorkScope_KP] [decimal](16, 6) NULL,
	[Start_Actual_KP] [decimal](16, 6) NULL,
	[Start_DTime] [datetime] NULL,
	[End_WorkScope_KP] [decimal](16, 6) NULL,
	[End_Actual_KP] [decimal](16, 6) NULL,
	[End_DTime] [datetime] NULL,
	[Comments] [text] NULL,
	[Report_Number] [text] NULL,
	[Operation_Code] [text] NULL,
	[ROVID] [int] NOT NULL,
	[WorkScopeID] [int] NOT NULL,
	[Line_Code] [text] NULL,
 CONSTRAINT [PK_tblSurveySections] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tblTasks]    Script Date: 10/4/2018 04:12:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblTasks]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblTasks](
	[ID] [int] NOT NULL,
	[GUID] [nvarchar](14) NULL,
	[FK_ProjectID] [int] NULL,
	[FK_PipeID] [int] NULL,
	[Title] [nvarchar](255) NULL,
	[Task] [ntext] NULL,
	[FK_TaskTypeID] [int] NULL,
	[TaskStatus] [int] NULL,
	[TaskPercentege] [float] NULL,
	[IssuingDTime] [datetime] NULL,
	[ReadDTime] [datetime] NULL,
	[ClosedDTime] [datetime] NULL,
	[IssuerID] [int] NULL,
	[IssuerName] [nvarchar](255) NULL,
	[UserID] [int] NULL,
	[UserName] [nvarchar](255) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tblTaskType]    Script Date: 10/4/2018 04:12:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblTaskType]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblTaskType](
	[ID] [int] NULL,
	[TaskType] [nvarchar](255) NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tblTide]    Script Date: 10/4/2018 04:12:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblTide]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblTide](
	[FK_ProjectID] [int] NOT NULL,
	[PK_TideID] [int] NOT NULL,
	[Name] [nvarchar](255) NULL,
	[Details] [xml] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tblTideDetails]    Script Date: 10/4/2018 04:12:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblTideDetails]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblTideDetails](
	[FK_ProjectId] [int] NOT NULL,
	[PK_TideID] [int] NOT NULL,
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DTime] [datetime] NULL,
	[KP] [float] NULL,
	[Value] [real] NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tblTitles]    Script Date: 10/4/2018 04:12:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblTitles]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblTitles](
	[FK_ProjectID] [int] NOT NULL,
	[ClientName] [text] NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[Duration] [real] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tblYears]    Script Date: 10/4/2018 04:12:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblYears]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblYears](
	[PK_ProjectYearID] [int] NOT NULL,
	[FK_ProjectID] [int] NOT NULL,
	[Year] [int] NOT NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tlbTeamAccounts]    Script Date: 10/4/2018 04:12:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tlbTeamAccounts]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tlbTeamAccounts](
	[FK_ProjectID] [int] NOT NULL,
	[ID] [int] NOT NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[Password] [nvarchar](50) NOT NULL,
	[RoleID] [int] NULL
) ON [PRIMARY]
END
GO
USE [master]
GO
ALTER DATABASE [MCSProjects5] SET  READ_WRITE 
GO
"


    Public scriptPipe As String = "USE [master]
GO
/****** Object:  Database [PipeBlank]    Script Date: 9/17/2018 02:19:44 PM ******/
IF NOT EXISTS (SELECT name FROM sys.databases WHERE name = N'PipeBlank')
BEGIN
CREATE DATABASE [PipeBlank]
-- CONTAINMENT = NONE
-- ON  PRIMARY 
--( NAME = N'PipeBlank', FILENAME = N'C:\AbdullahDatabases\PipeBlank.mdf' , SIZE = 6144KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
-- LOG ON 
--( NAME = N'PipeBlank_log', FILENAME = N'C:\AbdullahDatabases\PipeBlank_log.ldf' , SIZE = 22144KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
END

GO
ALTER DATABASE [PipeBlank] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [PipeBlank].[dbo].[sp_fulltext_database] @action = 'disable'
end
GO
ALTER DATABASE [PipeBlank] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [PipeBlank] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [PipeBlank] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [PipeBlank] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [PipeBlank] SET ARITHABORT OFF 
GO
ALTER DATABASE [PipeBlank] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [PipeBlank] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [PipeBlank] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [PipeBlank] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [PipeBlank] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [PipeBlank] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [PipeBlank] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [PipeBlank] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [PipeBlank] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [PipeBlank] SET  DISABLE_BROKER 
GO
ALTER DATABASE [PipeBlank] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [PipeBlank] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [PipeBlank] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [PipeBlank] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [PipeBlank] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [PipeBlank] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [PipeBlank] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [PipeBlank] SET RECOVERY FULL 
GO
ALTER DATABASE [PipeBlank] SET  MULTI_USER 
GO
ALTER DATABASE [PipeBlank] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [PipeBlank] SET DB_CHAINING OFF 
GO
ALTER DATABASE [PipeBlank] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [PipeBlank] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [PipeBlank] SET DELAYED_DURABILITY = DISABLED 
GO
USE [PipeBlank]
GO
/****** Object:  UserDefinedTableType [dbo].[CommentType]    Script Date: 9/17/2018 02:19:44 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'CommentType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[CommentType] AS TABLE(
	[ID] [int] NOT NULL,
	[Comment] [text] NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[DateTimeType]    Script Date: 9/17/2018 02:19:44 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'DateTimeType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[DateTimeType] AS TABLE(
	[ID] [int] NOT NULL,
	[DTime] [datetime] NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[DepthKPType]    Script Date: 9/17/2018 02:19:44 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'DepthKPType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[DepthKPType] AS TABLE(
	[ID] [int] NOT NULL,
	[DepthAltKP] [float] NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[DepthType]    Script Date: 9/17/2018 02:19:44 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'DepthType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[DepthType] AS TABLE(
	[ID] [int] NOT NULL,
	[Depth] [real] NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[EastNorthDepthType]    Script Date: 9/17/2018 02:19:44 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'EastNorthDepthType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[EastNorthDepthType] AS TABLE(
	[ID] [int] NOT NULL,
	[East] [decimal](16, 6) NULL,
	[North] [decimal](16, 6) NULL,
	[Depth] [real] NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[EastNorthType]    Script Date: 9/17/2018 02:19:44 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'EastNorthType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[EastNorthType] AS TABLE(
	[ID] [int] NOT NULL,
	[East] [decimal](16, 6) NULL,
	[North] [decimal](16, 6) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[EventProcLengthHeightType]    Script Date: 9/17/2018 02:19:44 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'EventProcLengthHeightType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[EventProcLengthHeightType] AS TABLE(
	[ID] [int] NOT NULL,
	[Length] [real] NULL,
	[Height] [real] NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[EXP_EventScanProc]    Script Date: 9/17/2018 02:19:44 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'EXP_EventScanProc' AND ss.name = N'dbo')
CREATE TYPE [dbo].[EXP_EventScanProc] AS TABLE(
	[ID] [int] NOT NULL,
	[DTime] [datetime] NOT NULL,
	[PCode] [nvarchar](15) NULL,
	[SCode] [nvarchar](15) NULL,
	[East] [decimal](16, 6) NULL,
	[North] [decimal](16, 6) NULL,
	[KP] [decimal](16, 6) NULL,
	[Depth] [real] NULL,
	[Length] [real] NULL,
	[Height] [real] NULL,
	[Width] [real] NULL,
	[CP] [real] NULL,
	[Comment] [text] NULL,
	[ROVID] [int] NOT NULL,
	[Active] [bit] NULL,
	[Depletion] [int] NULL,
	[DepletionValue] [real] NULL,
	[Angle] [real] NULL,
	[ClockNotation] [int] NULL,
	[Distance] [real] NULL,
	[DistanceNotation] [nvarchar](50) NULL,
	[Contact] [bit] NULL,
	[FieldJointNumber] [int] NULL,
	[CrossingName] [nvarchar](50) NULL,
	[CrossingClearance] [real] NULL,
	[CPReading] [real] NULL,
	[Anomaly] [bit] NULL,
	[CustomComment] [text] NULL,
	[PiggyBackValue] [int] NULL,
	[HourLocation] [nvarchar](50) NULL,
	[AnodeSecure] [bit] NULL,
	[MG_Coverage] [nvarchar](2000) NULL,
	[MG_Thickness] [real] NULL,
	[CPFieldGradiant] [real] NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[EXP_PosProcType]    Script Date: 9/17/2018 02:19:44 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'EXP_PosProcType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[EXP_PosProcType] AS TABLE(
	[ID] [int] NOT NULL,
	[FixNum] [int] NOT NULL,
	[DTime] [datetime] NOT NULL,
	[East] [decimal](16, 6) NULL,
	[North] [decimal](16, 6) NULL,
	[Depth] [real] NULL,
	[Kp] [decimal](16, 6) NULL,
	[Pitch] [real] NULL,
	[Roll] [real] NULL,
	[Heading] [real] NULL,
	[Logged] [bit] NULL,
	[ROVID] [int] NOT NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[EXP_SBScanProc]    Script Date: 9/17/2018 02:19:44 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'EXP_SBScanProc' AND ss.name = N'dbo')
CREATE TYPE [dbo].[EXP_SBScanProc] AS TABLE(
	[ScanNum] [int] NOT NULL,
	[DTime] [datetime] NOT NULL,
	[ScanData1] [image] NULL,
	[ScanData2] [image] NULL,
	[MasterNumScans] [smallint] NOT NULL,
	[SlaveNumScans] [smallint] NOT NULL,
	[MasterAngStart] [real] NOT NULL,
	[SlaveAngStart] [real] NOT NULL,
	[MasterAngStep] [real] NOT NULL,
	[SlaveAngStep] [real] NOT NULL,
	[DeltaX1] [real] NULL,
	[DeltaY1] [real] NULL,
	[DeltaZ1] [real] NULL,
	[RX1] [real] NULL,
	[RY1] [real] NULL,
	[RZ1] [real] NOT NULL,
	[DeltaX2] [real] NULL,
	[DeltaY2] [real] NULL,
	[DeltaZ2] [real] NULL,
	[RX2] [real] NULL,
	[RY2] [real] NULL,
	[RZ2] [real] NULL,
	[SSpeed] [real] NULL,
	[ROVID] [int] NOT NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[EXP_ScanProc]    Script Date: 9/17/2018 02:19:44 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'EXP_ScanProc' AND ss.name = N'dbo')
CREATE TYPE [dbo].[EXP_ScanProc] AS TABLE(
	[ScanNum] [int] NOT NULL,
	[DTime] [datetime] NOT NULL,
	[ScanData1] [image] NULL,
	[ScanData2] [image] NULL,
	[MasterNumScans] [smallint] NOT NULL,
	[SlaveNumScans] [smallint] NOT NULL,
	[MasterAngStart] [real] NOT NULL,
	[SlaveAngStart] [real] NOT NULL,
	[MasterAngStep] [real] NOT NULL,
	[SlaveAngStep] [real] NOT NULL,
	[DeltaX1] [real] NULL,
	[DeltaY1] [real] NULL,
	[DeltaZ1] [real] NULL,
	[RX1] [real] NULL,
	[RY1] [real] NULL,
	[RZ1] [real] NOT NULL,
	[DeltaX2] [real] NULL,
	[DeltaY2] [real] NULL,
	[DeltaZ2] [real] NULL,
	[RX2] [real] NULL,
	[RY2] [real] NULL,
	[RZ2] [real] NULL,
	[ScanExtraData1] [image] NULL,
	[ScanExtraData2] [image] NULL,
	[SSpeed] [real] NULL,
	[ROVID] [int] NOT NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[EXP_ScanProc_PRC]    Script Date: 9/17/2018 02:19:44 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'EXP_ScanProc_PRC' AND ss.name = N'dbo')
CREATE TYPE [dbo].[EXP_ScanProc_PRC] AS TABLE(
	[ScanNum] [int] NOT NULL,
	[DTime] [datetime] NOT NULL,
	[ScanData1] [image] NULL,
	[ScanData2] [image] NULL,
	[MasterNumScans] [smallint] NOT NULL,
	[SlaveNumScans] [smallint] NOT NULL,
	[MasterAngStart] [real] NOT NULL,
	[SlaveAngStart] [real] NOT NULL,
	[MasterAngStep] [real] NOT NULL,
	[SlaveAngStep] [real] NOT NULL,
	[DeltaX1] [real] NULL,
	[DeltaY1] [real] NULL,
	[DeltaZ1] [real] NULL,
	[RX1] [real] NULL,
	[RY1] [real] NULL,
	[RZ1] [real] NOT NULL,
	[DeltaX2] [real] NULL,
	[DeltaY2] [real] NULL,
	[DeltaZ2] [real] NULL,
	[RX2] [real] NULL,
	[RY2] [real] NULL,
	[RZ2] [real] NULL,
	[ScanExtraData1] [image] NULL,
	[ScanExtraData2] [image] NULL,
	[SSpeed] [real] NULL,
	[ROVID] [int] NOT NULL,
	[IsPRC] [bit] NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[FreeSpanEvent]    Script Date: 9/17/2018 02:19:44 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'FreeSpanEvent' AND ss.name = N'dbo')
CREATE TYPE [dbo].[FreeSpanEvent] AS TABLE(
	[ID] [int] NOT NULL,
	[KP] [decimal](16, 6) NULL,
	[DTime] [datetime] NULL,
	[Discard] [bit] NULL,
	[ROVID] [int] NOT NULL,
	[PCode] [nvarchar](15) NULL,
	[SCode] [nvarchar](15) NULL,
	[East] [decimal](16, 6) NULL,
	[North] [decimal](16, 6) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[Heading_HeadingDevType]    Script Date: 9/17/2018 02:19:44 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'Heading_HeadingDevType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[Heading_HeadingDevType] AS TABLE(
	[ID] [int] NOT NULL,
	[Heading] [real] NULL,
	[HeadingDev] [real] NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[HeadingDevKPType]    Script Date: 9/17/2018 02:19:44 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'HeadingDevKPType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[HeadingDevKPType] AS TABLE(
	[ID] [int] NOT NULL,
	[HeadingDev] [real] NULL,
	[KP] [decimal](16, 6) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[HeadingDevType]    Script Date: 9/17/2018 02:19:44 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'HeadingDevType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[HeadingDevType] AS TABLE(
	[ID] [int] NOT NULL,
	[HeadingDev] [real] NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[HeadingType]    Script Date: 9/17/2018 02:19:45 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'HeadingType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[HeadingType] AS TABLE(
	[ID] [int] NOT NULL,
	[Heading] [real] NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[IDType]    Script Date: 9/17/2018 02:19:45 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'IDType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[IDType] AS TABLE(
	[ID] [int] NOT NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[IMP_AuditTableType]    Script Date: 9/17/2018 02:19:45 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'IMP_AuditTableType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[IMP_AuditTableType] AS TABLE(
	[ID] [int] NOT NULL,
	[FileName] [nvarchar](50) NULL,
	[RecordDate] [datetime] NULL,
	[StopDate] [datetime] NULL,
	[StartKP] [real] NULL,
	[EndKP] [real] NULL,
	[QC] [bit] NULL,
	[Reviewed] [bit] NULL,
	[Combined] [bit] NULL,
	[Copied] [bit] NULL,
	[BackedUp] [bit] NULL,
	[ROVID] [int] NOT NULL,
	[FilePath] [nvarchar](1000) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[IMP_BatchesDetailsType]    Script Date: 9/17/2018 02:19:45 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'IMP_BatchesDetailsType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[IMP_BatchesDetailsType] AS TABLE(
	[ID] [int] NOT NULL,
	[FK_BatchID] [int] NOT NULL,
	[FK_NavigationDetailID] [int] NULL,
	[DTime] [datetime] NULL,
	[CameraImageNames] [varbinary](max) NULL,
	[CameraPositions] [varbinary](max) NULL,
	[ProcessedCameraPositions] [varbinary](max) NULL,
	[CameraAvgPosX] [decimal](16, 6) NULL,
	[CameraAvgPosY] [decimal](16, 6) NULL,
	[CameraAvgPosZ] [decimal](16, 6) NULL,
	[ProcessedAvgPosX] [decimal](16, 6) NULL,
	[ProcessedAvgPosY] [decimal](16, 6) NULL,
	[ProcessedAvgPosZ] [decimal](16, 6) NULL,
	[IsCameraUsed] [bit] NULL,
	[IsCameraNeglected] [tinyint] NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[IMP_BatchesType]    Script Date: 9/17/2018 02:19:45 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'IMP_BatchesType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[IMP_BatchesType] AS TABLE(
	[PK_BatchID] [int] NOT NULL,
	[FK_ClusterID] [int] NOT NULL,
	[PlannedStartKP] [decimal](16, 6) NULL,
	[StartKP] [decimal](16, 6) NULL,
	[StartDTime] [datetime] NULL,
	[StartEast] [decimal](16, 6) NULL,
	[StartNorth] [decimal](16, 6) NULL,
	[StartDepth] [decimal](16, 6) NULL,
	[PlannedEndKP] [decimal](16, 6) NULL,
	[EndKP] [decimal](16, 6) NULL,
	[EndDTime] [datetime] NULL,
	[EndEast] [decimal](16, 6) NULL,
	[EndNorth] [decimal](16, 6) NULL,
	[EndDepth] [decimal](16, 6) NULL,
	[BatchPath] [nvarchar](255) NULL,
	[TransformationMatrix] [ntext] NULL,
	[RotationMatrix] [ntext] NULL,
	[FinishPercentage] [int] NULL,
	[ProcessingStatus] [int] NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[IMP_ClustersType]    Script Date: 9/17/2018 02:19:45 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'IMP_ClustersType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[IMP_ClustersType] AS TABLE(
	[PK_ClusterID] [int] NOT NULL,
	[PlannedStartKP] [decimal](16, 6) NULL,
	[StartKP] [decimal](16, 6) NOT NULL,
	[StartDTime] [datetime] NOT NULL,
	[StartEast] [decimal](16, 6) NULL,
	[StartNorth] [decimal](16, 6) NULL,
	[StartDepth] [decimal](16, 6) NULL,
	[StartEvent] [nvarchar](50) NULL,
	[StartEventID] [int] NULL,
	[PlannedEndKP] [decimal](16, 6) NULL,
	[EndKP] [decimal](16, 6) NOT NULL,
	[EndDTime] [datetime] NOT NULL,
	[EndEast] [decimal](16, 6) NULL,
	[EndNorth] [decimal](16, 6) NULL,
	[EndDepth] [decimal](16, 6) NULL,
	[EndEvent] [nvarchar](50) NULL,
	[EndEventID] [int] NULL,
	[GenerationType] [int] NULL,
	[ImagesCount] [int] NULL,
	[PerUnit] [float] NULL,
	[MaxBatchSize] [float] NULL,
	[OverlapLength] [float] NULL,
	[IsKPSelection] [tinyint] NULL,
	[ClusterPath] [nvarchar](255) NULL,
	[ProcessedPLYPath] [nvarchar](255) NULL,
	[PLYStartCameraX] [decimal](16, 6) NULL,
	[PLYStartCameraY] [decimal](16, 6) NULL,
	[PLYStartCameraZ] [decimal](16, 6) NULL,
	[ScaleMatrix] [ntext] NULL,
	[RotationMatrix] [ntext] NULL,
	[RollMatrix] [ntext] NULL,
	[DensePointIndices] [image] NULL,
	[ClusterRaduis] [float] NULL,
	[ProcessingStatus] [int] NULL,
	[ProcessingStatusString] [nvarchar](255) NULL,
	[IsFailed] [tinyint] NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[IMP_CodesDataFromatType]    Script Date: 9/17/2018 02:19:45 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'IMP_CodesDataFromatType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[IMP_CodesDataFromatType] AS TABLE(
	[ID] [int] NOT NULL,
	[FK_FormatID] [int] NOT NULL,
	[FK_CodeID] [int] NOT NULL,
	[CodeName] [nvarchar](50) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[IMP_CorrPosProcType]    Script Date: 9/17/2018 02:19:45 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'IMP_CorrPosProcType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[IMP_CorrPosProcType] AS TABLE(
	[ID] [int] NOT NULL,
	[DTime] [datetime] NULL,
	[PTracker] [bit] NULL,
	[DCC] [real] NULL,
	[Depth] [real] NULL,
	[Discard] [bit] NULL,
	[KP] [decimal](16, 6) NULL,
	[East] [decimal](16, 6) NULL,
	[North] [decimal](16, 6) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[IMP_CPLogType]    Script Date: 9/17/2018 02:19:45 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'IMP_CPLogType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[IMP_CPLogType] AS TABLE(
	[ID] [int] NOT NULL,
	[DTime] [datetime] NOT NULL,
	[KP] [real] NULL,
	[East] [decimal](16, 6) NULL,
	[North] [decimal](16, 6) NULL,
	[CP] [real] NULL,
	[FG] [real] NULL,
	[CorrCP] [real] NULL,
	[CorrFG] [real] NULL,
	[DesignKp] [decimal](16, 6) NULL,
	[Discard] [bit] NULL,
	[ROVID] [int] NOT NULL,
	[ContactCP] [real] NULL,
	[CpCal] [real] NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[IMP_DataFormatsType]    Script Date: 9/17/2018 02:19:45 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'IMP_DataFormatsType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[IMP_DataFormatsType] AS TABLE(
	[PK_FormatID] [int] NOT NULL,
	[FormatName] [nvarchar](50) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[IMP_DeletedEventsType]    Script Date: 9/17/2018 02:19:45 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'IMP_DeletedEventsType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[IMP_DeletedEventsType] AS TABLE(
	[ID] [int] NOT NULL,
	[EventNum] [int] NULL,
	[DTime] [datetime] NOT NULL,
	[PCode] [nvarchar](15) NULL,
	[SCode] [nvarchar](15) NULL,
	[KP] [decimal](16, 6) NULL,
	[East] [decimal](16, 6) NULL,
	[North] [decimal](16, 6) NULL,
	[PipeDepth] [real] NULL,
	[SeabedDepth] [real] NULL,
	[BurialDepth] [real] NULL,
	[Length] [real] NULL,
	[Height] [real] NULL,
	[Width] [real] NULL,
	[CP] [real] NULL,
	[ProximityCP] [real] NULL,
	[ContactCP] [real] NULL,
	[CPFieldGradiant] [real] NULL,
	[Comment] [text] NULL,
	[Depth] [real] NULL,
	[ROVID] [int] NOT NULL,
	[Active] [bit] NULL,
	[Depletion] [int] NULL,
	[DepletionValue] [real] NULL,
	[Angle] [real] NULL,
	[ClockNotation] [int] NULL,
	[Distance] [real] NULL,
	[DistanceNotation] [char](25) NULL,
	[Contact] [bit] NULL,
	[FieldJointNumber] [int] NULL,
	[CrossingName] [char](25) NULL,
	[CrossingClearance] [real] NULL,
	[CPReading] [real] NULL,
	[Anomaly] [bit] NULL,
	[Discard] [bit] NULL,
	[CustomComment] [text] NULL,
	[PiggyBackValue] [int] NULL,
	[Reviewed] [bit] NULL,
	[EventQC] [bit] NULL,
	[KP_Fixed] [decimal](16, 6) NULL,
	[DesignKp] [decimal](16, 6) NULL,
	[OldKP] [decimal](16, 6) NULL,
	[DesignDCC] [real] NULL,
	[OldDCC] [real] NULL,
	[DCC] [real] NULL,
	[MeanSeabedDep] [real] NULL,
	[AdjSeabedDep] [real] NULL,
	[East_Fixed] [decimal](16, 6) NULL,
	[North_Fixed] [decimal](16, 6) NULL,
	[Loc] [nvarchar](50) NULL,
	[SurveyDir] [int] NULL,
	[LocNum] [nvarchar](50) NULL,
	[AnomalyNumber] [int] NULL,
	[AnomalyUniqueNumber] [nvarchar](255) NULL,
	[AnomalyCode] [nvarchar](15) NULL,
	[AnomalyClass] [nvarchar](15) NULL,
	[HourLocation] [nvarchar](25) NULL,
	[OldEvent] [bit] NULL,
	[Dive_Number] [int] NULL,
	[AnodeSecure] [bit] NULL,
	[MG_Coverage] [nvarchar](2000) NULL,
	[MG_Thickness] [real] NULL,
	[PercentageBurial] [real] NULL,
	[Heading] [real] NULL,
	[HeadingDev] [real] NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[IMP_DepthLogType]    Script Date: 9/17/2018 02:19:45 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'IMP_DepthLogType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[IMP_DepthLogType] AS TABLE(
	[ID] [int] NOT NULL,
	[DTime] [datetime] NOT NULL,
	[Depth] [real] NULL,
	[Altimeter] [real] NULL,
	[DepthTide] [real] NULL,
	[CorrDepthTide] [real] NULL,
	[CorrAltimeter] [real] NULL,
	[Discard] [bit] NULL,
	[DepthAltKP] [float] NULL,
	[SSpeed] [real] NULL,
	[ROVID] [int] NOT NULL,
	[DepthTideAlt] [real] NULL,
	[Tide] [real] NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[IMP_DesignEventProcType]    Script Date: 9/17/2018 02:19:45 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'IMP_DesignEventProcType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[IMP_DesignEventProcType] AS TABLE(
	[ID] [int] NOT NULL,
	[EventNum] [int] NULL,
	[DTime] [datetime] NULL,
	[PCode] [nvarchar](15) NULL,
	[SCode] [nvarchar](15) NULL,
	[KP] [decimal](16, 6) NULL,
	[East] [decimal](16, 6) NULL,
	[North] [decimal](16, 6) NULL,
	[PipeDepth] [real] NULL,
	[SeabedDepth] [real] NULL,
	[BurialDepth] [real] NULL,
	[Length] [real] NULL,
	[Height] [real] NULL,
	[Width] [real] NULL,
	[CP] [real] NULL,
	[ProximityCP] [real] NULL,
	[Heading] [real] NULL,
	[HeadingDev] [real] NULL,
	[CPFieldGradiant] [real] NULL,
	[Comment] [text] NULL,
	[Depth] [real] NULL,
	[ROVID] [int] NULL,
	[Active] [bit] NULL,
	[Depletion] [int] NULL,
	[DepletionValue] [real] NULL,
	[Angle] [real] NULL,
	[ClockNotation] [int] NULL,
	[Distance] [real] NULL,
	[DistanceNotation] [char](25) NULL,
	[Contact] [bit] NULL,
	[FieldJointNumber] [int] NULL,
	[CrossingName] [char](25) NULL,
	[CrossingClearance] [real] NULL,
	[CPReading] [real] NULL,
	[Anomaly] [bit] NULL,
	[Discard] [bit] NULL,
	[CustomComment] [text] NULL,
	[PiggyBackValue] [int] NULL,
	[Reviewed] [bit] NULL,
	[EventQC] [bit] NULL,
	[KP_Fixed] [decimal](16, 6) NULL,
	[DesignKp] [decimal](16, 6) NULL,
	[OldKP] [decimal](16, 6) NULL,
	[DesignDCC] [real] NULL,
	[OldDCC] [real] NULL,
	[DCC] [real] NULL,
	[AdjSeabedDep] [real] NULL,
	[MeanSeabedDep] [real] NULL,
	[North_Fixed] [decimal](16, 6) NULL,
	[East_Fixed] [decimal](16, 6) NULL,
	[Loc] [nvarchar](50) NULL,
	[SurveyDir] [int] NULL,
	[LocNum] [nvarchar](50) NULL,
	[AnomalyNumber] [int] NULL,
	[AnomalyUniqueNumber] [nvarchar](255) NULL,
	[AnomalyCode] [nvarchar](15) NULL,
	[AnomalyClass] [nvarchar](15) NULL,
	[HourLocation] [nvarchar](25) NULL,
	[OldEvent] [bit] NULL,
	[Dive_Number] [int] NULL,
	[AnodeSecure] [bit] NULL,
	[MG_Coverage] [nvarchar](2000) NULL,
	[MG_Thickness] [real] NULL,
	[PercentageBurial] [real] NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[IMP_DesignPosProcType]    Script Date: 9/17/2018 02:19:46 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'IMP_DesignPosProcType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[IMP_DesignPosProcType] AS TABLE(
	[ID] [int] NOT NULL,
	[DTime] [datetime] NULL,
	[FixNum] [int] NULL,
	[North] [decimal](16, 6) NULL,
	[East] [decimal](16, 6) NULL,
	[Kp] [decimal](16, 6) NULL,
	[Depth] [real] NULL,
	[Heading] [real] NULL,
	[Pitch] [real] NULL,
	[Roll] [real] NULL,
	[Logged] [bit] NULL,
	[HeadingDev] [real] NULL,
	[Discard] [bit] NULL,
	[Digitized] [bit] NULL,
	[ROVID] [int] NULL,
	[DesignKp] [decimal](16, 6) NULL,
	[OldKP] [decimal](16, 6) NULL,
	[DesignDCC] [real] NULL,
	[OldDCC] [real] NULL,
	[De-spiked] [bit] NULL,
	[DCC] [real] NULL,
	[Long] [nvarchar](50) NULL,
	[LAT] [nvarchar](50) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[IMP_DesignScanProcType]    Script Date: 9/17/2018 02:19:46 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'IMP_DesignScanProcType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[IMP_DesignScanProcType] AS TABLE(
	[ID] [int] NOT NULL,
	[DTime] [datetime] NULL,
	[ScanNum] [int] NULL,
	[East] [decimal](16, 6) NULL,
	[North] [decimal](16, 6) NULL,
	[KP] [decimal](16, 6) NULL,
	[Pitch] [real] NULL,
	[Roll] [real] NULL,
	[Heading] [real] NULL,
	[MasterNumScans] [smallint] NULL,
	[SlaveNumScans] [smallint] NULL,
	[MasterAngStart] [real] NULL,
	[SlaveAngStart] [real] NULL,
	[MasterAngStep] [real] NULL,
	[SlaveAngStep] [real] NULL,
	[PipeDepth] [real] NULL,
	[SeaBedDep] [real] NULL,
	[PipeDeltaY] [real] NULL,
	[ScanData1] [image] NULL,
	[DeltaX1] [real] NULL,
	[DeltaY1] [real] NULL,
	[DeltaZ1] [real] NULL,
	[RX1] [real] NULL,
	[RY1] [real] NULL,
	[RZ1] [real] NULL,
	[ScanData2] [image] NULL,
	[DeltaX2] [real] NULL,
	[DeltaY2] [real] NULL,
	[DeltaZ2] [real] NULL,
	[RX2] [real] NULL,
	[RY2] [real] NULL,
	[RZ2] [real] NULL,
	[Delta2CorY] [real] NULL,
	[Delta2CorZ] [real] NULL,
	[R2CorZ] [real] NULL,
	[HeadingDev] [real] NULL,
	[SSpeed] [real] NULL,
	[SeaBedLHS1Dep] [real] NULL,
	[SeaBedLHS2Dep] [real] NULL,
	[SeaBedLHS3Dep] [real] NULL,
	[SeaBedLHS4Dep] [real] NULL,
	[FProc] [bit] NULL,
	[FChart] [bit] NULL,
	[FFProc] [bit] NULL,
	[Discard] [bit] NULL,
	[Depth] [real] NULL,
	[KPDifference] [float] NULL,
	[ScanExtraData1] [image] NULL,
	[ScanExtraData2] [image] NULL,
	[SeaBedRHS1Dep] [real] NULL,
	[SeaBedRHS2Dep] [real] NULL,
	[SeaBedRHS3Dep] [real] NULL,
	[SeaBedRHS4Dep] [real] NULL,
	[ROVID] [int] NULL,
	[SeaBedLHS1S] [real] NULL,
	[SeaBedLHS2S] [real] NULL,
	[SeaBedLHS3S] [real] NULL,
	[SeaBedLHS4S] [real] NULL,
	[SeaBedLHS1E] [real] NULL,
	[SeaBedLHS2E] [real] NULL,
	[SeaBedLHS3E] [real] NULL,
	[SeaBedLHS4E] [real] NULL,
	[SeaBedRHS1S] [real] NULL,
	[SeaBedRHS2S] [real] NULL,
	[SeaBedRHS3S] [real] NULL,
	[SeaBedRHS4S] [real] NULL,
	[SeaBedRHS1E] [real] NULL,
	[SeaBedRHS2E] [real] NULL,
	[SeaBedRHS3E] [real] NULL,
	[SeaBedRHS4E] [real] NULL,
	[SeaBed0Range] [real] NULL,
	[AverageSeabedDepth] [real] NULL,
	[DoNotShowPipe] [bit] NULL,
	[PTProc] [bit] NULL,
	[SmoothedPipe] [bit] NULL,
	[SmoothedSeabed] [bit] NULL,
	[PTAdded] [bit] NULL,
	[PipeDiameter] [real] NULL,
	[DesignKp] [decimal](16, 6) NULL,
	[OldKP] [decimal](16, 6) NULL,
	[DesignDCC] [real] NULL,
	[OldDCC] [real] NULL,
	[DCC] [real] NULL,
	[HeadEast] [decimal](16, 6) NULL,
	[HeadNorth] [decimal](16, 6) NULL,
	[CorrEast] [decimal](16, 6) NULL,
	[CorrNorth] [decimal](16, 6) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[IMP_DigitizedPipeType]    Script Date: 9/17/2018 02:19:46 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'IMP_DigitizedPipeType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[IMP_DigitizedPipeType] AS TABLE(
	[ID] [int] NOT NULL,
	[East] [decimal](16, 6) NULL,
	[North] [decimal](16, 6) NULL,
	[KP] [decimal](16, 6) NOT NULL,
	[Discard] [bit] NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[IMP_DigitizedprofileType]    Script Date: 9/17/2018 02:19:46 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'IMP_DigitizedprofileType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[IMP_DigitizedprofileType] AS TABLE(
	[ID] [int] NOT NULL,
	[East] [decimal](16, 6) NULL,
	[North] [decimal](16, 6) NULL,
	[KP] [decimal](16, 6) NOT NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[IMP_DigitizedSeaBedType]    Script Date: 9/17/2018 02:19:46 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'IMP_DigitizedSeaBedType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[IMP_DigitizedSeaBedType] AS TABLE(
	[ID] [int] NOT NULL,
	[East] [decimal](16, 6) NULL,
	[North] [decimal](16, 6) NULL,
	[KP] [decimal](16, 6) NOT NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[IMP_DopplerLogType]    Script Date: 9/17/2018 02:19:46 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'IMP_DopplerLogType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[IMP_DopplerLogType] AS TABLE(
	[DTime] [datetime] NOT NULL,
	[Pitch] [float] NULL,
	[Roll] [float] NULL,
	[Heading] [float] NULL,
	[ShipX] [float] NULL,
	[ShipY] [float] NULL,
	[ShipZ] [float] NULL,
	[InsX] [float] NULL,
	[InsY] [float] NULL,
	[InsZ] [float] NULL,
	[EarthX] [float] NULL,
	[EarthY] [float] NULL,
	[EarthZ] [float] NULL,
	[qc] [bit] NOT NULL,
	[ID] [int] NOT NULL,
	[DopplerDTime] [datetime] NULL,
	[Salinity] [float] NULL,
	[Temperature] [float] NULL,
	[Depth] [float] NULL,
	[SoundSpeed] [float] NULL,
	[DopplerBIT] [int] NULL,
	[IErrorVelocity] [float] NULL,
	[IStatusVelocity] [bit] NULL,
	[SStatusVelocity] [bit] NULL,
	[EStatusVelocity] [bit] NULL,
	[EDNorth] [float] NULL,
	[EDUpward] [float] NULL,
	[EDRange] [float] NULL,
	[EDTime] [float] NULL,
	[ROVID] [int] NOT NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[IMP_EMMScanLogType]    Script Date: 9/17/2018 02:19:46 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'IMP_EMMScanLogType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[IMP_EMMScanLogType] AS TABLE(
	[ID] [int] NOT NULL,
	[em_model] [int] NULL,
	[CompTime] [datetime] NULL,
	[DataTime] [datetime] NULL,
	[ping_counter] [int] NULL,
	[serial_number] [int] NULL,
	[transmit_sectors] [int] NULL,
	[valid_beams] [int] NULL,
	[sampling_frequency] [float] NULL,
	[rov_depth] [int] NULL,
	[sound_speed] [int] NULL,
	[max_num_beams] [int] NULL,
	[spare1] [int] NULL,
	[spare2] [int] NULL,
	[tilt_angle] [image] NULL,
	[focus_range] [image] NULL,
	[signal_length] [image] NULL,
	[transmit_time_offset] [image] NULL,
	[center_frequency] [image] NULL,
	[bandwidth] [image] NULL,
	[signal_waveform] [image] NULL,
	[transmit_sector_number] [image] NULL,
	[beam_pointing_angle] [image] NULL,
	[range] [image] NULL,
	[trans_sector_num] [image] NULL,
	[reflectivity] [image] NULL,
	[quality_factor] [image] NULL,
	[detection_window] [image] NULL,
	[beam_no] [image] NULL,
	[spare] [image] NULL,
	[total_beams] [int] NULL,
	[offsetx] [float] NULL,
	[offsety] [float] NULL,
	[rotation] [float] NULL,
	[ROVID] [int] NOT NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[IMP_EMSScanLogType]    Script Date: 9/17/2018 02:19:46 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'IMP_EMSScanLogType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[IMP_EMSScanLogType] AS TABLE(
	[ID] [int] NOT NULL,
	[em_model] [int] NULL,
	[CompTime] [datetime] NULL,
	[DataTime] [datetime] NULL,
	[ping_counter] [int] NULL,
	[serial_number] [int] NULL,
	[transmit_sectors] [int] NULL,
	[valid_beams] [int] NULL,
	[sampling_frequency] [float] NULL,
	[rov_depth] [int] NULL,
	[sound_speed] [int] NULL,
	[max_num_beams] [int] NULL,
	[spare1] [int] NULL,
	[spare2] [int] NULL,
	[tilt_angle] [image] NULL,
	[focus_range] [image] NULL,
	[signal_length] [image] NULL,
	[transmit_time_offset] [image] NULL,
	[center_frequency] [image] NULL,
	[bandwidth] [image] NULL,
	[signal_waveform] [image] NULL,
	[transmit_sector_number] [image] NULL,
	[beam_pointing_angle] [image] NULL,
	[range] [image] NULL,
	[trans_sector_num] [image] NULL,
	[reflectivity] [image] NULL,
	[quality_factor] [image] NULL,
	[detection_window] [image] NULL,
	[beam_no] [image] NULL,
	[spare] [image] NULL,
	[total_beams] [int] NULL,
	[offsetx] [float] NULL,
	[offsety] [float] NULL,
	[rotation] [float] NULL,
	[ROVID] [int] NOT NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[IMP_ErrorMsgType]    Script Date: 9/17/2018 02:19:46 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'IMP_ErrorMsgType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[IMP_ErrorMsgType] AS TABLE(
	[ID] [int] NOT NULL,
	[DTime] [datetime] NOT NULL,
	[Msg] [ntext] NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[IMP_EventLogType]    Script Date: 9/17/2018 02:19:46 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'IMP_EventLogType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[IMP_EventLogType] AS TABLE(
	[ID] [int] NOT NULL,
	[DTime] [datetime] NOT NULL,
	[PCode] [nvarchar](15) NULL,
	[SCode] [nvarchar](15) NULL,
	[KP] [real] NULL,
	[East] [float] NULL,
	[North] [float] NULL,
	[PipeDepth] [real] NULL,
	[SeabedDepth] [real] NULL,
	[BurialDepth] [real] NULL,
	[Length] [real] NULL,
	[Height] [real] NULL,
	[Width] [real] NULL,
	[CP] [real] NULL,
	[ProximityCP] [real] NULL,
	[ContactCP] [real] NULL,
	[CPFieldGradiant] [real] NULL,
	[Comment] [text] NULL,
	[Depth] [real] NULL,
	[ROVID] [int] NOT NULL,
	[Active] [bit] NULL,
	[Depletion] [int] NULL,
	[DepletionValue] [real] NULL,
	[Angle] [real] NULL,
	[ClockNotation] [int] NULL,
	[Distance] [real] NULL,
	[DistanceNotation] [char](25) NULL,
	[Contact] [bit] NULL,
	[FieldJointNumber] [int] NULL,
	[CrossingName] [char](25) NULL,
	[CrossingClearance] [real] NULL,
	[CPReading] [real] NULL,
	[Anomaly] [bit] NULL,
	[Discard] [bit] NULL,
	[CustomComment] [text] NULL,
	[PiggyBackValue] [int] NULL,
	[Reviewed] [bit] NULL,
	[EventQC] [bit] NULL,
	[DesignKp] [real] NULL,
	[OldKP] [real] NULL,
	[DesignDCC] [real] NULL,
	[OldDCC] [real] NULL,
	[HourLocation] [char](25) NULL,
	[AnomalyNumber] [int] NULL,
	[AnomalyUniqueNumber] [nvarchar](255) NULL,
	[AnomalyCode] [nvarchar](15) NULL,
	[AnomalyClass] [nvarchar](15) NULL,
	[OldEvent] [bit] NULL,
	[EventNum] [int] NULL,
	[Dive_Number] [int] NULL,
	[AnodeSecure] [bit] NULL,
	[MG_Coverage] [nvarchar](2000) NULL,
	[MG_Thickness] [real] NULL,
	[PercentageBurial] [real] NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[IMP_EventProcType]    Script Date: 9/17/2018 02:19:47 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'IMP_EventProcType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[IMP_EventProcType] AS TABLE(
	[ID] [int] NOT NULL,
	[EventNum] [int] NULL,
	[DTime] [datetime] NOT NULL,
	[PCode] [nvarchar](15) NULL,
	[SCode] [nvarchar](15) NULL,
	[KP] [decimal](16, 6) NULL,
	[East] [decimal](16, 6) NULL,
	[North] [decimal](16, 6) NULL,
	[PipeDepth] [real] NULL,
	[SeabedDepth] [real] NULL,
	[BurialDepth] [real] NULL,
	[Length] [real] NULL,
	[Height] [real] NULL,
	[Width] [real] NULL,
	[CP] [real] NULL,
	[ProximityCP] [real] NULL,
	[Heading] [real] NULL,
	[HeadingDev] [real] NULL,
	[ContactCP] [real] NULL,
	[CPFieldGradiant] [real] NULL,
	[Comment] [text] NULL,
	[Depth] [real] NULL,
	[ROVID] [int] NOT NULL,
	[Active] [bit] NULL,
	[Depletion] [int] NULL,
	[DepletionValue] [real] NULL,
	[Angle] [real] NULL,
	[ClockNotation] [int] NULL,
	[Distance] [real] NULL,
	[DistanceNotation] [nvarchar](50) NULL,
	[Contact] [bit] NULL,
	[FieldJointNumber] [int] NULL,
	[CrossingName] [nvarchar](50) NULL,
	[CrossingClearance] [real] NULL,
	[CPReading] [real] NULL,
	[Anomaly] [bit] NULL,
	[Discard] [bit] NULL,
	[CustomComment] [text] NULL,
	[PiggyBackValue] [int] NULL,
	[Reviewed] [bit] NULL,
	[EventQC] [bit] NULL,
	[KP_Fixed] [decimal](16, 6) NULL,
	[North_Fixed] [decimal](16, 6) NULL,
	[East_Fixed] [decimal](16, 6) NULL,
	[DesignKp] [decimal](16, 6) NULL,
	[OldKP] [decimal](16, 6) NULL,
	[DesignDCC] [real] NULL,
	[OldDCC] [real] NULL,
	[DCC] [real] NULL,
	[HourLocation] [nvarchar](50) NULL,
	[MeanSeabedDep] [real] NULL,
	[AdjSeabedDep] [real] NULL,
	[Loc] [nvarchar](50) NULL,
	[SurveyDir] [int] NULL,
	[LocNum] [nvarchar](50) NULL,
	[AnomalyNumber] [int] NULL,
	[AnomalyUniqueNumber] [nvarchar](255) NULL,
	[AnomalyCode] [nvarchar](15) NULL,
	[AnomalyClass] [nvarchar](15) NULL,
	[OldEvent] [bit] NULL,
	[Dive_Number] [int] NULL,
	[AnodeSecure] [bit] NULL,
	[MG_Coverage] [nvarchar](2000) NULL,
	[MG_Thickness] [real] NULL,
	[PercentageBurial] [real] NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[IMP_FreeSpanListType]    Script Date: 9/17/2018 02:19:47 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'IMP_FreeSpanListType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[IMP_FreeSpanListType] AS TABLE(
	[ID] [int] NOT NULL,
	[PCode] [char](10) NULL,
	[KP] [decimal](16, 6) NULL,
	[DTime] [datetime] NULL,
	[SCode] [char](10) NULL,
	[eventnum] [int] NULL,
	[ROVID] [int] NOT NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[IMP_HeadingLogType]    Script Date: 9/17/2018 02:19:47 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'IMP_HeadingLogType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[IMP_HeadingLogType] AS TABLE(
	[ID] [int] NOT NULL,
	[DTime] [datetime] NOT NULL,
	[Heading] [real] NOT NULL,
	[CorrHeading] [real] NULL,
	[Discard] [bit] NULL,
	[ROVID] [int] NOT NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[IMP_OldEventProcType]    Script Date: 9/17/2018 02:19:47 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'IMP_OldEventProcType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[IMP_OldEventProcType] AS TABLE(
	[ID] [int] NOT NULL,
	[DTime] [datetime] NULL,
	[PCode] [nvarchar](15) NULL,
	[SCode] [nvarchar](15) NULL,
	[KP] [decimal](16, 6) NULL,
	[East] [decimal](16, 6) NULL,
	[North] [decimal](16, 6) NULL,
	[PipeDepth] [real] NULL,
	[SeabedDepth] [real] NULL,
	[BurialDepth] [real] NULL,
	[Length] [real] NULL,
	[Height] [real] NULL,
	[Width] [real] NULL,
	[CP] [real] NULL,
	[CPFieldGradiant] [real] NULL,
	[Comment] [text] NULL,
	[Depth] [real] NULL,
	[ROVID] [int] NULL,
	[Active] [bit] NULL,
	[Depletion] [int] NULL,
	[DepletionValue] [real] NULL,
	[Angle] [real] NULL,
	[ClockNotation] [int] NULL,
	[Distance] [real] NULL,
	[DistanceNotation] [nvarchar](50) NULL,
	[Contact] [bit] NULL,
	[FieldJointNumber] [int] NULL,
	[CrossingName] [nvarchar](50) NULL,
	[CrossingClearance] [real] NULL,
	[CPReading] [real] NULL,
	[Anomaly] [bit] NULL,
	[Discard] [bit] NULL,
	[CustomComment] [text] NULL,
	[PiggyBackValue] [int] NULL,
	[Reviewed] [bit] NULL,
	[EventQC] [bit] NULL,
	[KP_Fixed] [real] NULL,
	[DesignKp] [decimal](16, 6) NULL,
	[OldKP] [decimal](16, 6) NULL,
	[DesignDCC] [real] NULL,
	[OldDCC] [real] NULL,
	[DCC] [real] NULL,
	[AdjSeabedDep] [real] NULL,
	[MeanSeabedDep] [real] NULL,
	[North_Fixed] [decimal](16, 6) NULL,
	[East_Fixed] [decimal](16, 6) NULL,
	[Loc] [nvarchar](50) NULL,
	[SurveyDir] [int] NULL,
	[LocNum] [nvarchar](50) NULL,
	[AnomalyNumber] [int] NULL,
	[AnomalyUniqueNumber] [nvarchar](255) NULL,
	[AnomalyCode] [nvarchar](15) NULL,
	[AnomalyClass] [nvarchar](15) NULL,
	[EventNum] [int] NULL,
	[HourLocation] [nvarchar](25) NULL,
	[OldEvent] [bit] NULL,
	[Dive_Number] [int] NULL,
	[AnodeSecure] [bit] NULL,
	[MG_Coverage] [nvarchar](2000) NULL,
	[MG_Thickness] [real] NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[IMP_OldPosProcType]    Script Date: 9/17/2018 02:19:47 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'IMP_OldPosProcType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[IMP_OldPosProcType] AS TABLE(
	[ID] [int] NOT NULL,
	[DTime] [datetime] NULL,
	[FixNum] [int] NULL,
	[East] [decimal](16, 6) NULL,
	[North] [decimal](16, 6) NULL,
	[Kp] [decimal](16, 6) NULL,
	[Depth] [real] NULL,
	[Heading] [real] NULL,
	[Pitch] [real] NULL,
	[Roll] [real] NULL,
	[Logged] [bit] NULL,
	[HeadingDev] [real] NULL,
	[Discard] [bit] NULL,
	[Digitized] [bit] NULL,
	[ROVID] [int] NULL,
	[DesignKp] [decimal](16, 6) NULL,
	[OldKP] [decimal](16, 6) NULL,
	[DesignDCC] [real] NULL,
	[OldDCC] [real] NULL,
	[DCC] [real] NULL,
	[De-spiked] [bit] NULL,
	[Long] [nvarchar](50) NULL,
	[LAT] [nvarchar](50) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[IMP_OldScanProcType]    Script Date: 9/17/2018 02:19:47 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'IMP_OldScanProcType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[IMP_OldScanProcType] AS TABLE(
	[ID] [int] NOT NULL,
	[DTime] [datetime] NULL,
	[ScanNum] [int] NULL,
	[East] [decimal](16, 6) NULL,
	[North] [decimal](16, 6) NULL,
	[KP] [decimal](16, 6) NULL,
	[Pitch] [real] NULL,
	[Roll] [real] NULL,
	[Heading] [real] NULL,
	[MasterNumScans] [smallint] NULL,
	[SlaveNumScans] [smallint] NULL,
	[MasterAngStart] [real] NULL,
	[SlaveAngStart] [real] NULL,
	[MasterAngStep] [real] NULL,
	[SlaveAngStep] [real] NULL,
	[PipeDepth] [real] NULL,
	[SeaBedDep] [real] NULL,
	[PipeDeltaY] [real] NULL,
	[ScanData1] [image] NULL,
	[DeltaX1] [real] NULL,
	[DeltaY1] [real] NULL,
	[DeltaZ1] [real] NULL,
	[RX1] [real] NULL,
	[RY1] [real] NULL,
	[RZ1] [real] NULL,
	[ScanData2] [image] NULL,
	[DeltaX2] [real] NULL,
	[DeltaY2] [real] NULL,
	[DeltaZ2] [real] NULL,
	[RX2] [real] NULL,
	[RY2] [real] NULL,
	[RZ2] [real] NULL,
	[Delta2CorY] [real] NULL,
	[Delta2CorZ] [real] NULL,
	[R2CorZ] [real] NULL,
	[HeadingDev] [real] NULL,
	[SSpeed] [real] NULL,
	[SeaBedLHS1Dep] [real] NULL,
	[SeaBedLHS2Dep] [real] NULL,
	[SeaBedLHS3Dep] [real] NULL,
	[SeaBedLHS4Dep] [real] NULL,
	[FProc] [bit] NULL,
	[FChart] [bit] NULL,
	[FFProc] [bit] NULL,
	[Discard] [bit] NULL,
	[Depth] [real] NULL,
	[KPDifference] [float] NULL,
	[ScanExtraData1] [image] NULL,
	[ScanExtraData2] [image] NULL,
	[SeaBedRHS1Dep] [real] NULL,
	[SeaBedRHS2Dep] [real] NULL,
	[SeaBedRHS3Dep] [real] NULL,
	[SeaBedRHS4Dep] [real] NULL,
	[ROVID] [int] NULL,
	[SeaBedLHS1S] [real] NULL,
	[SeaBedLHS2S] [real] NULL,
	[SeaBedLHS3S] [real] NULL,
	[SeaBedLHS4S] [real] NULL,
	[SeaBedLHS1E] [real] NULL,
	[SeaBedLHS2E] [real] NULL,
	[SeaBedLHS3E] [real] NULL,
	[SeaBedLHS4E] [real] NULL,
	[SeaBedRHS1S] [real] NULL,
	[SeaBedRHS2S] [real] NULL,
	[SeaBedRHS3S] [real] NULL,
	[SeaBedRHS4S] [real] NULL,
	[SeaBedRHS1E] [real] NULL,
	[SeaBedRHS2E] [real] NULL,
	[SeaBedRHS3E] [real] NULL,
	[SeaBedRHS4E] [real] NULL,
	[SeaBed0Range] [real] NULL,
	[AverageSeabedDepth] [real] NULL,
	[DoNotShowPipe] [bit] NULL,
	[PTProc] [bit] NULL,
	[SmoothedPipe] [bit] NULL,
	[SmoothedSeabed] [bit] NULL,
	[PTAdded] [bit] NULL,
	[PipeDiameter] [real] NULL,
	[DesignKp] [decimal](16, 6) NULL,
	[OldKP] [decimal](16, 6) NULL,
	[DesignDCC] [real] NULL,
	[OldDCC] [real] NULL,
	[DCC] [real] NULL,
	[CorrEast] [decimal](16, 6) NULL,
	[CorrNorth] [decimal](16, 6) NULL,
	[HeadEast] [decimal](16, 6) NULL,
	[HeadNorth] [decimal](16, 6) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[IMP_PCodeType]    Script Date: 9/17/2018 02:19:48 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'IMP_PCodeType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[IMP_PCodeType] AS TABLE(
	[ID] [int] NOT NULL,
	[Code] [nvarchar](15) NULL,
	[Name] [nvarchar](50) NULL,
	[Picture] [image] NULL,
	[PShortcut] [int] NULL,
	[Type] [int] NULL,
	[Layername] [nvarchar](50) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[IMP_PipeListType]    Script Date: 9/17/2018 02:19:48 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'IMP_PipeListType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[IMP_PipeListType] AS TABLE(
	[PipeDepth] [real] NULL,
	[SeaBedDepth] [real] NULL,
	[ID] [int] NULL,
	[ROVID] [int] NOT NULL,
	[SeaBedLHS1Dep] [real] NULL,
	[SeaBedLHS2Dep] [real] NULL,
	[SeaBedLHS3Dep] [real] NULL,
	[SeaBedLHS4Dep] [real] NULL,
	[SeaBedRHS1Dep] [real] NULL,
	[SeaBedRHS2Dep] [real] NULL,
	[SeaBedRHS3Dep] [real] NULL,
	[SeaBedRHS4Dep] [real] NULL,
	[OldKP] [real] NULL,
	[DesignDCC] [real] NULL,
	[OldDCC] [real] NULL,
	[DTime] [datetime] NULL,
	[Depth] [real] NULL,
	[DCC] [real] NULL,
	[PipeDiameter] [real] NULL,
	[Long] [nvarchar](50) NULL,
	[Lat] [nvarchar](50) NULL,
	[Discard] [bit] NULL,
	[DesignKp] [decimal](16, 6) NULL,
	[East] [decimal](16, 6) NULL,
	[North] [decimal](16, 6) NULL,
	[KP] [decimal](16, 6) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[IMP_PitchRoleLogType]    Script Date: 9/17/2018 02:19:48 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'IMP_PitchRoleLogType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[IMP_PitchRoleLogType] AS TABLE(
	[ID] [int] NOT NULL,
	[DTime] [datetime] NOT NULL,
	[Pitch] [real] NULL,
	[Role] [real] NULL,
	[CorrPitch] [real] NULL,
	[CorrRole] [real] NULL,
	[Discard] [bit] NULL,
	[ROVID] [int] NOT NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[IMP_PitchRollLogType]    Script Date: 9/17/2018 02:19:48 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'IMP_PitchRollLogType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[IMP_PitchRollLogType] AS TABLE(
	[ID] [int] NOT NULL,
	[DTime] [datetime] NOT NULL,
	[Pitch] [real] NULL,
	[Roll] [real] NULL,
	[CorrPitch] [real] NULL,
	[CorrRoll] [real] NULL,
	[Discard] [bit] NULL,
	[ROVID] [int] NOT NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[IMP_PosLogType]    Script Date: 9/17/2018 02:19:48 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'IMP_PosLogType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[IMP_PosLogType] AS TABLE(
	[ID] [int] NOT NULL,
	[DTime] [datetime] NOT NULL,
	[Depth] [real] NULL,
	[East] [decimal](16, 6) NULL,
	[North] [decimal](16, 6) NULL,
	[KP] [decimal](16, 6) NULL,
	[Logged] [bit] NULL,
	[Discard] [bit] NULL,
	[ROVID] [int] NOT NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[IMP_PosProcType]    Script Date: 9/17/2018 02:19:48 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'IMP_PosProcType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[IMP_PosProcType] AS TABLE(
	[ID] [int] NOT NULL,
	[DTime] [datetime] NOT NULL,
	[FixNum] [int] NOT NULL,
	[East] [decimal](16, 6) NULL,
	[North] [decimal](16, 6) NULL,
	[Kp] [decimal](16, 6) NULL,
	[Depth] [real] NULL,
	[Heading] [real] NULL,
	[Pitch] [real] NULL,
	[Roll] [real] NULL,
	[Logged] [bit] NULL,
	[HeadingDev] [real] NULL,
	[Discard] [bit] NULL,
	[ROVID] [int] NOT NULL,
	[De-spiked] [bit] NULL,
	[Digitized] [bit] NULL,
	[DesignKp] [decimal](16, 6) NULL,
	[OldKP] [decimal](16, 6) NULL,
	[DesignDCC] [real] NULL,
	[OldDCC] [real] NULL,
	[DCC] [real] NULL,
	[Long] [nvarchar](50) NULL,
	[LAT] [nvarchar](50) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[IMP_PRCNavigationDetailsType]    Script Date: 9/17/2018 02:19:48 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'IMP_PRCNavigationDetailsType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[IMP_PRCNavigationDetailsType] AS TABLE(
	[ID] [int] NOT NULL,
	[FK_PRCNavID] [int] NOT NULL,
	[DTime] [datetime] NULL,
	[East] [decimal](16, 6) NULL,
	[North] [decimal](16, 6) NULL,
	[KP] [decimal](16, 6) NULL,
	[Depth] [real] NULL,
	[Heading] [real] NULL,
	[Pitch] [real] NULL,
	[Roll] [real] NULL,
	[HeadingDev] [real] NULL,
	[Timestamp] [bigint] NULL,
	[ImageNames] [varbinary](max) NULL,
	[DroppedFrame] [bit] NULL,
	[ROVID] [int] NOT NULL,
	[Discard] [bit] NULL,
	[INS_TimeTag] [datetime] NULL,
	[INS_Latitude] [decimal](19, 16) NULL,
	[INS_Longitude] [decimal](19, 16) NULL,
	[INS_East] [decimal](16, 6) NULL,
	[INS_North] [decimal](16, 6) NULL,
	[INS_Depth] [float] NULL,
	[INS_Altitude] [float] NULL,
	[INS_Roll] [float] NULL,
	[INS_Pitch] [float] NULL,
	[INS_Heading] [float] NULL,
	[INS_VelocityNorth] [float] NULL,
	[INS_VelocityEast] [float] NULL,
	[INS_VelocityDown] [float] NULL,
	[INS_wFwd] [float] NULL,
	[INS_wStbd] [float] NULL,
	[INS_wDwn] [float] NULL,
	[INS_AccelerationFwd] [float] NULL,
	[INS_AccelerationStbd] [float] NULL,
	[INS_AccelerationDwn] [float] NULL,
	[INS_PosMajor] [real] NULL,
	[INS_PosMinor] [real] NULL,
	[INS_dirPMajor] [real] NULL,
	[INS_StdDepth] [real] NULL,
	[INS_StdLevN] [real] NULL,
	[INS_StdLevE] [real] NULL,
	[INS_stdHeading] [real] NULL,
	[INS_velMajor] [real] NULL,
	[INS_velMinor] [real] NULL,
	[INS_dirVMajor] [real] NULL,
	[INS_VelDown] [float] NULL,
	[INS_Status] [binary](16) NULL,
	[INS_CurrentDTime] [datetime2](7) NULL,
	[INS_PreviousDTime] [datetime2](7) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[IMP_PRCNavigationType]    Script Date: 9/17/2018 02:19:48 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'IMP_PRCNavigationType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[IMP_PRCNavigationType] AS TABLE(
	[PK_PRCNavID] [int] NOT NULL,
	[LoggingPath] [nvarchar](255) NULL,
	[NavDate] [nvarchar](50) NULL,
	[ImageFolders] [varbinary](max) NULL,
	[CalibrationFiles] [varbinary](max) NULL,
	[IsImported] [bit] NULL,
	[IsHeadingPitchRollCalculated] [bit] NULL,
	[IsKPHeadingDevCalculated] [bit] NULL,
	[IsDepthCalculated] [bit] NULL,
	[ImportStatus] [tinyint] NULL,
	[ImportMessage] [nvarchar](200) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[IMP_PTrackerLogType]    Script Date: 9/17/2018 02:19:48 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'IMP_PTrackerLogType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[IMP_PTrackerLogType] AS TABLE(
	[ID] [int] NOT NULL,
	[DTime] [datetime] NOT NULL,
	[LAT] [real] NULL,
	[COV] [real] NULL,
	[VRT] [real] NULL,
	[ALT] [real] NULL,
	[STBDCH] [real] NULL,
	[CenterCH] [real] NULL,
	[PORTCH] [real] NULL,
	[RedundCH] [real] NULL,
	[QCCode] [int] NULL,
	[ROVID] [int] NOT NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[IMP_PTrackerProcType]    Script Date: 9/17/2018 02:19:48 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'IMP_PTrackerProcType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[IMP_PTrackerProcType] AS TABLE(
	[ID] [int] NOT NULL,
	[PTrackerNum] [int] NULL,
	[DTime] [datetime] NOT NULL,
	[Depth] [real] NULL,
	[KP] [decimal](16, 6) NULL,
	[DesignKP] [decimal](16, 6) NULL,
	[East] [decimal](16, 6) NULL,
	[North] [decimal](16, 6) NULL,
	[FProc] [bit] NULL,
	[LAT] [real] NULL,
	[COV] [real] NULL,
	[VRT] [real] NULL,
	[ALT] [real] NULL,
	[STBDCH] [real] NULL,
	[CenterCH] [real] NULL,
	[PORTCH] [real] NULL,
	[RedundCH] [real] NULL,
	[QCCode] [int] NULL,
	[Heading] [real] NULL,
	[Pitch] [real] NULL,
	[Roll] [real] NULL,
	[HeadingDev] [real] NULL,
	[PipeDepth] [real] NULL,
	[SeabedDepth] [real] NULL,
	[Discard] [bit] NULL,
	[ROVID] [int] NOT NULL,
	[PTProc] [bit] NULL,
	[CorrEast] [decimal](16, 6) NULL,
	[CorrNorth] [decimal](16, 6) NULL,
	[DesignDCC] [real] NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[IMP_RAWSurveyType]    Script Date: 9/17/2018 02:19:48 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'IMP_RAWSurveyType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[IMP_RAWSurveyType] AS TABLE(
	[DTime] [datetime] NOT NULL,
	[KP] [decimal](16, 6) NULL,
	[DCC] [decimal](16, 6) NULL,
	[East] [decimal](16, 6) NULL,
	[North] [decimal](16, 6) NULL,
	[Depth] [decimal](16, 6) NULL,
	[Altimeter] [decimal](16, 6) NULL,
	[Heading] [decimal](16, 6) NULL,
	[Pitch] [decimal](16, 6) NULL,
	[Roll] [decimal](16, 6) NULL,
	[ROVID] [int] NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[IMP_SB7125MScanLogType]    Script Date: 9/17/2018 02:19:48 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'IMP_SB7125MScanLogType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[IMP_SB7125MScanLogType] AS TABLE(
	[ID] [int] NOT NULL,
	[DataTime] [datetime] NULL,
	[CompTime] [datetime] NULL,
	[Sonar_Id] [bigint] NULL,
	[Ping_Number] [bigint] NULL,
	[Multi_Ping_Sequence] [int] NULL,
	[Beam_Count] [int] NULL,
	[Layer_Comp_Flag] [smallint] NULL,
	[Sound_Velocity_Flag] [smallint] NULL,
	[Sound_Velocity] [float] NULL,
	[Min_Filter_Info] [float] NULL,
	[Max_Filter_Info] [float] NULL,
	[Quality] [image] NULL,
	[Offsetx] [float] NULL,
	[Offsety] [float] NULL,
	[Rotation] [float] NULL,
	[MasterMirror] [bit] NULL,
	[Range] [image] NULL,
	[Intensity] [image] NULL,
	[Beam_hz_angle] [image] NULL,
	[ROVID] [int] NOT NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[IMP_SB7125SScanLogType]    Script Date: 9/17/2018 02:19:49 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'IMP_SB7125SScanLogType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[IMP_SB7125SScanLogType] AS TABLE(
	[ID] [int] NOT NULL,
	[DataTime] [datetime] NULL,
	[CompTime] [datetime] NULL,
	[Sonar_Id] [bigint] NULL,
	[Ping_Number] [bigint] NULL,
	[Multi_Ping_Sequence] [int] NULL,
	[Beam_Count] [int] NULL,
	[Layer_Comp_Flag] [smallint] NULL,
	[Sound_Velocity_Flag] [smallint] NULL,
	[Sound_Velocity] [float] NULL,
	[Min_Filter_Info] [float] NULL,
	[Max_Filter_Info] [float] NULL,
	[Quality] [image] NULL,
	[Offsetx] [float] NULL,
	[Offsety] [float] NULL,
	[Rotation] [float] NULL,
	[MasterMirror] [bit] NULL,
	[Range] [image] NULL,
	[Intensity] [image] NULL,
	[Beam_hz_angle] [image] NULL,
	[ROVID] [int] NOT NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[IMP_SBMScanLogType]    Script Date: 9/17/2018 02:19:49 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'IMP_SBMScanLogType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[IMP_SBMScanLogType] AS TABLE(
	[ID] [int] NOT NULL,
	[DataTime] [datetime] NULL,
	[MasterNumScans] [smallint] NULL,
	[MasterAngStart] [real] NULL,
	[MasterAngStep] [real] NULL,
	[ScanData] [image] NULL,
	[MasterSSpeed] [real] NULL,
	[MasterMirror] [bit] NULL,
	[CompTime] [datetime] NULL,
	[Latency] [int] NULL,
	[packet_type] [int] NULL,
	[packet_subtype] [int] NULL,
	[ping_number] [bigint] NULL,
	[sonar_id] [bigint] NULL,
	[sonar_model] [int] NULL,
	[freqency] [int] NULL,
	[velocity] [int] NULL,
	[sample_rate] [int] NULL,
	[ping_rate] [int] NULL,
	[range_set] [int] NULL,
	[power] [int] NULL,
	[gain] [int] NULL,
	[pulse_width] [int] NULL,
	[pulse_width_old] [int] NULL,
	[tvg_spread] [int] NULL,
	[tvg_absorb] [int] NULL,
	[projector_type] [int] NULL,
	[projector_beam_width] [int] NULL,
	[beam_width] [int] NULL,
	[beam_spacing_num] [int] NULL,
	[beam_spacing_denom] [int] NULL,
	[projector_angle] [int] NULL,
	[min_range] [int] NULL,
	[max_range] [int] NULL,
	[min_depth] [int] NULL,
	[max_depth] [int] NULL,
	[filters_active] [int] NULL,
	[flags] [int] NULL,
	[temperature] [int] NULL,
	[beam_count] [int] NULL,
	[quality] [varchar](300) NULL,
	[offsetx] [float] NULL,
	[offsety] [float] NULL,
	[rotation] [float] NULL,
	[ROVID] [int] NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[IMP_SBSScanLogType]    Script Date: 9/17/2018 02:19:49 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'IMP_SBSScanLogType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[IMP_SBSScanLogType] AS TABLE(
	[ID] [int] NOT NULL,
	[DataTime] [datetime] NULL,
	[SlaveNumScans] [smallint] NULL,
	[SlaveAngStart] [real] NULL,
	[SlaveAngStep] [real] NULL,
	[ScanData] [image] NULL,
	[SlaveSSpeed] [real] NULL,
	[SlaveMirror] [bit] NULL,
	[CompTime] [datetime] NULL,
	[Latency] [int] NULL,
	[packet_type] [int] NULL,
	[packet_subtype] [int] NULL,
	[ping_number] [bigint] NULL,
	[sonar_id] [bigint] NULL,
	[sonar_model] [int] NULL,
	[freqency] [int] NULL,
	[velocity] [int] NULL,
	[sample_rate] [int] NULL,
	[ping_rate] [int] NULL,
	[range_set] [int] NULL,
	[power] [int] NULL,
	[gain] [int] NULL,
	[pulse_width] [int] NULL,
	[pulse_width_old] [int] NULL,
	[tvg_spread] [int] NULL,
	[tvg_absorb] [int] NULL,
	[projector_type] [int] NULL,
	[projector_beam_width] [int] NULL,
	[beam_width] [int] NULL,
	[beam_spacing_num] [int] NULL,
	[beam_spacing_denom] [int] NULL,
	[projector_angle] [int] NULL,
	[min_range] [int] NULL,
	[max_range] [int] NULL,
	[min_depth] [int] NULL,
	[max_depth] [int] NULL,
	[filters_active] [int] NULL,
	[flags] [int] NULL,
	[temperature] [int] NULL,
	[beam_count] [int] NULL,
	[quality] [varchar](300) NULL,
	[offsetx] [float] NULL,
	[offsety] [float] NULL,
	[rotation] [float] NULL,
	[ROVID] [int] NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[IMP_ScanLogType]    Script Date: 9/17/2018 02:19:49 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'IMP_ScanLogType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[IMP_ScanLogType] AS TABLE(
	[ID] [int] NOT NULL,
	[MasterProfTime] [datetime] NULL,
	[SlaveProfTime] [datetime] NULL,
	[MasterNumScans] [smallint] NOT NULL,
	[SlaveNumScans] [smallint] NOT NULL,
	[MasterAngStart] [real] NULL,
	[SlaveAngStart] [real] NULL,
	[MasterAngStep] [real] NULL,
	[SlaveAngStep] [real] NULL,
	[MasterOffsetX] [real] NULL,
	[SlaveOffsetX] [real] NULL,
	[MasterOffsetY] [real] NULL,
	[SlaveOffsetY] [real] NULL,
	[MasterOffsetZ] [real] NULL,
	[SlaveOffsetZ] [real] NULL,
	[MasterOffsetR] [real] NULL,
	[SlaveOffsetR] [real] NULL,
	[ScanData1] [image] NULL,
	[ScanData2] [image] NULL,
	[MasterSSpeed] [real] NULL,
	[SlaveSSpeed] [real] NULL,
	[MasterMirror] [bit] NULL,
	[SlaveMirror] [bit] NULL,
	[ScanExtraData1] [image] NULL,
	[ScanExtraData2] [image] NULL,
	[ExDataFormat1] [int] NULL,
	[ExDataFormat2] [int] NULL,
	[ROVID] [int] NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[IMP_ScanProcType]    Script Date: 9/17/2018 02:19:49 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'IMP_ScanProcType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[IMP_ScanProcType] AS TABLE(
	[ID] [int] NOT NULL,
	[DTime] [datetime] NOT NULL,
	[ScanNum] [int] NOT NULL,
	[East] [decimal](16, 6) NULL,
	[North] [decimal](16, 6) NULL,
	[KP] [decimal](16, 6) NULL,
	[Pitch] [real] NULL,
	[Roll] [real] NULL,
	[Heading] [real] NULL,
	[MasterNumScans] [smallint] NOT NULL,
	[SlaveNumScans] [smallint] NOT NULL,
	[MasterAngStart] [real] NOT NULL,
	[SlaveAngStart] [real] NOT NULL,
	[MasterAngStep] [real] NOT NULL,
	[SlaveAngStep] [real] NOT NULL,
	[PipeDepth] [real] NULL,
	[SeaBedDep] [real] NULL,
	[PipeDeltaY] [real] NULL,
	[ScanData1] [image] NULL,
	[DeltaX1] [real] NULL,
	[DeltaY1] [real] NULL,
	[DeltaZ1] [real] NULL,
	[RX1] [real] NULL,
	[RY1] [real] NULL,
	[RZ1] [real] NOT NULL,
	[ScanData2] [image] NULL,
	[DeltaX2] [real] NULL,
	[DeltaY2] [real] NULL,
	[DeltaZ2] [real] NULL,
	[RX2] [real] NULL,
	[RY2] [real] NULL,
	[RZ2] [real] NULL,
	[Delta2CorY] [real] NULL,
	[Delta2CorZ] [real] NULL,
	[R2CorZ] [real] NULL,
	[HeadingDev] [real] NULL,
	[SSpeed] [real] NULL,
	[SeaBedLHS1Dep] [real] NULL,
	[SeaBedLHS2Dep] [real] NULL,
	[SeaBedLHS3Dep] [real] NULL,
	[SeaBedLHS4Dep] [real] NULL,
	[FProc] [bit] NULL,
	[FChart] [bit] NULL,
	[FFProc] [bit] NULL,
	[Discard] [bit] NULL,
	[Depth] [real] NULL,
	[KPDifference] [float] NULL,
	[ScanExtraData1] [image] NULL,
	[ScanExtraData2] [image] NULL,
	[SeaBedRHS1Dep] [real] NULL,
	[SeaBedRHS2Dep] [real] NULL,
	[SeaBedRHS3Dep] [real] NULL,
	[SeaBedRHS4Dep] [real] NULL,
	[ROVID] [int] NOT NULL,
	[SeaBedLHS1S] [real] NULL,
	[SeaBedLHS2S] [real] NULL,
	[SeaBedLHS3S] [real] NULL,
	[SeaBedLHS4S] [real] NULL,
	[SeaBedLHS1E] [real] NULL,
	[SeaBedLHS2E] [real] NULL,
	[SeaBedLHS3E] [real] NULL,
	[SeaBedLHS4E] [real] NULL,
	[SeaBedRHS1S] [real] NULL,
	[SeaBedRHS2S] [real] NULL,
	[SeaBedRHS3S] [real] NULL,
	[SeaBedRHS4S] [real] NULL,
	[SeaBedRHS1E] [real] NULL,
	[SeaBedRHS2E] [real] NULL,
	[SeaBedRHS3E] [real] NULL,
	[SeaBedRHS4E] [real] NULL,
	[SeaBed0Range] [real] NULL,
	[DoNotShowPipe] [bit] NULL,
	[PTProc] [bit] NULL,
	[SmoothedPipe] [bit] NULL,
	[SmoothedSeabed] [bit] NULL,
	[PTAdded] [bit] NULL,
	[AverageSeabedDepth] [real] NULL,
	[PipeDiameter] [real] NULL,
	[DesignKp] [decimal](16, 6) NULL,
	[OldKP] [decimal](16, 6) NULL,
	[DesignDCC] [real] NULL,
	[OldDCC] [real] NULL,
	[DCC] [real] NULL,
	[CorrEast] [decimal](16, 6) NULL,
	[CorrNorth] [decimal](16, 6) NULL,
	[ScanDataProp1] [image] NULL,
	[ScanDataProp2] [image] NULL,
	[HeadEast] [decimal](16, 6) NULL,
	[HeadNorth] [decimal](16, 6) NULL,
	[IsPRC] [bit] NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[IMP_SonarLogType]    Script Date: 9/17/2018 02:19:49 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'IMP_SonarLogType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[IMP_SonarLogType] AS TABLE(
	[ID] [int] NOT NULL,
	[msg_length] [int] NULL,
	[CompTime] [datetime] NULL,
	[tx_nde] [smallint] NULL,
	[rx_nde] [smallint] NULL,
	[single_packet] [smallint] NULL,
	[mt_head_data] [smallint] NULL,
	[seq] [smallint] NULL,
	[nde] [smallint] NULL,
	[count] [int] NULL,
	[sonhd] [smallint] NULL,
	[status] [smallint] NULL,
	[sweep] [smallint] NULL,
	[hdctrl] [int] NULL,
	[range] [int] NULL,
	[txn] [bigint] NULL,
	[gain] [smallint] NULL,
	[slope] [int] NULL,
	[adspan] [smallint] NULL,
	[adlow] [smallint] NULL,
	[heading_offset] [int] NULL,
	[adinterval] [int] NULL,
	[llimit] [int] NULL,
	[rlimit] [int] NULL,
	[steps] [smallint] NULL,
	[bearing] [int] NULL,
	[dbytes_count] [int] NULL,
	[dbytes] [image] NULL,
	[ROVID] [int] NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[IMP_STCodeType]    Script Date: 9/17/2018 02:19:50 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'IMP_STCodeType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[IMP_STCodeType] AS TABLE(
	[PrimaryID] [int] NOT NULL,
	[ID] [int] NOT NULL,
	[Code] [nvarchar](15) NOT NULL,
	[Type] [int] NULL,
	[LayerName] [nvarchar](50) NOT NULL,
	[LayerName_Side] [nvarchar](50) NOT NULL,
	[Description] [text] NULL,
	[Comment] [text] NULL,
	[ProfileEventFlag] [bit] NULL,
	[PlanEventFlag] [bit] NULL,
	[AddPlanTextFlag] [bit] NULL,
	[AddProfileTextFlag] [bit] NULL,
	[Discard] [bit] NULL,
	[imagecap] [bit] NULL,
	[AnomalyCode] [nvarchar](15) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[IMP_TideTableDetailsType]    Script Date: 9/17/2018 02:19:50 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'IMP_TideTableDetailsType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[IMP_TideTableDetailsType] AS TABLE(
	[ID] [int] NOT NULL,
	[TideTableID] [int] NULL,
	[FromKP] [decimal](16, 6) NULL,
	[ToKP] [decimal](16, 6) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[IMP_TideType]    Script Date: 9/17/2018 02:19:50 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'IMP_TideType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[IMP_TideType] AS TABLE(
	[ID] [nchar](10) NULL,
	[DTime] [datetime] NOT NULL,
	[KP] [float] NULL,
	[Tide] [real] NOT NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[IMP_VideoLogType]    Script Date: 9/17/2018 02:19:50 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'IMP_VideoLogType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[IMP_VideoLogType] AS TABLE(
	[ID] [int] NOT NULL,
	[SDTime] [datetime] NOT NULL,
	[EDTime] [datetime] NULL,
	[FileName] [nvarchar](50) NOT NULL,
	[FilePath] [nvarchar](1000) NOT NULL,
	[Discard] [bit] NULL,
	[StartKP] [real] NULL,
	[EndKP] [real] NULL,
	[Combined] [bit] NULL,
	[ROVID] [int] NOT NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[KPDataType]    Script Date: 9/17/2018 02:19:50 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'KPDataType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[KPDataType] AS TABLE(
	[ID] [int] NOT NULL,
	[KP] [decimal](16, 6) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[KpDccType]    Script Date: 9/17/2018 02:19:50 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'KpDccType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[KpDccType] AS TABLE(
	[ID] [int] NOT NULL,
	[Kp] [decimal](16, 6) NULL,
	[DCC] [real] NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[KPsType]    Script Date: 9/17/2018 02:19:50 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'KPsType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[KPsType] AS TABLE(
	[KP] [decimal](16, 6) NULL,
	[KP2] [decimal](16, 6) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[LocationType]    Script Date: 9/17/2018 02:19:50 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'LocationType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[LocationType] AS TABLE(
	[ID] [int] NOT NULL,
	[Location] [nvarchar](50) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[LongLATType]    Script Date: 9/17/2018 02:19:50 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'LongLATType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[LongLATType] AS TABLE(
	[ID] [int] NOT NULL,
	[Long] [nvarchar](50) NULL,
	[LAT] [nvarchar](50) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[NewEventProcType]    Script Date: 9/17/2018 02:19:50 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'NewEventProcType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[NewEventProcType] AS TABLE(
	[DTime] [datetime] NOT NULL,
	[KP] [decimal](18, 0) NULL,
	[ROVID] [int] NOT NULL,
	[PCode] [nvarchar](15) NULL,
	[SCode] [nvarchar](15) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[PipeDeltaType]    Script Date: 9/17/2018 02:19:50 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'PipeDeltaType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[PipeDeltaType] AS TABLE(
	[ID] [int] NOT NULL,
	[PipeDelta] [real] NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[PipeDiameterType]    Script Date: 9/17/2018 02:19:50 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'PipeDiameterType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[PipeDiameterType] AS TABLE(
	[ID] [int] NOT NULL,
	[PipeDiameter] [real] NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[PipeListType]    Script Date: 9/17/2018 02:19:50 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'PipeListType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[PipeListType] AS TABLE(
	[ID] [int] NULL,
	[KP] [decimal](16, 6) NULL,
	[East] [decimal](16, 6) NULL,
	[North] [decimal](16, 6) NULL,
	[PipeDepth] [real] NULL,
	[SeaBedDepth] [real] NULL,
	[SeaBedLHS1Dep] [real] NULL,
	[SeaBedLHS2Dep] [real] NULL,
	[SeaBedLHS3Dep] [real] NULL,
	[SeaBedLHS4Dep] [real] NULL,
	[SeaBedRHS1Dep] [real] NULL,
	[SeaBedRHS2Dep] [real] NULL,
	[SeaBedRHS3Dep] [real] NULL,
	[SeaBedRHS4Dep] [real] NULL,
	[DesignKp] [decimal](16, 6) NULL,
	[DCC] [real] NULL,
	[ROVID] [int] NOT NULL,
	[Depth] [real] NULL,
	[DTime] [datetime] NULL,
	[DesignDCC] [real] NULL,
	[PipeDiameter] [real] NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[PitchRoleLogType]    Script Date: 9/17/2018 02:19:50 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'PitchRoleLogType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[PitchRoleLogType] AS TABLE(
	[DTime] [datetime] NOT NULL,
	[Pitch] [real] NULL,
	[Role] [real] NULL,
	[CorrPitch] [real] NULL,
	[CorrRole] [real] NULL,
	[Discard] [bit] NULL,
	[ROVID] [int] NOT NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[PitchRollLogType]    Script Date: 9/17/2018 02:19:50 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'PitchRollLogType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[PitchRollLogType] AS TABLE(
	[DTime] [datetime] NOT NULL,
	[Pitch] [real] NULL,
	[Roll] [real] NULL,
	[CorrPitch] [real] NULL,
	[CorrRoll] [real] NULL,
	[Discard] [bit] NULL,
	[ROVID] [int] NOT NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[PitchType]    Script Date: 9/17/2018 02:19:50 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'PitchType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[PitchType] AS TABLE(
	[ID] [int] NOT NULL,
	[Pitch] [real] NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[PosProcKPType]    Script Date: 9/17/2018 02:19:50 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'PosProcKPType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[PosProcKPType] AS TABLE(
	[ID] [int] NOT NULL,
	[KP] [decimal](16, 6) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[PRC_PRCNavigationDetailsType]    Script Date: 9/17/2018 02:19:50 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'PRC_PRCNavigationDetailsType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[PRC_PRCNavigationDetailsType] AS TABLE(
	[FK_PRCNavID] [int] NOT NULL,
	[DTime] [datetime] NULL,
	[East] [decimal](16, 6) NULL,
	[North] [decimal](16, 6) NULL,
	[KP] [decimal](16, 6) NULL,
	[Depth] [real] NULL,
	[Heading] [real] NULL,
	[Pitch] [real] NULL,
	[Roll] [real] NULL,
	[HeadingDev] [real] NULL,
	[Timestamp] [bigint] NULL,
	[ImageNames] [varbinary](max) NULL,
	[DroppedFrame] [bit] NULL,
	[ROVID] [int] NOT NULL,
	[Discard] [bit] NULL,
	[INS_TimeTag] [datetime] NULL,
	[INS_Latitude] [decimal](19, 16) NULL,
	[INS_Longitude] [decimal](19, 16) NULL,
	[INS_East] [decimal](16, 6) NULL,
	[INS_North] [decimal](16, 6) NULL,
	[INS_Depth] [float] NULL,
	[INS_Altitude] [float] NULL,
	[INS_Roll] [float] NULL,
	[INS_Pitch] [float] NULL,
	[INS_Heading] [float] NULL,
	[INS_VelocityNorth] [float] NULL,
	[INS_VelocityEast] [float] NULL,
	[INS_VelocityDown] [float] NULL,
	[INS_wFwd] [float] NULL,
	[INS_wStbd] [float] NULL,
	[INS_wDwn] [float] NULL,
	[INS_AccelerationFwd] [float] NULL,
	[INS_AccelerationStbd] [float] NULL,
	[INS_AccelerationDwn] [float] NULL,
	[INS_PosMajor] [real] NULL,
	[INS_PosMinor] [real] NULL,
	[INS_dirPMajor] [real] NULL,
	[INS_StdDepth] [real] NULL,
	[INS_StdLevN] [real] NULL,
	[INS_StdLevE] [real] NULL,
	[INS_stdHeading] [real] NULL,
	[INS_velMajor] [real] NULL,
	[INS_velMinor] [real] NULL,
	[INS_dirVMajor] [real] NULL,
	[INS_VelDown] [float] NULL,
	[INS_Status] [binary](16) NULL,
	[INS_CurrentDTime] [datetime2](7) NULL,
	[INS_PreviousDTime] [datetime2](7) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[PRCNavDetailsType]    Script Date: 9/17/2018 02:19:50 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'PRCNavDetailsType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[PRCNavDetailsType] AS TABLE(
	[FK_PRCNavID] [int] NOT NULL,
	[DTime] [datetime] NULL,
	[East] [decimal](16, 6) NULL,
	[North] [decimal](16, 6) NULL,
	[KP] [decimal](16, 6) NULL,
	[Depth] [real] NULL,
	[Heading] [real] NULL,
	[Pitch] [real] NULL,
	[Roll] [real] NULL,
	[HeadingDev] [real] NULL,
	[Timestamp] [bigint] NULL,
	[ImageNames] [varbinary](max) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[RollPitchHeadingType]    Script Date: 9/17/2018 02:19:50 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'RollPitchHeadingType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[RollPitchHeadingType] AS TABLE(
	[ID] [int] NOT NULL,
	[Heading] [real] NULL,
	[Pitch] [real] NULL,
	[Roll] [real] NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[RollType]    Script Date: 9/17/2018 02:19:50 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'RollType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[RollType] AS TABLE(
	[ID] [int] NOT NULL,
	[Roll] [real] NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[SeaPipeDepthType]    Script Date: 9/17/2018 02:19:50 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'SeaPipeDepthType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[SeaPipeDepthType] AS TABLE(
	[ID] [int] NOT NULL,
	[Depth] [real] NULL,
	[SeabedDepth] [real] NULL,
	[PipeDepth] [real] NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[SeaPipeType]    Script Date: 9/17/2018 02:19:50 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'SeaPipeType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[SeaPipeType] AS TABLE(
	[ID] [int] NOT NULL,
	[SeabedDepth] [real] NULL,
	[PipeDepth] [real] NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[StartEndKPType]    Script Date: 9/17/2018 02:19:50 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'StartEndKPType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[StartEndKPType] AS TABLE(
	[ID] [int] NOT NULL,
	[StartKP] [real] NULL,
	[EndKP] [real] NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[SurveyDirType]    Script Date: 9/17/2018 02:19:50 PM ******/
IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'SurveyDirType' AND ss.name = N'dbo')
CREATE TYPE [dbo].[SurveyDirType] AS TABLE(
	[ID] [int] NOT NULL,
	[SurveyDir] [int] NULL
)
GO
/****** Object:  Table [dbo].[AOCEventSelection]    Script Date: 9/17/2018 02:19:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AOCEventSelection]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[AOCEventSelection](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[StartEventID] [int] NULL,
	[EndEventID] [int] NULL,
	[ProcessingPath] [nvarchar](250) NULL,
	[NumImagesPer] [real] NULL,
	[IsDTimeSelection] [bit] NULL,
	[GenerationType] [smallint] NULL,
	[FromDTime] [datetime] NULL,
	[ToDTime] [datetime] NULL,
	[FromKP] [decimal](16, 6) NULL,
	[ToKP] [decimal](16, 6) NULL,
	[RangeBefore] [real] NULL,
	[RangeAfter] [real] NULL,
	[StartEvent] [nvarchar](50) NULL,
	[EndEvent] [nvarchar](50) NULL,
	[IsEventQC] [smallint] NULL,
	[IsAnomaly] [smallint] NULL,
	[StartKP] [decimal](16, 6) NULL,
	[EndKP] [decimal](16, 6) NULL,
	[StartDTime] [datetime] NULL,
	[EndDTime] [datetime] NULL,
	[Status] [smallint] NULL,
	[FilterType] [smallint] NULL,
	[IsDiscarded] [bit] NULL,
	[IsIncludedInSelection] [bit] NULL,
	[IsIncludedInProcessing] [bit] NULL,
	[Priority] [int] NULL,
	[AOCType] [tinyint] NULL,
	[PLYSettings] [tinyint] NULL,
 CONSTRAINT [PK_AOCEventSelection] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[AOCProcessingEvents]    Script Date: 9/17/2018 02:19:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AOCProcessingEvents]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[AOCProcessingEvents](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AOCSelectionID] [int] NULL,
	[StartEventID] [int] NULL,
	[StartDTime] [datetime] NULL,
	[StartKP] [decimal](16, 6) NULL,
	[StartEast] [decimal](16, 6) NULL,
	[StartNorth] [decimal](16, 6) NULL,
	[StartDepth] [decimal](16, 6) NULL,
	[EndDTime] [datetime] NULL,
	[EndEventID] [int] NULL,
	[EndKP] [decimal](16, 6) NULL,
	[EndEast] [decimal](16, 6) NULL,
	[EndNorth] [decimal](16, 6) NULL,
	[EndDepth] [decimal](16, 6) NULL,
	[Status] [tinyint] NULL,
	[PLYPath] [nvarchar](255) NULL,
	[GenerationType] [smallint] NULL,
	[NumImagesPer] [real] NULL,
	[RangeBefore] [real] NULL,
	[RangeAfter] [real] NULL,
	[ImagesCount] [int] NULL,
	[Priority] [int] NULL,
	[ErrorMessage] [nvarchar](4000) NULL,
	[IsImagesChanged] [bit] NULL,
	[AOCType] [tinyint] NULL,
	[PLYSettings] [tinyint] NULL,
 CONSTRAINT [PK_AOCProcessingEvents] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[AuditTable]    Script Date: 9/17/2018 02:19:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AuditTable]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[AuditTable](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FileName] [nvarchar](50) NULL,
	[RecordDate] [datetime] NULL,
	[StopDate] [datetime] NULL,
	[StartKP] [real] NULL,
	[EndKP] [real] NULL,
	[QC] [bit] NULL,
	[Reviewed] [bit] NULL,
	[Combined] [bit] NULL,
	[Copied] [bit] NULL,
	[BackedUp] [bit] NULL,
	[ROVID] [int] NOT NULL,
	[FilePath] [nvarchar](1000) NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Batches]    Script Date: 9/17/2018 02:19:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Batches]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Batches](
	[PK_BatchID] [int] IDENTITY(1,1) NOT NULL,
	[FK_ClusterID] [int] NOT NULL,
	[PlannedStartKP] [decimal](16, 6) NULL,
	[StartKP] [decimal](16, 6) NULL,
	[StartDTime] [datetime] NULL,
	[StartEast] [decimal](16, 6) NULL,
	[StartNorth] [decimal](16, 6) NULL,
	[StartDepth] [decimal](16, 6) NULL,
	[PlannedEndKP] [decimal](16, 6) NULL,
	[EndKP] [decimal](16, 6) NULL,
	[EndDTime] [datetime] NULL,
	[EndEast] [decimal](16, 6) NULL,
	[EndNorth] [decimal](16, 6) NULL,
	[EndDepth] [decimal](16, 6) NULL,
	[ImagesCount] [int] NULL,
	[BatchPath] [nvarchar](255) NULL,
	[TransformationMatrix] [ntext] NULL,
	[RotationMatrix] [ntext] NULL,
	[FinishPercentage] [int] NULL,
	[ProcessingStatus] [int] NULL,
	[Status] [tinyint] NULL,
	[Priority] [int] NULL,
	[ErrorMessage] [nvarchar](4000) NULL,
	[IsImagesChanged] [bit] NULL,
 CONSTRAINT [PK_Batches] PRIMARY KEY CLUSTERED 
(
	[PK_BatchID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[BatchesDetails]    Script Date: 9/17/2018 02:19:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[BatchesDetails]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[BatchesDetails](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FK_BatchID] [int] NOT NULL,
	[FK_NavigationDetailID] [int] NULL,
	[FK_ScanID] [int] NULL,
	[DTime] [datetime] NULL,
	[CameraImageNames] [varbinary](max) NULL,
	[CameraPositions] [varbinary](max) NULL,
	[ProcessedCameraPositions] [varbinary](max) NULL,
	[CameraAvgPosX] [decimal](16, 6) NULL,
	[CameraAvgPosY] [decimal](16, 6) NULL,
	[CameraAvgPosZ] [decimal](16, 6) NULL,
	[ProcessedAvgPosX] [decimal](16, 6) NULL,
	[ProcessedAvgPosY] [decimal](16, 6) NULL,
	[ProcessedAvgPosZ] [decimal](16, 6) NULL,
	[IsCameraUsed] [bit] NULL,
	[IsCameraNeglected] [tinyint] NULL,
 CONSTRAINT [PK_BatchesDetails] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CheckCategory]    Script Date: 9/17/2018 02:19:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CheckCategory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CheckCategory](
	[ID] [int] NOT NULL,
	[Name] [nvarchar](100) NULL,
 CONSTRAINT [PK_CheckCategory] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[CheckData]    Script Date: 9/17/2018 02:19:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CheckData]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CheckData](
	[ID] [int] NOT NULL,
	[CategoryID] [int] NULL,
	[Name] [nvarchar](200) NULL,
	[Description] [nvarchar](max) NULL,
	[Query] [varbinary](max) NULL,
	[SubQuery] [varbinary](max) NULL,
 CONSTRAINT [PK_CheckData] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Clusters]    Script Date: 9/17/2018 02:19:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Clusters]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Clusters](
	[PK_ClusterID] [int] IDENTITY(1,1) NOT NULL,
	[PlannedStartKP] [decimal](16, 6) NULL,
	[StartKP] [decimal](16, 6) NOT NULL,
	[StartDTime] [datetime] NOT NULL,
	[StartEast] [decimal](16, 6) NULL,
	[StartNorth] [decimal](16, 6) NULL,
	[StartDepth] [decimal](16, 6) NULL,
	[StartEvent] [nvarchar](50) NULL,
	[StartEventID] [int] NULL,
	[PlannedEndKP] [decimal](16, 6) NULL,
	[EndKP] [decimal](16, 6) NOT NULL,
	[EndDTime] [datetime] NOT NULL,
	[EndEast] [decimal](16, 6) NULL,
	[EndNorth] [decimal](16, 6) NULL,
	[EndDepth] [decimal](16, 6) NULL,
	[EndEvent] [nvarchar](50) NULL,
	[EndEventID] [int] NULL,
	[GenerationType] [int] NULL,
	[ImagesCount] [int] NULL,
	[PerUnit] [float] NULL,
	[MaxBatchSize] [float] NULL,
	[IsKPSelection] [tinyint] NULL,
	[ErrorDistance] [float] NULL,
	[OverlapLength] [float] NULL,
	[BatchSplitGap] [float] NULL,
	[CamerasCount] [int] NULL,
	[ClusterPath] [nvarchar](255) NULL,
	[ProcessedPLYPath] [nvarchar](255) NULL,
	[PLYStartCameraX] [decimal](16, 6) NULL,
	[PLYStartCameraY] [decimal](16, 6) NULL,
	[PLYStartCameraZ] [decimal](16, 6) NULL,
	[ScaleMatrix] [ntext] NULL,
	[RotationMatrix] [ntext] NULL,
	[RollMatrix] [ntext] NULL,
	[DensePointIndices] [image] NULL,
	[ClusterRaduis] [real] NULL,
	[ProcessingStatus] [int] NULL,
	[ProcessingStatusString] [nvarchar](255) NULL,
	[IsFailed] [tinyint] NULL,
	[IsPRC] [bit] NULL,
	[ClusterType] [int] NULL,
	[ProcessingPath] [nvarchar](250) NULL,
	[ActualLength] [decimal](16, 6) NULL,
	[PlannedLength] [decimal](16, 6) NULL,
	[Status] [tinyint] NULL,
	[Priority] [int] NULL,
	[ErrorMessage] [nvarchar](4000) NULL,
	[HeadingDeviationLimit] [float] NULL,
 CONSTRAINT [PK_Clusters] PRIMARY KEY CLUSTERED 
(
	[PK_ClusterID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[CodesDataFromat]    Script Date: 9/17/2018 02:19:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CodesDataFromat]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CodesDataFromat](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FK_FormatID] [int] NOT NULL,
	[FK_CodeID] [int] NOT NULL,
	[CodeName] [nvarchar](50) NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[CorrPosProc]    Script Date: 9/17/2018 02:19:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CorrPosProc]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CorrPosProc](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DTime] [datetime] NULL,
	[PTracker] [bit] NULL,
	[DCC] [real] NULL,
	[Depth] [real] NULL,
	[Discard] [bit] NULL,
	[KP] [decimal](16, 6) NULL,
	[East] [decimal](16, 6) NULL,
	[North] [decimal](16, 6) NULL
) ON [PRIMARY]
END
GO
/****** Object:  Index [PK_CorrPosProc]    Script Date: 9/17/2018 02:19:50 PM ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[CorrPosProc]') AND name = N'PK_CorrPosProc')
CREATE CLUSTERED INDEX [PK_CorrPosProc] ON [dbo].[CorrPosProc]
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CPLog]    Script Date: 9/17/2018 02:19:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CPLog]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CPLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DTime] [datetime] NOT NULL,
	[KP] [real] NULL,
	[East] [decimal](16, 6) NULL,
	[North] [decimal](16, 6) NULL,
	[CP] [real] NULL,
	[FG] [real] NULL,
	[CorrCP] [real] NULL,
	[CorrFG] [real] NULL,
	[DesignKp] [decimal](16, 6) NULL,
	[Discard] [bit] NULL,
	[ROVID] [int] NOT NULL,
	[ContactCP] [real] NULL,
	[CpCal] [real] NULL,
 CONSTRAINT [PK_CPLog] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[DataFormats]    Script Date: 9/17/2018 02:19:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DataFormats]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[DataFormats](
	[PK_FormatID] [int] IDENTITY(1,1) NOT NULL,
	[FormatName] [nvarchar](50) NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[DeletedEvents]    Script Date: 9/17/2018 02:19:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DeletedEvents]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[DeletedEvents](
	[ID] [int] NOT NULL,
	[EventNum] [int] NULL,
	[DTime] [datetime] NOT NULL,
	[PCode] [nvarchar](15) NULL,
	[SCode] [nvarchar](15) NULL,
	[KP] [decimal](16, 6) NULL,
	[East] [decimal](16, 6) NULL,
	[North] [decimal](16, 6) NULL,
	[PipeDepth] [real] NULL,
	[SeabedDepth] [real] NULL,
	[BurialDepth] [real] NULL,
	[Length] [real] NULL,
	[Height] [real] NULL,
	[Width] [real] NULL,
	[Heading] [real] NULL,
	[HeadingDev] [real] NULL,
	[CP] [real] NULL,
	[ProximityCP] [real] NULL,
	[ContactCP] [real] NULL,
	[CPFieldGradiant] [real] NULL,
	[Comment] [text] NULL,
	[Depth] [real] NULL,
	[ROVID] [int] NOT NULL,
	[Active] [bit] NULL,
	[Depletion] [int] NULL,
	[DepletionValue] [real] NULL,
	[Angle] [real] NULL,
	[ClockNotation] [int] NULL,
	[Distance] [real] NULL,
	[DistanceNotation] [char](25) NULL,
	[Contact] [bit] NULL,
	[FieldJointNumber] [int] NULL,
	[CrossingName] [char](25) NULL,
	[CrossingClearance] [real] NULL,
	[CPReading] [real] NULL,
	[Anomaly] [bit] NULL,
	[Discard] [bit] NULL,
	[CustomComment] [text] NULL,
	[PiggyBackValue] [int] NULL,
	[Reviewed] [bit] NULL,
	[EventQC] [bit] NULL,
	[KP_Fixed] [decimal](16, 6) NULL,
	[DesignKp] [decimal](16, 6) NULL,
	[OldKP] [decimal](16, 6) NULL,
	[DesignDCC] [real] NULL,
	[OldDCC] [real] NULL,
	[DCC] [real] NULL,
	[MeanSeabedDep] [real] NULL,
	[AdjSeabedDep] [real] NULL,
	[East_Fixed] [decimal](16, 6) NULL,
	[North_Fixed] [decimal](16, 6) NULL,
	[Loc] [nvarchar](50) NULL,
	[SurveyDir] [int] NULL,
	[LocNum] [nvarchar](50) NULL,
	[AnomalyNumber] [int] NULL,
	[AnomalyUniqueNumber] [nvarchar](255) NULL,
	[AnomalyCode] [nvarchar](15) NULL,
	[AnomalyClass] [nvarchar](15) NULL,
	[HourLocation] [nvarchar](25) NULL,
	[OldEvent] [bit] NULL,
	[Dive_Number] [int] NULL,
	[AnodeSecure] [bit] NULL,
	[MG_Coverage] [nvarchar](2000) NULL,
	[MG_Thickness] [real] NULL,
	[PercentageBurial] [real] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DepthLog]    Script Date: 9/17/2018 02:19:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DepthLog]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[DepthLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DTime] [datetime] NOT NULL,
	[Depth] [real] NULL,
	[Altimeter] [real] NULL,
	[DepthTide] [real] NULL,
	[CorrDepthTide] [real] NULL,
	[CorrAltimeter] [real] NULL,
	[Discard] [bit] NULL,
	[DepthAltKP] [float] NULL,
	[SSpeed] [real] NULL,
	[ROVID] [int] NOT NULL,
	[DepthTideAlt] [real] NULL,
	[Tide] [real] NULL,
 CONSTRAINT [PK_DepthLog] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[DesignEventProc]    Script Date: 9/17/2018 02:19:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DesignEventProc]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[DesignEventProc](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EventNum] [int] NULL,
	[DTime] [datetime] NULL,
	[PCode] [nvarchar](15) NULL,
	[SCode] [nvarchar](15) NULL,
	[KP] [decimal](16, 6) NULL,
	[East] [decimal](16, 6) NULL,
	[North] [decimal](16, 6) NULL,
	[PipeDepth] [real] NULL,
	[SeabedDepth] [real] NULL,
	[BurialDepth] [real] NULL,
	[Length] [real] NULL,
	[Height] [real] NULL,
	[Width] [real] NULL,
	[CP] [real] NULL,
	[ProximityCP] [real] NULL,
	[Heading] [real] NULL,
	[HeadingDev] [real] NULL,
	[CPFieldGradiant] [real] NULL,
	[Comment] [text] NULL,
	[Depth] [real] NULL,
	[ROVID] [int] NULL,
	[Active] [bit] NULL,
	[Depletion] [int] NULL,
	[DepletionValue] [real] NULL,
	[Angle] [real] NULL,
	[ClockNotation] [int] NULL,
	[Distance] [real] NULL,
	[DistanceNotation] [char](25) NULL,
	[Contact] [bit] NULL,
	[FieldJointNumber] [int] NULL,
	[CrossingName] [char](25) NULL,
	[CrossingClearance] [real] NULL,
	[CPReading] [real] NULL,
	[Anomaly] [bit] NULL,
	[Discard] [bit] NULL,
	[CustomComment] [text] NULL,
	[PiggyBackValue] [int] NULL,
	[Reviewed] [bit] NULL,
	[EventQC] [bit] NULL,
	[KP_Fixed] [decimal](16, 6) NULL,
	[DesignKp] [decimal](16, 6) NULL,
	[OldKP] [decimal](16, 6) NULL,
	[DesignDCC] [real] NULL,
	[OldDCC] [real] NULL,
	[DCC] [real] NULL,
	[AdjSeabedDep] [real] NULL,
	[MeanSeabedDep] [real] NULL,
	[North_Fixed] [decimal](16, 6) NULL,
	[East_Fixed] [decimal](16, 6) NULL,
	[Loc] [nvarchar](50) NULL,
	[SurveyDir] [int] NULL,
	[LocNum] [nvarchar](50) NULL,
	[AnomalyNumber] [int] NULL,
	[AnomalyUniqueNumber] [nvarchar](255) NULL,
	[AnomalyCode] [nvarchar](15) NULL,
	[AnomalyClass] [nvarchar](15) NULL,
	[HourLocation] [nvarchar](25) NULL,
	[OldEvent] [bit] NULL,
	[Dive_Number] [int] NULL,
	[AnodeSecure] [bit] NULL,
	[MG_Coverage] [nvarchar](2000) NULL,
	[MG_Thickness] [real] NULL,
	[PercentageBurial] [real] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DesignPosProc]    Script Date: 9/17/2018 02:19:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DesignPosProc]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[DesignPosProc](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DTime] [datetime] NULL,
	[FixNum] [int] NULL,
	[North] [decimal](16, 6) NULL,
	[East] [decimal](16, 6) NULL,
	[Kp] [decimal](16, 6) NULL,
	[Depth] [real] NULL,
	[Heading] [real] NULL,
	[Pitch] [real] NULL,
	[Role] [real] NULL,
	[Roll] [real] NULL,
	[Logged] [bit] NULL,
	[HeadingDev] [real] NULL,
	[Discard] [bit] NULL,
	[Digitized] [bit] NULL,
	[ROVID] [int] NULL,
	[DesignKp] [decimal](16, 6) NULL,
	[OldKP] [decimal](16, 6) NULL,
	[DesignDCC] [real] NULL,
	[OldDCC] [real] NULL,
	[De-spiked] [bit] NULL,
	[DCC] [real] NULL,
	[Long] [nvarchar](50) NULL,
	[LAT] [nvarchar](50) NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[DesignScanProc]    Script Date: 9/17/2018 02:19:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DesignScanProc]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[DesignScanProc](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DTime] [datetime] NULL,
	[ScanNum] [int] NULL,
	[East] [decimal](16, 6) NULL,
	[North] [decimal](16, 6) NULL,
	[KP] [decimal](16, 6) NULL,
	[Pitch] [real] NULL,
	[Role] [real] NULL,
	[Roll] [real] NULL,
	[Heading] [real] NULL,
	[MasterNumScans] [smallint] NULL,
	[SlaveNumScans] [smallint] NULL,
	[MasterAngStart] [real] NULL,
	[SlaveAngStart] [real] NULL,
	[MasterAngStep] [real] NULL,
	[SlaveAngStep] [real] NULL,
	[PipeDepth] [real] NULL,
	[SeaBedDep] [real] NULL,
	[PipeDeltaY] [real] NULL,
	[ScanData1] [image] NULL,
	[DeltaX1] [real] NULL,
	[DeltaY1] [real] NULL,
	[DeltaZ1] [real] NULL,
	[RX1] [real] NULL,
	[RY1] [real] NULL,
	[RZ1] [real] NULL,
	[ScanData2] [image] NULL,
	[DeltaX2] [real] NULL,
	[DeltaY2] [real] NULL,
	[DeltaZ2] [real] NULL,
	[RX2] [real] NULL,
	[RY2] [real] NULL,
	[RZ2] [real] NULL,
	[Delta2CorY] [real] NULL,
	[Delta2CorZ] [real] NULL,
	[R2CorZ] [real] NULL,
	[HeadingDev] [real] NULL,
	[SSpeed] [real] NULL,
	[SeaBedLHS1Dep] [real] NULL,
	[SeaBedLHS2Dep] [real] NULL,
	[SeaBedLHS3Dep] [real] NULL,
	[SeaBedLHS4Dep] [real] NULL,
	[FProc] [bit] NULL,
	[FChart] [bit] NULL,
	[FFProc] [bit] NULL,
	[Discard] [bit] NULL,
	[Depth] [real] NULL,
	[KPDifference] [float] NULL,
	[ScanExtraData1] [image] NULL,
	[ScanExtraData2] [image] NULL,
	[SeaBedRHS1Dep] [real] NULL,
	[SeaBedRHS2Dep] [real] NULL,
	[SeaBedRHS3Dep] [real] NULL,
	[SeaBedRHS4Dep] [real] NULL,
	[ROVID] [int] NULL,
	[SeaBedLHS1S] [real] NULL,
	[SeaBedLHS2S] [real] NULL,
	[SeaBedLHS3S] [real] NULL,
	[SeaBedLHS4S] [real] NULL,
	[SeaBedLHS1E] [real] NULL,
	[SeaBedLHS2E] [real] NULL,
	[SeaBedLHS3E] [real] NULL,
	[SeaBedLHS4E] [real] NULL,
	[SeaBedRHS1S] [real] NULL,
	[SeaBedRHS2S] [real] NULL,
	[SeaBedRHS3S] [real] NULL,
	[SeaBedRHS4S] [real] NULL,
	[SeaBedRHS1E] [real] NULL,
	[SeaBedRHS2E] [real] NULL,
	[SeaBedRHS3E] [real] NULL,
	[SeaBedRHS4E] [real] NULL,
	[SeaBed0Range] [real] NULL,
	[AverageSeabedDepth] [real] NULL,
	[DoNotShowPipe] [bit] NULL,
	[PTProc] [bit] NULL,
	[SmoothedPipe] [bit] NULL,
	[SmoothedSeabed] [bit] NULL,
	[PTAdded] [bit] NULL,
	[PipeDiameter] [real] NULL,
	[DesignKp] [decimal](16, 6) NULL,
	[OldKP] [decimal](16, 6) NULL,
	[DesignDCC] [real] NULL,
	[OldDCC] [real] NULL,
	[DCC] [real] NULL,
	[HeadEast] [decimal](16, 6) NULL,
	[HeadNorth] [decimal](16, 6) NULL,
	[CorrEast] [decimal](16, 6) NULL,
	[CorrNorth] [decimal](16, 6) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[DigitizedPipe]    Script Date: 9/17/2018 02:19:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DigitizedPipe]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[DigitizedPipe](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[East] [decimal](16, 6) NULL,
	[North] [decimal](16, 6) NULL,
	[KP] [decimal](16, 6) NOT NULL,
	[Discard] [bit] NULL,
	[Depth] [real] NULL
) ON [PRIMARY]
END
GO
/****** Object:  Index [PK_DigitizedPipe]    Script Date: 9/17/2018 02:19:50 PM ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[DigitizedPipe]') AND name = N'PK_DigitizedPipe')
CREATE CLUSTERED INDEX [PK_DigitizedPipe] ON [dbo].[DigitizedPipe]
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Digitizedprofile]    Script Date: 9/17/2018 02:19:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Digitizedprofile]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Digitizedprofile](
	[ID] [int] NOT NULL,
	[East] [decimal](16, 6) NULL,
	[North] [decimal](16, 6) NULL,
	[KP] [decimal](16, 6) NOT NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[DigitizedSeaBed]    Script Date: 9/17/2018 02:19:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DigitizedSeaBed]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[DigitizedSeaBed](
	[ID] [int] NOT NULL,
	[East] [decimal](16, 6) NULL,
	[North] [decimal](16, 6) NULL,
	[KP] [decimal](16, 6) NOT NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[DopplerLog]    Script Date: 9/17/2018 02:19:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DopplerLog]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[DopplerLog](
	[DTime] [datetime] NOT NULL,
	[Pitch] [float] NULL,
	[Role] [float] NULL,
	[Roll] [float] NULL,
	[Heading] [float] NULL,
	[ShipX] [float] NULL,
	[ShipY] [float] NULL,
	[ShipZ] [float] NULL,
	[InsX] [float] NULL,
	[InsY] [float] NULL,
	[InsZ] [float] NULL,
	[EarthX] [float] NULL,
	[EarthY] [float] NULL,
	[EarthZ] [float] NULL,
	[qc] [bit] NOT NULL,
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DopplerDTime] [datetime] NULL,
	[Salinity] [float] NULL,
	[Temperature] [float] NULL,
	[Depth] [float] NULL,
	[SoundSpeed] [float] NULL,
	[DopplerBIT] [int] NULL,
	[IErrorVelocity] [float] NULL,
	[IStatusVelocity] [bit] NULL,
	[SStatusVelocity] [bit] NULL,
	[EStatusVelocity] [bit] NULL,
	[EDNorth] [float] NULL,
	[EDUpward] [float] NULL,
	[EDRange] [float] NULL,
	[EDTime] [float] NULL,
	[ROVID] [int] NOT NULL,
 CONSTRAINT [PK_DopplerLog] PRIMARY KEY CLUSTERED 
(
	[DTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[EMMScanLog]    Script Date: 9/17/2018 02:19:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EMMScanLog]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EMMScanLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[em_model] [int] NULL,
	[CompTime] [datetime] NULL,
	[DataTime] [datetime] NULL,
	[ping_counter] [int] NULL,
	[serial_number] [int] NULL,
	[transmit_sectors] [int] NULL,
	[valid_beams] [int] NULL,
	[sampling_frequency] [float] NULL,
	[rov_depth] [int] NULL,
	[sound_speed] [int] NULL,
	[max_num_beams] [int] NULL,
	[spare1] [int] NULL,
	[spare2] [int] NULL,
	[tilt_angle] [image] NULL,
	[focus_range] [image] NULL,
	[signal_length] [image] NULL,
	[transmit_time_offset] [image] NULL,
	[center_frequency] [image] NULL,
	[bandwidth] [image] NULL,
	[signal_waveform] [image] NULL,
	[transmit_sector_number] [image] NULL,
	[beam_pointing_angle] [image] NULL,
	[range] [image] NULL,
	[trans_sector_num] [image] NULL,
	[reflectivity] [image] NULL,
	[quality_factor] [image] NULL,
	[detection_window] [image] NULL,
	[beam_no] [image] NULL,
	[spare] [image] NULL,
	[total_beams] [int] NULL,
	[offsetx] [float] NULL,
	[offsety] [float] NULL,
	[rotation] [float] NULL,
	[ROVID] [int] NOT NULL,
 CONSTRAINT [PK_EMMScanLog] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[EMSScanLog]    Script Date: 9/17/2018 02:19:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EMSScanLog]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EMSScanLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[em_model] [int] NULL,
	[CompTime] [datetime] NULL,
	[DataTime] [datetime] NULL,
	[ping_counter] [int] NULL,
	[serial_number] [int] NULL,
	[transmit_sectors] [int] NULL,
	[valid_beams] [int] NULL,
	[sampling_frequency] [float] NULL,
	[rov_depth] [int] NULL,
	[sound_speed] [int] NULL,
	[max_num_beams] [int] NULL,
	[spare1] [int] NULL,
	[spare2] [int] NULL,
	[tilt_angle] [image] NULL,
	[focus_range] [image] NULL,
	[signal_length] [image] NULL,
	[transmit_time_offset] [image] NULL,
	[center_frequency] [image] NULL,
	[bandwidth] [image] NULL,
	[signal_waveform] [image] NULL,
	[transmit_sector_number] [image] NULL,
	[beam_pointing_angle] [image] NULL,
	[range] [image] NULL,
	[trans_sector_num] [image] NULL,
	[reflectivity] [image] NULL,
	[quality_factor] [image] NULL,
	[detection_window] [image] NULL,
	[beam_no] [image] NULL,
	[spare] [image] NULL,
	[total_beams] [int] NULL,
	[offsetx] [float] NULL,
	[offsety] [float] NULL,
	[rotation] [float] NULL,
	[ROVID] [int] NOT NULL,
 CONSTRAINT [PK_EMSScanLog] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[ErrorMsg]    Script Date: 9/17/2018 02:19:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ErrorMsg]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ErrorMsg](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DTime] [datetime] NOT NULL,
	[Msg] [ntext] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[EventLog]    Script Date: 9/17/2018 02:19:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EventLog]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EventLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DTime] [datetime] NOT NULL,
	[PCode] [nvarchar](15) NULL,
	[SCode] [nvarchar](15) NULL,
	[KP] [real] NULL,
	[East] [float] NULL,
	[North] [float] NULL,
	[PipeDepth] [real] NULL,
	[SeabedDepth] [real] NULL,
	[BurialDepth] [real] NULL,
	[Length] [real] NULL,
	[Height] [real] NULL,
	[Width] [real] NULL,
	[CP] [real] NULL,
	[ProximityCP] [real] NULL,
	[ContactCP] [real] NULL,
	[CPFieldGradiant] [real] NULL,
	[Comment] [text] NULL,
	[Depth] [real] NULL,
	[ROVID] [int] NOT NULL,
	[Active] [bit] NULL,
	[Depletion] [int] NULL,
	[DepletionValue] [real] NULL,
	[Angle] [real] NULL,
	[ClockNotation] [int] NULL,
	[Distance] [real] NULL,
	[DistanceNotation] [char](25) NULL,
	[Contact] [bit] NULL,
	[FieldJointNumber] [int] NULL,
	[CrossingName] [char](25) NULL,
	[CrossingClearance] [real] NULL,
	[CPReading] [real] NULL,
	[Anomaly] [bit] NULL,
	[Discard] [bit] NULL,
	[CustomComment] [text] NULL,
	[PiggyBackValue] [int] NULL,
	[Reviewed] [bit] NULL,
	[EventQC] [bit] NULL,
	[DesignKp] [real] NULL,
	[OldKP] [real] NULL,
	[DesignDCC] [real] NULL,
	[OldDCC] [real] NULL,
	[HourLocation] [char](25) NULL,
	[AnomalyNumber] [int] NULL,
	[AnomalyUniqueNumber] [nvarchar](255) NULL,
	[AnomalyCode] [nvarchar](15) NULL,
	[AnomalyClass] [nvarchar](15) NULL,
	[OldEvent] [bit] NULL,
	[EventNum] [int] NULL,
	[Dive_Number] [int] NULL,
	[AnodeSecure] [bit] NULL,
	[MG_Coverage] [nvarchar](2000) NULL,
	[MG_Thickness] [real] NULL,
	[PercentageBurial] [real] NULL,
 CONSTRAINT [PK_EventLog] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EventProc]    Script Date: 9/17/2018 02:19:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EventProc]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EventProc](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EventNum] [int] NULL,
	[DTime] [datetime] NOT NULL,
	[PCode] [nvarchar](15) NULL,
	[SCode] [nvarchar](15) NULL,
	[KP] [decimal](16, 6) NULL,
	[East] [decimal](16, 6) NULL,
	[North] [decimal](16, 6) NULL,
	[PipeDepth] [real] NULL,
	[SeabedDepth] [real] NULL,
	[BurialDepth] [real] NULL,
	[Length] [real] NULL,
	[Height] [real] NULL,
	[Width] [real] NULL,
	[CP] [real] NULL,
	[ProximityCP] [real] NULL,
	[Heading] [real] NULL,
	[HeadingDev] [real] NULL,
	[ContactCP] [real] NULL,
	[CPFieldGradiant] [real] NULL,
	[Comment] [text] NULL,
	[Depth] [real] NULL,
	[ROVID] [int] NOT NULL,
	[Active] [bit] NULL,
	[Depletion] [int] NULL,
	[DepletionValue] [real] NULL,
	[Angle] [real] NULL,
	[ClockNotation] [int] NULL,
	[Distance] [real] NULL,
	[DistanceNotation] [nvarchar](50) NULL,
	[Contact] [bit] NULL,
	[FieldJointNumber] [int] NULL,
	[CrossingName] [nvarchar](50) NULL,
	[CrossingClearance] [real] NULL,
	[CPReading] [real] NULL,
	[Anomaly] [bit] NULL,
	[Discard] [bit] NULL,
	[CustomComment] [text] NULL,
	[PiggyBackValue] [int] NULL,
	[Reviewed] [bit] NULL,
	[EventQC] [bit] NULL,
	[KP_Fixed] [decimal](16, 6) NULL,
	[North_Fixed] [decimal](16, 6) NULL,
	[East_Fixed] [decimal](16, 6) NULL,
	[DesignKp] [decimal](16, 6) NULL,
	[OldKP] [decimal](16, 6) NULL,
	[DesignDCC] [real] NULL,
	[OldDCC] [real] NULL,
	[DCC] [real] NULL,
	[HourLocation] [nvarchar](50) NULL,
	[MeanSeabedDep] [real] NULL,
	[AdjSeabedDep] [real] NULL,
	[Loc] [nvarchar](50) NULL,
	[SurveyDir] [int] NULL,
	[LocNum] [nvarchar](50) NULL,
	[AnomalyNumber] [int] NULL,
	[AnomalyUniqueNumber] [nvarchar](255) NULL,
	[AnomalyCode] [nvarchar](15) NULL,
	[AnomalyClass] [nvarchar](15) NULL,
	[OldEvent] [bit] NULL,
	[Dive_Number] [int] NULL,
	[AnodeSecure] [bit] NULL,
	[MG_Coverage] [nvarchar](2000) NULL,
	[MG_Thickness] [real] NULL,
	[PercentageBurial] [real] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Index [PK_EventProc]    Script Date: 9/17/2018 02:19:50 PM ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EventProc]') AND name = N'PK_EventProc')
CREATE CLUSTERED INDEX [PK_EventProc] ON [dbo].[EventProc]
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FreeSpanList]    Script Date: 9/17/2018 02:19:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FreeSpanList]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[FreeSpanList](
	[ID] [int] NOT NULL,
	[PCode] [char](10) NULL,
	[KP] [decimal](16, 6) NULL,
	[DTime] [datetime] NULL,
	[SCode] [char](10) NULL,
	[eventnum] [int] NULL,
	[ROVID] [int] NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[HeadingLog]    Script Date: 9/17/2018 02:19:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HeadingLog]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[HeadingLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DTime] [datetime] NOT NULL,
	[Heading] [real] NOT NULL,
	[CorrHeading] [real] NULL,
	[Discard] [bit] NULL,
	[ROVID] [int] NOT NULL,
 CONSTRAINT [PK_HeadingLog] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[OldEventProc]    Script Date: 9/17/2018 02:19:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OldEventProc]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[OldEventProc](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DTime] [datetime] NULL,
	[PCode] [nvarchar](15) NULL,
	[SCode] [nvarchar](15) NULL,
	[KP] [decimal](16, 6) NULL,
	[East] [decimal](16, 6) NULL,
	[North] [decimal](16, 6) NULL,
	[PipeDepth] [real] NULL,
	[SeabedDepth] [real] NULL,
	[BurialDepth] [real] NULL,
	[Length] [real] NULL,
	[Height] [real] NULL,
	[Width] [real] NULL,
	[CP] [real] NULL,
	[CPFieldGradiant] [real] NULL,
	[Comment] [text] NULL,
	[Depth] [real] NULL,
	[ROVID] [int] NULL,
	[Active] [bit] NULL,
	[Depletion] [int] NULL,
	[DepletionValue] [real] NULL,
	[Angle] [real] NULL,
	[ClockNotation] [int] NULL,
	[Distance] [real] NULL,
	[DistanceNotation] [nvarchar](50) NULL,
	[Contact] [bit] NULL,
	[FieldJointNumber] [int] NULL,
	[CrossingName] [nvarchar](50) NULL,
	[CrossingClearance] [real] NULL,
	[CPReading] [real] NULL,
	[Anomaly] [bit] NULL,
	[Discard] [bit] NULL,
	[CustomComment] [text] NULL,
	[PiggyBackValue] [int] NULL,
	[Reviewed] [bit] NULL,
	[EventQC] [bit] NULL,
	[KP_Fixed] [real] NULL,
	[DesignKp] [decimal](16, 6) NULL,
	[OldKP] [decimal](16, 6) NULL,
	[DesignDCC] [real] NULL,
	[OldDCC] [real] NULL,
	[DCC] [real] NULL,
	[AdjSeabedDep] [real] NULL,
	[MeanSeabedDep] [real] NULL,
	[North_Fixed] [decimal](16, 6) NULL,
	[East_Fixed] [decimal](16, 6) NULL,
	[Loc] [nvarchar](50) NULL,
	[SurveyDir] [int] NULL,
	[LocNum] [nvarchar](50) NULL,
	[AnomalyNumber] [int] NULL,
	[AnomalyUniqueNumber] [nvarchar](255) NULL,
	[AnomalyCode] [nvarchar](15) NULL,
	[AnomalyClass] [nvarchar](15) NULL,
	[EventNum] [int] NULL,
	[HourLocation] [nvarchar](25) NULL,
	[OldEvent] [bit] NULL,
	[Dive_Number] [int] NULL,
	[AnodeSecure] [bit] NULL,
	[MG_Coverage] [nvarchar](2000) NULL,
	[MG_Thickness] [real] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[OldPosProc]    Script Date: 9/17/2018 02:19:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OldPosProc]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[OldPosProc](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DTime] [datetime] NULL,
	[FixNum] [int] NULL,
	[East] [decimal](16, 6) NULL,
	[North] [decimal](16, 6) NULL,
	[Kp] [decimal](16, 6) NULL,
	[Depth] [real] NULL,
	[Heading] [real] NULL,
	[Pitch] [real] NULL,
	[Role] [real] NULL,
	[Roll] [real] NULL,
	[Logged] [bit] NULL,
	[HeadingDev] [real] NULL,
	[Discard] [bit] NULL,
	[Digitized] [bit] NULL,
	[ROVID] [int] NULL,
	[DesignKp] [decimal](16, 6) NULL,
	[OldKP] [decimal](16, 6) NULL,
	[DesignDCC] [real] NULL,
	[OldDCC] [real] NULL,
	[DCC] [real] NULL,
	[De-spiked] [bit] NULL,
	[Long] [nvarchar](50) NULL,
	[LAT] [nvarchar](50) NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[OldScanProc]    Script Date: 9/17/2018 02:19:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OldScanProc]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[OldScanProc](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DTime] [datetime] NULL,
	[ScanNum] [int] NULL,
	[East] [decimal](16, 6) NULL,
	[North] [decimal](16, 6) NULL,
	[KP] [decimal](16, 6) NULL,
	[Pitch] [real] NULL,
	[Role] [real] NULL,
	[Roll] [real] NULL,
	[Heading] [real] NULL,
	[MasterNumScans] [smallint] NULL,
	[SlaveNumScans] [smallint] NULL,
	[MasterAngStart] [real] NULL,
	[SlaveAngStart] [real] NULL,
	[MasterAngStep] [real] NULL,
	[SlaveAngStep] [real] NULL,
	[PipeDepth] [real] NULL,
	[SeaBedDep] [real] NULL,
	[PipeDeltaY] [real] NULL,
	[ScanData1] [image] NULL,
	[DeltaX1] [real] NULL,
	[DeltaY1] [real] NULL,
	[DeltaZ1] [real] NULL,
	[RX1] [real] NULL,
	[RY1] [real] NULL,
	[RZ1] [real] NULL,
	[ScanData2] [image] NULL,
	[DeltaX2] [real] NULL,
	[DeltaY2] [real] NULL,
	[DeltaZ2] [real] NULL,
	[RX2] [real] NULL,
	[RY2] [real] NULL,
	[RZ2] [real] NULL,
	[Delta2CorY] [real] NULL,
	[Delta2CorZ] [real] NULL,
	[R2CorZ] [real] NULL,
	[HeadingDev] [real] NULL,
	[SSpeed] [real] NULL,
	[SeaBedLHS1Dep] [real] NULL,
	[SeaBedLHS2Dep] [real] NULL,
	[SeaBedLHS3Dep] [real] NULL,
	[SeaBedLHS4Dep] [real] NULL,
	[FProc] [bit] NULL,
	[FChart] [bit] NULL,
	[FFProc] [bit] NULL,
	[Discard] [bit] NULL,
	[Depth] [real] NULL,
	[KPDifference] [float] NULL,
	[ScanExtraData1] [image] NULL,
	[ScanExtraData2] [image] NULL,
	[SeaBedRHS1Dep] [real] NULL,
	[SeaBedRHS2Dep] [real] NULL,
	[SeaBedRHS3Dep] [real] NULL,
	[SeaBedRHS4Dep] [real] NULL,
	[ROVID] [int] NULL,
	[SeaBedLHS1S] [real] NULL,
	[SeaBedLHS2S] [real] NULL,
	[SeaBedLHS3S] [real] NULL,
	[SeaBedLHS4S] [real] NULL,
	[SeaBedLHS1E] [real] NULL,
	[SeaBedLHS2E] [real] NULL,
	[SeaBedLHS3E] [real] NULL,
	[SeaBedLHS4E] [real] NULL,
	[SeaBedRHS1S] [real] NULL,
	[SeaBedRHS2S] [real] NULL,
	[SeaBedRHS3S] [real] NULL,
	[SeaBedRHS4S] [real] NULL,
	[SeaBedRHS1E] [real] NULL,
	[SeaBedRHS2E] [real] NULL,
	[SeaBedRHS3E] [real] NULL,
	[SeaBedRHS4E] [real] NULL,
	[SeaBed0Range] [real] NULL,
	[AverageSeabedDepth] [real] NULL,
	[DoNotShowPipe] [bit] NULL,
	[PTProc] [bit] NULL,
	[SmoothedPipe] [bit] NULL,
	[SmoothedSeabed] [bit] NULL,
	[PTAdded] [bit] NULL,
	[PipeDiameter] [real] NULL,
	[DesignKp] [decimal](16, 6) NULL,
	[OldKP] [decimal](16, 6) NULL,
	[DesignDCC] [real] NULL,
	[OldDCC] [real] NULL,
	[DCC] [real] NULL,
	[CorrEast] [decimal](16, 6) NULL,
	[CorrNorth] [decimal](16, 6) NULL,
	[HeadEast] [decimal](16, 6) NULL,
	[HeadNorth] [decimal](16, 6) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[PCode]    Script Date: 9/17/2018 02:19:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PCode]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[PCode](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [nvarchar](15) NULL,
	[Name] [nvarchar](50) NULL,
	[Picture] [image] NULL,
	[PShortcut] [int] NULL,
	[Type] [int] NULL,
	[Layername] [nvarchar](50) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[PipeList]    Script Date: 9/17/2018 02:19:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PipeList]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[PipeList](
	[PipeDepth] [real] NULL,
	[SeaBedDepth] [real] NULL,
	[ID] [int] NULL,
	[ROVID] [int] NOT NULL,
	[SeaBedLHS1Dep] [real] NULL,
	[SeaBedLHS2Dep] [real] NULL,
	[SeaBedLHS3Dep] [real] NULL,
	[SeaBedLHS4Dep] [real] NULL,
	[SeaBedRHS1Dep] [real] NULL,
	[SeaBedRHS2Dep] [real] NULL,
	[SeaBedRHS3Dep] [real] NULL,
	[SeaBedRHS4Dep] [real] NULL,
	[OldKP] [real] NULL,
	[DesignDCC] [real] NULL,
	[OldDCC] [real] NULL,
	[DTime] [datetime] NULL,
	[Depth] [real] NULL,
	[DCC] [real] NULL,
	[PipeDiameter] [real] NULL,
	[Long] [nvarchar](50) NULL,
	[Lat] [nvarchar](50) NULL,
	[Discard] [bit] NULL,
	[DesignKp] [decimal](16, 6) NULL,
	[East] [decimal](16, 6) NULL,
	[North] [decimal](16, 6) NULL,
	[KP] [decimal](16, 6) NULL
) ON [PRIMARY]
END
GO
/****** Object:  Index [PK_PipeList]    Script Date: 9/17/2018 02:19:50 PM ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[PipeList]') AND name = N'PK_PipeList')
CREATE CLUSTERED INDEX [PK_PipeList] ON [dbo].[PipeList]
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PitchRoleLog]    Script Date: 9/17/2018 02:19:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PitchRoleLog]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[PitchRoleLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DTime] [datetime] NOT NULL,
	[Pitch] [real] NULL,
	[Role] [real] NULL,
	[CorrPitch] [real] NULL,
	[CorrRole] [real] NULL,
	[Discard] [bit] NULL,
	[ROVID] [int] NOT NULL,
 CONSTRAINT [PK_PitchRoleLog] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[PitchRollLog]    Script Date: 9/17/2018 02:19:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PitchRollLog]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[PitchRollLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DTime] [datetime] NOT NULL,
	[Pitch] [real] NULL,
	[Roll] [real] NULL,
	[CorrPitch] [real] NULL,
	[CorrRoll] [real] NULL,
	[Discard] [bit] NULL,
	[ROVID] [int] NOT NULL,
 CONSTRAINT [PK_PitchRollLog] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[PosLog]    Script Date: 9/17/2018 02:19:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PosLog]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[PosLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DTime] [datetime] NOT NULL,
	[Depth] [real] NULL,
	[East] [decimal](16, 6) NULL,
	[North] [decimal](16, 6) NULL,
	[KP] [decimal](16, 6) NULL,
	[Logged] [bit] NULL,
	[Discard] [bit] NULL,
	[ROVID] [int] NOT NULL,
 CONSTRAINT [PK_PosLog] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[PosProc]    Script Date: 9/17/2018 02:19:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PosProc]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[PosProc](
	[ID] [int] NOT NULL,
	[DTime] [datetime] NOT NULL,
	[FixNum] [int] NOT NULL,
	[East] [decimal](16, 6) NULL,
	[North] [decimal](16, 6) NULL,
	[Kp] [decimal](16, 6) NULL,
	[Depth] [real] NULL,
	[Heading] [real] NULL,
	[Pitch] [real] NULL,
	[Role] [real] NULL,
	[Roll] [real] NULL,
	[Logged] [bit] NULL,
	[HeadingDev] [real] NULL,
	[Discard] [bit] NULL,
	[ROVID] [int] NOT NULL,
	[De-spiked] [bit] NULL,
	[Digitized] [bit] NULL,
	[DesignKp] [decimal](16, 6) NULL,
	[OldKP] [decimal](16, 6) NULL,
	[DesignDCC] [real] NULL,
	[OldDCC] [real] NULL,
	[DCC] [real] NULL,
	[Long] [nvarchar](50) NULL,
	[LAT] [nvarchar](50) NULL
) ON [PRIMARY]
END
GO
/****** Object:  Index [PK_PosProc]    Script Date: 9/17/2018 02:19:50 PM ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[PosProc]') AND name = N'PK_PosProc')
CREATE CLUSTERED INDEX [PK_PosProc] ON [dbo].[PosProc]
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PRCNavigation]    Script Date: 9/17/2018 02:19:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PRCNavigation]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[PRCNavigation](
	[PK_PRCNavID] [int] IDENTITY(1,1) NOT NULL,
	[LoggingPath] [nvarchar](255) NULL,
	[NavDate] [nvarchar](50) NULL,
	[ImageFolders] [varbinary](max) NULL,
	[CalibrationFiles] [varbinary](max) NULL,
	[IsImported] [bit] NULL,
	[IsHeadingPitchRollCalculated] [bit] NULL,
	[IsKPHeadingDevCalculated] [bit] NULL,
	[IsDepthCalculated] [bit] NULL,
	[ImportStatus] [tinyint] NULL,
	[ImportMessage] [nvarchar](200) NULL,
 CONSTRAINT [PK_PRCNavigation] PRIMARY KEY CLUSTERED 
(
	[PK_PRCNavID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PRCNavigationDetails]    Script Date: 9/17/2018 02:19:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PRCNavigationDetails]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[PRCNavigationDetails](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FK_PRCNavID] [int] NOT NULL,
	[DTime] [datetime] NULL,
	[East] [decimal](16, 6) NULL,
	[North] [decimal](16, 6) NULL,
	[KP] [decimal](16, 6) NULL,
	[Depth] [real] NULL,
	[Heading] [real] NULL,
	[Pitch] [real] NULL,
	[Roll] [real] NULL,
	[HeadingDev] [real] NULL,
	[Timestamp] [bigint] NULL,
	[ImageNames] [varbinary](max) NULL,
	[Discard] [bit] NULL,
	[ROVID] [int] NULL,
	[DroppedFrame] [bit] NULL,
	[INS_TimeTag] [datetime] NULL,
	[INS_Latitude] [decimal](19, 16) NULL,
	[INS_Longitude] [decimal](19, 16) NULL,
	[INS_Depth] [float] NULL,
	[INS_Altitude] [float] NULL,
	[INS_Roll] [float] NULL,
	[INS_Pitch] [float] NULL,
	[INS_Heading] [float] NULL,
	[INS_VelocityNorth] [float] NULL,
	[INS_VelocityEast] [float] NULL,
	[INS_VelocityDown] [float] NULL,
	[INS_wFwd] [float] NULL,
	[INS_wStbd] [float] NULL,
	[INS_wDwn] [float] NULL,
	[INS_AccelerationFwd] [float] NULL,
	[INS_AccelerationStbd] [float] NULL,
	[INS_AccelerationDwn] [float] NULL,
	[INS_PosMajor] [real] NULL,
	[INS_PosMinor] [real] NULL,
	[INS_dirPMajor] [real] NULL,
	[INS_StdDepth] [real] NULL,
	[INS_StdLevN] [real] NULL,
	[INS_StdLevE] [real] NULL,
	[INS_stdHeading] [real] NULL,
	[INS_velMajor] [real] NULL,
	[INS_velMinor] [real] NULL,
	[INS_dirVMajor] [real] NULL,
	[INS_VelDown] [real] NULL,
	[INS_Status] [binary](16) NULL,
	[INS_East] [decimal](16, 6) NULL,
	[INS_North] [decimal](16, 6) NULL,
	[INS_CurrentDTime] [datetime2](7) NULL,
	[INS_PreviousDTime] [datetime2](7) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PTrackerLog]    Script Date: 9/17/2018 02:19:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PTrackerLog]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[PTrackerLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DTime] [datetime] NOT NULL,
	[LAT] [real] NULL,
	[COV] [real] NULL,
	[VRT] [real] NULL,
	[ALT] [real] NULL,
	[STBDCH] [real] NULL,
	[CenterCH] [real] NULL,
	[PORTCH] [real] NULL,
	[RedundCH] [real] NULL,
	[QCCode] [int] NULL,
	[ROVID] [int] NOT NULL,
 CONSTRAINT [PK_PTrackerLog] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[PTrackerProc]    Script Date: 9/17/2018 02:19:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PTrackerProc]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[PTrackerProc](
	[ID] [int] NOT NULL,
	[PTrackerNum] [int] NULL,
	[DTime] [datetime] NOT NULL,
	[Depth] [real] NULL,
	[KP] [decimal](16, 6) NULL,
	[DesignKP] [decimal](16, 6) NULL,
	[East] [decimal](16, 6) NULL,
	[North] [decimal](16, 6) NULL,
	[FProc] [bit] NULL,
	[LAT] [real] NULL,
	[COV] [real] NULL,
	[VRT] [real] NULL,
	[ALT] [real] NULL,
	[STBDCH] [real] NULL,
	[CenterCH] [real] NULL,
	[PORTCH] [real] NULL,
	[RedundCH] [real] NULL,
	[QCCode] [int] NULL,
	[Heading] [real] NULL,
	[Pitch] [real] NULL,
	[Roll] [real] NULL,
	[HeadingDev] [real] NULL,
	[PipeDepth] [real] NULL,
	[SeabedDepth] [real] NULL,
	[Discard] [bit] NULL,
	[ROVID] [int] NOT NULL,
	[PTProc] [bit] NULL,
	[CorrEast] [decimal](16, 6) NULL,
	[CorrNorth] [decimal](16, 6) NULL,
	[DesignDCC] [real] NULL
) ON [PRIMARY]
END
GO
/****** Object:  Index [PK_PTrackerProc]    Script Date: 9/17/2018 02:19:50 PM ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[PTrackerProc]') AND name = N'PK_PTrackerProc')
CREATE CLUSTERED INDEX [PK_PTrackerProc] ON [dbo].[PTrackerProc]
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RAWSurvey]    Script Date: 9/17/2018 02:19:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RAWSurvey]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[RAWSurvey](
	[DTime] [datetime] NOT NULL,
	[KP] [decimal](16, 6) NULL,
	[DCC] [decimal](16, 6) NULL,
	[East] [decimal](16, 6) NULL,
	[North] [decimal](16, 6) NULL,
	[Depth] [decimal](16, 6) NULL,
	[Altimeter] [decimal](16, 6) NULL,
	[Heading] [decimal](16, 6) NULL,
	[Pitch] [decimal](16, 6) NULL,
	[Roll] [decimal](16, 6) NULL,
	[ROVID] [int] NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[SB7125MScanLog]    Script Date: 9/17/2018 02:19:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SB7125MScanLog]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SB7125MScanLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DataTime] [datetime] NULL,
	[CompTime] [datetime] NULL,
	[Sonar_Id] [bigint] NULL,
	[Ping_Number] [bigint] NULL,
	[Multi_Ping_Sequence] [int] NULL,
	[Beam_Count] [int] NULL,
	[Layer_Comp_Flag] [smallint] NULL,
	[Sound_Velocity_Flag] [smallint] NULL,
	[Sound_Velocity] [float] NULL,
	[Min_Filter_Info] [float] NULL,
	[Max_Filter_Info] [float] NULL,
	[Quality] [image] NULL,
	[Offsetx] [float] NULL,
	[Offsety] [float] NULL,
	[Rotation] [float] NULL,
	[MasterMirror] [bit] NULL,
	[Range] [image] NULL,
	[Intensity] [image] NULL,
	[Beam_hz_angle] [image] NULL,
	[ROVID] [int] NOT NULL,
 CONSTRAINT [PK_SB7125MScanLog] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[SB7125SScanLog]    Script Date: 9/17/2018 02:19:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SB7125SScanLog]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SB7125SScanLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DataTime] [datetime] NULL,
	[CompTime] [datetime] NULL,
	[Sonar_Id] [bigint] NULL,
	[Ping_Number] [bigint] NULL,
	[Multi_Ping_Sequence] [int] NULL,
	[Beam_Count] [int] NULL,
	[Layer_Comp_Flag] [smallint] NULL,
	[Sound_Velocity_Flag] [smallint] NULL,
	[Sound_Velocity] [float] NULL,
	[Min_Filter_Info] [float] NULL,
	[Max_Filter_Info] [float] NULL,
	[Quality] [image] NULL,
	[Offsetx] [float] NULL,
	[Offsety] [float] NULL,
	[Rotation] [float] NULL,
	[MasterMirror] [bit] NULL,
	[Range] [image] NULL,
	[Intensity] [image] NULL,
	[Beam_hz_angle] [image] NULL,
	[ROVID] [int] NOT NULL,
 CONSTRAINT [PK_SB7125SScanLog] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[SBMScanLog]    Script Date: 9/17/2018 02:19:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SBMScanLog]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SBMScanLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DataTime] [datetime] NULL,
	[MasterNumScans] [smallint] NULL,
	[MasterAngStart] [real] NULL,
	[MasterAngStep] [real] NULL,
	[ScanData] [image] NULL,
	[MasterSSpeed] [real] NULL,
	[MasterMirror] [bit] NULL,
	[CompTime] [datetime] NULL,
	[Latency] [int] NULL,
	[packet_type] [int] NULL,
	[packet_subtype] [int] NULL,
	[ping_number] [bigint] NULL,
	[sonar_id] [bigint] NULL,
	[sonar_model] [int] NULL,
	[freqency] [int] NULL,
	[velocity] [int] NULL,
	[sample_rate] [int] NULL,
	[ping_rate] [int] NULL,
	[range_set] [int] NULL,
	[power] [int] NULL,
	[gain] [int] NULL,
	[pulse_width] [int] NULL,
	[pulse_width_old] [int] NULL,
	[tvg_spread] [int] NULL,
	[tvg_absorb] [int] NULL,
	[projector_type] [int] NULL,
	[projector_beam_width] [int] NULL,
	[beam_width] [int] NULL,
	[beam_spacing_num] [int] NULL,
	[beam_spacing_denom] [int] NULL,
	[projector_angle] [int] NULL,
	[min_range] [int] NULL,
	[max_range] [int] NULL,
	[min_depth] [int] NULL,
	[max_depth] [int] NULL,
	[filters_active] [int] NULL,
	[flags] [int] NULL,
	[temperature] [int] NULL,
	[beam_count] [int] NULL,
	[quality] [varchar](300) NULL,
	[offsetx] [float] NULL,
	[offsety] [float] NULL,
	[rotation] [float] NULL,
	[ROVID] [int] NULL,
 CONSTRAINT [PK_SBMScanLog] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SBSScanLog]    Script Date: 9/17/2018 02:19:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SBSScanLog]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SBSScanLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DataTime] [datetime] NULL,
	[SlaveNumScans] [smallint] NULL,
	[SlaveAngStart] [real] NULL,
	[SlaveAngStep] [real] NULL,
	[ScanData] [image] NULL,
	[SlaveSSpeed] [real] NULL,
	[SlaveMirror] [bit] NULL,
	[CompTime] [datetime] NULL,
	[Latency] [int] NULL,
	[packet_type] [int] NULL,
	[packet_subtype] [int] NULL,
	[ping_number] [bigint] NULL,
	[sonar_id] [bigint] NULL,
	[sonar_model] [int] NULL,
	[freqency] [int] NULL,
	[velocity] [int] NULL,
	[sample_rate] [int] NULL,
	[ping_rate] [int] NULL,
	[range_set] [int] NULL,
	[power] [int] NULL,
	[gain] [int] NULL,
	[pulse_width] [int] NULL,
	[pulse_width_old] [int] NULL,
	[tvg_spread] [int] NULL,
	[tvg_absorb] [int] NULL,
	[projector_type] [int] NULL,
	[projector_beam_width] [int] NULL,
	[beam_width] [int] NULL,
	[beam_spacing_num] [int] NULL,
	[beam_spacing_denom] [int] NULL,
	[projector_angle] [int] NULL,
	[min_range] [int] NULL,
	[max_range] [int] NULL,
	[min_depth] [int] NULL,
	[max_depth] [int] NULL,
	[filters_active] [int] NULL,
	[flags] [int] NULL,
	[temperature] [int] NULL,
	[beam_count] [int] NULL,
	[quality] [varchar](300) NULL,
	[offsetx] [float] NULL,
	[offsety] [float] NULL,
	[rotation] [float] NULL,
	[ROVID] [int] NULL,
 CONSTRAINT [PK_SBSScanLog] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ScanLog]    Script Date: 9/17/2018 02:19:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ScanLog]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ScanLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[MasterProfTime] [datetime] NULL,
	[SlaveProfTime] [datetime] NULL,
	[MasterNumScans] [smallint] NOT NULL,
	[SlaveNumScans] [smallint] NOT NULL,
	[MasterAngStart] [real] NULL,
	[SlaveAngStart] [real] NULL,
	[MasterAngStep] [real] NULL,
	[SlaveAngStep] [real] NULL,
	[MasterOffsetX] [real] NULL,
	[SlaveOffsetX] [real] NULL,
	[MasterOffsetY] [real] NULL,
	[SlaveOffsetY] [real] NULL,
	[MasterOffsetZ] [real] NULL,
	[SlaveOffsetZ] [real] NULL,
	[MasterOffsetR] [real] NULL,
	[SlaveOffsetR] [real] NULL,
	[ScanData1] [image] NULL,
	[ScanData2] [image] NULL,
	[MasterSSpeed] [real] NULL,
	[SlaveSSpeed] [real] NULL,
	[MasterMirror] [bit] NULL,
	[SlaveMirror] [bit] NULL,
	[ScanExtraData1] [image] NULL,
	[ScanExtraData2] [image] NULL,
	[ExDataFormat1] [int] NULL,
	[ExDataFormat2] [int] NULL,
	[ROVID] [int] NULL,
 CONSTRAINT [PK_ScanLog] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[ScanProc]    Script Date: 9/17/2018 02:19:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ScanProc]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ScanProc](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DTime] [datetime] NOT NULL,
	[ScanNum] [int] NOT NULL,
	[East] [decimal](16, 6) NULL,
	[North] [decimal](16, 6) NULL,
	[KP] [decimal](16, 6) NULL,
	[Pitch] [real] NULL,
	[Role] [real] NULL,
	[Roll] [real] NULL,
	[Heading] [real] NULL,
	[MasterNumScans] [smallint] NOT NULL,
	[SlaveNumScans] [smallint] NOT NULL,
	[MasterAngStart] [real] NOT NULL,
	[SlaveAngStart] [real] NOT NULL,
	[MasterAngStep] [real] NOT NULL,
	[SlaveAngStep] [real] NOT NULL,
	[PipeDepth] [real] NULL,
	[SeaBedDep] [real] NULL,
	[PipeDeltaY] [real] NULL,
	[ScanData1] [image] NULL,
	[DeltaX1] [real] NULL,
	[DeltaY1] [real] NULL,
	[DeltaZ1] [real] NULL,
	[RX1] [real] NULL,
	[RY1] [real] NULL,
	[RZ1] [real] NOT NULL,
	[ScanData2] [image] NULL,
	[DeltaX2] [real] NULL,
	[DeltaY2] [real] NULL,
	[DeltaZ2] [real] NULL,
	[RX2] [real] NULL,
	[RY2] [real] NULL,
	[RZ2] [real] NULL,
	[Delta2CorY] [real] NULL,
	[Delta2CorZ] [real] NULL,
	[R2CorZ] [real] NULL,
	[HeadingDev] [real] NULL,
	[SSpeed] [real] NULL,
	[SeaBedLHS1Dep] [real] NULL,
	[SeaBedLHS2Dep] [real] NULL,
	[SeaBedLHS3Dep] [real] NULL,
	[SeaBedLHS4Dep] [real] NULL,
	[FProc] [bit] NULL,
	[FChart] [bit] NULL,
	[FFProc] [bit] NULL,
	[Discard] [bit] NULL,
	[Depth] [real] NULL,
	[KPDifference] [float] NULL,
	[ScanExtraData1] [image] NULL,
	[ScanExtraData2] [image] NULL,
	[SeaBedRHS1Dep] [real] NULL,
	[SeaBedRHS2Dep] [real] NULL,
	[SeaBedRHS3Dep] [real] NULL,
	[SeaBedRHS4Dep] [real] NULL,
	[ROVID] [int] NOT NULL,
	[SeaBedLHS1S] [real] NULL,
	[SeaBedLHS2S] [real] NULL,
	[SeaBedLHS3S] [real] NULL,
	[SeaBedLHS4S] [real] NULL,
	[SeaBedLHS1E] [real] NULL,
	[SeaBedLHS2E] [real] NULL,
	[SeaBedLHS3E] [real] NULL,
	[SeaBedLHS4E] [real] NULL,
	[SeaBedRHS1S] [real] NULL,
	[SeaBedRHS2S] [real] NULL,
	[SeaBedRHS3S] [real] NULL,
	[SeaBedRHS4S] [real] NULL,
	[SeaBedRHS1E] [real] NULL,
	[SeaBedRHS2E] [real] NULL,
	[SeaBedRHS3E] [real] NULL,
	[SeaBedRHS4E] [real] NULL,
	[SeaBed0Range] [real] NULL,
	[DoNotShowPipe] [bit] NULL,
	[PTProc] [bit] NULL,
	[SmoothedPipe] [bit] NULL,
	[SmoothedSeabed] [bit] NULL,
	[PTAdded] [bit] NULL,
	[AverageSeabedDepth] [real] NULL,
	[PipeDiameter] [real] NULL,
	[DesignKp] [decimal](16, 6) NULL,
	[OldKP] [decimal](16, 6) NULL,
	[DesignDCC] [real] NULL,
	[OldDCC] [real] NULL,
	[DCC] [real] NULL,
	[CorrEast] [decimal](16, 6) NULL,
	[CorrNorth] [decimal](16, 6) NULL,
	[ScanDataProp1] [image] NULL,
	[ScanDataProp2] [image] NULL,
	[HeadEast] [decimal](16, 6) NULL,
	[HeadNorth] [decimal](16, 6) NULL,
	[IsPRC] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Index [PK_ScanProc]    Script Date: 9/17/2018 02:19:50 PM ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[ScanProc]') AND name = N'PK_ScanProc')
CREATE CLUSTERED INDEX [PK_ScanProc] ON [dbo].[ScanProc]
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SonarLog]    Script Date: 9/17/2018 02:19:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SonarLog]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SonarLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[msg_length] [int] NULL,
	[CompTime] [datetime] NULL,
	[tx_nde] [smallint] NULL,
	[rx_nde] [smallint] NULL,
	[single_packet] [smallint] NULL,
	[mt_head_data] [smallint] NULL,
	[seq] [smallint] NULL,
	[nde] [smallint] NULL,
	[count] [int] NULL,
	[sonhd] [smallint] NULL,
	[status] [smallint] NULL,
	[sweep] [smallint] NULL,
	[hdctrl] [int] NULL,
	[range] [int] NULL,
	[txn] [bigint] NULL,
	[gain] [smallint] NULL,
	[slope] [int] NULL,
	[adspan] [smallint] NULL,
	[adlow] [smallint] NULL,
	[heading_offset] [int] NULL,
	[adinterval] [int] NULL,
	[llimit] [int] NULL,
	[rlimit] [int] NULL,
	[steps] [smallint] NULL,
	[bearing] [int] NULL,
	[dbytes_count] [int] NULL,
	[dbytes] [image] NULL,
	[ROVID] [int] NULL,
 CONSTRAINT [PK_SonarLog] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[STCode]    Script Date: 9/17/2018 02:19:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[STCode]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[STCode](
	[PrimaryID] [int] NOT NULL,
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [nvarchar](15) NOT NULL,
	[Type] [int] NULL,
	[LayerName] [nvarchar](50) NOT NULL,
	[LayerName_Side] [nvarchar](50) NOT NULL,
	[Description] [text] NULL,
	[Comment] [text] NULL,
	[ProfileEventFlag] [bit] NULL,
	[PlanEventFlag] [bit] NULL,
	[AddPlanTextFlag] [bit] NULL,
	[AddProfileTextFlag] [bit] NULL,
	[Discard] [bit] NULL,
	[imagecap] [bit] NULL,
	[AnomalyCode] [nvarchar](15) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[StoredName]    Script Date: 9/17/2018 02:19:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[StoredName]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[StoredName](
	[_ID] [int] NOT NULL,
	[StoredName] [nvarchar](100) NOT NULL,
	[StoredTypeID] [int] NOT NULL,
	[HasInterpolation] [bit] NOT NULL,
	[HasExtraDoubleField] [bit] NULL,
	[HasExtraBooleanField] [bit] NULL,
	[DTimeName] [nchar](5) NULL,
	[KpName] [nchar](10) NULL,
	[DesignKpName] [nchar](8) NULL,
	[IsAppendAllNulls] [bit] NULL CONSTRAINT [DF__StoredNam__IsApp__6B79F03D]  DEFAULT ((1)),
 CONSTRAINT [PK_StoredName] PRIMARY KEY CLUSTERED 
(
	[_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[StoredType]    Script Date: 9/17/2018 02:19:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[StoredType]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[StoredType](
	[StoredTypeID] [int] IDENTITY(1,1) NOT NULL,
	[StoredTypeName] [nvarchar](60) NOT NULL,
 CONSTRAINT [PK_StoredType] PRIMARY KEY CLUSTERED 
(
	[StoredTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Tide]    Script Date: 9/17/2018 02:19:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Tide]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Tide](
	[ID] [nchar](10) NULL,
	[DTime] [datetime] NOT NULL,
	[KP] [float] NULL,
	[Tide] [real] NOT NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[TideTableDetails]    Script Date: 9/17/2018 02:19:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TideTableDetails]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TideTableDetails](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TideTableID] [int] NULL,
	[FromKP] [decimal](16, 6) NULL,
	[ToKP] [decimal](16, 6) NULL,
 CONSTRAINT [PK_TideTableDetails] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[TruePipe]    Script Date: 9/17/2018 02:19:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TruePipe]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TruePipe](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[LHS1X] [decimal](16, 6) NULL,
	[LHS1Y] [decimal](16, 6) NULL,
	[LHS1Z] [decimal](16, 6) NULL,
	[LHS2X] [decimal](16, 6) NULL,
	[LHS2Y] [decimal](16, 6) NULL,
	[LHS2Z] [decimal](16, 6) NULL,
	[LHS3X] [decimal](16, 6) NULL,
	[LHS3Y] [decimal](16, 6) NULL,
	[LHS3Z] [decimal](16, 6) NULL,
	[LHS4X] [decimal](16, 6) NULL,
	[LHS4Y] [decimal](16, 6) NULL,
	[LHS4Z] [decimal](16, 6) NULL,
	[Seabed0X] [decimal](16, 6) NULL,
	[Seabed0Y] [decimal](16, 6) NULL,
	[Seabed0Z] [decimal](16, 6) NULL,
	[RHS1X] [decimal](16, 6) NULL,
	[RHS1Y] [decimal](16, 6) NULL,
	[RHS1Z] [decimal](16, 6) NULL,
	[RHS2X] [decimal](16, 6) NULL,
	[RHS2Y] [decimal](16, 6) NULL,
	[RHS2Z] [decimal](16, 6) NULL,
	[RHS3X] [decimal](16, 6) NULL,
	[RHS3Y] [decimal](16, 6) NULL,
	[RHS3Z] [decimal](16, 6) NULL,
	[RHS4X] [decimal](16, 6) NULL,
	[RHS4Y] [decimal](16, 6) NULL,
	[RHS4Z] [decimal](16, 6) NULL,
	[TopOfPipeX] [decimal](16, 6) NULL,
	[TopOfPipeY] [decimal](16, 6) NULL,
	[TopOfPipeZ] [decimal](16, 6) NULL,
	[CenterOfPipeX] [decimal](16, 6) NULL,
	[CenterOfPipeY] [decimal](16, 6) NULL,
	[CenterOfPipeZ] [decimal](16, 6) NULL,
	[KP] [decimal](16, 6) NULL,
	[TrueKP] [decimal](16, 6) NULL,
	[ClusterID] [int] NULL,
	[Radius] [float] NULL,
 CONSTRAINT [PK_TruePipe] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[VideoLog]    Script Date: 9/17/2018 02:19:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VideoLog]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[VideoLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SDTime] [datetime] NOT NULL,
	[EDTime] [datetime] NULL,
	[FileName] [nvarchar](50) NOT NULL,
	[FilePath] [nvarchar](1000) NOT NULL,
	[Discard] [bit] NULL,
	[StartKP] [real] NULL,
	[EndKP] [real] NULL,
	[Combined] [bit] NULL,
	[ROVID] [int] NOT NULL,
 CONSTRAINT [PK_VideoLog] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  View [dbo].[StoredNameType]    Script Date: 9/17/2018 02:19:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[StoredNameType]'))
EXEC dbo.sp_executesql @statement = N'CREATE VIEW [dbo].[StoredNameType]
AS
SELECT        dbo.StoredName.StoredName, dbo.StoredType.StoredTypeName, dbo.StoredName.HasInterpolation, dbo.StoredName.HasExtraDoubleField, dbo.StoredName.HasExtraBooleanField, dbo.StoredName.StoredTypeID, 
                         dbo.StoredName.DTimeName, dbo.StoredName.KpName, dbo.StoredName.DesignKpName, dbo.StoredName.IsAppendAllNulls
FROM            dbo.StoredName INNER JOIN
                         dbo.StoredType ON dbo.StoredName.StoredTypeID = dbo.StoredType.StoredTypeID
' 
GO
/****** Object:  View [dbo].[Viewer_ClusterDetails_updated]    Script Date: 9/17/2018 02:19:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[Viewer_ClusterDetails_updated]'))
EXEC dbo.sp_executesql @statement = N'CREATE VIEW [dbo].[Viewer_ClusterDetails_updated]
AS
SELECT        dbo.BatchesDetails.ID, dbo.Batches.FK_ClusterID, dbo.BatchesDetails.CameraImageNames, dbo.BatchesDetails.ProcessedCameraPositions, 
                         dbo.BatchesDetails.ProcessedAvgPosX, dbo.BatchesDetails.ProcessedAvgPosY, dbo.BatchesDetails.ProcessedAvgPosZ, dbo.PRCNavigationDetails.DTime, 
                         dbo.PRCNavigationDetails.East, dbo.PRCNavigationDetails.North, dbo.PRCNavigationDetails.KP, dbo.PRCNavigationDetails.Depth, 
                         dbo.PRCNavigationDetails.Heading, dbo.PRCNavigationDetails.Pitch, dbo.PRCNavigationDetails.Roll, dbo.PRCNavigationDetails.HeadingDev, 
                         dbo.PRCNavigationDetails.ROVID
FROM            dbo.BatchesDetails INNER JOIN
                         dbo.Batches ON dbo.BatchesDetails.FK_BatchID = dbo.Batches.PK_BatchID INNER JOIN
                         dbo.Clusters ON dbo.Clusters.PK_ClusterID = dbo.Batches.FK_ClusterID INNER JOIN
                         dbo.PRCNavigationDetails ON dbo.PRCNavigationDetails.ID = dbo.BatchesDetails.FK_NavigationDetailID
WHERE        (dbo.BatchesDetails.IsCameraNeglected <> 1)
' 
GO
INSERT [dbo].[CheckCategory] ([ID], [Name]) VALUES (0, N'Correction Checks')
GO
INSERT [dbo].[CheckCategory] ([ID], [Name]) VALUES (1, N'PosProc Checks')
GO
INSERT [dbo].[CheckCategory] ([ID], [Name]) VALUES (2, N'DepthLog Checks')
GO
INSERT [dbo].[CheckCategory] ([ID], [Name]) VALUES (3, N'ScanProc Checks')
GO
INSERT [dbo].[CheckCategory] ([ID], [Name]) VALUES (4, N'PipeTracker Checks')
GO
INSERT [dbo].[CheckCategory] ([ID], [Name]) VALUES (5, N'CorrPosProc Checks')
GO
INSERT [dbo].[CheckCategory] ([ID], [Name]) VALUES (6, N'EventProc Checks')
GO
INSERT [dbo].[CheckCategory] ([ID], [Name]) VALUES (7, N'Start-End Sequenc Checks')
GO
INSERT [dbo].[CheckCategory] ([ID], [Name]) VALUES (8, N'Logging VS Processing')
GO
INSERT [dbo].[CheckCategory] ([ID], [Name]) VALUES (9, N'Logging VS Processing in Proc DB')
GO
INSERT [dbo].[CheckCategory] ([ID], [Name]) VALUES (10, N'Processing DB Status')
GO
INSERT [dbo].[CheckData] ([ID], [CategoryID], [Name], [Description], [Query], [SubQuery]) VALUES (0, 0, N'CorrHeading Check', N'Check Heading count equals CorrHeading count', 0x0001000000FFFFFFFF010000000000000006010000004A53656C656374202A2046726F6D2048656164696E674C6F672057686572652048656164696E67206973204E4F54204E756C6C20414E4420436F727248656164696E67206973204E554C4C0B, NULL)
GO
INSERT [dbo].[CheckData] ([ID], [CategoryID], [Name], [Description], [Query], [SubQuery]) VALUES (1, 0, N'CorrPitch Check', N'Check Pitch count equals CorrPitch count', 0x0001000000FFFFFFFF010000000000000006010000004853656C656374202A2046726F6D205069746368526F6C6C4C6F67205768657265205069746368206973204E4F54204E756C6C20414E4420436F72725069746368206973204E554C4C0B, NULL)
GO
INSERT [dbo].[CheckData] ([ID], [CategoryID], [Name], [Description], [Query], [SubQuery]) VALUES (2, 0, N'CorrRoll Check', N'Check Roll count equals CorrRoll count', 0x0001000000FFFFFFFF010000000000000006010000004653656C656374202A2046726F6D205069746368526F6C6C4C6F6720576865726520526F6C6C206973204E4F54204E756C6C20414E4420436F7272526F6C6C206973204E554C4C0B, NULL)
GO
INSERT [dbo].[CheckData] ([ID], [CategoryID], [Name], [Description], [Query], [SubQuery]) VALUES (3, 1, N'KP Check', N'Confirms that there is no null KP records where the Discard is not flagged', 0x0001000000FFFFFFFF010000000000000006010000004B53656C656374202A2046726F6D20506F7350726F63205768657265204B50206973204E554C4C20414E44202844697363617264206973204E554C4C206F722044697363617264203D2030290B, NULL)
GO
INSERT [dbo].[CheckData] ([ID], [CategoryID], [Name], [Description], [Query], [SubQuery]) VALUES (4, 1, N'DesignKP Check', N'Confirms that there is no null DesignKP records where the Discard is not flagged', 0x0001000000FFFFFFFF010000000000000006010000005153656C656374202A2046726F6D20506F7350726F632057686572652044657369676E4B50206973204E554C4C20414E44202844697363617264206973204E554C4C206F722044697363617264203D2030290B, NULL)
GO
INSERT [dbo].[CheckData] ([ID], [CategoryID], [Name], [Description], [Query], [SubQuery]) VALUES (5, 1, N'DCC Check', N'Confirms that there is no null DCC records where the Discard is not flagged', 0x0001000000FFFFFFFF010000000000000006010000004C53656C656374202A2046726F6D20506F7350726F6320576865726520444343206973204E554C4C20414E44202844697363617264206973204E554C4C206F722044697363617264203D2030290B, NULL)
GO
INSERT [dbo].[CheckData] ([ID], [CategoryID], [Name], [Description], [Query], [SubQuery]) VALUES (6, 2, N'CorrDepth Check', N'Check DepthTide count equals CorrDepthTide count', 0x0001000000FFFFFFFF010000000000000006010000004C53656C656374202A2046726F6D2044657074684C6F6720576865726520446570746854696465206973204E4F54204E756C6C20414E4420436F7272446570746854696465206973204E554C4C0B, NULL)
GO
INSERT [dbo].[CheckData] ([ID], [CategoryID], [Name], [Description], [Query], [SubQuery]) VALUES (7, 3, N'KP Check', N'Confirms that there is no null KP records', 0x0001000000FFFFFFFF010000000000000006010000002753656C656374202A2046726F6D205363616E50726F63205768657265204B50206973204E554C4C0B, NULL)
GO
INSERT [dbo].[CheckData] ([ID], [CategoryID], [Name], [Description], [Query], [SubQuery]) VALUES (8, 3, N'Depth Check', N'Confirms that there is no null Depth records', 0x0001000000FFFFFFFF010000000000000006010000002A53656C656374202A2046726F6D205363616E50726F63205768657265204465707468206973204E554C4C0B, NULL)
GO
INSERT [dbo].[CheckData] ([ID], [CategoryID], [Name], [Description], [Query], [SubQuery]) VALUES (9, 3, N'Position Check', N'Confirms that there is no null East, North records', 0x0001000000FFFFFFFF010000000000000006010000003A53656C656374202A2046726F6D205363616E50726F632057686572652045617374206973204E554C4C204F52204E6F727468206973204E554C4C0B, NULL)
GO
INSERT [dbo].[CheckData] ([ID], [CategoryID], [Name], [Description], [Query], [SubQuery]) VALUES (10, 3, N'FProc Check', N'Confirms that the Fproc = FFProc', 0x0001000000FFFFFFFF010000000000000006010000002C53656C656374202A2046726F6D205363616E50726F63205768657265204670726F63203C3E20466670726F630B, NULL)
GO
INSERT [dbo].[CheckData] ([ID], [CategoryID], [Name], [Description], [Query], [SubQuery]) VALUES (11, 3, N'CorrPos Check', N'Confirms that there is no null CorrEast, CorrNorth records', 0x0001000000FFFFFFFF010000000000000006010000005453656C656374202A2046726F6D205363616E50726F632057686572652028436F727245617374206973204E554C4C204F5220436F72724E6F727468206973204E554C4C2920414E4420284670726F63203D2031290B, NULL)
GO
INSERT [dbo].[CheckData] ([ID], [CategoryID], [Name], [Description], [Query], [SubQuery]) VALUES (12, 3, N'PDY Check', N'Confirms that there is no null PipeDeltaY records', 0x0001000000FFFFFFFF010000000000000006010000003D53656C656374202A2046726F6D205363616E50726F63205768657265205069706544656C746159206973204E554C4C20414E44204650526F63203D20310B, NULL)
GO
INSERT [dbo].[CheckData] ([ID], [CategoryID], [Name], [Description], [Query], [SubQuery]) VALUES (13, 3, N'PDEx Check', N'Confirms that there is no PipeDeltaY records greater than + or -2m', 0x0001000000FFFFFFFF010000000000000006010000004053656C656374202A2046726F6D205363616E50726F63205768657265205069706544656C746159203E3D2032204F52205069706544656C746159203C3D202D320B, NULL)
GO
INSERT [dbo].[CheckData] ([ID], [CategoryID], [Name], [Description], [Query], [SubQuery]) VALUES (14, 4, N'KP Check', N'Confirms that there is no null KP records', 0x0001000000FFFFFFFF010000000000000006010000002B53656C656374202A2046726F6D2050547261636B657250726F63205768657265204B50206973204E554C4C0B, NULL)
GO
INSERT [dbo].[CheckData] ([ID], [CategoryID], [Name], [Description], [Query], [SubQuery]) VALUES (15, 4, N'Depth Check', N'Confirms that there is no null Depth records', 0x0001000000FFFFFFFF010000000000000006010000002E53656C656374202A2046726F6D2050547261636B657250726F63205768657265204465707468206973204E554C4C0B, NULL)
GO
INSERT [dbo].[CheckData] ([ID], [CategoryID], [Name], [Description], [Query], [SubQuery]) VALUES (16, 4, N'Position Check', N'Confirms that there is no null East, North records', 0x0001000000FFFFFFFF010000000000000006010000003E53656C656374202A2046726F6D2050547261636B657250726F632057686572652045617374206973204E554C4C204F52204E6F727468206973204E554C4C0B, NULL)
GO
INSERT [dbo].[CheckData] ([ID], [CategoryID], [Name], [Description], [Query], [SubQuery]) VALUES (17, 4, N'CorrPosition Check', N'Confirms that there is no null CorrEast, CorrNorth records', 0x0001000000FFFFFFFF010000000000000006010000004653656C656374202A2046726F6D2050547261636B657250726F6320576865726520436F727245617374206973204E554C4C204F5220436F72724E6F727468206973204E554C4C0B, NULL)
GO
INSERT [dbo].[CheckData] ([ID], [CategoryID], [Name], [Description], [Query], [SubQuery]) VALUES (18, 5, N'CorrPos Check', N'Confirms that there is no null East, North records', 0x0001000000FFFFFFFF010000000000000006010000003D53656C656374202A2046726F6D20436F7272506F7350726F632057686572652045617374206973204E554C4C204F52204E6F727468206973204E554C4C0B, NULL)
GO
INSERT [dbo].[CheckData] ([ID], [CategoryID], [Name], [Description], [Query], [SubQuery]) VALUES (19, 5, N'CorrPosPTrack Check', N'Confirms that there is no null Ptracker field', 0x0001000000FFFFFFFF010000000000000006010000003053656C656374202A2046726F6D20436F7272506F7350726F632057686572652050747261636B6572206973204E554C4C0B, NULL)
GO
INSERT [dbo].[CheckData] ([ID], [CategoryID], [Name], [Description], [Query], [SubQuery]) VALUES (20, 6, N'KP Check', N'Confirms that there is no null KP records', 0x0001000000FFFFFFFF010000000000000006010000002853656C656374202A2046726F6D204576656E7450726F63205768657265204B50206973204E554C4C0B, NULL)
GO
INSERT [dbo].[CheckData] ([ID], [CategoryID], [Name], [Description], [Query], [SubQuery]) VALUES (21, 6, N'Position Check', N'Confirms that there is no null East, North records', 0x0001000000FFFFFFFF010000000000000006010000003B53656C656374202A2046726F6D204576656E7450726F632057686572652045617374206973204E554C4C204F52204E6F727468206973204E554C4C0B, NULL)
GO
INSERT [dbo].[CheckData] ([ID], [CategoryID], [Name], [Description], [Query], [SubQuery]) VALUES (22, 6, N'KP Fix Check', N'Confirms that the KP = KP_Fixed if KP_Fixed is used', 0x0001000000FFFFFFFF010000000000000006010000004553656C656374202A2046726F6D204576656E7450726F63205768657265204B50203C3E204B505F466978656420414E44204B505F4669786564204953204E4F54204E554C4C0B, NULL)
GO
INSERT [dbo].[CheckData] ([ID], [CategoryID], [Name], [Description], [Query], [SubQuery]) VALUES (23, 6, N'DesignKP Check', N'Confirms that there is no null DesignKP records', 0x0001000000FFFFFFFF010000000000000006010000002E53656C656374202A2046726F6D204576656E7450726F632057686572652044657369676E4B50206973204E554C4C0B, NULL)
GO
INSERT [dbo].[CheckData] ([ID], [CategoryID], [Name], [Description], [Query], [SubQuery]) VALUES (24, 7, N'No two Freespan start Check', N'No two Freespan start after each other', 0x0001000000FFFFFFFF010000000000000006010000004C53454C454354202A2046524F4D204576656E7450726F632057484552452053636F6465203D204053436F6465204F522053636F6465203D204053436F646532204F52444552204259204B50200B, NULL)
GO
INSERT [dbo].[CheckData] ([ID], [CategoryID], [Name], [Description], [Query], [SubQuery]) VALUES (25, 7, N'No two Freespan end Check', N'No two Freespan end after each other', 0x0001000000FFFFFFFF010000000000000006010000004C53454C454354202A2046524F4D204576656E7450726F632057484552452053636F6465203D204053436F6465204F522053636F6465203D204053436F646532204F52444552204259204B50200B, NULL)
GO
INSERT [dbo].[CheckData] ([ID], [CategoryID], [Name], [Description], [Query], [SubQuery]) VALUES (26, 7, N'No Scour is in between a freespan Check', N'No Scour event is in between a freespan start and a free span end. (Freespan Start - Scour - Freespan end)', 0x0001000000FFFFFFFF010000000000000006010000004C53454C454354202A2046524F4D204576656E7450726F632057484552452053636F6465203D204053436F6465204F522053636F6465203D204053436F646532204F52444552204259204B50200B, 0x0001000000FFFFFFFF010000000000000006010000005653454C454354202A2046524F4D204576656E7450726F632057484552452053636F6465203D204053436F64653320414E44204B50203E2040634B5020414E44204B50203C20406E4B50204F52444552204259204B50200B)
GO
INSERT [dbo].[CheckData] ([ID], [CategoryID], [Name], [Description], [Query], [SubQuery]) VALUES (27, 7, N'No touch down out a freespan Check', N'No touch down event in an area that is not a freespan. (Freespan end – Touch down – Freespan start)', 0x0001000000FFFFFFFF010000000000000006010000004C53454C454354202A2046524F4D204576656E7450726F632057484552452053636F6465203D204053436F6465204F522053636F6465203D204053436F646532204F52444552204259204B50200B, 0x0001000000FFFFFFFF010000000000000006010000005653454C454354202A2046524F4D204576656E7450726F632057484552452053636F6465203D204053436F64653320414E44204B50203E2040634B5020414E44204B50203C20406E4B50204F52444552204259204B50200B)
GO
INSERT [dbo].[CheckData] ([ID], [CategoryID], [Name], [Description], [Query], [SubQuery]) VALUES (28, 7, N'No two burial start Check', N'No two burial start after each other', 0x0001000000FFFFFFFF010000000000000006010000004C53454C454354202A2046524F4D204576656E7450726F632057484552452053636F6465203D204053436F6465204F522053636F6465203D204053436F646532204F52444552204259204B50200B, NULL)
GO
INSERT [dbo].[CheckData] ([ID], [CategoryID], [Name], [Description], [Query], [SubQuery]) VALUES (29, 7, N'No two burial end Check', N'No two burial end after each other', 0x0001000000FFFFFFFF010000000000000006010000004C53454C454354202A2046524F4D204576656E7450726F632057484552452053636F6465203D204053436F6465204F522053636F6465203D204053436F646532204F52444552204259204B50200B, NULL)
GO
INSERT [dbo].[CheckData] ([ID], [CategoryID], [Name], [Description], [Query], [SubQuery]) VALUES (30, 8, N'Logging VS Processing DB', N'Confirm that there is no difference in row count between log & proc DB', 0x0001000000FFFFFFFF01000000000000000601000000E50F73656C656374202741756469745461626C6527206173205461626C654E616D652C20436F756E742841756469745461626C652E49442920617320436F756E742C204D41582841756469745461626C652E53746172744B5029206173204D61784B502C204D696E2841756469745461626C652E53746172744B5029206173204D696E4B502C204D41582841756469745461626C652E5265636F72644461746529206173204474696D652066726F6D2041756469745461626C6520756E696F6E20616C6C2073656C656374202743504C6F67272C20436F756E742843504C6F672E49442920617320436F756E742C204D41582843504C6F672E4B5029206173204D61784B502C204D494E2843504C6F672E4B5029206173204D696E4B502C204D41582843504C6F672E4454696D6529206173204474696D652066726F6D2043504C6F6720756E696F6E20616C6C2073656C656374202744657074684C6F67272C20436F756E742844657074684C6F672E49442920617320436F756E742C204E554C4C206173204D61784B502C204E554C4C206173204D696E4B502C204D41582844657074684C6F672E4454696D6529206173204474696D652066726F6D2044657074684C6F6720756E696F6E20616C6C2073656C6563742027446F70706C65724C6F67272C20436F756E7428446F70706C65724C6F672E49442920617320436F756E742C204E554C4C206173204D61784B502C204E554C4C206173204D696E4B502C204D415828446F70706C65724C6F672E4454696D6529206173204474696D652066726F6D20446F70706C65724C6F6720756E696F6E20616C6C2073656C65637420274576656E744C6F6727206173205461626C654E616D652C20436F756E74284576656E744C6F672E49442920617320436F756E742C204D4158284576656E744C6F672E4B5029206173204D61784B502C204D696E284576656E744C6F672E4B5029206173204D696E4B502C204D4158284576656E744C6F672E4454696D6529206173204474696D652066726F6D204576656E744C6F6720756E696F6E20616C6C2073656C656374202748656164696E674C6F6727206173205461626C654E616D652C20436F756E742848656164696E674C6F672E49442920617320436F756E742C204E554C4C206173204D61784B502C204E554C4C206173204D696E4B502C204D41582848656164696E674C6F672E4454696D6529206173204474696D652066726F6D2048656164696E674C6F6720756E696F6E20616C6C2073656C65637420275069746368526F6C6C4C6F6727206173205461626C654E616D652C20436F756E74285069746368526F6C6C4C6F672E49442920617320436F756E742C204E554C4C206173204D61784B502C204E554C4C206173204D696E4B502C204D4158285069746368526F6C6C4C6F672E4454696D6529206173204474696D652066726F6D205069746368526F6C6C4C6F6720756E696F6E20616C6C2073656C6563742027506F734C6F6727206173205461626C654E616D652C20436F756E7428506F734C6F672E49442920617320436F756E742C204D415828506F734C6F672E4B5029206173204D61784B502C204D696E28506F734C6F672E4B5029206173204D696E4B502C204D415828506F734C6F672E4454696D6529206173204474696D652066726F6D20506F734C6F6720756E696F6E20616C6C2073656C656374202750547261636B65724C6F6727206173205461626C654E616D652C20436F756E742850547261636B65724C6F672E49442920617320436F756E742C204E554C4C206173204D61784B502C204E554C4C206173204D696E4B502C204D41582850547261636B65724C6F672E4454696D6529206173204474696D652066726F6D2050547261636B65724C6F6720756E696F6E20616C6C2073656C656374202753424D5363616E4C6F6727206173205461626C654E616D652C20436F756E742853424D5363616E4C6F672E49442920617320436F756E742C204E554C4C206173204D61784B502C204E554C4C206173204D696E4B502C204D41582853424D5363616E4C6F672E4461746154696D6529206173204474696D652066726F6D2053424D5363616E4C6F6720756E696F6E20616C6C2073656C65637420275342535363616E4C6F6727206173205461626C654E616D652C20436F756E74285342535363616E4C6F672E49442920617320436F756E742C204E554C4C206173204D61784B502C204E554C4C206173204D696E4B502C204D4158285342535363616E4C6F672E4461746154696D6529206173204474696D652066726F6D205342535363616E4C6F6720756E696F6E20616C6C2073656C65637420275363616E4C6F6727206173205461626C654E616D652C20436F756E74285363616E4C6F672E49442920617320436F756E742C204E554C4C206173204D61784B502C204E554C4C206173204D696E4B502C204D4158285363616E4C6F672E4D617374657250726F6654696D6529206173204474696D652066726F6D205363616E4C6F6720756E696F6E20616C6C2073656C6563742027566964656F4C6F6727206173205461626C654E616D652C20436F756E7428566964656F4C6F672E49442920617320436F756E742C204D415828566964656F4C6F672E53746172744B5029206173204D61784B502C204D696E28566964656F4C6F672E53746172744B5029206173204D696E4B502C204D415828566964656F4C6F672E534454696D6529206173204474696D652066726F6D20566964656F4C6F670B, NULL)
GO
INSERT [dbo].[CheckData] ([ID], [CategoryID], [Name], [Description], [Query], [SubQuery]) VALUES (31, 9, N'Logging VS Processing DB status in proc DB', N'Logging VS Processing status', 0x0001000000FFFFFFFF010000000000000006010000009F0B73656C65637420274576656E7427206173205461626C654E616D652C20436F756E744C6F672C20436F756E7450726F632C204D61784B504C6F672C204D61784B5050726F632C204D696E4B504C6F672C204D696E4B5050726F632C204474696D654C6F672C204474696D6550726F632066726F6D202873656C656374202731272061732049442C20436F756E74284576656E744C6F672E49442920617320436F756E744C6F672C204D4158284576656E744C6F672E4B5029206173204D61784B504C6F672C204D696E284576656E744C6F672E4B5029206173204D696E4B504C6F672C204D4158284576656E744C6F672E4454696D6529206173204474696D654C6F672066726F6D204576656E744C6F6729206173207461626C653120696E6E6572206A6F696E202873656C656374202731272061732049442C20436F756E74284576656E7450726F632E49442920617320436F756E7450726F632C204D4158284576656E7450726F632E4B5029206173204D61784B5050726F632C204D696E284576656E7450726F632E4B5029206173204D696E4B5050726F632C204D4158284576656E7450726F632E4454696D6529206173204474696D6550726F632066726F6D204576656E7450726F6329206173207461626C6532206F6E207461626C65312E4944203D207461626C65322E494420756E696F6E20616C6C2073656C6563742027506F73272C20436F756E744C6F672C20436F756E7450726F632C204D61784B504C6F672C204D61784B5050726F632C204D696E4B504C6F672C204D696E4B5050726F632C204474696D654C6F672C204474696D6550726F632066726F6D202873656C656374202731272061732049442C20436F756E7428506F734C6F672E49442920617320436F756E744C6F672C204D415828506F734C6F672E4B5029206173204D61784B504C6F672C204D696E28506F734C6F672E4B5029206173204D696E4B504C6F672C204D415828506F734C6F672E4454696D6529206173204474696D654C6F672066726F6D20506F734C6F6729206173207461626C653120696E6E6572206A6F696E202873656C656374202731272061732049442C20436F756E7428506F7350726F632E49442920617320436F756E7450726F632C204D415828506F7350726F632E4B5029206173204D61784B5050726F632C204D494E28506F7350726F632E4B5029206173204D696E4B5050726F632C204D415828506F7350726F632E4454696D6529206173204474696D6550726F632066726F6D20506F7350726F6329206173207461626C6532206F6E207461626C65312E4944203D207461626C65322E494420756E696F6E20616C6C2073656C65637420275363616E272C20436F756E744C6F672C20436F756E7450726F632C204D61784B504C6F672C204D61784B5050726F632C204D696E4B504C6F672C204D696E4B5050726F632C204474696D654C6F672C204474696D6550726F632066726F6D202873656C656374202731272061732049442C436F756E74285363616E4C6F672E49442920617320436F756E744C6F672C204E554C4C206173204D61784B504C6F672C204E554C4C206173204D696E4B504C6F672C204D4158285363616E4C6F672E4D617374657250726F6654696D6529206173204474696D654C6F672066726F6D205363616E4C6F6729206173207461626C653120696E6E6572206A6F696E202873656C656374202731272061732049442C436F756E74285363616E50726F632E49442920617320436F756E7450726F632C204D4158285363616E50726F632E4B5029206173204D61784B5050726F632C204D494E285363616E50726F632E4B5029206173204D696E4B5050726F632C204D4158285363616E50726F632E4454696D6529206173204474696D6550726F632066726F6D205363616E50726F6329206173207461626C6532206F6E207461626C65312E4944203D207461626C65322E49440B, NULL)
GO
INSERT [dbo].[CheckData] ([ID], [CategoryID], [Name], [Description], [Query], [SubQuery]) VALUES (32, 10, N'Processing DB Status', N'Show Proc Status', 0x0001000000FFFFFFFF01000000000000000601000000DC0373656C65637420274576656E7450726F6327206173205461626C654E616D652C20436F756E74284576656E7450726F632E49442920617320436F756E742C204D4158284576656E7450726F632E4B5029206173204D61784B502C204D696E284576656E7450726F632E4B5029206173204D696E4B502C204D4158284576656E7450726F632E4454696D6529206173204D61784474696D652066726F6D204576656E7450726F6320756E696F6E20616C6C2073656C6563742027506F7350726F63272C20436F756E7428506F7350726F632E49442920617320436F756E742C204D415828506F7350726F632E4B5029206173204D61784B502C204D494E28506F7350726F632E4B5029206173204D696E4B502C204D415828506F7350726F632E4454696D6529206173204D61784474696D652066726F6D20506F7350726F6320756E696F6E20616C6C2073656C65637420275363616E50726F63272C20436F756E74285363616E50726F632E49442920617320436F756E742C204D4158285363616E50726F632E4B5029206173204D61784B502C204D494E285363616E50726F632E4B5029206173204D696E4B502C204D4158285363616E50726F632E4454696D6529206173204D61784474696D652066726F6D205363616E50726F630B, NULL)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (1, N'CopyDepthTideToCorrDepthTide', 1, 0, 0, 0, N'DTime', N'DepthAltKP', NULL, 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (2, N'CalcDepthTide', 1, 0, 0, 0, N'DTime', N'DepthAltKP', NULL, 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (3, N'CalcDepthTideAlt', 1, 0, 0, 0, N'DTime', N'DepthAltKP', NULL, 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (4, N'CalcDepthKPFromPosProc', 1, 0, 0, 0, N'DTime', N'DepthAltKP', NULL, 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (5, N'CopyHeadingToCorrHeading', 2, 0, 0, 0, N'DTime', NULL, NULL, 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (6, N'CopyPitchRollToCorrPitchRoll', 2, 0, 0, 0, N'DTime', NULL, NULL, 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (7, N'ExportFromPosLogToPosProc', 3, 0, 0, 0, N'DTime', N'Kp        ', NULL, 0)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (8, N'ConvertEastNorthtoLongitudeLatitude', 3, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (9, N'CalculatePosProcDesignKPDCCFromDesignPosProc', 3, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 0)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (10, N'CalculatePosProcDesignKPDCCSCALING', 3, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 0)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (11, N'CalculatePosProcOldKPDCCFromOldPosProc', 3, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 0)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (12, N'InterpolatePosProcKPFromDigitizedPipe', 3, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 0)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (13, N'InterpolatePosProcHeadingDevFromDigitizedPipe', 3, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 0)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (14, N'InterpolatePosProcHeadingDevKPFromDigitizedPipe', 3, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 0)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (15, N'InterpolatePosProcKPFromDesignPosProc', 3, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 0)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (16, N'InterpolatePosProcHeadingDevFromDesignPosProc', 3, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 0)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (17, N'InterpolatePosProcHeadingDevKPFromDesignPosProc', 3, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 0)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (18, N'ExportFromScanLogToScanProc', 4, 0, 0, 0, N'DTime', NULL, NULL, 0)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (19, N'CalcAverageSeabedDepth', 4, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (20, N'CalcScanProcPipeDiameter', 4, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (21, N'CorrectScanProcPosition', 4, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (22, N'CalculateScanProcDesignKPDCCFromDesignPosProc', 4, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (23, N'CalculateScanProcOldKPDCCFromOldPosProc', 4, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 0)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (24, N'CalculateScanProcDesignKPDCCSCALING', 4, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 0)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (25, N'ExportFromSBScanLogToScanProc', 4, 0, 0, 1, N'DTime', N'Kp        ', N'DesignKp', 0)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (26, N'ExportFromSB7125ScanLogToScanProc', 4, 0, 0, 1, N'DTime', N'Kp        ', N'DesignKp', 0)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (27, N'InterpolateScanProcKPFromPosProc', 4, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (28, N'InterpolateScanProcHeadingDevFromPosProc', 4, 1, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (29, N'InterpolateScanProcHeadingDevKPFromPosProc', 4, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (30, N'InterpolateScanProcKPFromDigitizedPipe', 4, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 0)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (31, N'InterpolateScanProcHeadingDevFromDigitizedPipe', 4, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 0)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (32, N'InterpolateScanProcHeadingDevKPFromDigitizedPipe', 4, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 0)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (33, N'InterpolateScanProcDepthFromDepthLog', 4, 1, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (34, N'InterpolateScanProcDepthFromDesignScanProc', 4, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (35, N'InterpolateScanProcHeading', 4, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (36, N'InterpolateScanProcPitch', 4, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (37, N'InterpolateScanProcRoll', 4, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (38, N'InterpolateScanProcHeadingPitchRoll', 4, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (39, N'InterpolateScanProcPositionFromPosProc', 4, 1, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (40, N'InterpolateScanProcPositionFromPosProc_DontShowPipe', 4, 1, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (41, N'InterpolateScanProcPositionFromDigitizedPipe', 4, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (42, N'InterpolateScanProcPositionFromDesignPosProc', 4, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (43, N'UpdateScanProcPipeDepthFromPTrackerProcPipeDepth', 4, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (44, N'UpdateScanProcDeltaYFromCorrPosProc', 4, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (45, N'UpdateScanProcDeltaYFromDigitizedPipe', 4, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 0)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (46, N'UpdateScanProcHeadEastHeadNorth', 4, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (47, N'ExportFromEventLogToEventProc', 6, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 0)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (48, N'GenerateEventsComments', 6, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (49, N'CalculateEventProcDesignKPDCCFromDesignPosProc', 6, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 0)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (50, N'CalculateEventProcOldKPDCCFromOldPosProc', 6, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 0)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (51, N'CalculateEventProcDesignKPDCCSCALING', 6, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 0)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (52, N'CalculateEventProcLocationStringDAMAGEDEBRI', 6, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 0)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (53, N'CalculateEventProcLocationStringAllEvents', 6, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 0)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (54, N'CalculateEventProcSurveyDirection', 6, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (55, N'InterpolateEventProcKPFromPosProc', 6, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (56, N'InterpolateEventProcDateTimeFromPosProc', 6, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (57, N'InterpolateEventProcDepthFromDepthLog', 6, 1, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (58, N'InterpolateEventProcDepthFromScanProc', 6, 1, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (59, N'InterpolateEventProcDepthFromScanProc_DesignKP', 6, 1, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (60, N'InterpolateEventProcDepthFromDesignScanProc', 6, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (61, N'InterpolateEventProcPositionFromPosProc', 6, 1, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (62, N'InterpolateEventProcPositionFromDigitizedPipe', 6, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (63, N'InterpolateEventProcPositionFromScanProc_Head', 6, 1, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (64, N'InterpolateEventProcPositionFromDesignPosProc', 6, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (65, N'InterpolateEventProcHeading_HeadingDevFromPosProc', 6, 1, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (66, N'InterpolateEventProcHeading_HeadingDevFromScanProc', 6, 1, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (67, N'ExportFromPTrackerLogToPTrackerProc', 7, 0, 0, 0, N'DTime', NULL, NULL, 0)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (68, N'CorrectPTrackerProcPosition', 7, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (69, N'CalculatePTrackerProcDesignKPDCCSCALING', 7, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 0)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (70, N'InterpolatePTrackerProcHeadingPitchRoll', 7, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (71, N'InterpolatePTrackerProcPitch', 7, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (72, N'InterpolatePTrackerProcRoll', 7, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (73, N'InterpolatePTrackerProcHeading', 7, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (74, N'InterpolatePTrackerProcHeadingDevFromDigitizedPipe', 7, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 0)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (75, N'InterpolatePTrackerProcHeadingDevFromPosProc', 7, 1, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (76, N'InterpolatePTrackerProcHeadingDevFromDesignPosProc', 7, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 0)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (77, N'InterpolatePTrackerProcHeadingDevFromOldPosProc', 7, 1, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (78, N'InterpolatePTrackerProcHeadingDevKPFromDigitizedPipe', 7, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 0)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (79, N'InterpolatePTrackerProcHeadingDevKPFromPosProc', 7, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (80, N'InterpolatePTrackerProcHeadingDevKPFromDesignPosProc', 7, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 0)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (81, N'InterpolatePTrackerProcHeadingDevKPFromOldPosProc', 7, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 0)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (82, N'InterpolatePTrackerProcKPFromDigitizedPipe', 7, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 0)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (83, N'InterpolatePTrackerProcKPFromPosProc', 7, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (84, N'InterpolatePTrackerProcKPFromDesignPosProc', 7, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 0)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (85, N'InterpolatePTrackerProcKPFromOldPosProc', 7, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 0)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (86, N'InterpolatePTrackerProcDepthFromDepthLog', 7, 1, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (87, N'InterpolatePTrackerProcDepthFromScanProc', 7, 1, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (88, N'InterpolatePTrackerProcPositionFromDesignPosProc', 7, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (89, N'InterpolatePTrackerProcPositionFromDigitizedPipe', 7, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (90, N'InterpolatePTrackerProcPositionFromPosProc', 7, 1, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (91, N'CalcPTrackerProcPipeDepthSeabedDepth', 7, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 0)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (92, N'CalcPTrackerProcPipeDepth', 7, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 0)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (93, N'CalcPTrackerProcPipeSeabedDepth', 7, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 0)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (94, N'UpdatePTrackerProcFProcFromScanProcPTProc', 7, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (95, N'InterpolateCPLogPositionFromDigitizedPipe', 8, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (96, N'InterpolateCPLogKPFromPosProc', 8, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (97, N'CalculateCPLogDesignKPFromDesignPosProc', 8, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 0)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (98, N'CopyScanProcPTrackerCorrPosition', 9, 0, 0, 0, N'DTime', N'Kp        ', NULL, 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (99, N'CalcFreeSpansLengthHeight', 9, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (100, N'CalcFreeSpansLengthHeight_DesignKP', 9, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (101, N'CalcBurialLengthDepth', 9, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (102, N'CalcBurialLengthDepth_DesignKP', 9, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (103, N'CalcAuditTableStartKPandEndKPFromPosProc', 9, 0, 0, 0, N'DTime', NULL, NULL, 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (104, N'CalcVideoLogStartKPandEndKPFromPosProc', 9, 0, 0, 0, N'DTime', NULL, NULL, 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (105, N'GeneratePipeListFromScanProc', 9, 0, 1, 0, N'DTime', N'Kp        ', N'DesignKp', 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (106, N'GeneratePipeListFromScanProc_DesignKP', 9, 0, 1, 0, N'DTime', N'Kp        ', N'DesignKp', 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (107, N'CalcFreeSpanTime', 9, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (108, N'FlagScanProcFPROC', 9, 0, 1, 0, N'DTime', N'Kp        ', N'DesignKp', 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (109, N'FlagScanProcFChart', 9, 0, 1, 0, N'DTime', N'Kp        ', N'DesignKp', 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (110, N'InterpolateDigitizedDepthFromDepthLog', 9, 1, 0, 0, NULL, N'Kp        ', NULL, 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (111, N'InterpolateCamerasHeadingPitchRoll', 5, 0, 0, 0, N'DTime', N'Kp        ', NULL, 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (112, N'InterpolateCamerasHeadingDevKPFromPosProc', 5, 0, 0, 0, N'DTime', N'Kp        ', NULL, 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (113, N'InterpolateCameraPositionFromPosProc', 5, 1, 0, 0, N'DTime', N'Kp        ', NULL, 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (114, N'InterpolateCamerasDepthFromDepthLog', 5, 0, 0, 0, N'DTime', N'Kp        ', NULL, 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (115, N'InterpolateEventProcPositionFromTruePipe', 5, 1, 0, 0, NULL, N'Kp        ', NULL, 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (116, N'InterpolateEventProcDepthFromTruePipe', 5, 1, 0, 0, N'DTime', N'Kp        ', N'DesignKp', 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (117, N'InterpolatePosProcHeading', 3, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKP', 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (118, N'InterpolatePosProcPitch', 3, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKP', 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (119, N'InterpolatePosProcRoll', 3, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKP', 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (120, N'InterpolatePosProcHeadingPitchRoll', 3, 0, 0, 0, N'DTime', N'Kp        ', N'DesignKP', 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (121, N'InterpolatePRCNavigationPitchRollHeading', 10, 0, 0, 0, N'DTime', N'KP        ', NULL, 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (122, N'InterpolatePRCNavigationHeadingDevKPFromPosProc', 10, 0, 0, 0, N'DTime', N'KP        ', NULL, 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (123, N'InterpolatePRCNavigationDepthFromDepthLog', 10, 0, 0, 0, N'DTime', N'KP        ', NULL, 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (124, N'InterpolatePosProcDepthFromDepthLog', 3, 1, 0, 0, N'DTime', N'KP        ', NULL, 1)
GO
INSERT [dbo].[StoredName] ([_ID], [StoredName], [StoredTypeID], [HasInterpolation], [HasExtraDoubleField], [HasExtraBooleanField], [DTimeName], [KpName], [DesignKpName], [IsAppendAllNulls]) VALUES (125, N'ExportFromEMScanLogToScanProc', 4, 0, 0, 0, NULL, NULL, NULL, 0)
GO
SET IDENTITY_INSERT [dbo].[StoredType] ON 

GO
INSERT [dbo].[StoredType] ([StoredTypeID], [StoredTypeName]) VALUES (1, N'DepthLog')
GO
INSERT [dbo].[StoredType] ([StoredTypeID], [StoredTypeName]) VALUES (2, N'Heading / Pitch / Roll Log')
GO
INSERT [dbo].[StoredType] ([StoredTypeID], [StoredTypeName]) VALUES (3, N'PosProc')
GO
INSERT [dbo].[StoredType] ([StoredTypeID], [StoredTypeName]) VALUES (4, N'ScanProc')
GO
INSERT [dbo].[StoredType] ([StoredTypeID], [StoredTypeName]) VALUES (5, N'Patch Processing')
GO
INSERT [dbo].[StoredType] ([StoredTypeID], [StoredTypeName]) VALUES (6, N'EventProc')
GO
INSERT [dbo].[StoredType] ([StoredTypeID], [StoredTypeName]) VALUES (7, N'PTracker')
GO
INSERT [dbo].[StoredType] ([StoredTypeID], [StoredTypeName]) VALUES (8, N'CPLog')
GO
INSERT [dbo].[StoredType] ([StoredTypeID], [StoredTypeName]) VALUES (9, N'Misc.')
GO
INSERT [dbo].[StoredType] ([StoredTypeID], [StoredTypeName]) VALUES (10, N'PRCNavigationDetails')
GO
SET IDENTITY_INSERT [dbo].[StoredType] OFF
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_BatchesDetails_IsCameraNeglected]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[BatchesDetails] ADD  CONSTRAINT [DF_BatchesDetails_IsCameraNeglected]  DEFAULT ((0)) FOR [IsCameraNeglected]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_Clusters_IsFailed]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Clusters] ADD  CONSTRAINT [DF_Clusters_IsFailed]  DEFAULT ((0)) FOR [IsFailed]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_Clusters_IsPRC]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Clusters] ADD  CONSTRAINT [DF_Clusters_IsPRC]  DEFAULT ((1)) FOR [IsPRC]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_PCode_PShortcut]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[PCode] ADD  CONSTRAINT [DF_PCode_PShortcut]  DEFAULT ((0)) FOR [PShortcut]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_ScanProc_IsPRC]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ScanProc] ADD  CONSTRAINT [DF_ScanProc_IsPRC]  DEFAULT ((0)) FOR [IsPRC]
END

GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_StoredName_StoredType]') AND parent_object_id = OBJECT_ID(N'[dbo].[StoredName]'))
ALTER TABLE [dbo].[StoredName]  WITH CHECK ADD  CONSTRAINT [FK_StoredName_StoredType] FOREIGN KEY([StoredTypeID])
REFERENCES [dbo].[StoredType] ([StoredTypeID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_StoredName_StoredType]') AND parent_object_id = OBJECT_ID(N'[dbo].[StoredName]'))
ALTER TABLE [dbo].[StoredName] CHECK CONSTRAINT [FK_StoredName_StoredType]
GO
/****** Object:  StoredProcedure [dbo].[CalcAuditTableStartKPandEndKPFromPosProc]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CalcAuditTableStartKPandEndKPFromPosProc]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[CalcAuditTableStartKPandEndKPFromPosProc] AS' 
END
GO

ALTER PROCEDURE [dbo].[CalcAuditTableStartKPandEndKPFromPosProc]
	@AuditData StartEndKPType readonly
AS
BEGIN


	UPDATE AuditTable SET  AuditTable.StartKP = UpdatedAudit.StartKP, AuditTable.EndKP = UpdatedAudit.EndKP
	FROM  @AuditData AS UpdatedAudit
	Where AuditTable.ID = UpdatedAudit.ID
END


GO
/****** Object:  StoredProcedure [dbo].[CalcAverageSeabedDepth]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CalcAverageSeabedDepth]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[CalcAverageSeabedDepth] AS' 
END
GO

ALTER PROCEDURE [dbo].[CalcAverageSeabedDepth]
	@ScanProcData DepthType readonly
AS
BEGIN


	UPDATE ScanProc SET  ScanProc.AverageSeabedDepth = UpdatedScanProc.Depth
	FROM  @ScanProcData AS UpdatedScanProc
	Where ScanProc.ID = UpdatedScanProc.ID
END


GO
/****** Object:  StoredProcedure [dbo].[CalcBurialLengthDepth]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CalcBurialLengthDepth]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[CalcBurialLengthDepth] AS' 
END
GO

ALTER PROCEDURE [dbo].[CalcBurialLengthDepth]
	@EventProcData EventProcLengthHeightType readonly
AS
BEGIN


	UPDATE EventProc SET  EventProc.Length = UpdatedEventProc.Length, EventProc.BurialDepth = UpdatedEventProc.Height
	FROM  @EventProcData AS UpdatedEventProc
	Where EventProc.ID = UpdatedEventProc.ID
END


GO
/****** Object:  StoredProcedure [dbo].[CalcBurialLengthDepth_DesignKP]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CalcBurialLengthDepth_DesignKP]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[CalcBurialLengthDepth_DesignKP] AS' 
END
GO

ALTER PROCEDURE [dbo].[CalcBurialLengthDepth_DesignKP]
	@EventProcData EventProcLengthHeightType readonly
AS
BEGIN


	UPDATE EventProc SET  EventProc.Length = UpdatedEventProc.Length, EventProc.BurialDepth = UpdatedEventProc.Height
	FROM  @EventProcData AS UpdatedEventProc
	Where EventProc.ID = UpdatedEventProc.ID
END


GO
/****** Object:  StoredProcedure [dbo].[CalcDepthKPFromPosProc]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CalcDepthKPFromPosProc]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[CalcDepthKPFromPosProc] AS' 
END
GO

ALTER PROCEDURE [dbo].[CalcDepthKPFromPosProc]
	@DepthData DepthKPType readonly
AS
BEGIN


	UPDATE DepthLog SET DepthLog.DepthAltKP = UpdatedDepth.DepthAltKP FROM  @DepthData AS [UpdatedDepth] 
	Where DepthLog.ID = UpdatedDepth.ID
END


GO
/****** Object:  StoredProcedure [dbo].[CalcDepthTide]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CalcDepthTide]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[CalcDepthTide] AS' 
END
GO

ALTER PROCEDURE [dbo].[CalcDepthTide]
	@DepthData DepthType readonly
AS
BEGIN


	UPDATE DepthLog SET DepthLog.DepthTide = UpdatedDepth.Depth FROM  @DepthData AS [UpdatedDepth] 
	Where DepthLog.ID = UpdatedDepth.ID
END



GO
/****** Object:  StoredProcedure [dbo].[CalcDepthTideAlt]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CalcDepthTideAlt]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[CalcDepthTideAlt] AS' 
END
GO

ALTER PROCEDURE [dbo].[CalcDepthTideAlt]
	@DepthData DepthType readonly
AS
BEGIN


	UPDATE DepthLog SET DepthLog.DepthTideAlt = UpdatedDepth.Depth FROM  @DepthData AS [UpdatedDepth] 
	Where DepthLog.ID = UpdatedDepth.ID
END



GO
/****** Object:  StoredProcedure [dbo].[CalcFreeSpansLengthHeight]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CalcFreeSpansLengthHeight]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[CalcFreeSpansLengthHeight] AS' 
END
GO

ALTER PROCEDURE [dbo].[CalcFreeSpansLengthHeight]
	@EventProcData EventProcLengthHeightType readonly
AS
BEGIN


	UPDATE EventProc SET  EventProc.Length = UpdatedEventProc.Length, EventProc.Height = UpdatedEventProc.Height
	FROM  @EventProcData AS UpdatedEventProc
	Where EventProc.ID = UpdatedEventProc.ID
END



GO
/****** Object:  StoredProcedure [dbo].[CalcFreeSpansLengthHeight_DesignKP]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CalcFreeSpansLengthHeight_DesignKP]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[CalcFreeSpansLengthHeight_DesignKP] AS' 
END
GO

ALTER PROCEDURE [dbo].[CalcFreeSpansLengthHeight_DesignKP]
	@EventProcData EventProcLengthHeightType readonly
AS
BEGIN


	UPDATE EventProc SET  EventProc.Length = UpdatedEventProc.Length, EventProc.Height = UpdatedEventProc.Height
	FROM  @EventProcData AS UpdatedEventProc
	Where EventProc.ID = UpdatedEventProc.ID
END



GO
/****** Object:  StoredProcedure [dbo].[CalcFreeSpanTime]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CalcFreeSpanTime]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[CalcFreeSpanTime] AS' 
END
GO

ALTER PROCEDURE [dbo].[CalcFreeSpanTime]
	@EventProcData DateTimeType readonly
AS
BEGIN


	UPDATE EventProc SET  EventProc.DTime = UpdatedEventProc.DTime
	FROM  @EventProcData AS UpdatedEventProc
	Where EventProc.ID = UpdatedEventProc.ID
END



GO
/****** Object:  StoredProcedure [dbo].[CalcPTrackerProcPipeDepth]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CalcPTrackerProcPipeDepth]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[CalcPTrackerProcPipeDepth] AS' 
END
GO

ALTER PROCEDURE [dbo].[CalcPTrackerProcPipeDepth]
	@PTrackerData DepthType readonly
AS
BEGIN


	UPDATE PTrackerProc SET PTrackerProc.PipeDepth = UpdatedData.Depth
						
	                                
	FROM  @PTrackerData AS UpdatedData
	Where PTrackerProc.ID = UpdatedData.ID 
END



GO
/****** Object:  StoredProcedure [dbo].[CalcPTrackerProcPipeDepthSeabedDepth]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CalcPTrackerProcPipeDepthSeabedDepth]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[CalcPTrackerProcPipeDepthSeabedDepth] AS' 
END
GO

ALTER PROCEDURE [dbo].[CalcPTrackerProcPipeDepthSeabedDepth]
	@PTrackerData SeaPipeType readonly
AS
BEGIN


	UPDATE PTrackerProc SET PTrackerProc.PipeDepth = UpdatedData.PipeDepth, PTrackerProc.SeabedDepth = UpdatedData.SeabedDepth
						
	                                
	FROM  @PTrackerData AS UpdatedData
	Where PTrackerProc.ID = UpdatedData.ID 
END



GO
/****** Object:  StoredProcedure [dbo].[CalcPTrackerProcPipeSeabedDepth]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CalcPTrackerProcPipeSeabedDepth]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[CalcPTrackerProcPipeSeabedDepth] AS' 
END
GO

ALTER PROCEDURE [dbo].[CalcPTrackerProcPipeSeabedDepth]
	@PTrackerData DepthType readonly
AS
BEGIN


	UPDATE PTrackerProc SET PTrackerProc.SeabedDepth = UpdatedData.Depth
						
	                                
	FROM  @PTrackerData AS UpdatedData
	Where PTrackerProc.ID = UpdatedData.ID 
END



GO
/****** Object:  StoredProcedure [dbo].[CalcScanProcPipeDiameter]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CalcScanProcPipeDiameter]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[CalcScanProcPipeDiameter] AS' 
END
GO

ALTER PROCEDURE [dbo].[CalcScanProcPipeDiameter]
	@ScanData PipeDiameterType readonly
AS
BEGIN


	UPDATE ScanProc SET ScanProc.PipeDiameter = UpdatedScan.PipeDiameter FROM  @ScanData AS UpdatedScan
	Where ScanProc.ID = UpdatedScan.ID
END



GO
/****** Object:  StoredProcedure [dbo].[CalculateCPLogDesignKPFromDesignPosProc]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CalculateCPLogDesignKPFromDesignPosProc]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[CalculateCPLogDesignKPFromDesignPosProc] AS' 
END
GO

ALTER PROCEDURE [dbo].[CalculateCPLogDesignKPFromDesignPosProc]
	@CPLogData KpDataType readonly
AS
BEGIN


	UPDATE CPLog SET  CPLog.DesignKp = UpdatedData.Kp
	FROM  @CPLogData AS UpdatedData
	Where CPLog.ID = UpdatedData.ID 
END



GO
/****** Object:  StoredProcedure [dbo].[CalculateEventProcDesignKPDCCFromDesignPosProc]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CalculateEventProcDesignKPDCCFromDesignPosProc]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[CalculateEventProcDesignKPDCCFromDesignPosProc] AS' 
END
GO

ALTER PROCEDURE [dbo].[CalculateEventProcDesignKPDCCFromDesignPosProc]
	@EventProcData KpDccType readonly
AS
BEGIN


	UPDATE EventProc SET  EventProc.DesignDCC = UpdatedData.DCC, EventProc.DesignKp = UpdatedData.Kp
	FROM  @EventProcData AS UpdatedData
	Where EventProc.ID = UpdatedData.ID
END



GO
/****** Object:  StoredProcedure [dbo].[CalculateEventProcDesignKPDCCSCALING]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CalculateEventProcDesignKPDCCSCALING]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[CalculateEventProcDesignKPDCCSCALING] AS' 
END
GO

ALTER PROCEDURE [dbo].[CalculateEventProcDesignKPDCCSCALING]
	@EventProcData KpDccType readonly
AS
BEGIN


	UPDATE EventProc SET  EventProc.DesignKp = UpdatedData.Kp, EventProc.DesignDCC = UpdatedData.DCC
	FROM  @EventProcData AS UpdatedData
	Where EventProc.ID = UpdatedData.ID 
END



GO
/****** Object:  StoredProcedure [dbo].[CalculateEventProcLocationStringAllEvents]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CalculateEventProcLocationStringAllEvents]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[CalculateEventProcLocationStringAllEvents] AS' 
END
GO

ALTER PROCEDURE [dbo].[CalculateEventProcLocationStringAllEvents]
	@EventProcData LocationType readonly
AS
BEGIN


	UPDATE EventProc SET  EventProc.LocNum = UpdatedData.Location
	FROM  @EventProcData AS UpdatedData
	Where EventProc.ID = UpdatedData.ID 
END




GO
/****** Object:  StoredProcedure [dbo].[CalculateEventProcLocationStringDAMAGEDEBRI]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CalculateEventProcLocationStringDAMAGEDEBRI]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[CalculateEventProcLocationStringDAMAGEDEBRI] AS' 
END
GO

ALTER PROCEDURE [dbo].[CalculateEventProcLocationStringDAMAGEDEBRI]
	@EventProcData LocationType readonly
AS
BEGIN


	UPDATE EventProc SET  EventProc.Loc = UpdatedData.Location
	FROM  @EventProcData AS UpdatedData
	Where EventProc.ID = UpdatedData.ID 
END




GO
/****** Object:  StoredProcedure [dbo].[CalculateEventProcOldKPDCCFromOldPosProc]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CalculateEventProcOldKPDCCFromOldPosProc]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[CalculateEventProcOldKPDCCFromOldPosProc] AS' 
END
GO

ALTER PROCEDURE [dbo].[CalculateEventProcOldKPDCCFromOldPosProc]
	@EventProcData KpDccType readonly
AS
BEGIN


	UPDATE EventProc SET  EventProc.OldKP = UpdatedData.kp, EventProc.OLdDCC = UpdatedData.DCC
	FROM  @EventProcData AS UpdatedData
	Where EventProc.ID = UpdatedData.ID
END



GO
/****** Object:  StoredProcedure [dbo].[CalculateEventProcSurveyDirection]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CalculateEventProcSurveyDirection]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[CalculateEventProcSurveyDirection] AS' 
END
GO

ALTER PROCEDURE [dbo].[CalculateEventProcSurveyDirection]
	@EventProcData SurveyDirType readonly
AS
BEGIN


	UPDATE EventProc SET  EventProc.SurveyDir = UpdatedData.SurveyDir
	FROM  @EventProcData AS UpdatedData
	Where EventProc.ID = UpdatedData.ID 
END



GO
/****** Object:  StoredProcedure [dbo].[CalculatePosProcDesignKPDCCFromDesignPosProc]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CalculatePosProcDesignKPDCCFromDesignPosProc]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[CalculatePosProcDesignKPDCCFromDesignPosProc] AS' 
END
GO

ALTER PROCEDURE [dbo].[CalculatePosProcDesignKPDCCFromDesignPosProc]
	@PosProcData KpDccType readonly
AS
BEGIN


	UPDATE PosProc SET  PosProc.DesignDCC = UpdatedPosProc.DCC, PosProc.DesignKp = UpdatedPosProc.Kp
	FROM  @PosProcData AS UpdatedPosProc
	Where PosProc.ID = UpdatedPosProc.ID
END


GO
/****** Object:  StoredProcedure [dbo].[CalculatePosProcDesignKPDCCSCALING]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CalculatePosProcDesignKPDCCSCALING]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[CalculatePosProcDesignKPDCCSCALING] AS' 
END
GO

ALTER PROCEDURE [dbo].[CalculatePosProcDesignKPDCCSCALING]
	@PosProcData KpDccType readonly
AS
BEGIN


	UPDATE PosProc SET  PosProc.DesignKp = UpdatedData.Kp, PosProc.DesignDCC = UpdatedData.DCC
	FROM  @PosProcData AS UpdatedData
	Where PosProc.ID = UpdatedData.ID 
END


GO
/****** Object:  StoredProcedure [dbo].[CalculatePosProcOldKPDCCFromOldPosProc]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CalculatePosProcOldKPDCCFromOldPosProc]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[CalculatePosProcOldKPDCCFromOldPosProc] AS' 
END
GO

ALTER PROCEDURE [dbo].[CalculatePosProcOldKPDCCFromOldPosProc]
	@PosProcData KpDccType readonly
AS
BEGIN


	UPDATE PosProc SET  PosProc.OldKP = UpdatedData.kp, PosProc.OLdDCC = UpdatedData.DCC
	FROM  @PosProcData AS UpdatedData
	Where PosProc.ID = UpdatedData.ID
END


GO
/****** Object:  StoredProcedure [dbo].[CalculatePTrackerProcDesignKPDCCSCALING]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CalculatePTrackerProcDesignKPDCCSCALING]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[CalculatePTrackerProcDesignKPDCCSCALING] AS' 
END
GO

ALTER PROCEDURE [dbo].[CalculatePTrackerProcDesignKPDCCSCALING]
	@PTrackerData KpDccType readonly
AS
BEGIN


	UPDATE PTrackerProc SET  PTrackerProc.DesignKp = UpdatedData.Kp, PTrackerProc.DesignDCC = UpdatedData.DCC
	FROM  @PTrackerData AS UpdatedData
	Where PTrackerProc.ID = UpdatedData.ID 
END


GO
/****** Object:  StoredProcedure [dbo].[CalculateScanProcDesignKPDCCFromDesignPosProc]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CalculateScanProcDesignKPDCCFromDesignPosProc]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[CalculateScanProcDesignKPDCCFromDesignPosProc] AS' 
END
GO

ALTER PROCEDURE [dbo].[CalculateScanProcDesignKPDCCFromDesignPosProc]
	@ScanProcData KpDccType readonly
AS
BEGIN


	UPDATE ScanProc SET  ScanProc.DesignDCC = UpdatedScanProc.DCC, ScanProc.DesignKp = UpdatedScanProc.Kp
	FROM  @ScanProcData AS UpdatedScanProc
	Where ScanProc.ID = UpdatedScanProc.ID
END



GO
/****** Object:  StoredProcedure [dbo].[CalculateScanProcDesignKPDCCSCALING]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CalculateScanProcDesignKPDCCSCALING]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[CalculateScanProcDesignKPDCCSCALING] AS' 
END
GO

ALTER PROCEDURE [dbo].[CalculateScanProcDesignKPDCCSCALING]
	@ScanProcData KpDccType readonly
AS
BEGIN


	UPDATE ScanProc SET  ScanProc.DesignKp = UpdatedData.Kp, ScanProc.DesignDCC = UpdatedData.DCC
	FROM  @ScanProcData AS UpdatedData
	Where ScanProc.ID = UpdatedData.ID 
END


GO
/****** Object:  StoredProcedure [dbo].[CalculateScanProcOldKPDCCFromOldPosProc]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CalculateScanProcOldKPDCCFromOldPosProc]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[CalculateScanProcOldKPDCCFromOldPosProc] AS' 
END
GO

ALTER PROCEDURE [dbo].[CalculateScanProcOldKPDCCFromOldPosProc]
	@ScanProcData KpDccType readonly
AS
BEGIN


	UPDATE ScanProc SET  ScanProc.OldKP = UpdatedData.kp, ScanProc.OLdDCC = UpdatedData.DCC
	FROM  @ScanProcData AS UpdatedData
	Where ScanProc.ID = UpdatedData.ID
END


GO
/****** Object:  StoredProcedure [dbo].[CalcVideoLogStartKPandEndKPFromPosProc]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CalcVideoLogStartKPandEndKPFromPosProc]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[CalcVideoLogStartKPandEndKPFromPosProc] AS' 
END
GO

ALTER PROCEDURE [dbo].[CalcVideoLogStartKPandEndKPFromPosProc]
	@VideoData StartEndKPType readonly
AS
BEGIN


	UPDATE VideoLog SET  VideoLog.StartKP = UpdatedVideo.StartKP, VideoLog.EndKP = UpdatedVideo.EndKP
	FROM  @VideoData AS UpdatedVideo
	Where VideoLog.ID = UpdatedVideo.ID
END


GO
/****** Object:  StoredProcedure [dbo].[ConvertEastNorthtoLongitudeLatitude]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ConvertEastNorthtoLongitudeLatitude]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[ConvertEastNorthtoLongitudeLatitude] AS' 
END
GO

ALTER PROCEDURE [dbo].[ConvertEastNorthtoLongitudeLatitude]
	@PosProcData LongLATType readonly
AS
BEGIN


	UPDATE PosProc SET  PosProc.Long = UpdatedData.Long, PosProc.LAT = UpdatedData.LAT
	FROM  @PosProcData AS UpdatedData
	Where PosProc.ID = UpdatedData.ID 
END



GO
/****** Object:  StoredProcedure [dbo].[CopyDepthTideToCorrDepthTide]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CopyDepthTideToCorrDepthTide]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[CopyDepthTideToCorrDepthTide] AS' 
END
GO


ALTER PROCEDURE [dbo].[CopyDepthTideToCorrDepthTide]
	@DepthData DepthType readonly
AS
BEGIN
	UPDATE DepthLog 
	SET DepthLog.CorrDepthTide = UpdatedDepth.Depth
	FROM  @DepthData AS [UpdatedDepth] 
	Where DepthLog.ID = UpdatedDepth.ID
END


GO
/****** Object:  StoredProcedure [dbo].[CorrectPTrackerProcPosition]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CorrectPTrackerProcPosition]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[CorrectPTrackerProcPosition] AS' 
END
GO

ALTER PROCEDURE [dbo].[CorrectPTrackerProcPosition]
	@PTrackerData EastNorthType readonly
AS
BEGIN


	UPDATE PTrackerProc SET  PTrackerProc.CorrEast = UpdatedPTrackerProc.East, PTrackerProc.CorrNorth = UpdatedPTrackerProc.North
	FROM  @PTrackerData AS UpdatedPTrackerProc
	Where PTrackerProc.ID = UpdatedPTrackerProc.ID
END


GO
/****** Object:  StoredProcedure [dbo].[CorrectScanProcPosition]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CorrectScanProcPosition]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[CorrectScanProcPosition] AS' 
END
GO

ALTER PROCEDURE [dbo].[CorrectScanProcPosition]
	@ScanProcData EastNorthType readonly
AS
BEGIN


	UPDATE ScanProc SET  ScanProc.CorrEast = UpdatedScanProc.East, ScanProc.CorrNorth = UpdatedScanProc.North
	FROM  @ScanProcData AS UpdatedScanProc
	Where ScanProc.ID = UpdatedScanProc.ID
END


GO
/****** Object:  StoredProcedure [dbo].[DATA_DesignEventProc_Insert]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DATA_DesignEventProc_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[DATA_DesignEventProc_Insert] AS' 
END
GO

ALTER PROCEDURE [dbo].[DATA_DesignEventProc_Insert]
	@TableValue AS [IMP_DesignEventProcType] READONLY
AS
BEGIN
	SET DATEFORMAT YMD;
	BEGIN TRANSACTION Trans
	BEGIN TRY
    	DELETE FROM [DesignEventProc] 

		SET IDENTITY_INSERT [DesignEventProc] ON	

		INSERT INTO dbo.DesignEventProc
		        ( ID ,
		       EventNum ,
		       DTime ,
		       PCode ,
		       SCode ,
		       KP ,
		       East ,
		       North ,
		       PipeDepth ,
		       SeabedDepth ,
		       BurialDepth ,
		       Length ,
		       Height ,
		       Width ,
		       CP ,
		       ProximityCP ,
		       Heading ,
		       HeadingDev ,
		       CPFieldGradiant ,
		       Comment ,
		       Depth ,
		       ROVID ,
		       Active ,
		       Depletion ,
		       DepletionValue ,
		       Angle ,
		       ClockNotation ,
		       Distance ,
		       DistanceNotation ,
		       Contact ,
		       FieldJointNumber ,
		       CrossingName ,
		       CrossingClearance ,
		       CPReading ,
		       Anomaly ,
		       Discard ,
		       CustomComment ,
		       PiggyBackValue ,
		       Reviewed ,
		       EventQC ,
		       KP_Fixed ,
		       DesignKp ,
		       OldKP ,
		       DesignDCC ,
		       OldDCC ,
		       DCC ,
		       AdjSeabedDep ,
		       MeanSeabedDep ,
		       North_Fixed ,
		       East_Fixed ,
		       Loc ,
		       SurveyDir ,
		       LocNum ,
		       AnomalyNumber ,
		       AnomalyUniqueNumber ,
		       AnomalyCode ,
		       AnomalyClass ,
		       HourLocation ,
		       OldEvent ,
		       Dive_Number ,
		       AnodeSecure ,
		       MG_Coverage ,
		       MG_Thickness ,
		       PercentageBurial
		        )
		SELECT ID ,
		       EventNum ,
		       DTime ,
		       PCode ,
		       SCode ,
		       KP ,
		       East ,
		       North ,
		       PipeDepth ,
		       SeabedDepth ,
		       BurialDepth ,
		       Length ,
		       Height ,
		       Width ,
		       CP ,
		       ProximityCP ,
		       Heading ,
		       HeadingDev ,
		       CPFieldGradiant ,
		       Comment ,
		       Depth ,
		       ROVID ,
		       Active ,
		       Depletion ,
		       DepletionValue ,
		       Angle ,
		       ClockNotation ,
		       Distance ,
		       DistanceNotation ,
		       Contact ,
		       FieldJointNumber ,
		       CrossingName ,
		       CrossingClearance ,
		       CPReading ,
		       Anomaly ,
		       Discard ,
		       CustomComment ,
		       PiggyBackValue ,
		       Reviewed ,
		       EventQC ,
		       KP_Fixed ,
		       DesignKp ,
		       OldKP ,
		       DesignDCC ,
		       OldDCC ,
		       DCC ,
		       AdjSeabedDep ,
		       MeanSeabedDep ,
		       North_Fixed ,
		       East_Fixed ,
		       Loc ,
		       SurveyDir ,
		       LocNum ,
		       AnomalyNumber ,
		       AnomalyUniqueNumber ,
		       AnomalyCode ,
		       AnomalyClass ,
		       HourLocation ,
		       OldEvent ,
		       Dive_Number ,
		       AnodeSecure ,
		       MG_Coverage ,
		       MG_Thickness ,
		       PercentageBurial
			FROM @TableValue 

		SET IDENTITY_INSERT [DesignEventProc] OFF
		   
		COMMIT TRANSACTION Trans
	END TRY
    BEGIN CATCH
		ROLLBACK TRANSACTION Trans
	END CATCH
END



GO
/****** Object:  StoredProcedure [dbo].[DATA_DesignPosProc_Insert]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DATA_DesignPosProc_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[DATA_DesignPosProc_Insert] AS' 
END
GO

ALTER PROCEDURE [dbo].[DATA_DesignPosProc_Insert]
	@TableValue AS [IMP_DesignPosProcType] READONLY 
AS
BEGIN
	SET DATEFORMAT YMD;
	BEGIN TRANSACTION Trans
	BEGIN TRY
    	DELETE FROM [DesignPosProc]
			
		SET IDENTITY_INSERT [DesignPosProc] ON

		INSERT INTO dbo.DesignPosProc
		        ( ID ,
				  DTime ,
				  FixNum ,
				  North ,
				  East ,
				  Kp ,
				  Depth ,
				  Heading ,
				  Pitch ,
				  Roll ,
				  Logged ,
				  HeadingDev ,
				  Discard ,
				  Digitized ,
				  ROVID ,
				  DesignKp ,
				  OldKP ,
				  DesignDCC ,
				  OldDCC ,
				  [De-spiked] ,
				  DCC ,
				  Long ,
				  LAT
		        )
		SELECT ID ,
				  DTime ,
				  FixNum ,
				  North ,
				  East ,
				  Kp ,
				  Depth ,
				  Heading ,
				  Pitch ,
				  Roll ,
				  Logged ,
				  HeadingDev ,
				  Discard ,
				  Digitized ,
				  ROVID ,
				  DesignKp ,
				  OldKP ,
				  DesignDCC ,
				  OldDCC ,
				  [De-spiked] ,
				  DCC ,
				  Long ,
				  LAT
			FROM @TableValue 

		SET IDENTITY_INSERT [DesignPosProc] OFF
		  
		COMMIT TRANSACTION Trans
	END TRY
    BEGIN CATCH
		ROLLBACK TRANSACTION Trans
	END CATCH
END



GO
/****** Object:  StoredProcedure [dbo].[DATA_DesignScanProc_Insert]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DATA_DesignScanProc_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[DATA_DesignScanProc_Insert] AS' 
END
GO

ALTER PROCEDURE [dbo].[DATA_DesignScanProc_Insert]
	@TableValue AS [IMP_DesignScanProcType] READONLY 
AS
BEGIN
	SET DATEFORMAT YMD;
	BEGIN TRANSACTION Trans
	BEGIN TRY
    	DELETE FROM [DesignScanProc]

		SET IDENTITY_INSERT [DesignScanProc] ON

		INSERT INTO dbo.DesignScanProc
		        ( ID ,
	              DTime ,
	              ScanNum ,
	              East ,
	              North ,
	              KP ,
	              Pitch ,
	              Roll ,
	              Heading ,
	              MasterNumScans ,
	              SlaveNumScans ,
	              MasterAngStart ,
	              SlaveAngStart ,
	              MasterAngStep ,
	              SlaveAngStep ,
	              PipeDepth ,
	              SeaBedDep ,
	              PipeDeltaY ,
	              ScanData1 ,
	              DeltaX1 ,
	              DeltaY1 ,
	              DeltaZ1 ,
	              RX1 ,
	              RY1 ,
	              RZ1 ,
	              ScanData2 ,
	              DeltaX2 ,
	              DeltaY2 ,
	              DeltaZ2 ,
	              RX2 ,
	              RY2 ,
	              RZ2 ,
	              Delta2CorY ,
	              Delta2CorZ ,
	              R2CorZ ,
	              HeadingDev ,
	              SSpeed ,
	              SeaBedLHS1Dep ,
	              SeaBedLHS2Dep ,
	              SeaBedLHS3Dep ,
	              SeaBedLHS4Dep ,
	              FProc ,
	              FChart ,
	              FFProc ,
	              Discard ,
	              Depth ,
	              KPDifference ,
	              ScanExtraData1 ,
	              ScanExtraData2 ,
	              SeaBedRHS1Dep ,
	              SeaBedRHS2Dep ,
	              SeaBedRHS3Dep ,
	              SeaBedRHS4Dep ,
	              ROVID ,
	              SeaBedLHS1S ,
	              SeaBedLHS2S ,
	              SeaBedLHS3S ,
	              SeaBedLHS4S ,
	              SeaBedLHS1E ,
	              SeaBedLHS2E ,
	              SeaBedLHS3E ,
	              SeaBedLHS4E ,
	              SeaBedRHS1S ,
	              SeaBedRHS2S ,
	              SeaBedRHS3S ,
	              SeaBedRHS4S ,
	              SeaBedRHS1E ,
	              SeaBedRHS2E ,
	              SeaBedRHS3E ,
	              SeaBedRHS4E ,
	              SeaBed0Range ,
	              AverageSeabedDepth ,
	              DoNotShowPipe ,
	              PTProc ,
	              SmoothedPipe ,
	              SmoothedSeabed ,
	              PTAdded ,
	              PipeDiameter ,
	              DesignKp ,
	              OldKP ,
	              DesignDCC ,
	              OldDCC ,
	              DCC ,
	              HeadEast ,
	              HeadNorth ,
	              CorrEast ,
	              CorrNorth
		        )
		SELECT ID ,
	              DTime ,
	              ScanNum ,
	              East ,
	              North ,
	              KP ,
	              Pitch ,
	              Roll ,
	              Heading ,
	              MasterNumScans ,
	              SlaveNumScans ,
	              MasterAngStart ,
	              SlaveAngStart ,
	              MasterAngStep ,
	              SlaveAngStep ,
	              PipeDepth ,
	              SeaBedDep ,
	              PipeDeltaY ,
	              ScanData1 ,
	              DeltaX1 ,
	              DeltaY1 ,
	              DeltaZ1 ,
	              RX1 ,
	              RY1 ,
	              RZ1 ,
	              ScanData2 ,
	              DeltaX2 ,
	              DeltaY2 ,
	              DeltaZ2 ,
	              RX2 ,
	              RY2 ,
	              RZ2 ,
	              Delta2CorY ,
	              Delta2CorZ ,
	              R2CorZ ,
	              HeadingDev ,
	              SSpeed ,
	              SeaBedLHS1Dep ,
	              SeaBedLHS2Dep ,
	              SeaBedLHS3Dep ,
	              SeaBedLHS4Dep ,
	              FProc ,
	              FChart ,
	              FFProc ,
	              Discard ,
	              Depth ,
	              KPDifference ,
	              ScanExtraData1 ,
	              ScanExtraData2 ,
	              SeaBedRHS1Dep ,
	              SeaBedRHS2Dep ,
	              SeaBedRHS3Dep ,
	              SeaBedRHS4Dep ,
	              ROVID ,
	              SeaBedLHS1S ,
	              SeaBedLHS2S ,
	              SeaBedLHS3S ,
	              SeaBedLHS4S ,
	              SeaBedLHS1E ,
	              SeaBedLHS2E ,
	              SeaBedLHS3E ,
	              SeaBedLHS4E ,
	              SeaBedRHS1S ,
	              SeaBedRHS2S ,
	              SeaBedRHS3S ,
	              SeaBedRHS4S ,
	              SeaBedRHS1E ,
	              SeaBedRHS2E ,
	              SeaBedRHS3E ,
	              SeaBedRHS4E ,
	              SeaBed0Range ,
	              AverageSeabedDepth ,
	              DoNotShowPipe ,
	              PTProc ,
	              SmoothedPipe ,
	              SmoothedSeabed ,
	              PTAdded ,
	              PipeDiameter ,
	              DesignKp ,
	              OldKP ,
	              DesignDCC ,
	              OldDCC ,
	              DCC ,
	              HeadEast ,
	              HeadNorth ,
	              CorrEast ,
	              CorrNorth
			FROM @TableValue 

		SET IDENTITY_INSERT [DesignScanProc] OFF
		   
		COMMIT TRANSACTION Trans
	END TRY
    BEGIN CATCH
		ROLLBACK TRANSACTION Trans
	END CATCH
END



GO
/****** Object:  StoredProcedure [dbo].[DATA_OldEventProc_Insert]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DATA_OldEventProc_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[DATA_OldEventProc_Insert] AS' 
END
GO

ALTER PROCEDURE [dbo].[DATA_OldEventProc_Insert]
	@TableValue AS [IMP_OldEventProcType] READONLY
AS
BEGIN
	SET DATEFORMAT YMD;
	BEGIN TRANSACTION Trans
	BEGIN TRY
    	DELETE FROM [OldEventProc]
			
		SET IDENTITY_INSERT [OldEventProc] ON

		INSERT INTO dbo.OldEventProc
		        ( ID ,
		       DTime ,
		       PCode ,
		       SCode ,
		       KP ,
		       East ,
		       North ,
		       PipeDepth ,
		       SeabedDepth ,
		       BurialDepth ,
		       Length ,
		       Height ,
		       Width ,
		       CP ,
		       CPFieldGradiant ,
		       Comment ,
		       Depth ,
		       ROVID ,
		       Active ,
		       Depletion ,
		       DepletionValue ,
		       Angle ,
		       ClockNotation ,
		       Distance ,
		       DistanceNotation ,
		       Contact ,
		       FieldJointNumber ,
		       CrossingName ,
		       CrossingClearance ,
		       CPReading ,
		       Anomaly ,
		       Discard ,
		       CustomComment ,
		       PiggyBackValue ,
		       Reviewed ,
		       EventQC ,
		       KP_Fixed ,
		       DesignKp ,
		       OldKP ,
		       DesignDCC ,
		       OldDCC ,
		       DCC ,
		       AdjSeabedDep ,
		       MeanSeabedDep ,
		       North_Fixed ,
		       East_Fixed ,
		       Loc ,
		       SurveyDir ,
		       LocNum ,
		       AnomalyNumber ,
		       AnomalyUniqueNumber ,
		       AnomalyCode ,
		       AnomalyClass ,
		       EventNum ,
		       HourLocation ,
		       OldEvent ,
		       Dive_Number ,
		       AnodeSecure ,
		       MG_Coverage ,
		       MG_Thickness
		        )
		SELECT ID ,
		       DTime ,
		       PCode ,
		       SCode ,
		       KP ,
		       East ,
		       North ,
		       PipeDepth ,
		       SeabedDepth ,
		       BurialDepth ,
		       Length ,
		       Height ,
		       Width ,
		       CP ,
		       CPFieldGradiant ,
		       Comment ,
		       Depth ,
		       ROVID ,
		       Active ,
		       Depletion ,
		       DepletionValue ,
		       Angle ,
		       ClockNotation ,
		       Distance ,
		       DistanceNotation ,
		       Contact ,
		       FieldJointNumber ,
		       CrossingName ,
		       CrossingClearance ,
		       CPReading ,
		       Anomaly ,
		       Discard ,
		       CustomComment ,
		       PiggyBackValue ,
		       Reviewed ,
		       EventQC ,
		       KP_Fixed ,
		       DesignKp ,
		       OldKP ,
		       DesignDCC ,
		       OldDCC ,
		       DCC ,
		       AdjSeabedDep ,
		       MeanSeabedDep ,
		       North_Fixed ,
		       East_Fixed ,
		       Loc ,
		       SurveyDir ,
		       LocNum ,
		       AnomalyNumber ,
		       AnomalyUniqueNumber ,
		       AnomalyCode ,
		       AnomalyClass ,
		       EventNum ,
		       HourLocation ,
		       OldEvent ,
		       Dive_Number ,
		       AnodeSecure ,
		       MG_Coverage ,
		       MG_Thickness
			FROM @TableValue 

		SET IDENTITY_INSERT [OldEventProc] OFF
		
		COMMIT TRANSACTION Trans
	END TRY
    BEGIN CATCH
		ROLLBACK TRANSACTION Trans
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[DATA_OldPosProc_Insert]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DATA_OldPosProc_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[DATA_OldPosProc_Insert] AS' 
END
GO

ALTER PROCEDURE [dbo].[DATA_OldPosProc_Insert]
	@TableValue AS [IMP_OldPosProcType] READONLY
AS
BEGIN
	SET DATEFORMAT YMD;
	BEGIN TRANSACTION Trans
	BEGIN TRY
    	DELETE FROM [OldPosProc]

		SET IDENTITY_INSERT [OldPosProc] ON

		INSERT INTO dbo.OldPosProc
		        ( ID ,
				  DTime ,
		          FixNum ,
		          East ,
		          North ,
		          Kp ,
		          Depth ,
		          Heading ,
		          Pitch ,
		          [Roll] ,
		          Logged ,
		          HeadingDev ,
		          Discard ,
		          Digitized ,
		          ROVID ,
		          DesignKp ,
		          OldKP ,
		          DesignDCC ,
		          OldDCC ,
		          DCC ,
		          [De-spiked] ,
		          Long ,
		          LAT
		        )
		SELECT ID ,
				  DTime ,
		          FixNum ,
		          East ,
		          North ,
		          Kp ,
		          Depth ,
		          Heading ,
		          Pitch ,
		          [Roll] ,
		          Logged ,
		          HeadingDev ,
		          Discard ,
		          Digitized ,
		          ROVID ,
		          DesignKp ,
		          OldKP ,
		          DesignDCC ,
		          OldDCC ,
		          DCC ,
		          [De-spiked] ,
		          Long ,
		          LAT
			FROM @TableValue 

		SET IDENTITY_INSERT [OldPosProc] OFF
		   
		COMMIT TRANSACTION Trans
	END TRY
    BEGIN CATCH
		ROLLBACK TRANSACTION Trans
	END CATCH
END



GO
/****** Object:  StoredProcedure [dbo].[DATA_OldScanProc_Insert]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DATA_OldScanProc_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[DATA_OldScanProc_Insert] AS' 
END
GO

ALTER PROCEDURE [dbo].[DATA_OldScanProc_Insert]
	@TableValue AS [IMP_OldScanProcType] READONLY
AS
BEGIN
	SET DATEFORMAT YMD;
	BEGIN TRANSACTION Trans
	BEGIN TRY
		DELETE FROM [OldScanProc]

		SET IDENTITY_INSERT [OldScanProc] ON

		INSERT INTO dbo.OldScanProc
		        ( ID ,
				  DTime ,
		          ScanNum ,
		          East ,
		          North ,
		          KP ,
		          Pitch ,
		          [Roll] ,
		          Heading ,
		          MasterNumScans ,
		          SlaveNumScans ,
		          MasterAngStart ,
		          SlaveAngStart ,
		          MasterAngStep ,
		          SlaveAngStep ,
		          PipeDepth ,
		          SeaBedDep ,
		          PipeDeltaY ,
		          ScanData1 ,
		          DeltaX1 ,
		          DeltaY1 ,
		          DeltaZ1 ,
		          RX1 ,
		          RY1 ,
		          RZ1 ,
		          ScanData2 ,
		          DeltaX2 ,
		          DeltaY2 ,
		          DeltaZ2 ,
		          RX2 ,
		          RY2 ,
		          RZ2 ,
		          Delta2CorY ,
		          Delta2CorZ ,
		          R2CorZ ,
		          HeadingDev ,
		          SSpeed ,
		          SeaBedLHS1Dep ,
		          SeaBedLHS2Dep ,
		          SeaBedLHS3Dep ,
		          SeaBedLHS4Dep ,
		          FProc ,
		          FChart ,
		          FFProc ,
		          Discard ,
		          Depth ,
		          KPDifference ,
		          ScanExtraData1 ,
		          ScanExtraData2 ,
		          SeaBedRHS1Dep ,
		          SeaBedRHS2Dep ,
		          SeaBedRHS3Dep ,
		          SeaBedRHS4Dep ,
		          ROVID ,
		          SeaBedLHS1S ,
		          SeaBedLHS2S ,
		          SeaBedLHS3S ,
		          SeaBedLHS4S ,
		          SeaBedLHS1E ,
		          SeaBedLHS2E ,
		          SeaBedLHS3E ,
		          SeaBedLHS4E ,
		          SeaBedRHS1S ,
		          SeaBedRHS2S ,
		          SeaBedRHS3S ,
		          SeaBedRHS4S ,
		          SeaBedRHS1E ,
		          SeaBedRHS2E ,
		          SeaBedRHS3E ,
		          SeaBedRHS4E ,
		          SeaBed0Range ,
		          AverageSeabedDepth ,
		          DoNotShowPipe ,
		          PTProc ,
		          SmoothedPipe ,
		          SmoothedSeabed ,
		          PTAdded ,
		          PipeDiameter ,
		          DesignKp ,
		          OldKP ,
		          DesignDCC ,
		          OldDCC ,
		          DCC ,
		          CorrEast ,
		          CorrNorth ,
		          HeadEast ,
		          HeadNorth
		        )
		SELECT ID ,
				  DTime ,
		          ScanNum ,
		          East ,
		          North ,
		          KP ,
		          Pitch ,
		          [Roll] ,
		          Heading ,
		          MasterNumScans ,
		          SlaveNumScans ,
		          MasterAngStart ,
		          SlaveAngStart ,
		          MasterAngStep ,
		          SlaveAngStep ,
		          PipeDepth ,
		          SeaBedDep ,
		          PipeDeltaY ,
		          ScanData1 ,
		          DeltaX1 ,
		          DeltaY1 ,
		          DeltaZ1 ,
		          RX1 ,
		          RY1 ,
		          RZ1 ,
		          ScanData2 ,
		          DeltaX2 ,
		          DeltaY2 ,
		          DeltaZ2 ,
		          RX2 ,
		          RY2 ,
		          RZ2 ,
		          Delta2CorY ,
		          Delta2CorZ ,
		          R2CorZ ,
		          HeadingDev ,
		          SSpeed ,
		          SeaBedLHS1Dep ,
		          SeaBedLHS2Dep ,
		          SeaBedLHS3Dep ,
		          SeaBedLHS4Dep ,
		          FProc ,
		          FChart ,
		          FFProc ,
		          Discard ,
		          Depth ,
		          KPDifference ,
		          ScanExtraData1 ,
		          ScanExtraData2 ,
		          SeaBedRHS1Dep ,
		          SeaBedRHS2Dep ,
		          SeaBedRHS3Dep ,
		          SeaBedRHS4Dep ,
		          ROVID ,
		          SeaBedLHS1S ,
		          SeaBedLHS2S ,
		          SeaBedLHS3S ,
		          SeaBedLHS4S ,
		          SeaBedLHS1E ,
		          SeaBedLHS2E ,
		          SeaBedLHS3E ,
		          SeaBedLHS4E ,
		          SeaBedRHS1S ,
		          SeaBedRHS2S ,
		          SeaBedRHS3S ,
		          SeaBedRHS4S ,
		          SeaBedRHS1E ,
		          SeaBedRHS2E ,
		          SeaBedRHS3E ,
		          SeaBedRHS4E ,
		          SeaBed0Range ,
		          AverageSeabedDepth ,
		          DoNotShowPipe ,
		          PTProc ,
		          SmoothedPipe ,
		          SmoothedSeabed ,
		          PTAdded ,
		          PipeDiameter ,
		          DesignKp ,
		          OldKP ,
		          DesignDCC ,
		          OldDCC ,
		          DCC ,
		          CorrEast ,
		          CorrNorth ,
		          HeadEast ,
		          HeadNorth
			FROM @TableValue

		SET IDENTITY_INSERT [OldScanProc] OFF
		
		COMMIT TRANSACTION Trans
	END TRY
    BEGIN CATCH
		ROLLBACK TRANSACTION Trans
	END CATCH
END



GO
/****** Object:  StoredProcedure [dbo].[ExportFromEMScanLogToScanProc]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ExportFromEMScanLogToScanProc]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[ExportFromEMScanLogToScanProc] AS' 
END
GO

ALTER PROCEDURE [dbo].[ExportFromEMScanLogToScanProc]

	@ScanResults EXP_ScanProc_PRC readonly
AS
BEGIN
	Insert into ScanProc (ScanNum,DTime,ScanData1,ScanData2,MasterNumScans,SlaveNumScans,MasterAngStart,
                           SlaveAngStart,MasterAngStep,SlaveAngStep,DeltaX1,DeltaY1,DeltaZ1,RX1,
                           RY1,RZ1,DeltaX2,DeltaY2,DeltaZ2,RX2,RY2,RZ2,ScanExtraData1,ScanExtraData2,
                           SSpeed,ROVID,IsPRC)
	 select * from @ScanResults
END

GO
/****** Object:  StoredProcedure [dbo].[ExportFromEventLogToEventProc]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ExportFromEventLogToEventProc]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[ExportFromEventLogToEventProc] AS' 
END
GO
ALTER PROCEDURE [dbo].[ExportFromEventLogToEventProc]

	@EventProcResults EXP_EventScanProc readonly
AS
BEGIN
	insert into EventProc(EventNum,DTime,PCode,SCode,East,North,KP,Depth,Length,Height,Width,CP,Comment,ROVID,Active,Depletion,DepletionValue,Angle,
	ClockNotation,Distance,DistanceNotation,Contact,FieldJointNumber,CrossingName,CrossingClearance,CPReading,Anomaly,
	CustomComment,PiggyBackValue,HourLocation,AnodeSecure,MG_Coverage,MG_Thickness,CPFieldGradiant)

	 select * from @EventProcResults
END



GO
/****** Object:  StoredProcedure [dbo].[ExportFromPosLogToPosProc]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ExportFromPosLogToPosProc]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[ExportFromPosLogToPosProc] AS' 
END
GO
ALTER PROCEDURE [dbo].[ExportFromPosLogToPosProc]

	@InterpolatedResults EXP_PosProcType readonly
AS
BEGIN
	Insert into PosProc(ID,FixNum,DTime,East,North,Depth,KP,Pitch,Roll,Heading,Logged,ROVID)
	 select * from @InterpolatedResults
END


GO
/****** Object:  StoredProcedure [dbo].[ExportFromSB7125ScanLogToScanProc]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ExportFromSB7125ScanLogToScanProc]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[ExportFromSB7125ScanLogToScanProc] AS' 
END
GO

ALTER PROCEDURE [dbo].[ExportFromSB7125ScanLogToScanProc]

	@ScanResults EXP_ScanProc readonly
AS
BEGIN
	Insert into ScanProc (ScanNum,DTime,ScanData1,ScanData2,MasterNumScans,SlaveNumScans,MasterAngStart,
                           SlaveAngStart,MasterAngStep,SlaveAngStep,DeltaX1,DeltaY1,DeltaZ1,RX1,
                           RY1,RZ1,DeltaX2,DeltaY2,DeltaZ2,RX2,RY2,RZ2,ScanExtraData1,ScanExtraData2,
                           SSpeed,ROVID)
	 select * from @ScanResults
END


GO
/****** Object:  StoredProcedure [dbo].[ExportFromSBScanLogToScanProc]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ExportFromSBScanLogToScanProc]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[ExportFromSBScanLogToScanProc] AS' 
END
GO

ALTER PROCEDURE [dbo].[ExportFromSBScanLogToScanProc]

	@ScanResults EXP_SBScanProc readonly
AS
BEGIN
	Insert into ScanProc (ScanNum,DTime,ScanData1,ScanData2,MasterNumScans,SlaveNumScans,MasterAngStart,
                           SlaveAngStart,MasterAngStep,SlaveAngStep,DeltaX1,DeltaY1,DeltaZ1,RX1,
                           RY1,RZ1,DeltaX2,DeltaY2,DeltaZ2,RX2,RY2,RZ2,
                           SSpeed,ROVID)
	 select * from @ScanResults
END


GO
/****** Object:  StoredProcedure [dbo].[ExportFromScanLogToScanProc]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ExportFromScanLogToScanProc]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[ExportFromScanLogToScanProc] AS' 
END
GO

ALTER PROCEDURE [dbo].[ExportFromScanLogToScanProc]

	@ScanResults EXP_ScanProc readonly
AS
BEGIN
	Insert into ScanProc (ScanNum,DTime,ScanData1,ScanData2,MasterNumScans,SlaveNumScans,MasterAngStart,
                           SlaveAngStart,MasterAngStep,SlaveAngStep,DeltaX1,DeltaY1,DeltaZ1,RX1,
                           RY1,RZ1,DeltaX2,DeltaY2,DeltaZ2,RX2,RY2,RZ2,ScanExtraData1,ScanExtraData2,
                           SSpeed,ROVID)
	 select * from @ScanResults
END


GO
/****** Object:  StoredProcedure [dbo].[FlagScanProcFChart]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FlagScanProcFChart]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[FlagScanProcFChart] AS' 
END
GO

ALTER PROCEDURE [dbo].[FlagScanProcFChart]
	@ScanProcData IDType readonly
AS
BEGIN


	UPDATE ScanProc SET  ScanProc.FChart = '1'
	FROM  @ScanProcData AS UpdatedScanProc
	Where ScanProc.ID = UpdatedScanProc.ID
END


GO
/****** Object:  StoredProcedure [dbo].[FlagScanProcFPROC]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FlagScanProcFPROC]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[FlagScanProcFPROC] AS' 
END
GO

ALTER PROCEDURE [dbo].[FlagScanProcFPROC]
	@ScanProcData IDType readonly
AS
BEGIN


	UPDATE ScanProc SET  ScanProc.FProc = '1'
	FROM  @ScanProcData AS UpdatedScanProc
	Where ScanProc.ID = UpdatedScanProc.ID
END


GO
/****** Object:  StoredProcedure [dbo].[GenerateAutoComments]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GenerateAutoComments]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[GenerateAutoComments] AS' 
END
GO

ALTER PROCEDURE [dbo].[GenerateAutoComments]
	@EventProcData CommentType readonly
AS
BEGIN


	UPDATE EventProc SET  EventProc.Comment = UpdatedEventProc.Comment
	FROM  @EventProcData AS UpdatedEventProc
	Where EventProc.ID = UpdatedEventProc.ID
END



GO
/****** Object:  StoredProcedure [dbo].[GenerateEventsComments]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GenerateEventsComments]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[GenerateEventsComments] AS' 
END
GO

ALTER PROCEDURE [dbo].[GenerateEventsComments]
	@EventProcData CommentType readonly
AS
BEGIN


	UPDATE EventProc SET  EventProc.Comment = UpdatedEventProc.Comment
	FROM  @EventProcData AS UpdatedEventProc
	Where EventProc.ID = UpdatedEventProc.ID
END



GO
/****** Object:  StoredProcedure [dbo].[GeneratePipeListFromScanProc]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GeneratePipeListFromScanProc]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[GeneratePipeListFromScanProc] AS' 
END
GO

ALTER PROCEDURE [dbo].[GeneratePipeListFromScanProc]
	@PipeData PipeListType readonly
AS
BEGIN
INSERT INTO PipeList (ID,KP,East,North,PipeDepth,SeaBedDepth,SeaBedLHS1Dep,SeaBedLHS2Dep,SeaBedLHS3Dep,SeaBedLHS4Dep,SeaBedRHS1Dep,
                      SeaBedRHS2Dep,SeaBedRHS3Dep,SeaBedRHS4Dep,DesignKP,DCC,ROVID,Depth,DTime,DesignDCC,PipeDiameter) 
	   
	                select FullPipeData.ID,FullPipeData.KP,FullPipeData.East,FullPipeData.North,FullPipeData.PipeDepth,
					FullPipeData.SeaBedDepth,FullPipeData.SeaBedLHS1Dep,FullPipeData.SeaBedLHS2Dep,FullPipeData.SeaBedLHS3Dep,
					FullPipeData.SeaBedLHS4Dep,FullPipeData.SeaBedRHS1Dep,FullPipeData.SeaBedRHS1Dep,FullPipeData.SeaBedRHS3Dep,
					FullPipeData.SeaBedRHS4Dep, FullPipeData.DesignKp,FullPipeData.DCC,FullPipeData.ROVID,FullPipeData.Depth,
					FullPipeData.DTime,FullPipeData.DesignDCC,FullPipeData.PipeDiameter
					 from @PipeData AS FullPipeData

END


GO
/****** Object:  StoredProcedure [dbo].[GeneratePipeListFromScanProc_DesignKP]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GeneratePipeListFromScanProc_DesignKP]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[GeneratePipeListFromScanProc_DesignKP] AS' 
END
GO

ALTER PROCEDURE [dbo].[GeneratePipeListFromScanProc_DesignKP]
	@PipeData PipeListType readonly
AS
BEGIN
INSERT INTO PipeList (ID,KP,East,North,PipeDepth,SeaBedDepth,SeaBedLHS1Dep,SeaBedLHS2Dep,SeaBedLHS3Dep,SeaBedLHS4Dep,SeaBedRHS1Dep,
                      SeaBedRHS2Dep,SeaBedRHS3Dep,SeaBedRHS4Dep,DesignKP,DCC,ROVID,Depth,DTime,DesignDCC,PipeDiameter) 
	   
	                select FullPipeData.ID,FullPipeData.KP,FullPipeData.East,FullPipeData.North,FullPipeData.PipeDepth,
					FullPipeData.SeaBedDepth,FullPipeData.SeaBedLHS1Dep,FullPipeData.SeaBedLHS2Dep,FullPipeData.SeaBedLHS3Dep,
					FullPipeData.SeaBedLHS4Dep,FullPipeData.SeaBedRHS1Dep,FullPipeData.SeaBedRHS1Dep,FullPipeData.SeaBedRHS3Dep,
					FullPipeData.SeaBedRHS4Dep, FullPipeData.DesignKp,FullPipeData.DCC,FullPipeData.ROVID,FullPipeData.Depth,
					FullPipeData.DTime,FullPipeData.DesignDCC,FullPipeData.PipeDiameter
					 from @PipeData AS FullPipeData

END


GO
/****** Object:  StoredProcedure [dbo].[GetAllStoredNamesTypes]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetAllStoredNamesTypes]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[GetAllStoredNamesTypes] AS' 
END
GO

ALTER PROCEDURE [dbo].[GetAllStoredNamesTypes] 
	
AS
BEGIN
	select * from StoredNameType
	ORDER BY StoredTypeID
END


GO
/****** Object:  StoredProcedure [dbo].[IMP_AuditTable_Insert]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IMP_AuditTable_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[IMP_AuditTable_Insert] AS' 
END
GO
ALTER PROCEDURE [dbo].[IMP_AuditTable_Insert]
	@TableValue AS [IMP_AuditTableType] READONLY ,
	@AllowDelete AS BIT ,
	@DateTimeFieldName AS NVARCHAR(250) = NULL ,
	@FromDate AS DATETIME = NULL ,
	@ToDate AS DATETIME = NULL
AS
BEGIN
	SET DATEFORMAT YMD;
	BEGIN TRANSACTION Trans
	BEGIN TRY
    	IF @AllowDelete = 1
		BEGIN
			DECLARE @SQLStatment AS NVARCHAR(1000)
			SET @SQLStatment = 'DELETE FROM [AuditTable] '
			IF @FromDate IS NOT NULL
            BEGIN
            	SET @SQLStatment = @SQLStatment + 'WHERE ' + @DateTimeFieldName + ' > ''' + CONVERT(NVARCHAR(20), @FromDate, 120) + ''''

				IF @ToDate IS NOT NULL               
					SET @SQLStatment = @SQLStatment + 'AND ' + @DateTimeFieldName + ' < ''' + CONVERT(NVARCHAR(20), @ToDate, 120) + ''''
            END
			EXECUTE (@SQLStatment)

			SET IDENTITY_INSERT [AuditTable] ON
			INSERT INTO dbo.AuditTable
			        ( ID ,
			       FileName ,
			       RecordDate ,
			       StopDate ,
			       StartKP ,
			       EndKP ,
			       QC ,
			       Reviewed ,
			       Combined ,
			       Copied ,
			       BackedUp ,
			       ROVID ,
			       FilePath
			        )
			SELECT ID ,
			       FileName ,
			       RecordDate ,
			       StopDate ,
			       StartKP ,
			       EndKP ,
			       QC ,
			       Reviewed ,
			       Combined ,
			       Copied ,
			       BackedUp ,
			       ROVID ,
			       FilePath
				FROM @TableValue 
			SET IDENTITY_INSERT [AuditTable] OFF
		END
		ELSE
        BEGIN
	        INSERT INTO dbo.AuditTable
			        ( FileName ,
	               RecordDate ,
	               StopDate ,
	               StartKP ,
	               EndKP ,
	               QC ,
	               Reviewed ,
	               Combined ,
	               Copied ,
	               BackedUp ,
	               ROVID ,
	               FilePath
			        )
			SELECT FileName ,
	               RecordDate ,
	               StopDate ,
	               StartKP ,
	               EndKP ,
	               QC ,
	               Reviewed ,
	               Combined ,
	               Copied ,
	               BackedUp ,
	               ROVID ,
	               FilePath
				FROM @TableValue 
        END
		
		COMMIT TRANSACTION Trans
	END TRY
    BEGIN CATCH
		ROLLBACK TRANSACTION Trans
	END CATCH
END



GO
/****** Object:  StoredProcedure [dbo].[IMP_Batches_Insert]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IMP_Batches_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[IMP_Batches_Insert] AS' 
END
GO
ALTER PROCEDURE [dbo].[IMP_Batches_Insert]
	@TableValue AS [IMP_BatchesType] READONLY ,
	@AllowDelete AS BIT ,
	@DateTimeFieldName AS NVARCHAR(250) = NULL ,
	@FromDate AS DATETIME = NULL ,
	@ToDate AS DATETIME = NULL
AS
BEGIN
	SET DATEFORMAT YMD;
	BEGIN TRANSACTION Trans
	BEGIN TRY
    	IF @AllowDelete = 1
		BEGIN
			DECLARE @SQLStatment AS NVARCHAR(1000)
			SET @SQLStatment = 'DELETE FROM [Batches] '
			IF @FromDate IS NOT NULL
            BEGIN
            	SET @SQLStatment = @SQLStatment + 'WHERE ' + @DateTimeFieldName + ' > ''' + CONVERT(NVARCHAR(25), @FromDate, 13) + ''''

				IF @ToDate IS NOT NULL               
					SET @SQLStatment = @SQLStatment + 'AND ' + @DateTimeFieldName + ' < ''' + CONVERT(NVARCHAR(25), @ToDate, 13) + ''''
            END
			EXECUTE (@SQLStatment)

			SET IDENTITY_INSERT [Batches] ON
			INSERT INTO [Batches]
			        ( PK_BatchID ,
					  FK_ClusterID ,
			          StartDTime ,
			          EndDTime ,
			          PlannedStartKP ,
			          PlannedEndKP ,
			          StartKP ,
			          EndKP ,
			          StartEast ,
			          EndEast ,
			          StartNorth ,
			          EndNorth ,
			          BatchPath ,
			          TransformationMatrix ,
			          RotationMatrix ,
			          FinishPercentage ,
			          ProcessingStatus
			        )
			SELECT PK_BatchID ,
					  FK_ClusterID ,
			          StartDTime ,
			          EndDTime ,
			          PlannedStartKP ,
			          PlannedEndKP ,
			          StartKP ,
			          EndKP ,
			          StartEast ,
			          EndEast ,
			          StartNorth ,
			          EndNorth ,
			          BatchPath ,
			          TransformationMatrix ,
			          RotationMatrix ,
			          FinishPercentage ,
			          ProcessingStatus
				FROM @TableValue 
			SET IDENTITY_INSERT [Batches] OFF
		END
		ELSE
        BEGIN
			INSERT INTO [Batches]
			        ( FK_ClusterID ,
			          StartDTime ,
			          EndDTime ,
			          PlannedStartKP ,
			          PlannedEndKP ,
			          StartKP ,
			          EndKP ,
			          StartEast ,
			          EndEast ,
			          StartNorth ,
			          EndNorth ,
			          BatchPath ,
			          TransformationMatrix ,
			          RotationMatrix ,
			          FinishPercentage ,
			          ProcessingStatus
			        )
			SELECT FK_ClusterID ,
			          StartDTime ,
			          EndDTime ,
			          PlannedStartKP ,
			          PlannedEndKP ,
			          StartKP ,
			          EndKP ,
			          StartEast ,
			          EndEast ,
			          StartNorth ,
			          EndNorth ,
			          BatchPath ,
			          TransformationMatrix ,
			          RotationMatrix ,
			          FinishPercentage ,
			          ProcessingStatus
				FROM @TableValue 
        END
		   

		COMMIT TRANSACTION Trans
	END TRY
    BEGIN CATCH
		ROLLBACK TRANSACTION Trans
	END CATCH
END



GO
/****** Object:  StoredProcedure [dbo].[IMP_BatchesDetails_Insert]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IMP_BatchesDetails_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[IMP_BatchesDetails_Insert] AS' 
END
GO
ALTER PROCEDURE [dbo].[IMP_BatchesDetails_Insert]
	@TableValue AS [IMP_BatchesDetailsType] READONLY ,
	@AllowDelete AS BIT ,
	@DateTimeFieldName AS NVARCHAR(250) = NULL ,
	@FromDate AS DATETIME = NULL ,
	@ToDate AS DATETIME = NULL
AS
BEGIN
	SET DATEFORMAT YMD;
	BEGIN TRANSACTION Trans
	BEGIN TRY
    	IF @AllowDelete = 1
		BEGIN
			DECLARE @SQLStatment AS NVARCHAR(1000)
			SET @SQLStatment = 'DELETE FROM [BatchesDetails] '
			IF @FromDate IS NOT NULL
            BEGIN
            	SET @SQLStatment = @SQLStatment + 'WHERE ' + @DateTimeFieldName + ' > ''' + CONVERT(NVARCHAR(25), @FromDate, 13) + ''''

				IF @ToDate IS NOT NULL               
					SET @SQLStatment = @SQLStatment + 'AND ' + @DateTimeFieldName + ' < ''' + CONVERT(NVARCHAR(25), @ToDate, 13) + ''''
            END
			EXECUTE (@SQLStatment)

			SET IDENTITY_INSERT [BatchesDetails] ON
			INSERT INTO BatchesDetails
					(ID ,
					FK_BatchID ,
					FK_NavigationDetailID ,
					DTime ,
					CameraImageNames ,
					CameraPositions ,
					ProcessedCameraPositions ,
					CameraAvgPosX ,
					CameraAvgPosY ,
					CameraAvgPosZ ,
					ProcessedAvgPosX ,
					ProcessedAvgPosY ,
					ProcessedAvgPosZ ,
					IsCameraUsed ,
					IsCameraNeglected)
			SELECT ID ,
					FK_BatchID ,
					FK_NavigationDetailID ,
					DTime ,
					CameraImageNames ,
					CameraPositions ,
					ProcessedCameraPositions ,
					CameraAvgPosX ,
					CameraAvgPosY ,
					CameraAvgPosZ ,
					ProcessedAvgPosX ,
					ProcessedAvgPosY ,
					ProcessedAvgPosZ ,
					IsCameraUsed ,
					IsCameraNeglected 
				FROM @TableValue 
			SET IDENTITY_INSERT [BatchesDetails] OFF
		END
		ELSE
		BEGIN
			INSERT INTO BatchesDetails
					(FK_BatchID ,
					FK_NavigationDetailID ,
					DTime ,
					CameraImageNames ,
					CameraPositions ,
					ProcessedCameraPositions ,
					CameraAvgPosX ,
					CameraAvgPosY ,
					CameraAvgPosZ ,
					ProcessedAvgPosX ,
					ProcessedAvgPosY ,
					ProcessedAvgPosZ ,
					IsCameraUsed ,
					IsCameraNeglected)
			SELECT FK_BatchID ,
					FK_NavigationDetailID ,
					DTime ,
					CameraImageNames ,
					CameraPositions ,
					ProcessedCameraPositions ,
					CameraAvgPosX ,
					CameraAvgPosY ,
					CameraAvgPosZ ,
					ProcessedAvgPosX ,
					ProcessedAvgPosY ,
					ProcessedAvgPosZ ,
					IsCameraUsed ,
					IsCameraNeglected
				FROM @TableValue 
		END
		   
		COMMIT TRANSACTION Trans
	END TRY
    BEGIN CATCH
		ROLLBACK TRANSACTION Trans
	END CATCH
END



GO
/****** Object:  StoredProcedure [dbo].[IMP_Clusters_Insert]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IMP_Clusters_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[IMP_Clusters_Insert] AS' 
END
GO
ALTER PROCEDURE [dbo].[IMP_Clusters_Insert]
	@TableValue AS [IMP_ClustersType] READONLY ,
	@AllowDelete AS BIT ,
	@DateTimeFieldName AS NVARCHAR(250) = NULL ,
	@FromDate AS DATETIME = NULL ,
	@ToDate AS DATETIME = NULL
AS
BEGIN
	SET DATEFORMAT YMD;
	BEGIN TRANSACTION Trans
	BEGIN TRY
    	IF @AllowDelete = 1
		BEGIN
			DECLARE @SQLStatment AS NVARCHAR(1000)
			SET @SQLStatment = 'DELETE FROM [Clusters] '
			IF @FromDate IS NOT NULL
            BEGIN
            	SET @SQLStatment = @SQLStatment + 'WHERE ' + @DateTimeFieldName + ' > ''' + CONVERT(NVARCHAR(25), @FromDate, 13) + ''''

				IF @ToDate IS NOT NULL               
					SET @SQLStatment = @SQLStatment + 'AND ' + @DateTimeFieldName + ' < ''' + CONVERT(NVARCHAR(25), @ToDate, 13) + ''''
            END
			EXECUTE (@SQLStatment)

			SET IDENTITY_INSERT [Clusters] ON
			INSERT INTO [dbo].[Clusters] 
					([PK_ClusterID] ,
					[PlannedStartKP] ,
					[StartKP] ,
					[StartDTime] ,
					[StartEast] ,
					[StartNorth] ,
					[StartDepth] ,
					[StartEvent] ,
					[StartEventID] ,
					[PlannedEndKP] ,
					[EndKP] ,
					[EndDTime] ,
					[EndEast] ,
					[EndNorth] ,
					[EndDepth] ,
					[EndEvent] ,
					[EndEventID] ,
					[GenerationType] ,
					[ImagesCount] ,
					[PerUnit] ,
					[MaxBatchSize] ,
					[OverlapLength] ,
					[IsKPSelection] ,
					[ClusterPath] ,
					[ProcessedPLYPath] ,
					[PLYStartCameraX] ,
					[PLYStartCameraY] ,
					[PLYStartCameraZ] ,
					[ScaleMatrix] ,
					[RotationMatrix] ,
					[RollMatrix] ,
					[DensePointIndices] ,
					[ClusterRaduis] ,
					[ProcessingStatus] ,
					[ProcessingStatusString] ,
					[IsFailed])
			SELECT [PK_ClusterID] ,
					[PlannedStartKP] ,
					[StartKP] ,
					[StartDTime] ,
					[StartEast] ,
					[StartNorth] ,
					[StartDepth] ,
					[StartEvent] ,
					[StartEventID] ,
					[PlannedEndKP] ,
					[EndKP] ,
					[EndDTime] ,
					[EndEast] ,
					[EndNorth] ,
					[EndDepth] ,
					[EndEvent] ,
					[EndEventID] ,
					[GenerationType] ,
					[ImagesCount] ,
					[PerUnit] ,
					[MaxBatchSize] ,
					[OverlapLength] ,
					[IsKPSelection] ,
					[ClusterPath] ,
					[ProcessedPLYPath] ,
					[PLYStartCameraX] ,
					[PLYStartCameraY] ,
					[PLYStartCameraZ] ,
					[ScaleMatrix] ,
					[RotationMatrix] ,
					[RollMatrix] ,
					[DensePointIndices] ,
					[ClusterRaduis] ,
					[ProcessingStatus] ,
					[ProcessingStatusString] ,
					[IsFailed] 
				FROM @TableValue
			SET IDENTITY_INSERT [Clusters] OFF
		END
		ELSE
		BEGIN
			INSERT INTO [dbo].[Clusters]
					([PlannedStartKP] ,
					[StartKP] ,
					[StartDTime] ,
					[StartEast] ,
					[StartNorth] ,
					[StartDepth] ,
					[StartEvent] ,
					[StartEventID] ,
					[PlannedEndKP] ,
					[EndKP] ,
					[EndDTime] ,
					[EndEast] ,
					[EndNorth] ,
					[EndDepth] ,
					[EndEvent] ,
					[EndEventID] ,
					[GenerationType] ,
					[ImagesCount] ,
					[PerUnit] ,
					[MaxBatchSize] ,
					[OverlapLength] ,
					[IsKPSelection] ,
					[ClusterPath] ,
					[ProcessedPLYPath] ,
					[PLYStartCameraX] ,
					[PLYStartCameraY] ,
					[PLYStartCameraZ] ,
					[ScaleMatrix] ,
					[RotationMatrix] ,
					[RollMatrix] ,
					[DensePointIndices] ,
					[ClusterRaduis] ,
					[ProcessingStatus] ,
					[ProcessingStatusString] ,
					[IsFailed])
			SELECT [PlannedStartKP] ,
					[StartKP] ,
					[StartDTime] ,
					[StartEast] ,
					[StartNorth] ,
					[StartDepth] ,
					[StartEvent] ,
					[StartEventID] ,
					[PlannedEndKP] ,
					[EndKP] ,
					[EndDTime] ,
					[EndEast] ,
					[EndNorth] ,
					[EndDepth] ,
					[EndEvent] ,
					[EndEventID] ,
					[GenerationType] ,
					[ImagesCount] ,
					[PerUnit] ,
					[MaxBatchSize] ,
					[OverlapLength] ,
					[IsKPSelection] ,
					[ClusterPath] ,
					[ProcessedPLYPath] ,
					[PLYStartCameraX] ,
					[PLYStartCameraY] ,
					[PLYStartCameraZ] ,
					[ScaleMatrix] ,
					[RotationMatrix] ,
					[RollMatrix] ,
					[DensePointIndices] ,
					[ClusterRaduis] ,
					[ProcessingStatus] ,
					[ProcessingStatusString] ,
					[IsFailed]
				FROM @TableValue
		END

		COMMIT TRANSACTION Trans
	END TRY
    BEGIN CATCH
		ROLLBACK TRANSACTION Trans
	END CATCH
END



GO
/****** Object:  StoredProcedure [dbo].[IMP_CodesDataFromat_Insert]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IMP_CodesDataFromat_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[IMP_CodesDataFromat_Insert] AS' 
END
GO
ALTER PROCEDURE [dbo].[IMP_CodesDataFromat_Insert]
			@TableValue AS [IMP_CodesDataFromatType] READONLY ,
	@AllowDelete AS BIT ,
	@DateTimeFieldName AS NVARCHAR(250) = NULL ,
	@FromDate AS DATETIME = NULL ,
	@ToDate AS DATETIME = NULL
AS
BEGIN
	SET DATEFORMAT YMD;
	BEGIN TRANSACTION Trans
	BEGIN TRY
    	IF @AllowDelete = 1
		BEGIN
			DECLARE @SQLStatment AS NVARCHAR(1000)
			SET @SQLStatment = 'DELETE FROM [CodesDataFromat] '
			IF @FromDate IS NOT NULL
            BEGIN
            	SET @SQLStatment = @SQLStatment + 'WHERE ' + @DateTimeFieldName + ' > ''' + CONVERT(NVARCHAR(25), @FromDate, 13) + ''''

				IF @ToDate IS NOT NULL               
					SET @SQLStatment = @SQLStatment + 'AND ' + @DateTimeFieldName + ' < ''' + CONVERT(NVARCHAR(25), @ToDate, 13)  + ''''
            END
			EXECUTE (@SQLStatment)

			SET IDENTITY_INSERT [CodesDataFromat] ON
			INSERT INTO dbo.CodesDataFromat
			        ( ID ,
			       FK_FormatID ,
			       FK_CodeID ,
			       CodeName
			        )
			SELECT ID ,
			       FK_FormatID ,
			       FK_CodeID ,
			       CodeName
				FROM @TableValue 
			SET IDENTITY_INSERT [CodesDataFromat] OFF
		END
		ELSE
        BEGIN
			INSERT INTO dbo.CodesDataFromat
			        ( FK_FormatID ,
			       FK_CodeID ,
			       CodeName
			        )
			SELECT FK_FormatID ,
			       FK_CodeID ,
			       CodeName
				FROM @TableValue 
        END
		  
		COMMIT TRANSACTION Trans
	END TRY
    BEGIN CATCH
		ROLLBACK TRANSACTION Trans
	END CATCH
END



GO
/****** Object:  StoredProcedure [dbo].[IMP_CorrPosProc_Insert]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IMP_CorrPosProc_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[IMP_CorrPosProc_Insert] AS' 
END
GO
ALTER PROCEDURE [dbo].[IMP_CorrPosProc_Insert]
	@TableValue AS [IMP_CorrPosProcType] READONLY ,
	@AllowDelete AS BIT ,
	@DateTimeFieldName AS NVARCHAR(250) = NULL ,
	@FromDate AS DATETIME = NULL ,
	@ToDate AS DATETIME = NULL
AS
BEGIN
	SET DATEFORMAT YMD;
	BEGIN TRANSACTION Trans
	BEGIN TRY
    	IF @AllowDelete = 1
		BEGIN
			DECLARE @SQLStatment AS NVARCHAR(1000)
			SET @SQLStatment = 'DELETE FROM [CorrPosProc] '
			IF @FromDate IS NOT NULL
            BEGIN
            	SET @SQLStatment = @SQLStatment + 'WHERE ' + @DateTimeFieldName + ' > ''' + CONVERT(NVARCHAR(25), @FromDate, 13) + ''''

				IF @ToDate IS NOT NULL               
					SET @SQLStatment = @SQLStatment + 'AND ' + @DateTimeFieldName + ' < ''' + CONVERT(NVARCHAR(25), @ToDate, 13) + ''''
            END
			EXECUTE (@SQLStatment)

			SET IDENTITY_INSERT [CorrPosProc] ON
			INSERT INTO dbo.CorrPosProc
			        ( ID ,
			       DTime ,
			       PTracker ,
			       DCC ,
			       Depth ,
			       Discard ,
			       KP ,
			       East ,
			       North
			        )
			SELECT ID ,
			       DTime ,
			       PTracker ,
			       DCC ,
			       Depth ,
			       Discard ,
			       KP ,
			       East ,
			       North
				FROM @TableValue 
			SET IDENTITY_INSERT [CorrPosProc] OFF
		END
		ELSE
        BEGIN
			INSERT INTO dbo.CorrPosProc
			        ( DTime ,
			       PTracker ,
			       DCC ,
			       Depth ,
			       Discard ,
			       KP ,
			       East ,
			       North
			        )
			SELECT DTime ,
			       PTracker ,
			       DCC ,
			       Depth ,
			       Discard ,
			       KP ,
			       East ,
			       North
				FROM @TableValue 
        END
		   
		COMMIT TRANSACTION Trans
	END TRY
    BEGIN CATCH
		ROLLBACK TRANSACTION Trans
	END CATCH
END



GO
/****** Object:  StoredProcedure [dbo].[IMP_CPLog_Insert]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IMP_CPLog_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[IMP_CPLog_Insert] AS' 
END
GO
ALTER PROCEDURE [dbo].[IMP_CPLog_Insert]
	@TableValue AS [IMP_CPLogType] READONLY ,
	@AllowDelete AS BIT ,
	@DateTimeFieldName AS NVARCHAR(250) = NULL ,
	@FromDate AS DATETIME = NULL ,
	@ToDate AS DATETIME = NULL
AS
BEGIN
	SET DATEFORMAT YMD;
	BEGIN TRANSACTION Trans
	BEGIN TRY
    	IF @AllowDelete = 1
		BEGIN
			DECLARE @SQLStatment AS NVARCHAR(1000)
			SET @SQLStatment = 'DELETE FROM [CPLog] '
			IF @FromDate IS NOT NULL
            BEGIN
            	SET @SQLStatment = @SQLStatment + 'WHERE ' + @DateTimeFieldName + ' > ''' + CONVERT(NVARCHAR(25), @FromDate, 13) + ''''

				IF @ToDate IS NOT NULL               
					SET @SQLStatment = @SQLStatment + 'AND ' + @DateTimeFieldName + ' < ''' + CONVERT(NVARCHAR(25), @ToDate, 13) + ''''
            END
			EXECUTE (@SQLStatment)

			SET IDENTITY_INSERT [CPLog] ON
			INSERT INTO dbo.CPLog
			        ( ID ,
	                DTime ,
	                KP ,
	                East ,
	                North ,
	                CP ,
	                FG ,
	                CorrCP ,
	                CorrFG ,
	                DesignKp ,
	                Discard ,
	                ROVID ,
	                ContactCP ,
	                CpCal
			        )
			SELECT ID ,
	                DTime ,
	                KP ,
	                East ,
	                North ,
	                CP ,
	                FG ,
	                CorrCP ,
	                CorrFG ,
	                DesignKp ,
	                Discard ,
	                ROVID ,
	                ContactCP ,
	                CpCal
				FROM @TableValue 
			SET IDENTITY_INSERT [CPLog] OFF
		END
		ELSE
        BEGIN
			INSERT INTO dbo.CPLog
			        ( DTime ,
	                KP ,
	                East ,
	                North ,
	                CP ,
	                FG ,
	                CorrCP ,
	                CorrFG ,
	                DesignKp ,
	                Discard ,
	                ROVID ,
	                ContactCP ,
	                CpCal
			        )
			SELECT DTime ,
	                KP ,
	                East ,
	                North ,
	                CP ,
	                FG ,
	                CorrCP ,
	                CorrFG ,
	                DesignKp ,
	                Discard ,
	                ROVID ,
	                ContactCP ,
	                CpCal
				FROM @TableValue
        END
		   
		COMMIT TRANSACTION Trans
	END TRY
    BEGIN CATCH
		ROLLBACK TRANSACTION Trans
	END CATCH
END



GO
/****** Object:  StoredProcedure [dbo].[IMP_DataFormats_Insert]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IMP_DataFormats_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[IMP_DataFormats_Insert] AS' 
END
GO
ALTER PROCEDURE [dbo].[IMP_DataFormats_Insert]
	@TableValue AS [IMP_DataFormatsType] READONLY ,
	@AllowDelete AS BIT ,
	@DateTimeFieldName AS NVARCHAR(250) = NULL ,
	@FromDate AS DATETIME = NULL ,
	@ToDate AS DATETIME = NULL
AS
BEGIN
	SET DATEFORMAT YMD;
	BEGIN TRANSACTION Trans
	BEGIN TRY
    	IF @AllowDelete = 1
		BEGIN
			DECLARE @SQLStatment AS NVARCHAR(1000)
			SET @SQLStatment = 'DELETE FROM [DataFormats] '
			IF @FromDate IS NOT NULL
            BEGIN
            	SET @SQLStatment = @SQLStatment + 'WHERE ' + @DateTimeFieldName + ' > ''' + CONVERT(NVARCHAR(25), @FromDate, 13) + ''''

				IF @ToDate IS NOT NULL               
					SET @SQLStatment = @SQLStatment + 'AND ' + @DateTimeFieldName + ' < ''' + CONVERT(NVARCHAR(25), @ToDate, 13) + ''''
            END
			EXECUTE (@SQLStatment)

			SET IDENTITY_INSERT [DataFormats] ON
			INSERT INTO dbo.DataFormats
			        ( PK_FormatID ,
					  FormatName
			        )
			SELECT PK_FormatID ,
	               FormatName
				FROM @TableValue 
			SET IDENTITY_INSERT [DataFormats] OFF
		END
		ELSE
        BEGIN
			INSERT INTO dbo.DataFormats
			        ( FormatName
			        )
			SELECT FormatName
				FROM @TableValue 
        END
		   
		COMMIT TRANSACTION Trans
	END TRY
    BEGIN CATCH
		ROLLBACK TRANSACTION Trans
	END CATCH
END



GO
/****** Object:  StoredProcedure [dbo].[IMP_DeletedEvents_Insert]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IMP_DeletedEvents_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[IMP_DeletedEvents_Insert] AS' 
END
GO

ALTER PROCEDURE [dbo].[IMP_DeletedEvents_Insert]
	@TableValue AS [IMP_DeletedEventsType] READONLY ,
	@AllowDelete AS BIT ,
	@DateTimeFieldName AS NVARCHAR(250) = NULL ,
	@FromDate AS DATETIME = NULL ,
	@ToDate AS DATETIME = NULL
AS
BEGIN
	SET DATEFORMAT YMD;
	BEGIN TRANSACTION Trans
	BEGIN TRY
    	IF @AllowDelete = 1
		BEGIN
			DECLARE @SQLStatment AS NVARCHAR(1000)
			SET @SQLStatment = 'DELETE FROM [DeletedEvents] '
			IF @FromDate IS NOT NULL
            BEGIN
            	SET @SQLStatment = @SQLStatment + 'WHERE ' + @DateTimeFieldName + ' > ''' + CONVERT(NVARCHAR(25), @FromDate, 13) + ''''

				IF @ToDate IS NOT NULL               
					SET @SQLStatment = @SQLStatment + 'AND ' + @DateTimeFieldName + ' < ''' + CONVERT(NVARCHAR(25), @ToDate, 13) + ''''
            END
			EXECUTE (@SQLStatment)

			--SET IDENTITY_INSERT [DeletedEvents] ON
			INSERT INTO dbo.DeletedEvents
			        ( ID ,
			          EventNum ,
			          DTime ,
			          PCode ,
			          SCode ,
			          KP ,
			          East ,
			          North ,
			          PipeDepth ,
			          SeabedDepth ,
			          BurialDepth ,
			          [Length] ,
			          Height ,
			          Width ,
			          CP ,
			          ProximityCP ,
			          ContactCP ,
			          CPFieldGradiant ,
			          Comment ,
			          Depth ,
			          ROVID ,
			          Active ,
			          Depletion ,
			          DepletionValue ,
			          Angle ,
			          ClockNotation ,
			          Distance ,
			          DistanceNotation ,
			          Contact ,
			          FieldJointNumber ,
			          CrossingName ,
			          CrossingClearance ,
			          CPReading ,
			          Anomaly ,
			          Discard ,
			          CustomComment ,
			          PiggyBackValue ,
			          Reviewed ,
			          EventQC ,
			          KP_Fixed ,
			          DesignKp ,
			          OldKP ,
			          DesignDCC ,
			          OldDCC ,
			          DCC ,
			          MeanSeabedDep ,
			          AdjSeabedDep ,
			          East_Fixed ,
			          North_Fixed ,
			          Loc ,
			          SurveyDir ,
			          LocNum ,
			          AnomalyNumber ,
			          AnomalyUniqueNumber ,
			          AnomalyCode ,
			          AnomalyClass ,
			          HourLocation ,
			          OldEvent ,
			          Dive_Number ,
			          AnodeSecure ,
			          MG_Coverage ,
			          MG_Thickness ,
			          PercentageBurial ,
			          Heading ,
			          HeadingDev
			        )
			SELECT ID ,
			          EventNum ,
			          DTime ,
			          PCode ,
			          SCode ,
			          KP ,
			          East ,
			          North ,
			          PipeDepth ,
			          SeabedDepth ,
			          BurialDepth ,
			          [Length] ,
			          Height ,
			          Width ,
			          CP ,
			          ProximityCP ,
			          ContactCP ,
			          CPFieldGradiant ,
			          Comment ,
			          Depth ,
			          ROVID ,
			          Active ,
			          Depletion ,
			          DepletionValue ,
			          Angle ,
			          ClockNotation ,
			          Distance ,
			          DistanceNotation ,
			          Contact ,
			          FieldJointNumber ,
			          CrossingName ,
			          CrossingClearance ,
			          CPReading ,
			          Anomaly ,
			          Discard ,
			          CustomComment ,
			          PiggyBackValue ,
			          Reviewed ,
			          EventQC ,
			          KP_Fixed ,
			          DesignKp ,
			          OldKP ,
			          DesignDCC ,
			          OldDCC ,
			          DCC ,
			          MeanSeabedDep ,
			          AdjSeabedDep ,
			          East_Fixed ,
			          North_Fixed ,
			          Loc ,
			          SurveyDir ,
			          LocNum ,
			          AnomalyNumber ,
			          AnomalyUniqueNumber ,
			          AnomalyCode ,
			          AnomalyClass ,
			          HourLocation ,
			          OldEvent ,
			          Dive_Number ,
			          AnodeSecure ,
			          MG_Coverage ,
			          MG_Thickness ,
			          PercentageBurial ,
			          Heading ,
			          HeadingDev
				FROM @TableValue 
			--SET IDENTITY_INSERT [DeletedEvents] OFF
		END
		ELSE
		BEGIN
			INSERT INTO dbo.DeletedEvents
			        ( ID ,
			          EventNum ,
			          DTime ,
			          PCode ,
			          SCode ,
			          KP ,
			          East ,
			          North ,
			          PipeDepth ,
			          SeabedDepth ,
			          BurialDepth ,
			          [Length] ,
			          Height ,
			          Width ,
			          CP ,
			          ProximityCP ,
			          ContactCP ,
			          CPFieldGradiant ,
			          Comment ,
			          Depth ,
			          ROVID ,
			          Active ,
			          Depletion ,
			          DepletionValue ,
			          Angle ,
			          ClockNotation ,
			          Distance ,
			          DistanceNotation ,
			          Contact ,
			          FieldJointNumber ,
			          CrossingName ,
			          CrossingClearance ,
			          CPReading ,
			          Anomaly ,
			          Discard ,
			          CustomComment ,
			          PiggyBackValue ,
			          Reviewed ,
			          EventQC ,
			          KP_Fixed ,
			          DesignKp ,
			          OldKP ,
			          DesignDCC ,
			          OldDCC ,
			          DCC ,
			          MeanSeabedDep ,
			          AdjSeabedDep ,
			          East_Fixed ,
			          North_Fixed ,
			          Loc ,
			          SurveyDir ,
			          LocNum ,
			          AnomalyNumber ,
			          AnomalyUniqueNumber ,
			          AnomalyCode ,
			          AnomalyClass ,
			          HourLocation ,
			          OldEvent ,
			          Dive_Number ,
			          AnodeSecure ,
			          MG_Coverage ,
			          MG_Thickness ,
			          PercentageBurial ,
			          Heading ,
			          HeadingDev
			        )
			SELECT ((SELECT ISNULL(MAX(ID), 0) FROM DeletedEvents ) + 1) AS [ID] ,
			          EventNum ,
			          DTime ,
			          PCode ,
			          SCode ,
			          KP ,
			          East ,
			          North ,
			          PipeDepth ,
			          SeabedDepth ,
			          BurialDepth ,
			          [Length] ,
			          Height ,
			          Width ,
			          CP ,
			          ProximityCP ,
			          ContactCP ,
			          CPFieldGradiant ,
			          Comment ,
			          Depth ,
			          ROVID ,
			          Active ,
			          Depletion ,
			          DepletionValue ,
			          Angle ,
			          ClockNotation ,
			          Distance ,
			          DistanceNotation ,
			          Contact ,
			          FieldJointNumber ,
			          CrossingName ,
			          CrossingClearance ,
			          CPReading ,
			          Anomaly ,
			          Discard ,
			          CustomComment ,
			          PiggyBackValue ,
			          Reviewed ,
			          EventQC ,
			          KP_Fixed ,
			          DesignKp ,
			          OldKP ,
			          DesignDCC ,
			          OldDCC ,
			          DCC ,
			          MeanSeabedDep ,
			          AdjSeabedDep ,
			          East_Fixed ,
			          North_Fixed ,
			          Loc ,
			          SurveyDir ,
			          LocNum ,
			          AnomalyNumber ,
			          AnomalyUniqueNumber ,
			          AnomalyCode ,
			          AnomalyClass ,
			          HourLocation ,
			          OldEvent ,
			          Dive_Number ,
			          AnodeSecure ,
			          MG_Coverage ,
			          MG_Thickness ,
			          PercentageBurial ,
			          Heading ,
			          HeadingDev
				FROM @TableValue 
		END
		  
		COMMIT TRANSACTION Trans
	END TRY
    BEGIN CATCH
		ROLLBACK TRANSACTION Trans
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[IMP_DepthLog_Insert]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IMP_DepthLog_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[IMP_DepthLog_Insert] AS' 
END
GO
ALTER PROCEDURE [dbo].[IMP_DepthLog_Insert]
		@TableValue AS [IMP_DepthLogType] READONLY ,
	@AllowDelete AS BIT ,
	@DateTimeFieldName AS NVARCHAR(250) = NULL ,
	@FromDate AS DATETIME = NULL ,
	@ToDate AS DATETIME = NULL
AS
BEGIN
	SET DATEFORMAT YMD;
	BEGIN TRANSACTION Trans
	BEGIN TRY
    	IF @AllowDelete = 1
		BEGIN
			DECLARE @SQLStatment AS NVARCHAR(1000)
			SET @SQLStatment = 'DELETE FROM [DepthLog] '
			IF @FromDate IS NOT NULL
            BEGIN
            	SET @SQLStatment = @SQLStatment + 'WHERE ' + @DateTimeFieldName + ' > ''' + CONVERT(NVARCHAR(25), @FromDate, 13) + ''''

				IF @ToDate IS NOT NULL               
					SET @SQLStatment = @SQLStatment + 'AND ' + @DateTimeFieldName + ' < ''' + CONVERT(NVARCHAR(25), @ToDate, 13) + ''''
            END
			EXECUTE (@SQLStatment)

			SET IDENTITY_INSERT [DepthLog] ON
			INSERT INTO dbo.DepthLog
			        ( ID ,
	               DTime ,
	               Depth ,
	               Altimeter ,
	               DepthTide ,
	               CorrDepthTide ,
	               CorrAltimeter ,
	               Discard ,
	               DepthAltKP ,
	               SSpeed ,
	               ROVID ,
	               DepthTideAlt ,
	               Tide
			        )
			SELECT ID ,
	               DTime ,
	               Depth ,
	               Altimeter ,
	               DepthTide ,
	               CorrDepthTide ,
	               CorrAltimeter ,
	               Discard ,
	               DepthAltKP ,
	               SSpeed ,
	               ROVID ,
	               DepthTideAlt ,
	               Tide
				FROM @TableValue 
			SET IDENTITY_INSERT [DepthLog] OFF
		END
		ELSE
        BEGIN
        	INSERT INTO dbo.DepthLog
			        ( DTime ,
	               Depth ,
	               Altimeter ,
	               DepthTide ,
	               CorrDepthTide ,
	               CorrAltimeter ,
	               Discard ,
	               DepthAltKP ,
	               SSpeed ,
	               ROVID ,
	               DepthTideAlt ,
	               Tide
			        )
			SELECT DTime ,
	               Depth ,
	               Altimeter ,
	               DepthTide ,
	               CorrDepthTide ,
	               CorrAltimeter ,
	               Discard ,
	               DepthAltKP ,
	               SSpeed ,
	               ROVID ,
	               DepthTideAlt ,
	               Tide
				FROM @TableValue 
		END		   

		COMMIT TRANSACTION Trans
	END TRY
    BEGIN CATCH
		ROLLBACK TRANSACTION Trans
	END CATCH
END



GO
/****** Object:  StoredProcedure [dbo].[IMP_DesignEventProc_Insert]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IMP_DesignEventProc_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[IMP_DesignEventProc_Insert] AS' 
END
GO
ALTER PROCEDURE [dbo].[IMP_DesignEventProc_Insert]
	@TableValue AS [IMP_DesignEventProcType] READONLY ,
	@AllowDelete AS BIT ,
	@DateTimeFieldName AS NVARCHAR(250) = NULL ,
	@FromDate AS DATETIME = NULL ,
	@ToDate AS DATETIME = NULL
AS
BEGIN
	SET DATEFORMAT YMD;
	BEGIN TRANSACTION Trans
	BEGIN TRY
    	IF @AllowDelete = 1
		BEGIN
			DECLARE @SQLStatment AS NVARCHAR(1000)
			SET @SQLStatment = 'DELETE FROM [DesignEventProc] '
			IF @FromDate IS NOT NULL
            BEGIN
            	SET @SQLStatment = @SQLStatment + 'WHERE ' + @DateTimeFieldName + ' > ''' + CONVERT(NVARCHAR(25), @FromDate, 13) + ''''

				IF @ToDate IS NOT NULL               
					SET @SQLStatment = @SQLStatment + 'AND ' + @DateTimeFieldName + ' < ''' + CONVERT(NVARCHAR(25), @ToDate, 13) + ''''
            END
			EXECUTE (@SQLStatment)

			SET IDENTITY_INSERT [DesignEventProc] ON	
			INSERT INTO dbo.DesignEventProc
			        ( ID ,
			       EventNum ,
			       DTime ,
			       PCode ,
			       SCode ,
			       KP ,
			       East ,
			       North ,
			       PipeDepth ,
			       SeabedDepth ,
			       BurialDepth ,
			       Length ,
			       Height ,
			       Width ,
			       CP ,
			       ProximityCP ,
			       Heading ,
			       HeadingDev ,
			       CPFieldGradiant ,
			       Comment ,
			       Depth ,
			       ROVID ,
			       Active ,
			       Depletion ,
			       DepletionValue ,
			       Angle ,
			       ClockNotation ,
			       Distance ,
			       DistanceNotation ,
			       Contact ,
			       FieldJointNumber ,
			       CrossingName ,
			       CrossingClearance ,
			       CPReading ,
			       Anomaly ,
			       Discard ,
			       CustomComment ,
			       PiggyBackValue ,
			       Reviewed ,
			       EventQC ,
			       KP_Fixed ,
			       DesignKp ,
			       OldKP ,
			       DesignDCC ,
			       OldDCC ,
			       DCC ,
			       AdjSeabedDep ,
			       MeanSeabedDep ,
			       North_Fixed ,
			       East_Fixed ,
			       Loc ,
			       SurveyDir ,
			       LocNum ,
			       AnomalyNumber ,
			       AnomalyUniqueNumber ,
			       AnomalyCode ,
			       AnomalyClass ,
			       HourLocation ,
			       OldEvent ,
			       Dive_Number ,
			       AnodeSecure ,
			       MG_Coverage ,
			       MG_Thickness ,
			       PercentageBurial
			        )
			SELECT ID ,
			       EventNum ,
			       DTime ,
			       PCode ,
			       SCode ,
			       KP ,
			       East ,
			       North ,
			       PipeDepth ,
			       SeabedDepth ,
			       BurialDepth ,
			       Length ,
			       Height ,
			       Width ,
			       CP ,
			       ProximityCP ,
			       Heading ,
			       HeadingDev ,
			       CPFieldGradiant ,
			       Comment ,
			       Depth ,
			       ROVID ,
			       Active ,
			       Depletion ,
			       DepletionValue ,
			       Angle ,
			       ClockNotation ,
			       Distance ,
			       DistanceNotation ,
			       Contact ,
			       FieldJointNumber ,
			       CrossingName ,
			       CrossingClearance ,
			       CPReading ,
			       Anomaly ,
			       Discard ,
			       CustomComment ,
			       PiggyBackValue ,
			       Reviewed ,
			       EventQC ,
			       KP_Fixed ,
			       DesignKp ,
			       OldKP ,
			       DesignDCC ,
			       OldDCC ,
			       DCC ,
			       AdjSeabedDep ,
			       MeanSeabedDep ,
			       North_Fixed ,
			       East_Fixed ,
			       Loc ,
			       SurveyDir ,
			       LocNum ,
			       AnomalyNumber ,
			       AnomalyUniqueNumber ,
			       AnomalyCode ,
			       AnomalyClass ,
			       HourLocation ,
			       OldEvent ,
			       Dive_Number ,
			       AnodeSecure ,
			       MG_Coverage ,
			       MG_Thickness ,
			       PercentageBurial
				FROM @TableValue 
			SET IDENTITY_INSERT [DesignEventProc] OFF
		END
		ELSE
        BEGIN
			INSERT INTO dbo.DesignEventProc
			        ( EventNum ,
	               DTime ,
	               PCode ,
	               SCode ,
	               KP ,
	               East ,
	               North ,
	               PipeDepth ,
	               SeabedDepth ,
	               BurialDepth ,
	               Length ,
	               Height ,
	               Width ,
	               CP ,
	               ProximityCP ,
	               Heading ,
	               HeadingDev ,
	               CPFieldGradiant ,
	               Comment ,
	               Depth ,
	               ROVID ,
	               Active ,
	               Depletion ,
	               DepletionValue ,
	               Angle ,
	               ClockNotation ,
	               Distance ,
	               DistanceNotation ,
	               Contact ,
	               FieldJointNumber ,
	               CrossingName ,
	               CrossingClearance ,
	               CPReading ,
	               Anomaly ,
	               Discard ,
	               CustomComment ,
	               PiggyBackValue ,
	               Reviewed ,
	               EventQC ,
	               KP_Fixed ,
	               DesignKp ,
	               OldKP ,
	               DesignDCC ,
	               OldDCC ,
	               DCC ,
	               AdjSeabedDep ,
	               MeanSeabedDep ,
	               North_Fixed ,
	               East_Fixed ,
	               Loc ,
	               SurveyDir ,
	               LocNum ,
	               AnomalyNumber ,
	               AnomalyUniqueNumber ,
	               AnomalyCode ,
	               AnomalyClass ,
	               HourLocation ,
	               OldEvent ,
	               Dive_Number ,
	               AnodeSecure ,
	               MG_Coverage ,
	               MG_Thickness ,
	               PercentageBurial
			        )
			SELECT EventNum ,
	               DTime ,
	               PCode ,
	               SCode ,
	               KP ,
	               East ,
	               North ,
	               PipeDepth ,
	               SeabedDepth ,
	               BurialDepth ,
	               Length ,
	               Height ,
	               Width ,
	               CP ,
	               ProximityCP ,
	               Heading ,
	               HeadingDev ,
	               CPFieldGradiant ,
	               Comment ,
	               Depth ,
	               ROVID ,
	               Active ,
	               Depletion ,
	               DepletionValue ,
	               Angle ,
	               ClockNotation ,
	               Distance ,
	               DistanceNotation ,
	               Contact ,
	               FieldJointNumber ,
	               CrossingName ,
	               CrossingClearance ,
	               CPReading ,
	               Anomaly ,
	               Discard ,
	               CustomComment ,
	               PiggyBackValue ,
	               Reviewed ,
	               EventQC ,
	               KP_Fixed ,
	               DesignKp ,
	               OldKP ,
	               DesignDCC ,
	               OldDCC ,
	               DCC ,
	               AdjSeabedDep ,
	               MeanSeabedDep ,
	               North_Fixed ,
	               East_Fixed ,
	               Loc ,
	               SurveyDir ,
	               LocNum ,
	               AnomalyNumber ,
	               AnomalyUniqueNumber ,
	               AnomalyCode ,
	               AnomalyClass ,
	               HourLocation ,
	               OldEvent ,
	               Dive_Number ,
	               AnodeSecure ,
	               MG_Coverage ,
	               MG_Thickness ,
	               PercentageBurial
				FROM @TableValue 
        END
		   
		COMMIT TRANSACTION Trans
	END TRY
    BEGIN CATCH
		ROLLBACK TRANSACTION Trans
	END CATCH
END



GO
/****** Object:  StoredProcedure [dbo].[IMP_DesignPosProc_Insert]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IMP_DesignPosProc_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[IMP_DesignPosProc_Insert] AS' 
END
GO
ALTER PROCEDURE [dbo].[IMP_DesignPosProc_Insert]
	@TableValue AS [IMP_DesignPosProcType] READONLY ,
	@AllowDelete AS BIT ,
	@DateTimeFieldName AS NVARCHAR(250) = NULL ,
	@FromDate AS DATETIME = NULL ,
	@ToDate AS DATETIME = NULL
AS
BEGIN
	SET DATEFORMAT YMD;
	BEGIN TRANSACTION Trans
	BEGIN TRY
    	IF @AllowDelete = 1
		BEGIN
			DECLARE @SQLStatment AS NVARCHAR(1000)
			SET @SQLStatment = 'DELETE FROM [DesignPosProc] '
			IF @FromDate IS NOT NULL
            BEGIN
            	SET @SQLStatment = @SQLStatment + 'WHERE ' + @DateTimeFieldName + ' > ''' + CONVERT(NVARCHAR(25), @FromDate, 13) + ''''

				IF @ToDate IS NOT NULL               
					SET @SQLStatment = @SQLStatment + 'AND ' + @DateTimeFieldName + ' < ''' + CONVERT(NVARCHAR(25), @ToDate, 13) + ''''
            END
			EXECUTE (@SQLStatment)

			SET IDENTITY_INSERT [DesignPosProc] ON
			INSERT INTO dbo.DesignPosProc
			        ( ID ,
	               DTime ,
	               FixNum ,
	               North ,
	               East ,
	               Kp ,
	               Depth ,
	               Heading ,
	               Pitch ,
	               Roll ,
	               Logged ,
	               HeadingDev ,
	               Discard ,
	               Digitized ,
	               ROVID ,
	               DesignKp ,
	               OldKP ,
	               DesignDCC ,
	               OldDCC ,
	               [De-spiked] ,
	               DCC ,
	               Long ,
	               LAT
			        )
			SELECT ID ,
	               DTime ,
	               FixNum ,
	               North ,
	               East ,
	               Kp ,
	               Depth ,
	               Heading ,
	               Pitch ,
	               Roll ,
	               Logged ,
	               HeadingDev ,
	               Discard ,
	               Digitized ,
	               ROVID ,
	               DesignKp ,
	               OldKP ,
	               DesignDCC ,
	               OldDCC ,
	               [De-spiked] ,
	               DCC ,
	               Long ,
	               LAT
				FROM @TableValue 
			SET IDENTITY_INSERT [DesignPosProc] OFF
		END
		ELSE
        BEGIN
			INSERT INTO dbo.DesignPosProc
			        ( DTime ,
	               FixNum ,
	               North ,
	               East ,
	               Kp ,
	               Depth ,
	               Heading ,
	               Pitch ,
	               Roll ,
	               Logged ,
	               HeadingDev ,
	               Discard ,
	               Digitized ,
	               ROVID ,
	               DesignKp ,
	               OldKP ,
	               DesignDCC ,
	               OldDCC ,
	               [De-spiked] ,
	               DCC ,
	               Long ,
	               LAT
			        )
			SELECT DTime ,
	               FixNum ,
	               North ,
	               East ,
	               Kp ,
	               Depth ,
	               Heading ,
	               Pitch ,
	               Roll ,
	               Logged ,
	               HeadingDev ,
	               Discard ,
	               Digitized ,
	               ROVID ,
	               DesignKp ,
	               OldKP ,
	               DesignDCC ,
	               OldDCC ,
	               [De-spiked] ,
	               DCC ,
	               Long ,
	               LAT
				FROM @TableValue 
        END
		  
		COMMIT TRANSACTION Trans
	END TRY
    BEGIN CATCH
		ROLLBACK TRANSACTION Trans
	END CATCH
END



GO
/****** Object:  StoredProcedure [dbo].[IMP_DesignScanProc_Insert]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IMP_DesignScanProc_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[IMP_DesignScanProc_Insert] AS' 
END
GO
ALTER PROCEDURE [dbo].[IMP_DesignScanProc_Insert]
	@TableValue AS [IMP_DesignScanProcType] READONLY ,
	@AllowDelete AS BIT ,
	@DateTimeFieldName AS NVARCHAR(250) = NULL ,
	@FromDate AS DATETIME = NULL ,
	@ToDate AS DATETIME = NULL
AS
BEGIN
	SET DATEFORMAT YMD;
	BEGIN TRANSACTION Trans
	BEGIN TRY
    	IF @AllowDelete = 1
		BEGIN
			DECLARE @SQLStatment AS NVARCHAR(1000)
			SET @SQLStatment = 'DELETE FROM [DesignScanProc] '
			IF @FromDate IS NOT NULL
            BEGIN
            	SET @SQLStatment = @SQLStatment + 'WHERE ' + @DateTimeFieldName + ' > ''' + CONVERT(NVARCHAR(25), @FromDate, 13) + ''''

				IF @ToDate IS NOT NULL               
					SET @SQLStatment = @SQLStatment + 'AND ' + @DateTimeFieldName + ' < ''' + CONVERT(NVARCHAR(25), @ToDate, 13) + ''''
            END
			EXECUTE (@SQLStatment)

			SET IDENTITY_INSERT [DesignScanProc] ON
			INSERT INTO dbo.DesignScanProc
			        ( ID ,
	               DTime ,
	               ScanNum ,
	               East ,
	               North ,
	               KP ,
	               Pitch ,
	               Roll ,
	               Heading ,
	               MasterNumScans ,
	               SlaveNumScans ,
	               MasterAngStart ,
	               SlaveAngStart ,
	               MasterAngStep ,
	               SlaveAngStep ,
	               PipeDepth ,
	               SeaBedDep ,
	               PipeDeltaY ,
	               ScanData1 ,
	               DeltaX1 ,
	               DeltaY1 ,
	               DeltaZ1 ,
	               RX1 ,
	               RY1 ,
	               RZ1 ,
	               ScanData2 ,
	               DeltaX2 ,
	               DeltaY2 ,
	               DeltaZ2 ,
	               RX2 ,
	               RY2 ,
	               RZ2 ,
	               Delta2CorY ,
	               Delta2CorZ ,
	               R2CorZ ,
	               HeadingDev ,
	               SSpeed ,
	               SeaBedLHS1Dep ,
	               SeaBedLHS2Dep ,
	               SeaBedLHS3Dep ,
	               SeaBedLHS4Dep ,
	               FProc ,
	               FChart ,
	               FFProc ,
	               Discard ,
	               Depth ,
	               KPDifference ,
	               ScanExtraData1 ,
	               ScanExtraData2 ,
	               SeaBedRHS1Dep ,
	               SeaBedRHS2Dep ,
	               SeaBedRHS3Dep ,
	               SeaBedRHS4Dep ,
	               ROVID ,
	               SeaBedLHS1S ,
	               SeaBedLHS2S ,
	               SeaBedLHS3S ,
	               SeaBedLHS4S ,
	               SeaBedLHS1E ,
	               SeaBedLHS2E ,
	               SeaBedLHS3E ,
	               SeaBedLHS4E ,
	               SeaBedRHS1S ,
	               SeaBedRHS2S ,
	               SeaBedRHS3S ,
	               SeaBedRHS4S ,
	               SeaBedRHS1E ,
	               SeaBedRHS2E ,
	               SeaBedRHS3E ,
	               SeaBedRHS4E ,
	               SeaBed0Range ,
	               AverageSeabedDepth ,
	               DoNotShowPipe ,
	               PTProc ,
	               SmoothedPipe ,
	               SmoothedSeabed ,
	               PTAdded ,
	               PipeDiameter ,
	               DesignKp ,
	               OldKP ,
	               DesignDCC ,
	               OldDCC ,
	               DCC ,
	               HeadEast ,
	               HeadNorth ,
	               CorrEast ,
	               CorrNorth
			        )
			SELECT ID ,
	               DTime ,
	               ScanNum ,
	               East ,
	               North ,
	               KP ,
	               Pitch ,
	               Roll ,
	               Heading ,
	               MasterNumScans ,
	               SlaveNumScans ,
	               MasterAngStart ,
	               SlaveAngStart ,
	               MasterAngStep ,
	               SlaveAngStep ,
	               PipeDepth ,
	               SeaBedDep ,
	               PipeDeltaY ,
	               ScanData1 ,
	               DeltaX1 ,
	               DeltaY1 ,
	               DeltaZ1 ,
	               RX1 ,
	               RY1 ,
	               RZ1 ,
	               ScanData2 ,
	               DeltaX2 ,
	               DeltaY2 ,
	               DeltaZ2 ,
	               RX2 ,
	               RY2 ,
	               RZ2 ,
	               Delta2CorY ,
	               Delta2CorZ ,
	               R2CorZ ,
	               HeadingDev ,
	               SSpeed ,
	               SeaBedLHS1Dep ,
	               SeaBedLHS2Dep ,
	               SeaBedLHS3Dep ,
	               SeaBedLHS4Dep ,
	               FProc ,
	               FChart ,
	               FFProc ,
	               Discard ,
	               Depth ,
	               KPDifference ,
	               ScanExtraData1 ,
	               ScanExtraData2 ,
	               SeaBedRHS1Dep ,
	               SeaBedRHS2Dep ,
	               SeaBedRHS3Dep ,
	               SeaBedRHS4Dep ,
	               ROVID ,
	               SeaBedLHS1S ,
	               SeaBedLHS2S ,
	               SeaBedLHS3S ,
	               SeaBedLHS4S ,
	               SeaBedLHS1E ,
	               SeaBedLHS2E ,
	               SeaBedLHS3E ,
	               SeaBedLHS4E ,
	               SeaBedRHS1S ,
	               SeaBedRHS2S ,
	               SeaBedRHS3S ,
	               SeaBedRHS4S ,
	               SeaBedRHS1E ,
	               SeaBedRHS2E ,
	               SeaBedRHS3E ,
	               SeaBedRHS4E ,
	               SeaBed0Range ,
	               AverageSeabedDepth ,
	               DoNotShowPipe ,
	               PTProc ,
	               SmoothedPipe ,
	               SmoothedSeabed ,
	               PTAdded ,
	               PipeDiameter ,
	               DesignKp ,
	               OldKP ,
	               DesignDCC ,
	               OldDCC ,
	               DCC ,
	               HeadEast ,
	               HeadNorth ,
	               CorrEast ,
	               CorrNorth
				FROM @TableValue 
			SET IDENTITY_INSERT [DesignScanProc] OFF
		END
		ELSE
        BEGIN
			INSERT INTO dbo.DesignScanProc
			        ( DTime ,
	               ScanNum ,
	               East ,
	               North ,
	               KP ,
	               Pitch ,
	               Roll ,
	               Heading ,
	               MasterNumScans ,
	               SlaveNumScans ,
	               MasterAngStart ,
	               SlaveAngStart ,
	               MasterAngStep ,
	               SlaveAngStep ,
	               PipeDepth ,
	               SeaBedDep ,
	               PipeDeltaY ,
	               ScanData1 ,
	               DeltaX1 ,
	               DeltaY1 ,
	               DeltaZ1 ,
	               RX1 ,
	               RY1 ,
	               RZ1 ,
	               ScanData2 ,
	               DeltaX2 ,
	               DeltaY2 ,
	               DeltaZ2 ,
	               RX2 ,
	               RY2 ,
	               RZ2 ,
	               Delta2CorY ,
	               Delta2CorZ ,
	               R2CorZ ,
	               HeadingDev ,
	               SSpeed ,
	               SeaBedLHS1Dep ,
	               SeaBedLHS2Dep ,
	               SeaBedLHS3Dep ,
	               SeaBedLHS4Dep ,
	               FProc ,
	               FChart ,
	               FFProc ,
	               Discard ,
	               Depth ,
	               KPDifference ,
	               ScanExtraData1 ,
	               ScanExtraData2 ,
	               SeaBedRHS1Dep ,
	               SeaBedRHS2Dep ,
	               SeaBedRHS3Dep ,
	               SeaBedRHS4Dep ,
	               ROVID ,
	               SeaBedLHS1S ,
	               SeaBedLHS2S ,
	               SeaBedLHS3S ,
	               SeaBedLHS4S ,
	               SeaBedLHS1E ,
	               SeaBedLHS2E ,
	               SeaBedLHS3E ,
	               SeaBedLHS4E ,
	               SeaBedRHS1S ,
	               SeaBedRHS2S ,
	               SeaBedRHS3S ,
	               SeaBedRHS4S ,
	               SeaBedRHS1E ,
	               SeaBedRHS2E ,
	               SeaBedRHS3E ,
	               SeaBedRHS4E ,
	               SeaBed0Range ,
	               AverageSeabedDepth ,
	               DoNotShowPipe ,
	               PTProc ,
	               SmoothedPipe ,
	               SmoothedSeabed ,
	               PTAdded ,
	               PipeDiameter ,
	               DesignKp ,
	               OldKP ,
	               DesignDCC ,
	               OldDCC ,
	               DCC ,
	               HeadEast ,
	               HeadNorth ,
	               CorrEast ,
	               CorrNorth
			        )
			SELECT DTime ,
	               ScanNum ,
	               East ,
	               North ,
	               KP ,
	               Pitch ,
	               Roll ,
	               Heading ,
	               MasterNumScans ,
	               SlaveNumScans ,
	               MasterAngStart ,
	               SlaveAngStart ,
	               MasterAngStep ,
	               SlaveAngStep ,
	               PipeDepth ,
	               SeaBedDep ,
	               PipeDeltaY ,
	               ScanData1 ,
	               DeltaX1 ,
	               DeltaY1 ,
	               DeltaZ1 ,
	               RX1 ,
	               RY1 ,
	               RZ1 ,
	               ScanData2 ,
	               DeltaX2 ,
	               DeltaY2 ,
	               DeltaZ2 ,
	               RX2 ,
	               RY2 ,
	               RZ2 ,
	               Delta2CorY ,
	               Delta2CorZ ,
	               R2CorZ ,
	               HeadingDev ,
	               SSpeed ,
	               SeaBedLHS1Dep ,
	               SeaBedLHS2Dep ,
	               SeaBedLHS3Dep ,
	               SeaBedLHS4Dep ,
	               FProc ,
	               FChart ,
	               FFProc ,
	               Discard ,
	               Depth ,
	               KPDifference ,
	               ScanExtraData1 ,
	               ScanExtraData2 ,
	               SeaBedRHS1Dep ,
	               SeaBedRHS2Dep ,
	               SeaBedRHS3Dep ,
	               SeaBedRHS4Dep ,
	               ROVID ,
	               SeaBedLHS1S ,
	               SeaBedLHS2S ,
	               SeaBedLHS3S ,
	               SeaBedLHS4S ,
	               SeaBedLHS1E ,
	               SeaBedLHS2E ,
	               SeaBedLHS3E ,
	               SeaBedLHS4E ,
	               SeaBedRHS1S ,
	               SeaBedRHS2S ,
	               SeaBedRHS3S ,
	               SeaBedRHS4S ,
	               SeaBedRHS1E ,
	               SeaBedRHS2E ,
	               SeaBedRHS3E ,
	               SeaBedRHS4E ,
	               SeaBed0Range ,
	               AverageSeabedDepth ,
	               DoNotShowPipe ,
	               PTProc ,
	               SmoothedPipe ,
	               SmoothedSeabed ,
	               PTAdded ,
	               PipeDiameter ,
	               DesignKp ,
	               OldKP ,
	               DesignDCC ,
	               OldDCC ,
	               DCC ,
	               HeadEast ,
	               HeadNorth ,
	               CorrEast ,
	               CorrNorth
				FROM @TableValue 
        END
		   
		COMMIT TRANSACTION Trans
	END TRY
    BEGIN CATCH
		ROLLBACK TRANSACTION Trans
	END CATCH
END



GO
/****** Object:  StoredProcedure [dbo].[IMP_DigitizedPipe_Insert]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IMP_DigitizedPipe_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[IMP_DigitizedPipe_Insert] AS' 
END
GO
ALTER PROCEDURE [dbo].[IMP_DigitizedPipe_Insert]
	@TableValue AS [IMP_DigitizedPipeType] READONLY ,
	@AllowDelete AS BIT ,
	@DateTimeFieldName AS NVARCHAR(250) = NULL ,
	@FromDate AS DATETIME = NULL ,
	@ToDate AS DATETIME = NULL
AS
BEGIN
	SET DATEFORMAT YMD;
	BEGIN TRANSACTION Trans
	BEGIN TRY
    	IF @AllowDelete = 1
		BEGIN
			DECLARE @SQLStatment AS NVARCHAR(1000)
			SET @SQLStatment = 'DELETE FROM [DigitizedPipe] '
			IF @FromDate IS NOT NULL
            BEGIN
            	SET @SQLStatment = @SQLStatment + 'WHERE ' + @DateTimeFieldName + ' > ''' + CONVERT(NVARCHAR(25), @FromDate, 13) + ''''

				IF @ToDate IS NOT NULL               
					SET @SQLStatment = @SQLStatment + 'AND ' + @DateTimeFieldName + ' < ''' + CONVERT(NVARCHAR(25), @ToDate, 13) + ''''
            END
			EXECUTE (@SQLStatment)

			SET IDENTITY_INSERT [DigitizedPipe] ON
			INSERT INTO dbo.DigitizedPipe
			        ( ID ,
			       East ,
			       North ,
			       KP ,
			       Discard
			        )
			SELECT ID ,
			       East ,
			       North ,
			       KP ,
			       Discard
				FROM @TableValue 
			SET IDENTITY_INSERT [DigitizedPipe] OFF
		END
		ELSE
        BEGIN
			INSERT INTO dbo.DigitizedPipe
			        ( East ,
			       North ,
			       KP ,
			       Discard
			        )
			SELECT East ,
			       North ,
			       KP ,
			       Discard
				FROM @TableValue 
        END

		COMMIT TRANSACTION Trans
	END TRY
    BEGIN CATCH
		ROLLBACK TRANSACTION Trans
	END CATCH
END



GO
/****** Object:  StoredProcedure [dbo].[IMP_Digitizedprofile_Insert]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IMP_Digitizedprofile_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[IMP_Digitizedprofile_Insert] AS' 
END
GO
ALTER PROCEDURE [dbo].[IMP_Digitizedprofile_Insert]
	@TableValue AS [IMP_DigitizedprofileType] READONLY ,
	@AllowDelete AS BIT ,
	@DateTimeFieldName AS NVARCHAR(250) = NULL ,
	@FromDate AS DATETIME = NULL ,
	@ToDate AS DATETIME = NULL
AS
BEGIN
	SET DATEFORMAT YMD;
	BEGIN TRANSACTION Trans
	BEGIN TRY
    	IF @AllowDelete = 1
		BEGIN
			DECLARE @SQLStatment AS NVARCHAR(1000)
			SET @SQLStatment = 'DELETE FROM [Digitizedprofile] '
			IF @FromDate IS NOT NULL
            BEGIN
            	SET @SQLStatment = @SQLStatment + 'WHERE ' + @DateTimeFieldName + ' > ''' + CONVERT(NVARCHAR(25), @FromDate, 13) + ''''

				IF @ToDate IS NOT NULL               
					SET @SQLStatment = @SQLStatment + 'AND ' + @DateTimeFieldName + ' < ''' + CONVERT(NVARCHAR(25), @ToDate, 13) + ''''
            END
			EXECUTE (@SQLStatment)

			--SET IDENTITY_INSERT [Digitizedprofile] ON
			INSERT INTO dbo.Digitizedprofile
			        ( ID ,
			       East ,
			       North ,
			       KP
			        )
			SELECT ID ,
			       East ,
			       North ,
			       KP
				FROM @TableValue 
			--SET IDENTITY_INSERT [Digitizedprofile] OFF
		END
		ELSE
        BEGIN
			INSERT INTO dbo.Digitizedprofile
			        ( ID ,
			       East ,
			       North ,
			       KP
			        )
			SELECT ((SELECT ISNULL(MAX(ID), 0) FROM Digitizedprofile ) + 1) AS [ID] ,
			       East ,
			       North ,
			       KP
				FROM @TableValue 
        END

		COMMIT TRANSACTION Trans
	END TRY
    BEGIN CATCH
		ROLLBACK TRANSACTION Trans
	END CATCH
END



GO
/****** Object:  StoredProcedure [dbo].[IMP_DigitizedSeaBed_Insert]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IMP_DigitizedSeaBed_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[IMP_DigitizedSeaBed_Insert] AS' 
END
GO
ALTER PROCEDURE [dbo].[IMP_DigitizedSeaBed_Insert]
	@TableValue AS [IMP_DigitizedSeaBedType] READONLY ,
	@AllowDelete AS BIT ,
	@DateTimeFieldName AS NVARCHAR(250) = NULL ,
	@FromDate AS DATETIME = NULL ,
	@ToDate AS DATETIME = NULL
AS
BEGIN
	SET DATEFORMAT YMD;
	BEGIN TRANSACTION Trans
	BEGIN TRY
    	IF @AllowDelete = 1
		BEGIN
			DECLARE @SQLStatment AS NVARCHAR(1000)
			SET @SQLStatment = 'DELETE FROM [DigitizedSeaBed] '
			IF @FromDate IS NOT NULL
            BEGIN
            	SET @SQLStatment = @SQLStatment + 'WHERE ' + @DateTimeFieldName + ' > ''' + CONVERT(NVARCHAR(25), @FromDate, 13) + ''''

				IF @ToDate IS NOT NULL               
					SET @SQLStatment = @SQLStatment + 'AND ' + @DateTimeFieldName + ' < ''' + CONVERT(NVARCHAR(25), @ToDate, 13) + ''''
            END
			EXECUTE (@SQLStatment)
			--SET IDENTITY_INSERT [DigitizedSeaBed] ON
			INSERT INTO dbo.DigitizedSeaBed
			        ( ID ,
			       East ,
			       North ,
			       KP
			        )
			SELECT ID ,
			       East ,
			       North ,
			       KP
				FROM @TableValue 
			--SET IDENTITY_INSERT [DigitizedSeaBed] OFF
		END
		ELSE
		BEGIN
			INSERT INTO dbo.DigitizedSeaBed
			        ( ID ,
			       East ,
			       North ,
			       KP
			        )
			SELECT ((SELECT ISNULL(MAX(ID), 0) FROM DigitizedSeaBed) + 1) AS [ID] ,
			       East ,
			       North ,
			       KP
				FROM @TableValue 
		END

		COMMIT TRANSACTION Trans
	END TRY
    BEGIN CATCH
		ROLLBACK TRANSACTION Trans
	END CATCH
END



GO
/****** Object:  StoredProcedure [dbo].[IMP_DopplerLog_Insert]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IMP_DopplerLog_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[IMP_DopplerLog_Insert] AS' 
END
GO
ALTER PROCEDURE [dbo].[IMP_DopplerLog_Insert]
	@TableValue AS [IMP_DopplerLogType] READONLY ,
	@AllowDelete AS BIT ,
	@DateTimeFieldName AS NVARCHAR(250) = NULL ,
	@FromDate AS DATETIME = NULL ,
	@ToDate AS DATETIME = NULL
AS
BEGIN
	SET DATEFORMAT YMD;
   	BEGIN TRANSACTION Trans
	BEGIN TRY
    	IF @AllowDelete = 1
		BEGIN
			DECLARE @SQLStatment AS NVARCHAR(1000)
			SET @SQLStatment = 'DELETE FROM [DopplerLog] '
			IF @FromDate IS NOT NULL
            BEGIN
            	SET @SQLStatment = @SQLStatment + 'WHERE ' + @DateTimeFieldName + ' > ''' + CONVERT(NVARCHAR(25), @FromDate, 13) + ''''

				IF @ToDate IS NOT NULL               
					SET @SQLStatment = @SQLStatment + 'AND ' + @DateTimeFieldName + ' < ''' + CONVERT(NVARCHAR(25), @ToDate, 13) + ''''
            END
			EXECUTE (@SQLStatment)

			SET IDENTITY_INSERT [DopplerLog] ON
			INSERT INTO dbo.DopplerLog
			        ( DTime ,
			       Pitch ,
			       Roll ,
			       Heading ,
			       ShipX ,
			       ShipY ,
			       ShipZ ,
			       InsX ,
			       InsY ,
			       InsZ ,
			       EarthX ,
			       EarthY ,
			       EarthZ ,
			       qc ,
			       ID ,
			       DopplerDTime ,
			       Salinity ,
			       Temperature ,
			       Depth ,
			       SoundSpeed ,
			       DopplerBIT ,
			       IErrorVelocity ,
			       IStatusVelocity ,
			       SStatusVelocity ,
			       EStatusVelocity ,
			       EDNorth ,
			       EDUpward ,
			       EDRange ,
			       EDTime ,
			       ROVID
			        )
			SELECT DTime ,
			       Pitch ,
			       Roll ,
			       Heading ,
			       ShipX ,
			       ShipY ,
			       ShipZ ,
			       InsX ,
			       InsY ,
			       InsZ ,
			       EarthX ,
			       EarthY ,
			       EarthZ ,
			       qc ,
			       ID ,
			       DopplerDTime ,
			       Salinity ,
			       Temperature ,
			       Depth ,
			       SoundSpeed ,
			       DopplerBIT ,
			       IErrorVelocity ,
			       IStatusVelocity ,
			       SStatusVelocity ,
			       EStatusVelocity ,
			       EDNorth ,
			       EDUpward ,
			       EDRange ,
			       EDTime ,
			       ROVID
				FROM @TableValue 
			SET IDENTITY_INSERT [DopplerLog] OFF
		END
		ELSE
        BEGIN
			INSERT INTO dbo.DopplerLog
			        ( DTime ,
			       Pitch ,
			       Roll ,
			       Heading ,
			       ShipX ,
			       ShipY ,
			       ShipZ ,
			       InsX ,
			       InsY ,
			       InsZ ,
			       EarthX ,
			       EarthY ,
			       EarthZ ,
			       qc ,
			       DopplerDTime ,
			       Salinity ,
			       Temperature ,
			       Depth ,
			       SoundSpeed ,
			       DopplerBIT ,
			       IErrorVelocity ,
			       IStatusVelocity ,
			       SStatusVelocity ,
			       EStatusVelocity ,
			       EDNorth ,
			       EDUpward ,
			       EDRange ,
			       EDTime ,
			       ROVID
			        )
			SELECT DTime ,
			       Pitch ,
			       Roll ,
			       Heading ,
			       ShipX ,
			       ShipY ,
			       ShipZ ,
			       InsX ,
			       InsY ,
			       InsZ ,
			       EarthX ,
			       EarthY ,
			       EarthZ ,
			       qc ,
			       DopplerDTime ,
			       Salinity ,
			       Temperature ,
			       Depth ,
			       SoundSpeed ,
			       DopplerBIT ,
			       IErrorVelocity ,
			       IStatusVelocity ,
			       SStatusVelocity ,
			       EStatusVelocity ,
			       EDNorth ,
			       EDUpward ,
			       EDRange ,
			       EDTime ,
			       ROVID
				FROM @TableValue 
        END
		
		COMMIT TRANSACTION Trans
	END TRY
    BEGIN CATCH
		ROLLBACK TRANSACTION Trans
	END CATCH
END



GO
/****** Object:  StoredProcedure [dbo].[IMP_EMMScanLog_Insert]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IMP_EMMScanLog_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[IMP_EMMScanLog_Insert] AS' 
END
GO

ALTER PROCEDURE [dbo].[IMP_EMMScanLog_Insert]
	@TableValue AS [IMP_EMMScanLogType] READONLY ,
	@AllowDelete AS BIT ,
	@DateTimeFieldName AS NVARCHAR(250) = NULL ,
	@FromDate AS DATETIME = NULL ,
	@ToDate AS DATETIME = NULL
AS
BEGIN
	SET DATEFORMAT YMD;
	BEGIN TRANSACTION Trans
	BEGIN TRY
    	IF @AllowDelete = 1
		BEGIN
			DECLARE @SQLStatment AS NVARCHAR(1000)
			SET @SQLStatment = 'DELETE FROM [EMMScanLog] '
			IF @FromDate IS NOT NULL
            BEGIN
            	SET @SQLStatment = @SQLStatment + 'WHERE ' + @DateTimeFieldName + ' > ''' + CONVERT(NVARCHAR(25), @FromDate, 13) + ''''

				IF @ToDate IS NOT NULL               
					SET @SQLStatment = @SQLStatment + 'AND ' + @DateTimeFieldName + ' < ''' + CONVERT(NVARCHAR(25), @ToDate, 13) + ''''
            END
			EXECUTE (@SQLStatment)

			SET IDENTITY_INSERT [EMMScanLog] ON
			INSERT INTO dbo.EMMScanLog
			        ( ID ,
			       em_model ,
			       CompTime ,
			       DataTime ,
			       ping_counter ,
			       serial_number ,
			       transmit_sectors ,
			       valid_beams ,
			       sampling_frequency ,
			       rov_depth ,
			       sound_speed ,
			       max_num_beams ,
			       spare1 ,
			       spare2 ,
			       tilt_angle ,
			       focus_range ,
			       signal_length ,
			       transmit_time_offset ,
			       center_frequency ,
			       bandwidth ,
			       signal_waveform ,
			       transmit_sector_number ,
			       beam_pointing_angle ,
			       range ,
			       trans_sector_num ,
			       reflectivity ,
			       quality_factor ,
			       detection_window ,
			       beam_no ,
			       spare ,
			       total_beams ,
			       offsetx ,
			       offsety ,
			       rotation ,
			       ROVID
			        )
			SELECT ID ,
			       em_model ,
			       CompTime ,
			       DataTime ,
			       ping_counter ,
			       serial_number ,
			       transmit_sectors ,
			       valid_beams ,
			       sampling_frequency ,
			       rov_depth ,
			       sound_speed ,
			       max_num_beams ,
			       spare1 ,
			       spare2 ,
			       tilt_angle ,
			       focus_range ,
			       signal_length ,
			       transmit_time_offset ,
			       center_frequency ,
			       bandwidth ,
			       signal_waveform ,
			       transmit_sector_number ,
			       beam_pointing_angle ,
			       range ,
			       trans_sector_num ,
			       reflectivity ,
			       quality_factor ,
			       detection_window ,
			       beam_no ,
			       spare ,
			       total_beams ,
			       offsetx ,
			       offsety ,
			       rotation ,
			       ROVID
				FROM @TableValue 
			SET IDENTITY_INSERT [EMMScanLog] OFF
		END
		ELSE
        BEGIN
			INSERT INTO dbo.EMMScanLog
			        ( ID ,
			       em_model ,
			       CompTime ,
			       DataTime ,
			       ping_counter ,
			       serial_number ,
			       transmit_sectors ,
			       valid_beams ,
			       sampling_frequency ,
			       rov_depth ,
			       sound_speed ,
			       max_num_beams ,
			       spare1 ,
			       spare2 ,
			       tilt_angle ,
			       focus_range ,
			       signal_length ,
			       transmit_time_offset ,
			       center_frequency ,
			       bandwidth ,
			       signal_waveform ,
			       transmit_sector_number ,
			       beam_pointing_angle ,
			       range ,
			       trans_sector_num ,
			       reflectivity ,
			       quality_factor ,
			       detection_window ,
			       beam_no ,
			       spare ,
			       total_beams ,
			       offsetx ,
			       offsety ,
			       rotation ,
			       ROVID
			        )
			SELECT ((SELECT MAX(ID) FROM EMMScanLog ) + 1) AS [ID] ,
			       em_model ,
			       CompTime ,
			       DataTime ,
			       ping_counter ,
			       serial_number ,
			       transmit_sectors ,
			       valid_beams ,
			       sampling_frequency ,
			       rov_depth ,
			       sound_speed ,
			       max_num_beams ,
			       spare1 ,
			       spare2 ,
			       tilt_angle ,
			       focus_range ,
			       signal_length ,
			       transmit_time_offset ,
			       center_frequency ,
			       bandwidth ,
			       signal_waveform ,
			       transmit_sector_number ,
			       beam_pointing_angle ,
			       range ,
			       trans_sector_num ,
			       reflectivity ,
			       quality_factor ,
			       detection_window ,
			       beam_no ,
			       spare ,
			       total_beams ,
			       offsetx ,
			       offsety ,
			       rotation ,
			       ROVID
				FROM @TableValue 
        END
		   
		COMMIT TRANSACTION Trans
	END TRY
    BEGIN CATCH
		ROLLBACK TRANSACTION Trans
	END CATCH
END

GO
/****** Object:  StoredProcedure [dbo].[IMP_EMSScanLog_Insert]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IMP_EMSScanLog_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[IMP_EMSScanLog_Insert] AS' 
END
GO

ALTER PROCEDURE [dbo].[IMP_EMSScanLog_Insert]
	@TableValue AS [IMP_EMSScanLogType] READONLY ,
	@AllowDelete AS BIT ,
	@DateTimeFieldName AS NVARCHAR(250) = NULL ,
	@FromDate AS DATETIME = NULL ,
	@ToDate AS DATETIME = NULL
AS
BEGIN
	SET DATEFORMAT YMD;
	BEGIN TRANSACTION Trans
	BEGIN TRY
    	IF @AllowDelete = 1
		BEGIN
			DECLARE @SQLStatment AS NVARCHAR(1000)
			SET @SQLStatment = 'DELETE FROM [EMSScanLog] '
			IF @FromDate IS NOT NULL
            BEGIN
            	SET @SQLStatment = @SQLStatment + 'WHERE ' + @DateTimeFieldName + ' > ''' + CONVERT(NVARCHAR(25), @FromDate, 13) + ''''

				IF @ToDate IS NOT NULL               
					SET @SQLStatment = @SQLStatment + 'AND ' + @DateTimeFieldName + ' < ''' + CONVERT(NVARCHAR(25), @ToDate, 13) + ''''
            END
			EXECUTE (@SQLStatment)

			SET IDENTITY_INSERT [EMSScanLog] ON
			INSERT INTO dbo.EMSScanLog
			        ( ID ,
			       em_model ,
			       CompTime ,
			       DataTime ,
			       ping_counter ,
			       serial_number ,
			       transmit_sectors ,
			       valid_beams ,
			       sampling_frequency ,
			       rov_depth ,
			       sound_speed ,
			       max_num_beams ,
			       spare1 ,
			       spare2 ,
			       tilt_angle ,
			       focus_range ,
			       signal_length ,
			       transmit_time_offset ,
			       center_frequency ,
			       bandwidth ,
			       signal_waveform ,
			       transmit_sector_number ,
			       beam_pointing_angle ,
			       range ,
			       trans_sector_num ,
			       reflectivity ,
			       quality_factor ,
			       detection_window ,
			       beam_no ,
			       spare ,
			       total_beams ,
			       offsetx ,
			       offsety ,
			       rotation ,
			       ROVID
			        )
			SELECT ID ,
			       em_model ,
			       CompTime ,
			       DataTime ,
			       ping_counter ,
			       serial_number ,
			       transmit_sectors ,
			       valid_beams ,
			       sampling_frequency ,
			       rov_depth ,
			       sound_speed ,
			       max_num_beams ,
			       spare1 ,
			       spare2 ,
			       tilt_angle ,
			       focus_range ,
			       signal_length ,
			       transmit_time_offset ,
			       center_frequency ,
			       bandwidth ,
			       signal_waveform ,
			       transmit_sector_number ,
			       beam_pointing_angle ,
			       range ,
			       trans_sector_num ,
			       reflectivity ,
			       quality_factor ,
			       detection_window ,
			       beam_no ,
			       spare ,
			       total_beams ,
			       offsetx ,
			       offsety ,
			       rotation ,
			       ROVID
				FROM @TableValue 
			SET IDENTITY_INSERT [EMSScanLog] OFF
		END
		ELSE
        BEGIN
			INSERT INTO dbo.EMSScanLog
			        ( ID ,
			       em_model ,
			       CompTime ,
			       DataTime ,
			       ping_counter ,
			       serial_number ,
			       transmit_sectors ,
			       valid_beams ,
			       sampling_frequency ,
			       rov_depth ,
			       sound_speed ,
			       max_num_beams ,
			       spare1 ,
			       spare2 ,
			       tilt_angle ,
			       focus_range ,
			       signal_length ,
			       transmit_time_offset ,
			       center_frequency ,
			       bandwidth ,
			       signal_waveform ,
			       transmit_sector_number ,
			       beam_pointing_angle ,
			       range ,
			       trans_sector_num ,
			       reflectivity ,
			       quality_factor ,
			       detection_window ,
			       beam_no ,
			       spare ,
			       total_beams ,
			       offsetx ,
			       offsety ,
			       rotation ,
			       ROVID
			        )
			SELECT ((SELECT MAX(ID) FROM EMSScanLog ) + 1) AS [ID] ,
			       em_model ,
			       CompTime ,
			       DataTime ,
			       ping_counter ,
			       serial_number ,
			       transmit_sectors ,
			       valid_beams ,
			       sampling_frequency ,
			       rov_depth ,
			       sound_speed ,
			       max_num_beams ,
			       spare1 ,
			       spare2 ,
			       tilt_angle ,
			       focus_range ,
			       signal_length ,
			       transmit_time_offset ,
			       center_frequency ,
			       bandwidth ,
			       signal_waveform ,
			       transmit_sector_number ,
			       beam_pointing_angle ,
			       range ,
			       trans_sector_num ,
			       reflectivity ,
			       quality_factor ,
			       detection_window ,
			       beam_no ,
			       spare ,
			       total_beams ,
			       offsetx ,
			       offsety ,
			       rotation ,
			       ROVID
				FROM @TableValue 
	    END
		   
		COMMIT TRANSACTION Trans
	END TRY
    BEGIN CATCH
		ROLLBACK TRANSACTION Trans
	END CATCH
END

GO
/****** Object:  StoredProcedure [dbo].[IMP_ErrorMsg_Insert]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IMP_ErrorMsg_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[IMP_ErrorMsg_Insert] AS' 
END
GO
ALTER PROCEDURE [dbo].[IMP_ErrorMsg_Insert]
	@TableValue AS [IMP_ErrorMsgType] READONLY ,
	@AllowDelete AS BIT ,
	@DateTimeFieldName AS NVARCHAR(250) = NULL ,
	@FromDate AS DATETIME = NULL ,
	@ToDate AS DATETIME = NULL
AS
BEGIN
	SET DATEFORMAT YMD;
	BEGIN TRANSACTION Trans
	BEGIN TRY
    	IF @AllowDelete = 1
		BEGIN
			DECLARE @SQLStatment AS NVARCHAR(1000)
			SET @SQLStatment = 'DELETE FROM [ErrorMsg] '
			IF @FromDate IS NOT NULL
            BEGIN
            	SET @SQLStatment = @SQLStatment + 'WHERE ' + @DateTimeFieldName + ' > ''' + CONVERT(NVARCHAR(25), @FromDate, 13) + ''''

				IF @ToDate IS NOT NULL               
					SET @SQLStatment = @SQLStatment + 'AND ' + @DateTimeFieldName + ' < ''' + CONVERT(NVARCHAR(25), @ToDate, 13) + ''''
            END
			EXECUTE (@SQLStatment)

			SET IDENTITY_INSERT [ErrorMsg] ON
			INSERT INTO dbo.ErrorMsg
			        ( ID ,
			       DTime ,
			       Msg
			        )
			SELECT ID ,
			       DTime ,
			       Msg
				FROM @TableValue 
			SET IDENTITY_INSERT [ErrorMsg] OFF
		END
		ELSE
        BEGIN
			INSERT INTO dbo.ErrorMsg
			        ( DTime ,
			       Msg
			        )
			SELECT DTime ,
			       Msg
				FROM @TableValue 
        END
		
		COMMIT TRANSACTION Trans
	END TRY
    BEGIN CATCH
		ROLLBACK TRANSACTION Trans
	END CATCH
END



GO
/****** Object:  StoredProcedure [dbo].[IMP_EventLog_Insert]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IMP_EventLog_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[IMP_EventLog_Insert] AS' 
END
GO
ALTER PROCEDURE [dbo].[IMP_EventLog_Insert]
		@TableValue AS [IMP_EventLogType] READONLY ,
	@AllowDelete AS BIT ,
	@DateTimeFieldName AS NVARCHAR(250) = NULL ,
	@FromDate AS DATETIME = NULL ,
	@ToDate AS DATETIME = NULL
AS
BEGIN
	SET DATEFORMAT YMD;
	BEGIN TRANSACTION Trans
	BEGIN TRY
    	IF @AllowDelete = 1
		BEGIN
			DECLARE @SQLStatment AS NVARCHAR(1000)
			SET @SQLStatment = 'DELETE FROM [EventLog] '
			IF @FromDate IS NOT NULL
            BEGIN
            	SET @SQLStatment = @SQLStatment + 'WHERE ' + @DateTimeFieldName + ' > ''' + CONVERT(NVARCHAR(25), @FromDate, 13) + ''''

				IF @ToDate IS NOT NULL               
					SET @SQLStatment = @SQLStatment + 'AND ' + @DateTimeFieldName + ' < ''' + CONVERT(NVARCHAR(25), @ToDate, 13) + ''''
            END
			EXECUTE (@SQLStatment)

			SET IDENTITY_INSERT [EventLog] ON
			INSERT INTO dbo.EventLog
			        ( ID ,
			       DTime ,
			       PCode ,
			       SCode ,
			       KP ,
			       East ,
			       North ,
			       PipeDepth ,
			       SeabedDepth ,
			       BurialDepth ,
			       Length ,
			       Height ,
			       Width ,
			       CP ,
			       ProximityCP ,
			       ContactCP ,
			       CPFieldGradiant ,
			       Comment ,
			       Depth ,
			       ROVID ,
			       Active ,
			       Depletion ,
			       DepletionValue ,
			       Angle ,
			       ClockNotation ,
			       Distance ,
			       DistanceNotation ,
			       Contact ,
			       FieldJointNumber ,
			       CrossingName ,
			       CrossingClearance ,
			       CPReading ,
			       Anomaly ,
			       Discard ,
			       CustomComment ,
			       PiggyBackValue ,
			       Reviewed ,
			       EventQC ,
			       DesignKp ,
			       OldKP ,
			       DesignDCC ,
			       OldDCC ,
			       HourLocation ,
			       AnomalyNumber ,
			       AnomalyUniqueNumber ,
			       AnomalyCode ,
			       AnomalyClass ,
			       OldEvent ,
			       EventNum ,
			       Dive_Number ,
			       AnodeSecure ,
			       MG_Coverage ,
			       MG_Thickness ,
			       PercentageBurial
			        )
			SELECT ID ,
			       DTime ,
			       PCode ,
			       SCode ,
			       KP ,
			       East ,
			       North ,
			       PipeDepth ,
			       SeabedDepth ,
			       BurialDepth ,
			       Length ,
			       Height ,
			       Width ,
			       CP ,
			       ProximityCP ,
			       ContactCP ,
			       CPFieldGradiant ,
			       Comment ,
			       Depth ,
			       ROVID ,
			       Active ,
			       Depletion ,
			       DepletionValue ,
			       Angle ,
			       ClockNotation ,
			       Distance ,
			       DistanceNotation ,
			       Contact ,
			       FieldJointNumber ,
			       CrossingName ,
			       CrossingClearance ,
			       CPReading ,
			       Anomaly ,
			       Discard ,
			       CustomComment ,
			       PiggyBackValue ,
			       Reviewed ,
			       EventQC ,
			       DesignKp ,
			       OldKP ,
			       DesignDCC ,
			       OldDCC ,
			       HourLocation ,
			       AnomalyNumber ,
			       AnomalyUniqueNumber ,
			       AnomalyCode ,
			       AnomalyClass ,
			       OldEvent ,
			       EventNum ,
			       Dive_Number ,
			       AnodeSecure ,
			       MG_Coverage ,
			       MG_Thickness ,
			       PercentageBurial
				FROM @TableValue 
			SET IDENTITY_INSERT [EventLog] OFF
		END
		ELSE
        BEGIN
			INSERT INTO dbo.EventLog
			        ( DTime ,
			       PCode ,
			       SCode ,
			       KP ,
			       East ,
			       North ,
			       PipeDepth ,
			       SeabedDepth ,
			       BurialDepth ,
			       Length ,
			       Height ,
			       Width ,
			       CP ,
			       ProximityCP ,
			       ContactCP ,
			       CPFieldGradiant ,
			       Comment ,
			       Depth ,
			       ROVID ,
			       Active ,
			       Depletion ,
			       DepletionValue ,
			       Angle ,
			       ClockNotation ,
			       Distance ,
			       DistanceNotation ,
			       Contact ,
			       FieldJointNumber ,
			       CrossingName ,
			       CrossingClearance ,
			       CPReading ,
			       Anomaly ,
			       Discard ,
			       CustomComment ,
			       PiggyBackValue ,
			       Reviewed ,
			       EventQC ,
			       DesignKp ,
			       OldKP ,
			       DesignDCC ,
			       OldDCC ,
			       HourLocation ,
			       AnomalyNumber ,
			       AnomalyUniqueNumber ,
			       AnomalyCode ,
			       AnomalyClass ,
			       OldEvent ,
			       EventNum ,
			       Dive_Number ,
			       AnodeSecure ,
			       MG_Coverage ,
			       MG_Thickness ,
			       PercentageBurial
			        )
			SELECT DTime ,
			       PCode ,
			       SCode ,
			       KP ,
			       East ,
			       North ,
			       PipeDepth ,
			       SeabedDepth ,
			       BurialDepth ,
			       Length ,
			       Height ,
			       Width ,
			       CP ,
			       ProximityCP ,
			       ContactCP ,
			       CPFieldGradiant ,
			       Comment ,
			       Depth ,
			       ROVID ,
			       Active ,
			       Depletion ,
			       DepletionValue ,
			       Angle ,
			       ClockNotation ,
			       Distance ,
			       DistanceNotation ,
			       Contact ,
			       FieldJointNumber ,
			       CrossingName ,
			       CrossingClearance ,
			       CPReading ,
			       Anomaly ,
			       Discard ,
			       CustomComment ,
			       PiggyBackValue ,
			       Reviewed ,
			       EventQC ,
			       DesignKp ,
			       OldKP ,
			       DesignDCC ,
			       OldDCC ,
			       HourLocation ,
			       AnomalyNumber ,
			       AnomalyUniqueNumber ,
			       AnomalyCode ,
			       AnomalyClass ,
			       OldEvent ,
			       EventNum ,
			       Dive_Number ,
			       AnodeSecure ,
			       MG_Coverage ,
			       MG_Thickness ,
			       PercentageBurial
				FROM @TableValue 
        END
		   
		COMMIT TRANSACTION Trans
	END TRY
    BEGIN CATCH
		ROLLBACK TRANSACTION Trans
	END CATCH
END



GO
/****** Object:  StoredProcedure [dbo].[IMP_EventProc_Insert]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IMP_EventProc_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[IMP_EventProc_Insert] AS' 
END
GO
ALTER PROCEDURE [dbo].[IMP_EventProc_Insert]
	@TableValue AS [IMP_EventProcType] READONLY ,
	@AllowDelete AS BIT ,
	@DateTimeFieldName AS NVARCHAR(250) = NULL ,
	@FromDate AS DATETIME = NULL ,
	@ToDate AS DATETIME = NULL
AS
BEGIN
	SET DATEFORMAT YMD;
	BEGIN TRANSACTION Trans
	BEGIN TRY
    	IF @AllowDelete = 1
		BEGIN
			DECLARE @SQLStatment AS NVARCHAR(1000)
			SET @SQLStatment = 'DELETE FROM [EventProc] '
			IF @FromDate IS NOT NULL
            BEGIN
            	SET @SQLStatment = @SQLStatment + 'WHERE ' + @DateTimeFieldName + ' > ''' + CONVERT(NVARCHAR(25), @FromDate, 13) + ''''

				IF @ToDate IS NOT NULL               
					SET @SQLStatment = @SQLStatment + 'AND ' + @DateTimeFieldName + ' < ''' + CONVERT(NVARCHAR(25), @ToDate, 13) + ''''
            END
			EXECUTE (@SQLStatment)

			SET IDENTITY_INSERT [EventProc] ON
			INSERT INTO dbo.EventProc
			        ( ID ,
			       EventNum ,
			       DTime ,
			       PCode ,
			       SCode ,
			       KP ,
			       East ,
			       North ,
			       PipeDepth ,
			       SeabedDepth ,
			       BurialDepth ,
			       Length ,
			       Height ,
			       Width ,
			       CP ,
			       ProximityCP ,
			       Heading ,
			       HeadingDev ,
			       ContactCP ,
			       CPFieldGradiant ,
			       Comment ,
			       Depth ,
			       ROVID ,
			       Active ,
			       Depletion ,
			       DepletionValue ,
			       Angle ,
			       ClockNotation ,
			       Distance ,
			       DistanceNotation ,
			       Contact ,
			       FieldJointNumber ,
			       CrossingName ,
			       CrossingClearance ,
			       CPReading ,
			       Anomaly ,
			       Discard ,
			       CustomComment ,
			       PiggyBackValue ,
			       Reviewed ,
			       EventQC ,
			       KP_Fixed ,
			       North_Fixed ,
			       East_Fixed ,
			       DesignKp ,
			       OldKP ,
			       DesignDCC ,
			       OldDCC ,
			       DCC ,
			       HourLocation ,
			       MeanSeabedDep ,
			       AdjSeabedDep ,
			       Loc ,
			       SurveyDir ,
			       LocNum ,
			       AnomalyNumber ,
			       AnomalyUniqueNumber ,
			       AnomalyCode ,
			       AnomalyClass ,
			       OldEvent ,
			       Dive_Number ,
			       AnodeSecure ,
			       MG_Coverage ,
			       MG_Thickness ,
			       PercentageBurial
			        )
			SELECT ID ,
			          EventNum ,
			          DTime ,
			          PCode ,
			          SCode ,
			          KP ,
			          East ,
			          North ,
			          PipeDepth ,
			          SeabedDepth ,
			          BurialDepth ,
			          Length ,
			          Height ,
			          Width ,
			          CP ,
			          ProximityCP ,
			          Heading ,
			          HeadingDev ,
			          ContactCP ,
			          CPFieldGradiant ,
			          Comment ,
			          Depth ,
			          ROVID ,
			          Active ,
			          Depletion ,
			          DepletionValue ,
			          Angle ,
			          ClockNotation ,
			          Distance ,
			          DistanceNotation ,
			          Contact ,
			          FieldJointNumber ,
			          CrossingName ,
			          CrossingClearance ,
			          CPReading ,
			          Anomaly ,
			          Discard ,
			          CustomComment ,
			          PiggyBackValue ,
			          Reviewed ,
			          EventQC ,
			          KP_Fixed ,
			          North_Fixed ,
			          East_Fixed ,
			          DesignKp ,
			          OldKP ,
			          DesignDCC ,
			          OldDCC ,
			          DCC ,
			          HourLocation ,
			          MeanSeabedDep ,
			          AdjSeabedDep ,
			          Loc ,
			          SurveyDir ,
			          LocNum ,
			          AnomalyNumber ,
			          AnomalyUniqueNumber ,
			          AnomalyCode ,
			          AnomalyClass ,
			          OldEvent ,
			          Dive_Number ,
			          AnodeSecure ,
			          MG_Coverage ,
			          MG_Thickness ,
			          PercentageBurial
				FROM @TableValue 
			SET IDENTITY_INSERT [EventProc] OFF
		END
		ELSE
        BEGIN
			INSERT INTO dbo.EventProc
			        ( EventNum ,
			       DTime ,
			       PCode ,
			       SCode ,
			       KP ,
			       East ,
			       North ,
			       PipeDepth ,
			       SeabedDepth ,
			       BurialDepth ,
			       Length ,
			       Height ,
			       Width ,
			       CP ,
			       ProximityCP ,
			       Heading ,
			       HeadingDev ,
			       ContactCP ,
			       CPFieldGradiant ,
			       Comment ,
			       Depth ,
			       ROVID ,
			       Active ,
			       Depletion ,
			       DepletionValue ,
			       Angle ,
			       ClockNotation ,
			       Distance ,
			       DistanceNotation ,
			       Contact ,
			       FieldJointNumber ,
			       CrossingName ,
			       CrossingClearance ,
			       CPReading ,
			       Anomaly ,
			       Discard ,
			       CustomComment ,
			       PiggyBackValue ,
			       Reviewed ,
			       EventQC ,
			       KP_Fixed ,
			       North_Fixed ,
			       East_Fixed ,
			       DesignKp ,
			       OldKP ,
			       DesignDCC ,
			       OldDCC ,
			       DCC ,
			       HourLocation ,
			       MeanSeabedDep ,
			       AdjSeabedDep ,
			       Loc ,
			       SurveyDir ,
			       LocNum ,
			       AnomalyNumber ,
			       AnomalyUniqueNumber ,
			       AnomalyCode ,
			       AnomalyClass ,
			       OldEvent ,
			       Dive_Number ,
			       AnodeSecure ,
			       MG_Coverage ,
			       MG_Thickness ,
			       PercentageBurial
			        )
			SELECT EventNum ,
			       DTime ,
			       PCode ,
			       SCode ,
			       KP ,
			       East ,
			       North ,
			       PipeDepth ,
			       SeabedDepth ,
			       BurialDepth ,
			       Length ,
			       Height ,
			       Width ,
			       CP ,
			       ProximityCP ,
			       Heading ,
			       HeadingDev ,
			       ContactCP ,
			       CPFieldGradiant ,
			       Comment ,
			       Depth ,
			       ROVID ,
			       Active ,
			       Depletion ,
			       DepletionValue ,
			       Angle ,
			       ClockNotation ,
			       Distance ,
			       DistanceNotation ,
			       Contact ,
			       FieldJointNumber ,
			       CrossingName ,
			       CrossingClearance ,
			       CPReading ,
			       Anomaly ,
			       Discard ,
			       CustomComment ,
			       PiggyBackValue ,
			       Reviewed ,
			       EventQC ,
			       KP_Fixed ,
			       North_Fixed ,
			       East_Fixed ,
			       DesignKp ,
			       OldKP ,
			       DesignDCC ,
			       OldDCC ,
			       DCC ,
			       HourLocation ,
			       MeanSeabedDep ,
			       AdjSeabedDep ,
			       Loc ,
			       SurveyDir ,
			       LocNum ,
			       AnomalyNumber ,
			       AnomalyUniqueNumber ,
			       AnomalyCode ,
			       AnomalyClass ,
			       OldEvent ,
			       Dive_Number ,
			       AnodeSecure ,
			       MG_Coverage ,
			       MG_Thickness ,
			       PercentageBurial
				FROM @TableValue 
        END

		COMMIT TRANSACTION Trans
	END TRY
    BEGIN CATCH
		ROLLBACK TRANSACTION Trans
	END CATCH
END



GO
/****** Object:  StoredProcedure [dbo].[IMP_FreeSpanList_Insert]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IMP_FreeSpanList_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[IMP_FreeSpanList_Insert] AS' 
END
GO
ALTER PROCEDURE [dbo].[IMP_FreeSpanList_Insert]
	@TableValue AS [IMP_FreeSpanListType] READONLY ,
	@AllowDelete AS BIT = 0,
	@DateTimeFieldName AS NVARCHAR(250) = NULL ,
	@FromDate AS DATETIME = NULL ,
	@ToDate AS DATETIME = NULL
AS
BEGIN
	SET DATEFORMAT YMD;
	BEGIN TRANSACTION Trans
	BEGIN TRY
    	IF @AllowDelete = 1
		BEGIN
			DECLARE @SQLStatment AS NVARCHAR(1000)
			SET @SQLStatment = 'DELETE FROM [FreeSpanList] '
			IF @FromDate IS NOT NULL
            BEGIN
            	SET @SQLStatment = @SQLStatment + 'WHERE ' + @DateTimeFieldName + ' > ''' + CONVERT(NVARCHAR(25), @FromDate, 13) + ''''

				IF @ToDate IS NOT NULL               
					SET @SQLStatment = @SQLStatment + 'AND ' + @DateTimeFieldName + ' < ''' + CONVERT(NVARCHAR(25), @ToDate, 13) + ''''
            END
			EXECUTE (@SQLStatment)

			--SET IDENTITY_INSERT [FreeSpanList] ON
			INSERT INTO dbo.FreeSpanList
			        ( ID ,
			       PCode ,
			       KP ,
			       DTime ,
			       SCode ,
			       eventnum ,
			       ROVID
			        )
			SELECT ID ,
			       PCode ,
			       KP ,
			       DTime ,
			       SCode ,
			       eventnum ,
			       ROVID
				FROM @TableValue 
			--SET IDENTITY_INSERT [FreeSpanList] OFF
		END
		ELSE
        BEGIN
			INSERT INTO dbo.FreeSpanList
			        ( ID,
				   PCode ,
			       KP ,
			       DTime ,
			       SCode ,
			       eventnum ,
			       ROVID
			        )
			SELECT ((SELECT ISNULL(MAX(ID), 0) FROM FreeSpanList ) + 1) AS [ID],
				   PCode ,
			       KP ,
			       DTime ,
			       SCode ,
			       eventnum ,
			       ROVID
				FROM @TableValue 
        END

		COMMIT TRANSACTION Trans
	END TRY
    BEGIN CATCH
		ROLLBACK TRANSACTION Trans
	END CATCH
END



GO
/****** Object:  StoredProcedure [dbo].[IMP_HeadingLog_Insert]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IMP_HeadingLog_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[IMP_HeadingLog_Insert] AS' 
END
GO
ALTER PROCEDURE [dbo].[IMP_HeadingLog_Insert]
	@TableValue AS [IMP_HeadingLogType] READONLY ,
	@AllowDelete AS BIT ,
	@DateTimeFieldName AS NVARCHAR(250) = NULL ,
	@FromDate AS DATETIME = NULL ,
	@ToDate AS DATETIME = NULL
AS
BEGIN
	SET DATEFORMAT YMD;
	BEGIN TRANSACTION Trans
	BEGIN TRY
    	IF @AllowDelete = 1
		BEGIN
			DECLARE @SQLStatment AS NVARCHAR(1000)
			SET @SQLStatment = 'DELETE FROM [HeadingLog] '
			IF @FromDate IS NOT NULL
            BEGIN
            	SET @SQLStatment = @SQLStatment + 'WHERE ' + @DateTimeFieldName + ' > ''' + CONVERT(NVARCHAR(25), @FromDate, 13) + ''''

				IF @ToDate IS NOT NULL               
					SET @SQLStatment = @SQLStatment + 'AND ' + @DateTimeFieldName + ' < ''' + CONVERT(NVARCHAR(25), @ToDate, 13) + ''''
            END
			EXECUTE (@SQLStatment)

			SET IDENTITY_INSERT [HeadingLog] ON
			INSERT INTO dbo.HeadingLog
			        ( ID ,
			       DTime ,
			       Heading ,
			       CorrHeading ,
			       Discard ,
			       ROVID
			        )
			SELECT ID ,
			       DTime ,
			       Heading ,
			       CorrHeading ,
			       Discard ,
			       ROVID
				FROM @TableValue 
			SET IDENTITY_INSERT [HeadingLog] OFF
		END
		ELSE
        BEGIN
			INSERT INTO dbo.HeadingLog
			        ( DTime ,
			       Heading ,
			       CorrHeading ,
			       Discard ,
			       ROVID
			        )
			SELECT DTime ,
			       Heading ,
			       CorrHeading ,
			       Discard ,
			       ROVID
				FROM @TableValue 
        END
		
		COMMIT TRANSACTION Trans
	END TRY
    BEGIN CATCH
		ROLLBACK TRANSACTION Trans
	END CATCH
END



GO
/****** Object:  StoredProcedure [dbo].[IMP_OldEventProc_Insert]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IMP_OldEventProc_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[IMP_OldEventProc_Insert] AS' 
END
GO
ALTER PROCEDURE [dbo].[IMP_OldEventProc_Insert]
	@TableValue AS [IMP_OldEventProcType] READONLY ,
	@AllowDelete AS BIT ,
	@DateTimeFieldName AS NVARCHAR(250) = NULL ,
	@FromDate AS DATETIME = NULL ,
	@ToDate AS DATETIME = NULL
AS
BEGIN
	SET DATEFORMAT YMD;
	BEGIN TRANSACTION Trans
	BEGIN TRY
    	IF @AllowDelete = 1
		BEGIN
			DECLARE @SQLStatment AS NVARCHAR(1000)
			SET @SQLStatment = 'DELETE FROM [OldEventProc] '
			IF @FromDate IS NOT NULL
            BEGIN
            	SET @SQLStatment = @SQLStatment + 'WHERE ' + @DateTimeFieldName + ' > ''' + CONVERT(NVARCHAR(25), @FromDate, 13) + ''''

				IF @ToDate IS NOT NULL               
					SET @SQLStatment = @SQLStatment + 'AND ' + @DateTimeFieldName + ' < ''' + CONVERT(NVARCHAR(25), @ToDate, 13) + ''''
            END
			EXECUTE (@SQLStatment)

			SET IDENTITY_INSERT [OldEventProc] ON
			INSERT INTO dbo.OldEventProc
			        ( ID ,
			       DTime ,
			       PCode ,
			       SCode ,
			       KP ,
			       East ,
			       North ,
			       PipeDepth ,
			       SeabedDepth ,
			       BurialDepth ,
			       Length ,
			       Height ,
			       Width ,
			       CP ,
			       CPFieldGradiant ,
			       Comment ,
			       Depth ,
			       ROVID ,
			       Active ,
			       Depletion ,
			       DepletionValue ,
			       Angle ,
			       ClockNotation ,
			       Distance ,
			       DistanceNotation ,
			       Contact ,
			       FieldJointNumber ,
			       CrossingName ,
			       CrossingClearance ,
			       CPReading ,
			       Anomaly ,
			       Discard ,
			       CustomComment ,
			       PiggyBackValue ,
			       Reviewed ,
			       EventQC ,
			       KP_Fixed ,
			       DesignKp ,
			       OldKP ,
			       DesignDCC ,
			       OldDCC ,
			       DCC ,
			       AdjSeabedDep ,
			       MeanSeabedDep ,
			       North_Fixed ,
			       East_Fixed ,
			       Loc ,
			       SurveyDir ,
			       LocNum ,
			       AnomalyNumber ,
			       AnomalyUniqueNumber ,
			       AnomalyCode ,
			       AnomalyClass ,
			       EventNum ,
			       HourLocation ,
			       OldEvent ,
			       Dive_Number ,
			       AnodeSecure ,
			       MG_Coverage ,
			       MG_Thickness
			        )
			SELECT ID ,
			       DTime ,
			       PCode ,
			       SCode ,
			       KP ,
			       East ,
			       North ,
			       PipeDepth ,
			       SeabedDepth ,
			       BurialDepth ,
			       Length ,
			       Height ,
			       Width ,
			       CP ,
			       CPFieldGradiant ,
			       Comment ,
			       Depth ,
			       ROVID ,
			       Active ,
			       Depletion ,
			       DepletionValue ,
			       Angle ,
			       ClockNotation ,
			       Distance ,
			       DistanceNotation ,
			       Contact ,
			       FieldJointNumber ,
			       CrossingName ,
			       CrossingClearance ,
			       CPReading ,
			       Anomaly ,
			       Discard ,
			       CustomComment ,
			       PiggyBackValue ,
			       Reviewed ,
			       EventQC ,
			       KP_Fixed ,
			       DesignKp ,
			       OldKP ,
			       DesignDCC ,
			       OldDCC ,
			       DCC ,
			       AdjSeabedDep ,
			       MeanSeabedDep ,
			       North_Fixed ,
			       East_Fixed ,
			       Loc ,
			       SurveyDir ,
			       LocNum ,
			       AnomalyNumber ,
			       AnomalyUniqueNumber ,
			       AnomalyCode ,
			       AnomalyClass ,
			       EventNum ,
			       HourLocation ,
			       OldEvent ,
			       Dive_Number ,
			       AnodeSecure ,
			       MG_Coverage ,
			       MG_Thickness
				FROM @TableValue 
			SET IDENTITY_INSERT [OldEventProc] OFF
		END
		ELSE
        BEGIN
			INSERT INTO dbo.OldEventProc
			        ( DTime ,
			       PCode ,
			       SCode ,
			       KP ,
			       East ,
			       North ,
			       PipeDepth ,
			       SeabedDepth ,
			       BurialDepth ,
			       Length ,
			       Height ,
			       Width ,
			       CP ,
			       CPFieldGradiant ,
			       Comment ,
			       Depth ,
			       ROVID ,
			       Active ,
			       Depletion ,
			       DepletionValue ,
			       Angle ,
			       ClockNotation ,
			       Distance ,
			       DistanceNotation ,
			       Contact ,
			       FieldJointNumber ,
			       CrossingName ,
			       CrossingClearance ,
			       CPReading ,
			       Anomaly ,
			       Discard ,
			       CustomComment ,
			       PiggyBackValue ,
			       Reviewed ,
			       EventQC ,
			       KP_Fixed ,
			       DesignKp ,
			       OldKP ,
			       DesignDCC ,
			       OldDCC ,
			       DCC ,
			       AdjSeabedDep ,
			       MeanSeabedDep ,
			       North_Fixed ,
			       East_Fixed ,
			       Loc ,
			       SurveyDir ,
			       LocNum ,
			       AnomalyNumber ,
			       AnomalyUniqueNumber ,
			       AnomalyCode ,
			       AnomalyClass ,
			       EventNum ,
			       HourLocation ,
			       OldEvent ,
			       Dive_Number ,
			       AnodeSecure ,
			       MG_Coverage ,
			       MG_Thickness
			        )
			SELECT DTime ,
			       PCode ,
			       SCode ,
			       KP ,
			       East ,
			       North ,
			       PipeDepth ,
			       SeabedDepth ,
			       BurialDepth ,
			       Length ,
			       Height ,
			       Width ,
			       CP ,
			       CPFieldGradiant ,
			       Comment ,
			       Depth ,
			       ROVID ,
			       Active ,
			       Depletion ,
			       DepletionValue ,
			       Angle ,
			       ClockNotation ,
			       Distance ,
			       DistanceNotation ,
			       Contact ,
			       FieldJointNumber ,
			       CrossingName ,
			       CrossingClearance ,
			       CPReading ,
			       Anomaly ,
			       Discard ,
			       CustomComment ,
			       PiggyBackValue ,
			       Reviewed ,
			       EventQC ,
			       KP_Fixed ,
			       DesignKp ,
			       OldKP ,
			       DesignDCC ,
			       OldDCC ,
			       DCC ,
			       AdjSeabedDep ,
			       MeanSeabedDep ,
			       North_Fixed ,
			       East_Fixed ,
			       Loc ,
			       SurveyDir ,
			       LocNum ,
			       AnomalyNumber ,
			       AnomalyUniqueNumber ,
			       AnomalyCode ,
			       AnomalyClass ,
			       EventNum ,
			       HourLocation ,
			       OldEvent ,
			       Dive_Number ,
			       AnodeSecure ,
			       MG_Coverage ,
			       MG_Thickness
				FROM @TableValue 
        END

		COMMIT TRANSACTION Trans
	END TRY
    BEGIN CATCH
		ROLLBACK TRANSACTION Trans
	END CATCH
END



GO
/****** Object:  StoredProcedure [dbo].[IMP_OldPosProc_Insert]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IMP_OldPosProc_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[IMP_OldPosProc_Insert] AS' 
END
GO
ALTER PROCEDURE [dbo].[IMP_OldPosProc_Insert]
	@TableValue AS [IMP_OldPosProcType] READONLY ,
	@AllowDelete AS BIT ,
	@DateTimeFieldName AS NVARCHAR(250) = NULL ,
	@FromDate AS DATETIME = NULL ,
	@ToDate AS DATETIME = NULL
AS
BEGIN
	SET DATEFORMAT YMD;
	BEGIN TRANSACTION Trans
	BEGIN TRY
    	IF @AllowDelete = 1
		BEGIN
			DECLARE @SQLStatment AS NVARCHAR(1000)
			SET @SQLStatment = 'DELETE FROM [OldPosProc] '
			IF @FromDate IS NOT NULL
            BEGIN
            	SET @SQLStatment = @SQLStatment + 'WHERE ' + @DateTimeFieldName + ' > ''' + CONVERT(NVARCHAR(25), @FromDate, 13) + ''''

				IF @ToDate IS NOT NULL               
					SET @SQLStatment = @SQLStatment + 'AND ' + @DateTimeFieldName + ' < ''' + CONVERT(NVARCHAR(25), @ToDate, 13) + ''''
            END
			EXECUTE (@SQLStatment)

			SET IDENTITY_INSERT [OldPosProc] ON
			INSERT INTO dbo.OldPosProc
			        ( ID ,
					  DTime ,
			          FixNum ,
			          East ,
			          North ,
			          Kp ,
			          Depth ,
			          Heading ,
			          Pitch ,
			          [Roll] ,
			          Logged ,
			          HeadingDev ,
			          Discard ,
			          Digitized ,
			          ROVID ,
			          DesignKp ,
			          OldKP ,
			          DesignDCC ,
			          OldDCC ,
			          DCC ,
			          [De-spiked] ,
			          Long ,
			          LAT
			        )
			SELECT ID ,
					  DTime ,
			          FixNum ,
			          East ,
			          North ,
			          Kp ,
			          Depth ,
			          Heading ,
			          Pitch ,
			          [Roll] ,
			          Logged ,
			          HeadingDev ,
			          Discard ,
			          Digitized ,
			          ROVID ,
			          DesignKp ,
			          OldKP ,
			          DesignDCC ,
			          OldDCC ,
			          DCC ,
			          [De-spiked] ,
			          Long ,
			          LAT
				FROM @TableValue 
			SET IDENTITY_INSERT [OldPosProc] OFF
		END
		ELSE        
		BEGIN
			INSERT INTO dbo.OldPosProc
			        ( DTime ,
			          FixNum ,
			          East ,
			          North ,
			          Kp ,
			          Depth ,
			          Heading ,
			          Pitch ,
			          [Roll] ,
			          Logged ,
			          HeadingDev ,
			          Discard ,
			          Digitized ,
			          ROVID ,
			          DesignKp ,
			          OldKP ,
			          DesignDCC ,
			          OldDCC ,
			          DCC ,
			          [De-spiked] ,
			          Long ,
			          LAT
			        )
			SELECT DTime ,
			          FixNum ,
			          East ,
			          North ,
			          Kp ,
			          Depth ,
			          Heading ,
			          Pitch ,
			          [Roll] ,
			          Logged ,
			          HeadingDev ,
			          Discard ,
			          Digitized ,
			          ROVID ,
			          DesignKp ,
			          OldKP ,
			          DesignDCC ,
			          OldDCC ,
			          DCC ,
			          [De-spiked] ,
			          Long ,
			          LAT
				FROM @TableValue 
		END
		   
		COMMIT TRANSACTION Trans
	END TRY
    BEGIN CATCH
		ROLLBACK TRANSACTION Trans
	END CATCH
END



GO
/****** Object:  StoredProcedure [dbo].[IMP_OldScanProc_Insert]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IMP_OldScanProc_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[IMP_OldScanProc_Insert] AS' 
END
GO
ALTER PROCEDURE [dbo].[IMP_OldScanProc_Insert]
	@TableValue AS [IMP_OldScanProcType] READONLY ,
	@AllowDelete AS BIT ,
	@DateTimeFieldName AS NVARCHAR(250) = NULL ,
	@FromDate AS DATETIME = NULL ,
	@ToDate AS DATETIME = NULL
AS
BEGIN
	SET DATEFORMAT YMD;
	BEGIN TRANSACTION Trans
	BEGIN TRY
    	IF @AllowDelete = 1
		BEGIN
			DECLARE @SQLStatment AS NVARCHAR(1000)
			SET @SQLStatment = 'DELETE FROM [OldScanProc] '
			IF @FromDate IS NOT NULL
            BEGIN
            	SET @SQLStatment = @SQLStatment + 'WHERE ' + @DateTimeFieldName + ' > ''' + CONVERT(NVARCHAR(25), @FromDate, 13) + ''''

				IF @ToDate IS NOT NULL               
					SET @SQLStatment = @SQLStatment + 'AND ' + @DateTimeFieldName + ' < ''' + CONVERT(NVARCHAR(25), @ToDate, 13) + ''''
            END
			EXECUTE (@SQLStatment)

			SET IDENTITY_INSERT [OldScanProc] ON
			INSERT INTO dbo.OldScanProc
			        ( ID ,
					  DTime ,
			          ScanNum ,
			          East ,
			          North ,
			          KP ,
			          Pitch ,
			          [Roll] ,
			          Heading ,
			          MasterNumScans ,
			          SlaveNumScans ,
			          MasterAngStart ,
			          SlaveAngStart ,
			          MasterAngStep ,
			          SlaveAngStep ,
			          PipeDepth ,
			          SeaBedDep ,
			          PipeDeltaY ,
			          ScanData1 ,
			          DeltaX1 ,
			          DeltaY1 ,
			          DeltaZ1 ,
			          RX1 ,
			          RY1 ,
			          RZ1 ,
			          ScanData2 ,
			          DeltaX2 ,
			          DeltaY2 ,
			          DeltaZ2 ,
			          RX2 ,
			          RY2 ,
			          RZ2 ,
			          Delta2CorY ,
			          Delta2CorZ ,
			          R2CorZ ,
			          HeadingDev ,
			          SSpeed ,
			          SeaBedLHS1Dep ,
			          SeaBedLHS2Dep ,
			          SeaBedLHS3Dep ,
			          SeaBedLHS4Dep ,
			          FProc ,
			          FChart ,
			          FFProc ,
			          Discard ,
			          Depth ,
			          KPDifference ,
			          ScanExtraData1 ,
			          ScanExtraData2 ,
			          SeaBedRHS1Dep ,
			          SeaBedRHS2Dep ,
			          SeaBedRHS3Dep ,
			          SeaBedRHS4Dep ,
			          ROVID ,
			          SeaBedLHS1S ,
			          SeaBedLHS2S ,
			          SeaBedLHS3S ,
			          SeaBedLHS4S ,
			          SeaBedLHS1E ,
			          SeaBedLHS2E ,
			          SeaBedLHS3E ,
			          SeaBedLHS4E ,
			          SeaBedRHS1S ,
			          SeaBedRHS2S ,
			          SeaBedRHS3S ,
			          SeaBedRHS4S ,
			          SeaBedRHS1E ,
			          SeaBedRHS2E ,
			          SeaBedRHS3E ,
			          SeaBedRHS4E ,
			          SeaBed0Range ,
			          AverageSeabedDepth ,
			          DoNotShowPipe ,
			          PTProc ,
			          SmoothedPipe ,
			          SmoothedSeabed ,
			          PTAdded ,
			          PipeDiameter ,
			          DesignKp ,
			          OldKP ,
			          DesignDCC ,
			          OldDCC ,
			          DCC ,
			          CorrEast ,
			          CorrNorth ,
			          HeadEast ,
			          HeadNorth
			        )
			SELECT ID ,
					  DTime ,
			          ScanNum ,
			          East ,
			          North ,
			          KP ,
			          Pitch ,
			          [Roll] ,
			          Heading ,
			          MasterNumScans ,
			          SlaveNumScans ,
			          MasterAngStart ,
			          SlaveAngStart ,
			          MasterAngStep ,
			          SlaveAngStep ,
			          PipeDepth ,
			          SeaBedDep ,
			          PipeDeltaY ,
			          ScanData1 ,
			          DeltaX1 ,
			          DeltaY1 ,
			          DeltaZ1 ,
			          RX1 ,
			          RY1 ,
			          RZ1 ,
			          ScanData2 ,
			          DeltaX2 ,
			          DeltaY2 ,
			          DeltaZ2 ,
			          RX2 ,
			          RY2 ,
			          RZ2 ,
			          Delta2CorY ,
			          Delta2CorZ ,
			          R2CorZ ,
			          HeadingDev ,
			          SSpeed ,
			          SeaBedLHS1Dep ,
			          SeaBedLHS2Dep ,
			          SeaBedLHS3Dep ,
			          SeaBedLHS4Dep ,
			          FProc ,
			          FChart ,
			          FFProc ,
			          Discard ,
			          Depth ,
			          KPDifference ,
			          ScanExtraData1 ,
			          ScanExtraData2 ,
			          SeaBedRHS1Dep ,
			          SeaBedRHS2Dep ,
			          SeaBedRHS3Dep ,
			          SeaBedRHS4Dep ,
			          ROVID ,
			          SeaBedLHS1S ,
			          SeaBedLHS2S ,
			          SeaBedLHS3S ,
			          SeaBedLHS4S ,
			          SeaBedLHS1E ,
			          SeaBedLHS2E ,
			          SeaBedLHS3E ,
			          SeaBedLHS4E ,
			          SeaBedRHS1S ,
			          SeaBedRHS2S ,
			          SeaBedRHS3S ,
			          SeaBedRHS4S ,
			          SeaBedRHS1E ,
			          SeaBedRHS2E ,
			          SeaBedRHS3E ,
			          SeaBedRHS4E ,
			          SeaBed0Range ,
			          AverageSeabedDepth ,
			          DoNotShowPipe ,
			          PTProc ,
			          SmoothedPipe ,
			          SmoothedSeabed ,
			          PTAdded ,
			          PipeDiameter ,
			          DesignKp ,
			          OldKP ,
			          DesignDCC ,
			          OldDCC ,
			          DCC ,
			          CorrEast ,
			          CorrNorth ,
			          HeadEast ,
			          HeadNorth
				FROM @TableValue 
			SET IDENTITY_INSERT [OldScanProc] OFF
		END
		ELSE
		BEGIN
			INSERT INTO dbo.OldScanProc
			        ( DTime ,
			          ScanNum ,
			          East ,
			          North ,
			          KP ,
			          Pitch ,
			          [Roll] ,
			          Heading ,
			          MasterNumScans ,
			          SlaveNumScans ,
			          MasterAngStart ,
			          SlaveAngStart ,
			          MasterAngStep ,
			          SlaveAngStep ,
			          PipeDepth ,
			          SeaBedDep ,
			          PipeDeltaY ,
			          ScanData1 ,
			          DeltaX1 ,
			          DeltaY1 ,
			          DeltaZ1 ,
			          RX1 ,
			          RY1 ,
			          RZ1 ,
			          ScanData2 ,
			          DeltaX2 ,
			          DeltaY2 ,
			          DeltaZ2 ,
			          RX2 ,
			          RY2 ,
			          RZ2 ,
			          Delta2CorY ,
			          Delta2CorZ ,
			          R2CorZ ,
			          HeadingDev ,
			          SSpeed ,
			          SeaBedLHS1Dep ,
			          SeaBedLHS2Dep ,
			          SeaBedLHS3Dep ,
			          SeaBedLHS4Dep ,
			          FProc ,
			          FChart ,
			          FFProc ,
			          Discard ,
			          Depth ,
			          KPDifference ,
			          ScanExtraData1 ,
			          ScanExtraData2 ,
			          SeaBedRHS1Dep ,
			          SeaBedRHS2Dep ,
			          SeaBedRHS3Dep ,
			          SeaBedRHS4Dep ,
			          ROVID ,
			          SeaBedLHS1S ,
			          SeaBedLHS2S ,
			          SeaBedLHS3S ,
			          SeaBedLHS4S ,
			          SeaBedLHS1E ,
			          SeaBedLHS2E ,
			          SeaBedLHS3E ,
			          SeaBedLHS4E ,
			          SeaBedRHS1S ,
			          SeaBedRHS2S ,
			          SeaBedRHS3S ,
			          SeaBedRHS4S ,
			          SeaBedRHS1E ,
			          SeaBedRHS2E ,
			          SeaBedRHS3E ,
			          SeaBedRHS4E ,
			          SeaBed0Range ,
			          AverageSeabedDepth ,
			          DoNotShowPipe ,
			          PTProc ,
			          SmoothedPipe ,
			          SmoothedSeabed ,
			          PTAdded ,
			          PipeDiameter ,
			          DesignKp ,
			          OldKP ,
			          DesignDCC ,
			          OldDCC ,
			          DCC ,
			          CorrEast ,
			          CorrNorth ,
			          HeadEast ,
			          HeadNorth
			        )
			SELECT DTime ,
			          ScanNum ,
			          East ,
			          North ,
			          KP ,
			          Pitch ,
			          [Roll] ,
			          Heading ,
			          MasterNumScans ,
			          SlaveNumScans ,
			          MasterAngStart ,
			          SlaveAngStart ,
			          MasterAngStep ,
			          SlaveAngStep ,
			          PipeDepth ,
			          SeaBedDep ,
			          PipeDeltaY ,
			          ScanData1 ,
			          DeltaX1 ,
			          DeltaY1 ,
			          DeltaZ1 ,
			          RX1 ,
			          RY1 ,
			          RZ1 ,
			          ScanData2 ,
			          DeltaX2 ,
			          DeltaY2 ,
			          DeltaZ2 ,
			          RX2 ,
			          RY2 ,
			          RZ2 ,
			          Delta2CorY ,
			          Delta2CorZ ,
			          R2CorZ ,
			          HeadingDev ,
			          SSpeed ,
			          SeaBedLHS1Dep ,
			          SeaBedLHS2Dep ,
			          SeaBedLHS3Dep ,
			          SeaBedLHS4Dep ,
			          FProc ,
			          FChart ,
			          FFProc ,
			          Discard ,
			          Depth ,
			          KPDifference ,
			          ScanExtraData1 ,
			          ScanExtraData2 ,
			          SeaBedRHS1Dep ,
			          SeaBedRHS2Dep ,
			          SeaBedRHS3Dep ,
			          SeaBedRHS4Dep ,
			          ROVID ,
			          SeaBedLHS1S ,
			          SeaBedLHS2S ,
			          SeaBedLHS3S ,
			          SeaBedLHS4S ,
			          SeaBedLHS1E ,
			          SeaBedLHS2E ,
			          SeaBedLHS3E ,
			          SeaBedLHS4E ,
			          SeaBedRHS1S ,
			          SeaBedRHS2S ,
			          SeaBedRHS3S ,
			          SeaBedRHS4S ,
			          SeaBedRHS1E ,
			          SeaBedRHS2E ,
			          SeaBedRHS3E ,
			          SeaBedRHS4E ,
			          SeaBed0Range ,
			          AverageSeabedDepth ,
			          DoNotShowPipe ,
			          PTProc ,
			          SmoothedPipe ,
			          SmoothedSeabed ,
			          PTAdded ,
			          PipeDiameter ,
			          DesignKp ,
			          OldKP ,
			          DesignDCC ,
			          OldDCC ,
			          DCC ,
			          CorrEast ,
			          CorrNorth ,
			          HeadEast ,
			          HeadNorth
				FROM @TableValue 
		END   
		
		COMMIT TRANSACTION Trans
	END TRY
    BEGIN CATCH
		ROLLBACK TRANSACTION Trans
	END CATCH
END



GO
/****** Object:  StoredProcedure [dbo].[IMP_PCode_Insert]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IMP_PCode_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[IMP_PCode_Insert] AS' 
END
GO
ALTER PROCEDURE [dbo].[IMP_PCode_Insert]
	@TableValue AS [IMP_PCodeType] READONLY ,
	@AllowDelete AS BIT ,
	@DateTimeFieldName AS NVARCHAR(250) = NULL ,
	@FromDate AS DATETIME = NULL ,
	@ToDate AS DATETIME = NULL
AS
BEGIN
	SET DATEFORMAT YMD;
	BEGIN TRANSACTION Trans
	BEGIN TRY
    	IF @AllowDelete = 1
		BEGIN
			DECLARE @SQLStatment AS NVARCHAR(1000)
			SET @SQLStatment = 'DELETE FROM [PCode] '
			IF @FromDate IS NOT NULL
            BEGIN
            	SET @SQLStatment = @SQLStatment + 'WHERE ' + @DateTimeFieldName + ' > ''' + CONVERT(NVARCHAR(25), @FromDate, 13) + ''''

				IF @ToDate IS NOT NULL               
					SET @SQLStatment = @SQLStatment + 'AND ' + @DateTimeFieldName + ' < ''' + CONVERT(NVARCHAR(25), @ToDate, 13) + ''''
            END
			EXECUTE (@SQLStatment)

			SET IDENTITY_INSERT [PCode] ON
			INSERT INTO dbo.PCode
			        ( ID ,
			       Code ,
			       Name ,
			       Picture ,
			       PShortcut ,
			       Type ,
			       Layername
			        )
			SELECT ID ,
			       Code ,
			       Name ,
			       Picture ,
			       PShortcut ,
			       Type ,
			       Layername
				FROM @TableValue 
			SET IDENTITY_INSERT [PCode] OFF
		END
		ELSE
        BEGIN
			INSERT INTO dbo.PCode
			        ( Code ,
			       Name ,
			       Picture ,
			       PShortcut ,
			       Type ,
			       Layername
			        )
			SELECT Code ,
			       Name ,
			       Picture ,
			       PShortcut ,
			       Type ,
			       Layername
				FROM @TableValue 
        END

		COMMIT TRANSACTION Trans
	END TRY
    BEGIN CATCH
		ROLLBACK TRANSACTION Trans
	END CATCH
END



GO
/****** Object:  StoredProcedure [dbo].[IMP_PipeList_Insert]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IMP_PipeList_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[IMP_PipeList_Insert] AS' 
END
GO
ALTER PROCEDURE [dbo].[IMP_PipeList_Insert]
	@TableValue AS [IMP_PipeListType] READONLY ,
	@AllowDelete AS BIT ,
	@DateTimeFieldName AS NVARCHAR(250) = NULL ,
	@FromDate AS DATETIME = NULL ,
	@ToDate AS DATETIME = NULL
AS
BEGIN
	SET DATEFORMAT YMD;
	BEGIN TRANSACTION Trans
	BEGIN TRY
    	IF @AllowDelete = 1
		BEGIN
			DECLARE @SQLStatment AS NVARCHAR(1000)
			SET @SQLStatment = 'DELETE FROM [PipeList] '
			IF @FromDate IS NOT NULL
            BEGIN
            	SET @SQLStatment = @SQLStatment + 'WHERE ' + @DateTimeFieldName + ' > ''' + CONVERT(NVARCHAR(25), @FromDate, 13) + ''''

				IF @ToDate IS NOT NULL               
					SET @SQLStatment = @SQLStatment + 'AND ' + @DateTimeFieldName + ' < ''' + CONVERT(NVARCHAR(25), @ToDate, 13) + ''''
            END
			EXECUTE (@SQLStatment)

			--SET IDENTITY_INSERT [PipeList] ON
			INSERT INTO dbo.PipeList
			        ( PipeDepth ,
			        SeaBedDepth ,
			        ID ,
			        ROVID ,
			        SeaBedLHS1Dep ,
			        SeaBedLHS2Dep ,
			        SeaBedLHS3Dep ,
			        SeaBedLHS4Dep ,
			        SeaBedRHS1Dep ,
			        SeaBedRHS2Dep ,
			        SeaBedRHS3Dep ,
			        SeaBedRHS4Dep ,
			        OldKP ,
			        DesignDCC ,
			        OldDCC ,
			        DTime ,
			        Depth ,
			        DCC ,
			        PipeDiameter ,
			        Long ,
			        Lat ,
			        Discard ,
			        DesignKp ,
			        East ,
			        North ,
			        KP
			        )
			SELECT PipeDepth ,
			        SeaBedDepth ,
			        ID ,
			        ROVID ,
			        SeaBedLHS1Dep ,
			        SeaBedLHS2Dep ,
			        SeaBedLHS3Dep ,
			        SeaBedLHS4Dep ,
			        SeaBedRHS1Dep ,
			        SeaBedRHS2Dep ,
			        SeaBedRHS3Dep ,
			        SeaBedRHS4Dep ,
			        OldKP ,
			        DesignDCC ,
			        OldDCC ,
			        DTime ,
			        Depth ,
			        DCC ,
			        PipeDiameter ,
			        Long ,
			        Lat ,
			        Discard ,
			        DesignKp ,
			        East ,
			        North ,
			        KP
				FROM @TableValue 
			--SET IDENTITY_INSERT [PipeList] OFF
		END
		ELSE
        BEGIN
			INSERT INTO dbo.PipeList
			        ( PipeDepth ,
			        SeaBedDepth ,
			        ID ,
			        ROVID ,
			        SeaBedLHS1Dep ,
			        SeaBedLHS2Dep ,
			        SeaBedLHS3Dep ,
			        SeaBedLHS4Dep ,
			        SeaBedRHS1Dep ,
			        SeaBedRHS2Dep ,
			        SeaBedRHS3Dep ,
			        SeaBedRHS4Dep ,
			        OldKP ,
			        DesignDCC ,
			        OldDCC ,
			        DTime ,
			        Depth ,
			        DCC ,
			        PipeDiameter ,
			        Long ,
			        Lat ,
			        Discard ,
			        DesignKp ,
			        East ,
			        North ,
			        KP
			        )
			SELECT PipeDepth ,
			        SeaBedDepth ,
			        ((SELECT ISNULL(MAX(ID), 0) FROM PipeList ) + 1) AS [ID] ,
			        ROVID ,
			        SeaBedLHS1Dep ,
			        SeaBedLHS2Dep ,
			        SeaBedLHS3Dep ,
			        SeaBedLHS4Dep ,
			        SeaBedRHS1Dep ,
			        SeaBedRHS2Dep ,
			        SeaBedRHS3Dep ,
			        SeaBedRHS4Dep ,
			        OldKP ,
			        DesignDCC ,
			        OldDCC ,
			        DTime ,
			        Depth ,
			        DCC ,
			        PipeDiameter ,
			        Long ,
			        Lat ,
			        Discard ,
			        DesignKp ,
			        East ,
			        North ,
			        KP
				FROM @TableValue 
        END
		
		COMMIT TRANSACTION Trans
	END TRY
    BEGIN CATCH
		ROLLBACK TRANSACTION Trans
	END CATCH
END



GO
/****** Object:  StoredProcedure [dbo].[IMP_PitchRoleLog_Insert]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IMP_PitchRoleLog_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[IMP_PitchRoleLog_Insert] AS' 
END
GO

ALTER PROCEDURE [dbo].[IMP_PitchRoleLog_Insert]
	@TableValue AS [IMP_PitchRoleLogType] READONLY ,
	@AllowDelete AS BIT ,
	@DateTimeFieldName AS NVARCHAR(250) = NULL ,
	@FromDate AS DATETIME = NULL ,
	@ToDate AS DATETIME = NULL
AS
BEGIN
	SET DATEFORMAT YMD;
	BEGIN TRANSACTION Trans
	BEGIN TRY
    	IF @AllowDelete = 1
		BEGIN
			DECLARE @SQLStatment AS NVARCHAR(1000)
			SET @SQLStatment = 'DELETE FROM [PitchRoleLog] '
			IF @FromDate IS NOT NULL
            BEGIN
            	SET @SQLStatment = @SQLStatment + 'WHERE ' + @DateTimeFieldName + ' > ''' + CONVERT(NVARCHAR(25), @FromDate, 13) + ''''

				IF @ToDate IS NOT NULL               
					SET @SQLStatment = @SQLStatment + 'AND ' + @DateTimeFieldName + ' < ''' + CONVERT(NVARCHAR(25), @ToDate, 13) + ''''
            END
			EXECUTE (@SQLStatment)

			SET IDENTITY_INSERT [PitchRoleLog] ON
			INSERT INTO dbo.PitchRoleLog
			        ( ID ,
			       DTime ,
			       Pitch ,
			       [Role] ,
			       CorrPitch ,
			       CorrRole ,
			       Discard ,
			       ROVID
			        )
			SELECT ID ,
			       DTime ,
			       Pitch ,
			       [Role] ,
			       CorrPitch ,
			       CorrRole ,
			       Discard ,
			       ROVID
				FROM @TableValue 
			SET IDENTITY_INSERT [PitchRoleLog] OFF
		END
		ELSE
        BEGIN
			INSERT INTO dbo.PitchRoleLog
			        ( DTime ,
			       Pitch ,
			       [Role] ,
			       CorrPitch ,
			       CorrRole ,
			       Discard ,
			       ROVID
			        )
			SELECT DTime ,
			       Pitch ,
			       [Role] ,
			       CorrPitch ,
			       CorrRole ,
			       Discard ,
			       ROVID
				FROM @TableValue 
        END
		COMMIT TRANSACTION Trans
	END TRY
    BEGIN CATCH
		ROLLBACK TRANSACTION Trans
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[IMP_PitchRollLog_Insert]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IMP_PitchRollLog_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[IMP_PitchRollLog_Insert] AS' 
END
GO
ALTER PROCEDURE [dbo].[IMP_PitchRollLog_Insert]
	@TableValue AS [IMP_PitchRollLogType] READONLY ,
	@AllowDelete AS BIT ,
	@DateTimeFieldName AS NVARCHAR(250) = NULL ,
	@FromDate AS DATETIME = NULL ,
	@ToDate AS DATETIME = NULL
AS
BEGIN
	SET DATEFORMAT YMD;
	BEGIN TRANSACTION Trans
	BEGIN TRY
    	IF @AllowDelete = 1
		BEGIN
			DECLARE @SQLStatment AS NVARCHAR(1000)
			SET @SQLStatment = 'DELETE FROM [PitchRollLog] '
			IF @FromDate IS NOT NULL
            BEGIN
            	SET @SQLStatment = @SQLStatment + 'WHERE ' + @DateTimeFieldName + ' > ''' + CONVERT(NVARCHAR(25), @FromDate, 13) + ''''

				IF @ToDate IS NOT NULL               
					SET @SQLStatment = @SQLStatment + 'AND ' + @DateTimeFieldName + ' < ''' + CONVERT(NVARCHAR(25), @ToDate, 13) + ''''
            END
			EXECUTE (@SQLStatment)

			SET IDENTITY_INSERT [PitchRollLog] ON
			INSERT INTO dbo.PitchRollLog
			        ( ID ,
			       DTime ,
			       Pitch ,
			       Roll ,
			       CorrPitch ,
			       CorrRoll ,
			       Discard ,
			       ROVID
			        )
			SELECT ID ,
			       DTime ,
			       Pitch ,
			       Roll ,
			       CorrPitch ,
			       CorrRoll ,
			       Discard ,
			       ROVID
				FROM @TableValue 
			SET IDENTITY_INSERT [PitchRollLog] OFF
		END
		ELSE
        BEGIN
			INSERT INTO dbo.PitchRollLog
			        ( DTime ,
			       Pitch ,
			       Roll ,
			       CorrPitch ,
			       CorrRoll ,
			       Discard ,
			       ROVID
			        )
			SELECT DTime ,
			       Pitch ,
			       Roll ,
			       CorrPitch ,
			       CorrRoll ,
			       Discard ,
			       ROVID
				FROM @TableValue 
        END
		COMMIT TRANSACTION Trans
	END TRY
    BEGIN CATCH
		ROLLBACK TRANSACTION Trans
	END CATCH
END



GO
/****** Object:  StoredProcedure [dbo].[IMP_PosLog_Insert]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IMP_PosLog_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[IMP_PosLog_Insert] AS' 
END
GO
ALTER PROCEDURE [dbo].[IMP_PosLog_Insert]
	@TableValue AS [IMP_PosLogType] READONLY ,
	@AllowDelete AS BIT ,
	@DateTimeFieldName AS NVARCHAR(250) = NULL ,
	@FromDate AS DATETIME = NULL ,
	@ToDate AS DATETIME = NULL
AS
BEGIN
	SET DATEFORMAT YMD;
	BEGIN TRANSACTION Trans
	BEGIN TRY
    	IF @AllowDelete = 1
		BEGIN
			DECLARE @SQLStatment AS NVARCHAR(1000)
			SET @SQLStatment = 'DELETE FROM [PosLog] '
			IF @FromDate IS NOT NULL
            BEGIN
            	SET @SQLStatment = @SQLStatment + 'WHERE ' + @DateTimeFieldName + ' > ''' + CONVERT(NVARCHAR(25), @FromDate, 13) + ''''

				IF @ToDate IS NOT NULL               
					SET @SQLStatment = @SQLStatment + 'AND ' + @DateTimeFieldName + ' < ''' + CONVERT(NVARCHAR(25), @ToDate, 13) + ''''
            END
			EXECUTE (@SQLStatment)
			
			SET IDENTITY_INSERT [PosLog] ON
			INSERT INTO dbo.PosLog
			        ( ID ,
			       DTime ,
			       Depth ,
			       East ,
			       North ,
			       KP ,
			       Logged ,
			       Discard ,
			       ROVID
			        )
			SELECT ID ,
			       DTime ,
			       Depth ,
			       East ,
			       North ,
			       KP ,
			       Logged ,
			       Discard ,
			       ROVID
				FROM @TableValue 
			SET IDENTITY_INSERT [PosLog] OFF
		END
		ELSE
        BEGIN
			INSERT INTO dbo.PosLog
			        ( DTime ,
			       Depth ,
			       East ,
			       North ,
			       KP ,
			       Logged ,
			       Discard ,
			       ROVID
			        )
			SELECT DTime ,
			       Depth ,
			       East ,
			       North ,
			       KP ,
			       Logged ,
			       Discard ,
			       ROVID
				FROM @TableValue 
        END
		
		COMMIT TRANSACTION Trans
	END TRY
    BEGIN CATCH
		ROLLBACK TRANSACTION Trans
	END CATCH
END



GO
/****** Object:  StoredProcedure [dbo].[IMP_PosProc_Insert]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IMP_PosProc_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[IMP_PosProc_Insert] AS' 
END
GO
ALTER PROCEDURE [dbo].[IMP_PosProc_Insert]
	@TableValue AS [IMP_PosProcType] READONLY ,
	@AllowDelete AS BIT ,
	@DateTimeFieldName AS NVARCHAR(250) = NULL ,
	@FromDate AS DATETIME = NULL ,
	@ToDate AS DATETIME = NULL
AS
BEGIN
	SET DATEFORMAT YMD;
	BEGIN TRANSACTION Trans
	BEGIN TRY
    	IF @AllowDelete = 1
		BEGIN
			DECLARE @SQLStatment AS NVARCHAR(1000)
			SET @SQLStatment = 'DELETE FROM [PosProc] '
			IF @FromDate IS NOT NULL
            BEGIN
            	SET @SQLStatment = @SQLStatment + 'WHERE ' + @DateTimeFieldName + ' > ''' + CONVERT(NVARCHAR(25), @FromDate, 13) + ''''

				IF @ToDate IS NOT NULL               
					SET @SQLStatment = @SQLStatment + 'AND ' + @DateTimeFieldName + ' < ''' + CONVERT(NVARCHAR(25), @ToDate, 13) + ''''
            END
			EXECUTE (@SQLStatment)

			--SET IDENTITY_INSERT [PosProc] ON
			INSERT INTO dbo.PosProc
			        ( ID ,
			       DTime ,
			       FixNum ,
			       East ,
			       North ,
			       Kp ,
			       Depth ,
			       Heading ,
			       Pitch ,
			       Roll ,
			       Logged ,
			       HeadingDev ,
			       Discard ,
			       ROVID ,
			       [De-spiked] ,
			       Digitized ,
			       DesignKp ,
			       OldKP ,
			       DesignDCC ,
			       OldDCC ,
			       DCC ,
			       Long ,
			       LAT
			        )
			SELECT ID ,
			       DTime ,
			       FixNum ,
			       East ,
			       North ,
			       Kp ,
			       Depth ,
			       Heading ,
			       Pitch ,
			       Roll ,
			       Logged ,
			       HeadingDev ,
			       Discard ,
			       ROVID ,
			       [De-spiked] ,
			       Digitized ,
			       DesignKp ,
			       OldKP ,
			       DesignDCC ,
			       OldDCC ,
			       DCC ,
			       Long ,
			       LAT
				FROM @TableValue 
			--SET IDENTITY_INSERT [PosProc] OFF
		END
		ELSE
        BEGIN
			INSERT INTO dbo.PosProc
			        ( ID ,
			       DTime ,
			       FixNum ,
			       East ,
			       North ,
			       Kp ,
			       Depth ,
			       Heading ,
			       Pitch ,
			       Roll ,
			       Logged ,
			       HeadingDev ,
			       Discard ,
			       ROVID ,
			       [De-spiked] ,
			       Digitized ,
			       DesignKp ,
			       OldKP ,
			       DesignDCC ,
			       OldDCC ,
			       DCC ,
			       Long ,
			       LAT
			        )
			SELECT ((SELECT ISNULL(MAX(ID), 0) FROM PosProc ) + 1) AS [ID] ,
			       DTime ,
			       FixNum ,
			       East ,
			       North ,
			       Kp ,
			       Depth ,
			       Heading ,
			       Pitch ,
			       Roll ,
			       Logged ,
			       HeadingDev ,
			       Discard ,
			       ROVID ,
			       [De-spiked] ,
			       Digitized ,
			       DesignKp ,
			       OldKP ,
			       DesignDCC ,
			       OldDCC ,
			       DCC ,
			       Long ,
			       LAT
				FROM @TableValue 
        END
		   
		COMMIT TRANSACTION Trans
	END TRY
    BEGIN CATCH
		ROLLBACK TRANSACTION Trans
	END CATCH
END



GO
/****** Object:  StoredProcedure [dbo].[IMP_PRCNavigation_Insert]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IMP_PRCNavigation_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[IMP_PRCNavigation_Insert] AS' 
END
GO

ALTER PROCEDURE [dbo].[IMP_PRCNavigation_Insert]
	@TableValue AS [IMP_PRCNavigationType] READONLY ,
	@AllowDelete AS BIT ,
	@DateTimeFieldName AS NVARCHAR(250) = NULL ,
	@FromDate AS DATETIME = NULL ,
	@ToDate AS DATETIME = NULL
AS
BEGIN
	SET DATEFORMAT YMD;
	BEGIN TRANSACTION Trans
	BEGIN TRY
    	IF @AllowDelete = 1
		BEGIN
			DECLARE @SQLStatment AS NVARCHAR(1000)
			SET @SQLStatment = 'DELETE FROM [PRCNavigation] '
			IF @FromDate IS NOT NULL
            BEGIN
            	SET @SQLStatment = @SQLStatment + 'WHERE ' + @DateTimeFieldName + ' > ''' + CONVERT(NVARCHAR(25), @FromDate, 13) + ''''

				IF @ToDate IS NOT NULL               
					SET @SQLStatment = @SQLStatment + 'AND ' + @DateTimeFieldName + ' < ''' + CONVERT(NVARCHAR(25), @ToDate, 13) + ''''
            END
			EXECUTE (@SQLStatment)

			SET IDENTITY_INSERT [PRCNavigation] ON
			INSERT INTO dbo.PRCNavigation
			        ( PK_PRCNavID ,
					  LoggingPath ,
			          NavDate ,
			          ImageFolders ,
			          CalibrationFiles ,
					  IsImported ,
			          IsHeadingPitchRollCalculated ,
			          IsKPHeadingDevCalculated ,
			          IsDepthCalculated ,
			          ImportStatus ,
			          ImportMessage
			        )
			SELECT PK_PRCNavID ,
					  LoggingPath ,
			          NavDate ,
			          ImageFolders ,
			          CalibrationFiles ,
					  IsImported ,
			          IsHeadingPitchRollCalculated ,
			          IsKPHeadingDevCalculated ,
			          IsDepthCalculated ,
			          ImportStatus ,
			          ImportMessage
			FROM @TableValue 
			SET IDENTITY_INSERT [PRCNavigation] OFF
		END
		ELSE
        BEGIN
			INSERT INTO dbo.PRCNavigation
			        ( LoggingPath ,
			          NavDate ,
			          ImageFolders ,
			          CalibrationFiles ,
					  IsImported ,
			          IsHeadingPitchRollCalculated ,
			          IsKPHeadingDevCalculated ,
			          IsDepthCalculated ,
			          ImportStatus ,
			          ImportMessage
			        )
			SELECT LoggingPath ,
			          NavDate ,
			          ImageFolders ,
			          CalibrationFiles ,
					  IsImported ,
			          IsHeadingPitchRollCalculated ,
			          IsKPHeadingDevCalculated ,
			          IsDepthCalculated ,
			          ImportStatus ,
			          ImportMessage
			FROM @TableValue 
        END
		   
		COMMIT TRANSACTION Trans
	END TRY
    BEGIN CATCH
		ROLLBACK TRANSACTION Trans
	END CATCH
END





GO
/****** Object:  StoredProcedure [dbo].[IMP_PRCNavigationDetails_Insert]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IMP_PRCNavigationDetails_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[IMP_PRCNavigationDetails_Insert] AS' 
END
GO






ALTER PROCEDURE [dbo].[IMP_PRCNavigationDetails_Insert]
	@TableValue AS [IMP_PRCNavigationDetailsType] READONLY ,
	@AllowDelete AS BIT ,
	@DateTimeFieldName AS NVARCHAR(250) = NULL ,
	@FromDate AS DATETIME = NULL ,
	@ToDate AS DATETIME = NULL
AS
BEGIN
	SET DATEFORMAT YMD;
	BEGIN TRANSACTION Trans
	BEGIN TRY
    	IF @AllowDelete = 1
		BEGIN
			DECLARE @SQLStatment AS NVARCHAR(1000)
			SET @SQLStatment = 'DELETE FROM [PRCNavigationDetails] '
			IF @FromDate IS NOT NULL
            BEGIN
            	SET @SQLStatment = @SQLStatment + 'WHERE ' + @DateTimeFieldName + ' > ''' + CONVERT(NVARCHAR(25), @FromDate, 13) + ''''

				IF @ToDate IS NOT NULL               
					SET @SQLStatment = @SQLStatment + 'AND ' + @DateTimeFieldName + ' < ''' + CONVERT(NVARCHAR(25), @ToDate, 13) + ''''
            END
			EXECUTE (@SQLStatment)

			SET IDENTITY_INSERT [PRCNavigationDetails] ON
			INSERT INTO dbo.PRCNavigationDetails
			        ( ID ,
					  FK_PRCNavID ,
			          DTime ,
			          East ,
			          North ,
			          KP ,
			          Depth ,
			          Heading ,
			          Pitch ,
			          Roll ,
			          HeadingDev ,
					  [Timestamp] ,
			          ImageNames ,
					  DroppedFrame ,
					  ROVID,
					  Discard,
					  INS_TimeTag,
					  INS_Latitude,
					  INS_Longitude,
					  INS_Depth,
					  INS_Altitude,
					  INS_Roll,
					  INS_Pitch,
					  INS_Heading,
					  INS_VelocityNorth,
					  INS_VelocityEast,
					  INS_VelocityDown,
					  INS_wFwd,
					  INS_wStbd,
					  INS_wDwn,
					  INS_AccelerationFwd,
					  INS_AccelerationStbd,
					  INS_AccelerationDwn,
					  INS_PosMajor,
					  INS_PosMinor,
					  INS_dirPMajor,
					  INS_StdDepth,
					  INS_StdLevN,
					  INS_StdLevE,
					  INS_stdHeading,
					  INS_velMajor,
					  INS_velMinor,
					  INS_dirVMajor,
					  INS_VelDown,
					  INS_Status,
					  INS_East,
					  INS_North,
					  INS_CurrentDTime,
					  INS_PreviousDTime
			        )
			SELECT ID ,
			       FK_PRCNavID ,
			       DTime ,
			       East ,
			       North ,
			       KP ,
			       Depth ,
			       Heading ,
			       Pitch ,
			       Roll ,
			       HeadingDev ,
				   [Timestamp] ,
			       ImageNames ,
				   DroppedFrame ,
				   ROVID,
				   Discard,
				   INS_TimeTag ,
				   INS_Latitude,
				   INS_Longitude,
				   INS_Depth,
				   INS_Altitude,
				   INS_Roll,
				   INS_Pitch,
				   INS_Heading,
				   INS_VelocityNorth,
				   INS_VelocityEast,
				   INS_VelocityDown,
				   INS_wFwd,
				   INS_wStbd,
				   INS_wDwn,
				   INS_AccelerationFwd,
				   INS_AccelerationStbd,
				   INS_AccelerationDwn,
				   INS_PosMajor,
				   INS_PosMinor,
				   INS_dirPMajor,
				   INS_StdDepth,
				   INS_StdLevN,
				   INS_StdLevE,
				   INS_stdHeading,
				   INS_velMajor,
				   INS_velMinor,
				   INS_dirVMajor,
				   INS_VelDown,
				   INS_Status,
				   INS_East,
				   INS_North,
				   INS_CurrentDTime,
				   INS_PreviousDTime
			FROM @TableValue 
			SET IDENTITY_INSERT [PRCNavigationDetails] OFF
		END
		ELSE
        BEGIN
			INSERT INTO dbo.PRCNavigationDetails
			        ( FK_PRCNavID ,
			          DTime ,
			          East ,
			          North ,
			          KP ,
			          Depth ,
			          Heading ,
			          Pitch ,
			          Roll ,
			          HeadingDev ,
					  [Timestamp] ,
			          ImageNames ,
					  DroppedFrame ,
					  ROVID,
					  INS_TimeTag,
					  INS_Latitude,
					  INS_Longitude,
					  INS_Depth,
					  INS_Altitude,
					  INS_Roll,
					  INS_Pitch,
					  INS_Heading,
					  INS_VelocityNorth,
					  INS_VelocityEast,
					  INS_VelocityDown,
					  INS_wFwd,
					  INS_wStbd,
					  INS_wDwn,
					  INS_AccelerationFwd,
					  INS_AccelerationStbd,
					  INS_AccelerationDwn,
					  INS_PosMajor,
					  INS_PosMinor,
					  INS_dirPMajor,
					  INS_StdDepth,
					  INS_StdLevN,
					  INS_StdLevE,
					  INS_stdHeading,
					  INS_velMajor,
					  INS_velMinor,
					  INS_dirVMajor,
					  INS_VelDown,
					  INS_Status,
					  INS_East,
					  INS_North,
					  INS_CurrentDTime,
					  INS_PreviousDTime
			        )
			SELECT FK_PRCNavID ,
			       DTime ,
			       East ,
			       North ,
			       KP ,
			       Depth ,
			       Heading ,
			       Pitch ,
			       Roll ,
			       HeadingDev ,
				   [Timestamp] ,
			       ImageNames ,
				   DroppedFrame ,
				   ROVID,
				   INS_TimeTag ,
				   INS_Latitude,
				   INS_Longitude,
				   INS_Depth,
				   INS_Altitude,
				   INS_Roll,
				   INS_Pitch,
				   INS_Heading,
				   INS_VelocityNorth,
				   INS_VelocityEast,
				   INS_VelocityDown,
				   INS_wFwd,
				   INS_wStbd,
				   INS_wDwn,
				   INS_AccelerationFwd,
				   INS_AccelerationStbd,
				   INS_AccelerationDwn,
				   INS_PosMajor,
				   INS_PosMinor,
				   INS_dirPMajor,
				   INS_StdDepth,
				   INS_StdLevN,
				   INS_StdLevE,
				   INS_stdHeading,
				   INS_velMajor,
				   INS_velMinor,
				   INS_dirVMajor,
				   INS_VelDown,
				   INS_Status,
				   INS_East,
				   INS_North,
				   INS_CurrentDTime,
				   INS_PreviousDTime
			FROM @TableValue 
        END
		COMMIT TRANSACTION Trans
	END TRY
    BEGIN CATCH
		ROLLBACK TRANSACTION Trans
	END CATCH
END









GO
/****** Object:  StoredProcedure [dbo].[IMP_PTrackerLog_Insert]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IMP_PTrackerLog_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[IMP_PTrackerLog_Insert] AS' 
END
GO
ALTER PROCEDURE [dbo].[IMP_PTrackerLog_Insert]
	@TableValue AS [IMP_PTrackerLogType] READONLY ,
	@AllowDelete AS BIT ,
	@DateTimeFieldName AS NVARCHAR(250) = NULL ,
	@FromDate AS DATETIME = NULL ,
	@ToDate AS DATETIME = NULL
AS
BEGIN
	SET DATEFORMAT YMD;
	BEGIN TRANSACTION Trans
	BEGIN TRY
    	IF @AllowDelete = 1
		BEGIN
			DECLARE @SQLStatment AS NVARCHAR(1000)
			SET @SQLStatment = 'DELETE FROM [PTrackerLog] '
			IF @FromDate IS NOT NULL
            BEGIN
            	SET @SQLStatment = @SQLStatment + 'WHERE ' + @DateTimeFieldName + ' > ''' + CONVERT(NVARCHAR(25), @FromDate, 13) + ''''

				IF @ToDate IS NOT NULL               
					SET @SQLStatment = @SQLStatment + 'AND ' + @DateTimeFieldName + ' < ''' + CONVERT(NVARCHAR(25), @ToDate, 13) + ''''
            END
			EXECUTE (@SQLStatment)

			SET IDENTITY_INSERT [PTrackerLog] ON
			INSERT INTO dbo.PTrackerLog
			        ( ID ,
			       DTime ,
			       LAT ,
			       COV ,
			       VRT ,
			       ALT ,
			       STBDCH ,
			       CenterCH ,
			       PORTCH ,
			       RedundCH ,
			       QCCode ,
			       ROVID
			        )
			SELECT ID ,
			       DTime ,
			       LAT ,
			       COV ,
			       VRT ,
			       ALT ,
			       STBDCH ,
			       CenterCH ,
			       PORTCH ,
			       RedundCH ,
			       QCCode ,
			       ROVID
				FROM @TableValue 
			SET IDENTITY_INSERT [PTrackerLog] OFF
		END
		ELSE
		BEGIN
			INSERT INTO dbo.PTrackerLog
			        ( DTime ,
			       LAT ,
			       COV ,
			       VRT ,
			       ALT ,
			       STBDCH ,
			       CenterCH ,
			       PORTCH ,
			       RedundCH ,
			       QCCode ,
			       ROVID
			        )
			SELECT DTime ,
			       LAT ,
			       COV ,
			       VRT ,
			       ALT ,
			       STBDCH ,
			       CenterCH ,
			       PORTCH ,
			       RedundCH ,
			       QCCode ,
			       ROVID
				FROM @TableValue 
		END   
		
		COMMIT TRANSACTION Trans
	END TRY
    BEGIN CATCH
		ROLLBACK TRANSACTION Trans
	END CATCH
END



GO
/****** Object:  StoredProcedure [dbo].[IMP_PTrackerProc_Insert]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IMP_PTrackerProc_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[IMP_PTrackerProc_Insert] AS' 
END
GO
ALTER PROCEDURE [dbo].[IMP_PTrackerProc_Insert]
	@TableValue AS [IMP_PTrackerProcType] READONLY ,
	@AllowDelete AS BIT ,
	@DateTimeFieldName AS NVARCHAR(250) = NULL ,
	@FromDate AS DATETIME = NULL ,
	@ToDate AS DATETIME = NULL
AS
BEGIN
	SET DATEFORMAT YMD;
	BEGIN TRANSACTION Trans
	BEGIN TRY
    	IF @AllowDelete = 1
		BEGIN
			DECLARE @SQLStatment AS NVARCHAR(1000)
			SET @SQLStatment = 'DELETE FROM [PTrackerProc] '
			IF @FromDate IS NOT NULL
            BEGIN
            	SET @SQLStatment = @SQLStatment + 'WHERE ' + @DateTimeFieldName + ' > ''' + CONVERT(NVARCHAR(25), @FromDate, 13) + ''''

				IF @ToDate IS NOT NULL               
					SET @SQLStatment = @SQLStatment + 'AND ' + @DateTimeFieldName + ' < ''' + CONVERT(NVARCHAR(25), @ToDate, 13) + ''''
            END
			EXECUTE (@SQLStatment)

			--SET IDENTITY_INSERT [PTrackerProc] ON
			INSERT INTO dbo.PTrackerProc
			        ( ID ,
			       PTrackerNum ,
			       DTime ,
			       Depth ,
			       KP ,
			       DesignKP ,
			       East ,
			       North ,
			       FProc ,
			       LAT ,
			       COV ,
			       VRT ,
			       ALT ,
			       STBDCH ,
			       CenterCH ,
			       PORTCH ,
			       RedundCH ,
			       QCCode ,
			       Heading ,
			       Pitch ,
			       Roll ,
			       HeadingDev ,
			       PipeDepth ,
			       SeabedDepth ,
			       Discard ,
			       ROVID ,
			       PTProc ,
			       CorrEast ,
			       CorrNorth ,
			       DesignDCC
			        )
			SELECT ID ,
			       PTrackerNum ,
			       DTime ,
			       Depth ,
			       KP ,
			       DesignKP ,
			       East ,
			       North ,
			       FProc ,
			       LAT ,
			       COV ,
			       VRT ,
			       ALT ,
			       STBDCH ,
			       CenterCH ,
			       PORTCH ,
			       RedundCH ,
			       QCCode ,
			       Heading ,
			       Pitch ,
			       Roll ,
			       HeadingDev ,
			       PipeDepth ,
			       SeabedDepth ,
			       Discard ,
			       ROVID ,
			       PTProc ,
			       CorrEast ,
			       CorrNorth ,
			       DesignDCC
				FROM @TableValue 
			--SET IDENTITY_INSERT [PTrackerProc] OFF
		END
		ELSE
        BEGIN
			INSERT INTO dbo.PTrackerProc
			        ( ID ,
			       PTrackerNum ,
			       DTime ,
			       Depth ,
			       KP ,
			       DesignKP ,
			       East ,
			       North ,
			       FProc ,
			       LAT ,
			       COV ,
			       VRT ,
			       ALT ,
			       STBDCH ,
			       CenterCH ,
			       PORTCH ,
			       RedundCH ,
			       QCCode ,
			       Heading ,
			       Pitch ,
			       Roll ,
			       HeadingDev ,
			       PipeDepth ,
			       SeabedDepth ,
			       Discard ,
			       ROVID ,
			       PTProc ,
			       CorrEast ,
			       CorrNorth ,
			       DesignDCC
			        )
			SELECT ((SELECT ISNULL(MAX(ID), 0) FROM PTrackerProc ) + 1) AS [ID] ,
			       PTrackerNum ,
			       DTime ,
			       Depth ,
			       KP ,
			       DesignKP ,
			       East ,
			       North ,
			       FProc ,
			       LAT ,
			       COV ,
			       VRT ,
			       ALT ,
			       STBDCH ,
			       CenterCH ,
			       PORTCH ,
			       RedundCH ,
			       QCCode ,
			       Heading ,
			       Pitch ,
			       Roll ,
			       HeadingDev ,
			       PipeDepth ,
			       SeabedDepth ,
			       Discard ,
			       ROVID ,
			       PTProc ,
			       CorrEast ,
			       CorrNorth ,
			       DesignDCC
				FROM @TableValue 
        END

		COMMIT TRANSACTION Trans
	END TRY
    BEGIN CATCH
		ROLLBACK TRANSACTION Trans
	END CATCH
END



GO
/****** Object:  StoredProcedure [dbo].[IMP_RAWSurvey_Insert]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IMP_RAWSurvey_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[IMP_RAWSurvey_Insert] AS' 
END
GO
ALTER PROCEDURE [dbo].[IMP_RAWSurvey_Insert]
	@TableValue AS [IMP_RAWSurveyType] READONLY ,
	@AllowDelete AS BIT ,
	@DateTimeFieldName AS NVARCHAR(250) = NULL ,
	@FromDate AS DATETIME = NULL ,
	@ToDate AS DATETIME = NULL
AS
BEGIN
	SET DATEFORMAT YMD;
	BEGIN TRANSACTION Trans
	BEGIN TRY
    	IF @AllowDelete = 1
		BEGIN
			DECLARE @SQLStatment AS NVARCHAR(1000)
			SET @SQLStatment = 'DELETE FROM [RAWSurvey] '
			IF @FromDate IS NOT NULL
            BEGIN
            	SET @SQLStatment = @SQLStatment + 'WHERE ' + @DateTimeFieldName + ' > ''' + CONVERT(NVARCHAR(25), @FromDate, 13) + ''''

				IF @ToDate IS NOT NULL               
					SET @SQLStatment = @SQLStatment + 'AND ' + @DateTimeFieldName + ' < ''' + CONVERT(NVARCHAR(25), @ToDate, 13) + ''''
            END
			EXECUTE (@SQLStatment)

			--SET IDENTITY_INSERT [RAWSurvey] ON
			INSERT INTO dbo.RAWSurvey
			        ( DTime ,
			       KP ,
			       DCC ,
			       East ,
			       North ,
			       Depth ,
			       Altimeter ,
			       Heading ,
			       Pitch ,
			       Roll ,
			       ROVID
			        )
			SELECT DTime ,
			       KP ,
			       DCC ,
			       East ,
			       North ,
			       Depth ,
			       Altimeter ,
			       Heading ,
			       Pitch ,
			       Roll ,
			       ROVID
				FROM @TableValue 
			--SET IDENTITY_INSERT [RAWSurvey] OFF
		END
		ELSE
        BEGIN
			INSERT INTO dbo.RAWSurvey
			        ( DTime ,
			       KP ,
			       DCC ,
			       East ,
			       North ,
			       Depth ,
			       Altimeter ,
			       Heading ,
			       Pitch ,
			       Roll ,
			       ROVID
			        )
			SELECT DTime ,
			       KP ,
			       DCC ,
			       East ,
			       North ,
			       Depth ,
			       Altimeter ,
			       Heading ,
			       Pitch ,
			       Roll ,
			       ROVID
				FROM @TableValue 
        END
		
		COMMIT TRANSACTION Trans
	END TRY
    BEGIN CATCH
		ROLLBACK TRANSACTION Trans
	END CATCH
END



GO
/****** Object:  StoredProcedure [dbo].[IMP_SB7125MScanLog_Insert]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IMP_SB7125MScanLog_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[IMP_SB7125MScanLog_Insert] AS' 
END
GO
ALTER PROCEDURE [dbo].[IMP_SB7125MScanLog_Insert]
	@TableValue AS [IMP_SB7125MScanLogType] READONLY ,
	@AllowDelete AS BIT ,
	@DateTimeFieldName AS NVARCHAR(250) = NULL ,
	@FromDate AS DATETIME = NULL ,
	@ToDate AS DATETIME = NULL
AS
BEGIN
	SET DATEFORMAT YMD;
	BEGIN TRANSACTION Trans
	BEGIN TRY
    	IF @AllowDelete = 1
		BEGIN
			DECLARE @SQLStatment AS NVARCHAR(1000)
			SET @SQLStatment = 'DELETE FROM [SB7125MScanLog] '
			IF @FromDate IS NOT NULL
            BEGIN
            	SET @SQLStatment = @SQLStatment + 'WHERE ' + @DateTimeFieldName + ' > ''' + CONVERT(NVARCHAR(25), @FromDate, 13) + ''''

				IF @ToDate IS NOT NULL               
					SET @SQLStatment = @SQLStatment + 'AND ' + @DateTimeFieldName + ' < ''' + CONVERT(NVARCHAR(25), @ToDate, 13) + ''''
            END
			EXECUTE (@SQLStatment)

			SET IDENTITY_INSERT [SB7125MScanLog] ON
			INSERT INTO dbo.SB7125MScanLog
			        ( ID ,
			       DataTime ,
			       CompTime ,
			       Sonar_Id ,
			       Ping_Number ,
			       Multi_Ping_Sequence ,
			       Beam_Count ,
			       Layer_Comp_Flag ,
			       Sound_Velocity_Flag ,
			       Sound_Velocity ,
			       Min_Filter_Info ,
			       Max_Filter_Info ,
			       Quality ,
			       Offsetx ,
			       Offsety ,
			       Rotation ,
			       MasterMirror ,
			       Range ,
			       Intensity ,
			       Beam_hz_angle ,
			       ROVID
			        )
			SELECT ID ,
			       DataTime ,
			       CompTime ,
			       Sonar_Id ,
			       Ping_Number ,
			       Multi_Ping_Sequence ,
			       Beam_Count ,
			       Layer_Comp_Flag ,
			       Sound_Velocity_Flag ,
			       Sound_Velocity ,
			       Min_Filter_Info ,
			       Max_Filter_Info ,
			       Quality ,
			       Offsetx ,
			       Offsety ,
			       Rotation ,
			       MasterMirror ,
			       Range ,
			       Intensity ,
			       Beam_hz_angle ,
			       ROVID
				FROM @TableValue 
			SET IDENTITY_INSERT [SB7125MScanLog] OFF
		END
		ELSE
        BEGIN
			INSERT INTO dbo.SB7125MScanLog
			        ( DataTime ,
			       CompTime ,
			       Sonar_Id ,
			       Ping_Number ,
			       Multi_Ping_Sequence ,
			       Beam_Count ,
			       Layer_Comp_Flag ,
			       Sound_Velocity_Flag ,
			       Sound_Velocity ,
			       Min_Filter_Info ,
			       Max_Filter_Info ,
			       Quality ,
			       Offsetx ,
			       Offsety ,
			       Rotation ,
			       MasterMirror ,
			       Range ,
			       Intensity ,
			       Beam_hz_angle ,
			       ROVID
			        )
			SELECT DataTime ,
			       CompTime ,
			       Sonar_Id ,
			       Ping_Number ,
			       Multi_Ping_Sequence ,
			       Beam_Count ,
			       Layer_Comp_Flag ,
			       Sound_Velocity_Flag ,
			       Sound_Velocity ,
			       Min_Filter_Info ,
			       Max_Filter_Info ,
			       Quality ,
			       Offsetx ,
			       Offsety ,
			       Rotation ,
			       MasterMirror ,
			       Range ,
			       Intensity ,
			       Beam_hz_angle ,
			       ROVID
				FROM @TableValue 
        END
		   
		COMMIT TRANSACTION Trans
	END TRY
    BEGIN CATCH
		ROLLBACK TRANSACTION Trans
	END CATCH
END



GO
/****** Object:  StoredProcedure [dbo].[IMP_SB7125SScanLog_Insert]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IMP_SB7125SScanLog_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[IMP_SB7125SScanLog_Insert] AS' 
END
GO
ALTER PROCEDURE [dbo].[IMP_SB7125SScanLog_Insert]
	@TableValue AS [IMP_SB7125SScanLogType] READONLY ,
	@AllowDelete AS BIT ,
	@DateTimeFieldName AS NVARCHAR(250) = NULL ,
	@FromDate AS DATETIME = NULL ,
	@ToDate AS DATETIME = NULL
AS
BEGIN
	SET DATEFORMAT YMD;
	BEGIN TRANSACTION Trans
	BEGIN TRY
    	IF @AllowDelete = 1
		BEGIN
			DECLARE @SQLStatment AS NVARCHAR(1000)
			SET @SQLStatment = 'DELETE FROM [SB7125SScanLog] '
			IF @FromDate IS NOT NULL
            BEGIN
            	SET @SQLStatment = @SQLStatment + 'WHERE ' + @DateTimeFieldName + ' > ''' + CONVERT(NVARCHAR(25), @FromDate, 13) + ''''

				IF @ToDate IS NOT NULL               
					SET @SQLStatment = @SQLStatment + 'AND ' + @DateTimeFieldName + ' < ''' + CONVERT(NVARCHAR(25), @ToDate, 13) + ''''
            END
			EXECUTE (@SQLStatment)

			SET IDENTITY_INSERT [SB7125SScanLog] ON
			INSERT INTO dbo.SB7125SScanLog
			        ( ID ,
			       DataTime ,
			       CompTime ,
			       Sonar_Id ,
			       Ping_Number ,
			       Multi_Ping_Sequence ,
			       Beam_Count ,
			       Layer_Comp_Flag ,
			       Sound_Velocity_Flag ,
			       Sound_Velocity ,
			       Min_Filter_Info ,
			       Max_Filter_Info ,
			       Quality ,
			       Offsetx ,
			       Offsety ,
			       Rotation ,
			       MasterMirror ,
			       Range ,
			       Intensity ,
			       Beam_hz_angle ,
			       ROVID
			        )
			SELECT ID ,
			       DataTime ,
			       CompTime ,
			       Sonar_Id ,
			       Ping_Number ,
			       Multi_Ping_Sequence ,
			       Beam_Count ,
			       Layer_Comp_Flag ,
			       Sound_Velocity_Flag ,
			       Sound_Velocity ,
			       Min_Filter_Info ,
			       Max_Filter_Info ,
			       Quality ,
			       Offsetx ,
			       Offsety ,
			       Rotation ,
			       MasterMirror ,
			       Range ,
			       Intensity ,
			       Beam_hz_angle ,
			       ROVID
				FROM @TableValue 
			SET IDENTITY_INSERT [SB7125SScanLog] OFF
		END
		ELSE
        BEGIN
			INSERT INTO dbo.SB7125SScanLog
			        ( DataTime ,
			       CompTime ,
			       Sonar_Id ,
			       Ping_Number ,
			       Multi_Ping_Sequence ,
			       Beam_Count ,
			       Layer_Comp_Flag ,
			       Sound_Velocity_Flag ,
			       Sound_Velocity ,
			       Min_Filter_Info ,
			       Max_Filter_Info ,
			       Quality ,
			       Offsetx ,
			       Offsety ,
			       Rotation ,
			       MasterMirror ,
			       Range ,
			       Intensity ,
			       Beam_hz_angle ,
			       ROVID
			        )
			SELECT DataTime ,
			       CompTime ,
			       Sonar_Id ,
			       Ping_Number ,
			       Multi_Ping_Sequence ,
			       Beam_Count ,
			       Layer_Comp_Flag ,
			       Sound_Velocity_Flag ,
			       Sound_Velocity ,
			       Min_Filter_Info ,
			       Max_Filter_Info ,
			       Quality ,
			       Offsetx ,
			       Offsety ,
			       Rotation ,
			       MasterMirror ,
			       Range ,
			       Intensity ,
			       Beam_hz_angle ,
			       ROVID
				FROM @TableValue 
        END

		COMMIT TRANSACTION Trans
	END TRY
    BEGIN CATCH
		ROLLBACK TRANSACTION Trans
	END CATCH
END


GO
/****** Object:  StoredProcedure [dbo].[IMP_SBMScanLog_Insert]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IMP_SBMScanLog_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[IMP_SBMScanLog_Insert] AS' 
END
GO
ALTER PROCEDURE [dbo].[IMP_SBMScanLog_Insert]
	@TableValue AS [IMP_SBMScanLogType] READONLY ,
	@AllowDelete AS BIT ,
	@DateTimeFieldName AS NVARCHAR(250) = NULL ,
	@FromDate AS DATETIME = NULL ,
	@ToDate AS DATETIME = NULL
AS
BEGIN
	SET DATEFORMAT YMD;
	BEGIN TRANSACTION Trans
	BEGIN TRY
    	IF @AllowDelete = 1
		BEGIN
			DECLARE @SQLStatment AS NVARCHAR(1000)
			SET @SQLStatment = 'DELETE FROM [SBMScanLog] '
			IF @FromDate IS NOT NULL
            BEGIN
            	SET @SQLStatment = @SQLStatment + 'WHERE ' + @DateTimeFieldName + ' > ''' + CONVERT(NVARCHAR(25), @FromDate, 13) + ''''

				IF @ToDate IS NOT NULL               
					SET @SQLStatment = @SQLStatment + 'AND ' + @DateTimeFieldName + ' < ''' + CONVERT(NVARCHAR(25), @ToDate, 13) + ''''
            END
			EXECUTE (@SQLStatment)

			SET IDENTITY_INSERT [SBMScanLog] ON
			INSERT INTO dbo.SBMScanLog
			        ( ID ,
			       DataTime ,
			       MasterNumScans ,
			       MasterAngStart ,
			       MasterAngStep ,
			       ScanData ,
			       MasterSSpeed ,
			       MasterMirror ,
			       CompTime ,
			       Latency ,
			       packet_type ,
			       packet_subtype ,
			       ping_number ,
			       sonar_id ,
			       sonar_model ,
			       freqency ,
			       velocity ,
			       sample_rate ,
			       ping_rate ,
			       range_set ,
			       power ,
			       gain ,
			       pulse_width ,
			       pulse_width_old ,
			       tvg_spread ,
			       tvg_absorb ,
			       projector_type ,
			       projector_beam_width ,
			       beam_width ,
			       beam_spacing_num ,
			       beam_spacing_denom ,
			       projector_angle ,
			       min_range ,
			       max_range ,
			       min_depth ,
			       max_depth ,
			       filters_active ,
			       flags ,
			       temperature ,
			       beam_count ,
			       quality ,
			       offsetx ,
			       offsety ,
			       rotation ,
			       ROVID
			        )
			SELECT ID ,
			       DataTime ,
			       MasterNumScans ,
			       MasterAngStart ,
			       MasterAngStep ,
			       ScanData ,
			       MasterSSpeed ,
			       MasterMirror ,
			       CompTime ,
			       Latency ,
			       packet_type ,
			       packet_subtype ,
			       ping_number ,
			       sonar_id ,
			       sonar_model ,
			       freqency ,
			       velocity ,
			       sample_rate ,
			       ping_rate ,
			       range_set ,
			       power ,
			       gain ,
			       pulse_width ,
			       pulse_width_old ,
			       tvg_spread ,
			       tvg_absorb ,
			       projector_type ,
			       projector_beam_width ,
			       beam_width ,
			       beam_spacing_num ,
			       beam_spacing_denom ,
			       projector_angle ,
			       min_range ,
			       max_range ,
			       min_depth ,
			       max_depth ,
			       filters_active ,
			       flags ,
			       temperature ,
			       beam_count ,
			       quality ,
			       offsetx ,
			       offsety ,
			       rotation ,
			       ROVID
				FROM @TableValue 
			SET IDENTITY_INSERT [SBMScanLog] OFF
		END
		ELSE
        BEGIN
			INSERT INTO dbo.SBMScanLog
			        ( DataTime ,
			       MasterNumScans ,
			       MasterAngStart ,
			       MasterAngStep ,
			       ScanData ,
			       MasterSSpeed ,
			       MasterMirror ,
			       CompTime ,
			       Latency ,
			       packet_type ,
			       packet_subtype ,
			       ping_number ,
			       sonar_id ,
			       sonar_model ,
			       freqency ,
			       velocity ,
			       sample_rate ,
			       ping_rate ,
			       range_set ,
			       power ,
			       gain ,
			       pulse_width ,
			       pulse_width_old ,
			       tvg_spread ,
			       tvg_absorb ,
			       projector_type ,
			       projector_beam_width ,
			       beam_width ,
			       beam_spacing_num ,
			       beam_spacing_denom ,
			       projector_angle ,
			       min_range ,
			       max_range ,
			       min_depth ,
			       max_depth ,
			       filters_active ,
			       flags ,
			       temperature ,
			       beam_count ,
			       quality ,
			       offsetx ,
			       offsety ,
			       rotation ,
			       ROVID
			        )
			SELECT DataTime ,
			       MasterNumScans ,
			       MasterAngStart ,
			       MasterAngStep ,
			       ScanData ,
			       MasterSSpeed ,
			       MasterMirror ,
			       CompTime ,
			       Latency ,
			       packet_type ,
			       packet_subtype ,
			       ping_number ,
			       sonar_id ,
			       sonar_model ,
			       freqency ,
			       velocity ,
			       sample_rate ,
			       ping_rate ,
			       range_set ,
			       power ,
			       gain ,
			       pulse_width ,
			       pulse_width_old ,
			       tvg_spread ,
			       tvg_absorb ,
			       projector_type ,
			       projector_beam_width ,
			       beam_width ,
			       beam_spacing_num ,
			       beam_spacing_denom ,
			       projector_angle ,
			       min_range ,
			       max_range ,
			       min_depth ,
			       max_depth ,
			       filters_active ,
			       flags ,
			       temperature ,
			       beam_count ,
			       quality ,
			       offsetx ,
			       offsety ,
			       rotation ,
			       ROVID
				FROM @TableValue 
        END

		COMMIT TRANSACTION Trans
	END TRY
    BEGIN CATCH
		ROLLBACK TRANSACTION Trans
	END CATCH
END



GO
/****** Object:  StoredProcedure [dbo].[IMP_SBSScanLog_Insert]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IMP_SBSScanLog_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[IMP_SBSScanLog_Insert] AS' 
END
GO
ALTER PROCEDURE [dbo].[IMP_SBSScanLog_Insert]
	@TableValue AS [IMP_SBSScanLogType] READONLY ,
	@AllowDelete AS BIT ,
	@DateTimeFieldName AS NVARCHAR(250) = NULL ,
	@FromDate AS DATETIME = NULL ,
	@ToDate AS DATETIME = NULL
AS
BEGIN
	SET DATEFORMAT YMD;
	BEGIN TRANSACTION Trans
	BEGIN TRY
    	IF @AllowDelete = 1
		BEGIN
			DECLARE @SQLStatment AS NVARCHAR(1000)
			SET @SQLStatment = 'DELETE FROM [SBSScanLog] '
			IF @FromDate IS NOT NULL
            BEGIN
            	SET @SQLStatment = @SQLStatment + 'WHERE ' + @DateTimeFieldName + ' > ''' + CONVERT(NVARCHAR(25), @FromDate, 13) + ''''

				IF @ToDate IS NOT NULL               
					SET @SQLStatment = @SQLStatment + 'AND ' + @DateTimeFieldName + ' < ''' + CONVERT(NVARCHAR(25), @ToDate, 13) + ''''
            END
			EXECUTE (@SQLStatment)

			SET IDENTITY_INSERT [SBSScanLog] ON
			INSERT INTO dbo.SBSScanLog
			        ( ID ,
			       DataTime ,
			       SlaveNumScans ,
			       SlaveAngStart ,
			       SlaveAngStep ,
			       ScanData ,
			       SlaveSSpeed ,
			       SlaveMirror ,
			       CompTime ,
			       Latency ,
			       packet_type ,
			       packet_subtype ,
			       ping_number ,
			       sonar_id ,
			       sonar_model ,
			       freqency ,
			       velocity ,
			       sample_rate ,
			       ping_rate ,
			       range_set ,
			       power ,
			       gain ,
			       pulse_width ,
			       pulse_width_old ,
			       tvg_spread ,
			       tvg_absorb ,
			       projector_type ,
			       projector_beam_width ,
			       beam_width ,
			       beam_spacing_num ,
			       beam_spacing_denom ,
			       projector_angle ,
			       min_range ,
			       max_range ,
			       min_depth ,
			       max_depth ,
			       filters_active ,
			       flags ,
			       temperature ,
			       beam_count ,
			       quality ,
			       offsetx ,
			       offsety ,
			       rotation ,
			       ROVID
			        )
			SELECT ID ,
			       DataTime ,
			       SlaveNumScans ,
			       SlaveAngStart ,
			       SlaveAngStep ,
			       ScanData ,
			       SlaveSSpeed ,
			       SlaveMirror ,
			       CompTime ,
			       Latency ,
			       packet_type ,
			       packet_subtype ,
			       ping_number ,
			       sonar_id ,
			       sonar_model ,
			       freqency ,
			       velocity ,
			       sample_rate ,
			       ping_rate ,
			       range_set ,
			       power ,
			       gain ,
			       pulse_width ,
			       pulse_width_old ,
			       tvg_spread ,
			       tvg_absorb ,
			       projector_type ,
			       projector_beam_width ,
			       beam_width ,
			       beam_spacing_num ,
			       beam_spacing_denom ,
			       projector_angle ,
			       min_range ,
			       max_range ,
			       min_depth ,
			       max_depth ,
			       filters_active ,
			       flags ,
			       temperature ,
			       beam_count ,
			       quality ,
			       offsetx ,
			       offsety ,
			       rotation ,
			       ROVID
				FROM @TableValue 
			SET IDENTITY_INSERT [SBSScanLog] OFF
		END
		ELSE
        BEGIN
			INSERT INTO dbo.SBSScanLog
			        ( DataTime ,
			       SlaveNumScans ,
			       SlaveAngStart ,
			       SlaveAngStep ,
			       ScanData ,
			       SlaveSSpeed ,
			       SlaveMirror ,
			       CompTime ,
			       Latency ,
			       packet_type ,
			       packet_subtype ,
			       ping_number ,
			       sonar_id ,
			       sonar_model ,
			       freqency ,
			       velocity ,
			       sample_rate ,
			       ping_rate ,
			       range_set ,
			       power ,
			       gain ,
			       pulse_width ,
			       pulse_width_old ,
			       tvg_spread ,
			       tvg_absorb ,
			       projector_type ,
			       projector_beam_width ,
			       beam_width ,
			       beam_spacing_num ,
			       beam_spacing_denom ,
			       projector_angle ,
			       min_range ,
			       max_range ,
			       min_depth ,
			       max_depth ,
			       filters_active ,
			       flags ,
			       temperature ,
			       beam_count ,
			       quality ,
			       offsetx ,
			       offsety ,
			       rotation ,
			       ROVID
			        )
			SELECT DataTime ,
			       SlaveNumScans ,
			       SlaveAngStart ,
			       SlaveAngStep ,
			       ScanData ,
			       SlaveSSpeed ,
			       SlaveMirror ,
			       CompTime ,
			       Latency ,
			       packet_type ,
			       packet_subtype ,
			       ping_number ,
			       sonar_id ,
			       sonar_model ,
			       freqency ,
			       velocity ,
			       sample_rate ,
			       ping_rate ,
			       range_set ,
			       power ,
			       gain ,
			       pulse_width ,
			       pulse_width_old ,
			       tvg_spread ,
			       tvg_absorb ,
			       projector_type ,
			       projector_beam_width ,
			       beam_width ,
			       beam_spacing_num ,
			       beam_spacing_denom ,
			       projector_angle ,
			       min_range ,
			       max_range ,
			       min_depth ,
			       max_depth ,
			       filters_active ,
			       flags ,
			       temperature ,
			       beam_count ,
			       quality ,
			       offsetx ,
			       offsety ,
			       rotation ,
			       ROVID
				FROM @TableValue 
        END
		
		COMMIT TRANSACTION Trans
	END TRY
    BEGIN CATCH
		ROLLBACK TRANSACTION Trans
	END CATCH
END



GO
/****** Object:  StoredProcedure [dbo].[IMP_ScanLog_Insert]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IMP_ScanLog_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[IMP_ScanLog_Insert] AS' 
END
GO
ALTER PROCEDURE [dbo].[IMP_ScanLog_Insert]
	@TableValue AS [IMP_ScanLogType] READONLY ,
	@AllowDelete AS BIT ,
	@DateTimeFieldName AS NVARCHAR(250) = NULL ,
	@FromDate AS DATETIME = NULL ,
	@ToDate AS DATETIME = NULL
AS
BEGIN
	SET DATEFORMAT YMD;
	BEGIN TRANSACTION Trans
	BEGIN TRY
    	IF @AllowDelete = 1
		BEGIN
			DECLARE @SQLStatment AS NVARCHAR(1000)
			SET @SQLStatment = 'DELETE FROM [ScanLog] '
			IF @FromDate IS NOT NULL
            BEGIN
            	SET @SQLStatment = @SQLStatment + 'WHERE ' + @DateTimeFieldName + ' > ''' + CONVERT(NVARCHAR(25), @FromDate, 13) + ''''

				IF @ToDate IS NOT NULL               
					SET @SQLStatment = @SQLStatment + 'AND ' + @DateTimeFieldName + ' < ''' + CONVERT(NVARCHAR(25), @ToDate, 13) + ''''
            END
			EXECUTE (@SQLStatment)

			SET IDENTITY_INSERT [ScanLog] ON
			INSERT INTO dbo.ScanLog
			        ( ID ,
			       MasterProfTime ,
			       SlaveProfTime ,
			       MasterNumScans ,
			       SlaveNumScans ,
			       MasterAngStart ,
			       SlaveAngStart ,
			       MasterAngStep ,
			       SlaveAngStep ,
			       MasterOffsetX ,
			       SlaveOffsetX ,
			       MasterOffsetY ,
			       SlaveOffsetY ,
			       MasterOffsetZ ,
			       SlaveOffsetZ ,
			       MasterOffsetR ,
			       SlaveOffsetR ,
			       ScanData1 ,
			       ScanData2 ,
			       MasterSSpeed ,
			       SlaveSSpeed ,
			       MasterMirror ,
			       SlaveMirror ,
			       ScanExtraData1 ,
			       ScanExtraData2 ,
			       ExDataFormat1 ,
			       ExDataFormat2 ,
			       ROVID
			        )
			SELECT ID ,
			       MasterProfTime ,
			       SlaveProfTime ,
			       MasterNumScans ,
			       SlaveNumScans ,
			       MasterAngStart ,
			       SlaveAngStart ,
			       MasterAngStep ,
			       SlaveAngStep ,
			       MasterOffsetX ,
			       SlaveOffsetX ,
			       MasterOffsetY ,
			       SlaveOffsetY ,
			       MasterOffsetZ ,
			       SlaveOffsetZ ,
			       MasterOffsetR ,
			       SlaveOffsetR ,
			       ScanData1 ,
			       ScanData2 ,
			       MasterSSpeed ,
			       SlaveSSpeed ,
			       MasterMirror ,
			       SlaveMirror ,
			       ScanExtraData1 ,
			       ScanExtraData2 ,
			       ExDataFormat1 ,
			       ExDataFormat2 ,
			       ROVID
				FROM @TableValue 
			SET IDENTITY_INSERT [ScanLog] OFF
		END
		ELSE
        BEGIN
			INSERT INTO dbo.ScanLog
			        (MasterProfTime ,
			       SlaveProfTime ,
			       MasterNumScans ,
			       SlaveNumScans ,
			       MasterAngStart ,
			       SlaveAngStart ,
			       MasterAngStep ,
			       SlaveAngStep ,
			       MasterOffsetX ,
			       SlaveOffsetX ,
			       MasterOffsetY ,
			       SlaveOffsetY ,
			       MasterOffsetZ ,
			       SlaveOffsetZ ,
			       MasterOffsetR ,
			       SlaveOffsetR ,
			       ScanData1 ,
			       ScanData2 ,
			       MasterSSpeed ,
			       SlaveSSpeed ,
			       MasterMirror ,
			       SlaveMirror ,
			       ScanExtraData1 ,
			       ScanExtraData2 ,
			       ExDataFormat1 ,
			       ExDataFormat2 ,
			       ROVID
			        )
			SELECT MasterProfTime ,
			       SlaveProfTime ,
			       MasterNumScans ,
			       SlaveNumScans ,
			       MasterAngStart ,
			       SlaveAngStart ,
			       MasterAngStep ,
			       SlaveAngStep ,
			       MasterOffsetX ,
			       SlaveOffsetX ,
			       MasterOffsetY ,
			       SlaveOffsetY ,
			       MasterOffsetZ ,
			       SlaveOffsetZ ,
			       MasterOffsetR ,
			       SlaveOffsetR ,
			       ScanData1 ,
			       ScanData2 ,
			       MasterSSpeed ,
			       SlaveSSpeed ,
			       MasterMirror ,
			       SlaveMirror ,
			       ScanExtraData1 ,
			       ScanExtraData2 ,
			       ExDataFormat1 ,
			       ExDataFormat2 ,
			       ROVID
				FROM @TableValue 
		END
		
		COMMIT TRANSACTION Trans
	END TRY
    BEGIN CATCH
		ROLLBACK TRANSACTION Trans
	END CATCH
END



GO
/****** Object:  StoredProcedure [dbo].[IMP_ScanProc_Insert]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IMP_ScanProc_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[IMP_ScanProc_Insert] AS' 
END
GO
ALTER PROCEDURE [dbo].[IMP_ScanProc_Insert]
	@TableValue AS [IMP_ScanProcType] READONLY ,
	@AllowDelete AS BIT ,
	@DateTimeFieldName AS NVARCHAR(250) = NULL ,
	@FromDate AS DATETIME = NULL ,
	@ToDate AS DATETIME = NULL
AS
BEGIN
	SET DATEFORMAT YMD;
	BEGIN TRANSACTION Trans
	BEGIN TRY
    	IF @AllowDelete = 1
		BEGIN
			DECLARE @SQLStatment AS NVARCHAR(1000)
			SET @SQLStatment = 'DELETE FROM [ScanProc] '
			IF @FromDate IS NOT NULL
            BEGIN
            	SET @SQLStatment = @SQLStatment + 'WHERE ' + @DateTimeFieldName + ' > ''' + CONVERT(NVARCHAR(25), @FromDate, 13) + ''''

				IF @ToDate IS NOT NULL               
					SET @SQLStatment = @SQLStatment + 'AND ' + @DateTimeFieldName + ' < ''' + CONVERT(NVARCHAR(25), @ToDate, 13) + ''''
            END
			EXECUTE (@SQLStatment)

			SET IDENTITY_INSERT [ScanProc] ON
			INSERT INTO dbo.ScanProc
			        ( ID ,
			       DTime ,
			       ScanNum ,
			       East ,
			       North ,
			       KP ,
			       Pitch ,
			       Roll ,
			       Heading ,
			       MasterNumScans ,
			       SlaveNumScans ,
			       MasterAngStart ,
			       SlaveAngStart ,
			       MasterAngStep ,
			       SlaveAngStep ,
			       PipeDepth ,
			       SeaBedDep ,
			       PipeDeltaY ,
			       ScanData1 ,
			       DeltaX1 ,
			       DeltaY1 ,
			       DeltaZ1 ,
			       RX1 ,
			       RY1 ,
			       RZ1 ,
			       ScanData2 ,
			       DeltaX2 ,
			       DeltaY2 ,
			       DeltaZ2 ,
			       RX2 ,
			       RY2 ,
			       RZ2 ,
			       Delta2CorY ,
			       Delta2CorZ ,
			       R2CorZ ,
			       HeadingDev ,
			       SSpeed ,
			       SeaBedLHS1Dep ,
			       SeaBedLHS2Dep ,
			       SeaBedLHS3Dep ,
			       SeaBedLHS4Dep ,
			       FProc ,
			       FChart ,
			       FFProc ,
			       Discard ,
			       Depth ,
			       KPDifference ,
			       ScanExtraData1 ,
			       ScanExtraData2 ,
			       SeaBedRHS1Dep ,
			       SeaBedRHS2Dep ,
			       SeaBedRHS3Dep ,
			       SeaBedRHS4Dep ,
			       ROVID ,
			       SeaBedLHS1S ,
			       SeaBedLHS2S ,
			       SeaBedLHS3S ,
			       SeaBedLHS4S ,
			       SeaBedLHS1E ,
			       SeaBedLHS2E ,
			       SeaBedLHS3E ,
			       SeaBedLHS4E ,
			       SeaBedRHS1S ,
			       SeaBedRHS2S ,
			       SeaBedRHS3S ,
			       SeaBedRHS4S ,
			       SeaBedRHS1E ,
			       SeaBedRHS2E ,
			       SeaBedRHS3E ,
			       SeaBedRHS4E ,
			       SeaBed0Range ,
			       DoNotShowPipe ,
			       PTProc ,
			       SmoothedPipe ,
			       SmoothedSeabed ,
			       PTAdded ,
			       AverageSeabedDepth ,
			       PipeDiameter ,
			       DesignKp ,
			       OldKP ,
			       DesignDCC ,
			       OldDCC ,
			       DCC ,
			       CorrEast ,
			       CorrNorth ,
			       ScanDataProp1 ,
			       ScanDataProp2 ,
			       HeadEast ,
			       HeadNorth,
				   IsPRC)
			SELECT ID ,
			       DTime ,
			       ScanNum ,
			       East ,
			       North ,
			       KP ,
			       Pitch ,
			       Roll ,
			       Heading ,
			       MasterNumScans ,
			       SlaveNumScans ,
			       MasterAngStart ,
			       SlaveAngStart ,
			       MasterAngStep ,
			       SlaveAngStep ,
			       PipeDepth ,
			       SeaBedDep ,
			       PipeDeltaY ,
			       ScanData1 ,
			       DeltaX1 ,
			       DeltaY1 ,
			       DeltaZ1 ,
			       RX1 ,
			       RY1 ,
			       RZ1 ,
			       ScanData2 ,
			       DeltaX2 ,
			       DeltaY2 ,
			       DeltaZ2 ,
			       RX2 ,
			       RY2 ,
			       RZ2 ,
			       Delta2CorY ,
			       Delta2CorZ ,
			       R2CorZ ,
			       HeadingDev ,
			       SSpeed ,
			       SeaBedLHS1Dep ,
			       SeaBedLHS2Dep ,
			       SeaBedLHS3Dep ,
			       SeaBedLHS4Dep ,
			       FProc ,
			       FChart ,
			       FFProc ,
			       Discard ,
			       Depth ,
			       KPDifference ,
			       ScanExtraData1 ,
			       ScanExtraData2 ,
			       SeaBedRHS1Dep ,
			       SeaBedRHS2Dep ,
			       SeaBedRHS3Dep ,
			       SeaBedRHS4Dep ,
			       ROVID ,
			       SeaBedLHS1S ,
			       SeaBedLHS2S ,
			       SeaBedLHS3S ,
			       SeaBedLHS4S ,
			       SeaBedLHS1E ,
			       SeaBedLHS2E ,
			       SeaBedLHS3E ,
			       SeaBedLHS4E ,
			       SeaBedRHS1S ,
			       SeaBedRHS2S ,
			       SeaBedRHS3S ,
			       SeaBedRHS4S ,
			       SeaBedRHS1E ,
			       SeaBedRHS2E ,
			       SeaBedRHS3E ,
			       SeaBedRHS4E ,
			       SeaBed0Range ,
			       DoNotShowPipe ,
			       PTProc ,
			       SmoothedPipe ,
			       SmoothedSeabed ,
			       PTAdded ,
			       AverageSeabedDepth ,
			       PipeDiameter ,
			       DesignKp ,
			       OldKP ,
			       DesignDCC ,
			       OldDCC ,
			       DCC ,
			       CorrEast ,
			       CorrNorth ,
			       ScanDataProp1 ,
			       ScanDataProp2 ,
			       HeadEast ,
			       HeadNorth ,
				   IsPRC
				FROM @TableValue 
			SET IDENTITY_INSERT [ScanProc] OFF
		END
		ELSE
        BEGIN
			INSERT INTO dbo.ScanProc
			        (DTime ,
			       ScanNum ,
			       East ,
			       North ,
			       KP ,
			       Pitch ,
			       Roll ,
			       Heading ,
			       MasterNumScans ,
			       SlaveNumScans ,
			       MasterAngStart ,
			       SlaveAngStart ,
			       MasterAngStep ,
			       SlaveAngStep ,
			       PipeDepth ,
			       SeaBedDep ,
			       PipeDeltaY ,
			       ScanData1 ,
			       DeltaX1 ,
			       DeltaY1 ,
			       DeltaZ1 ,
			       RX1 ,
			       RY1 ,
			       RZ1 ,
			       ScanData2 ,
			       DeltaX2 ,
			       DeltaY2 ,
			       DeltaZ2 ,
			       RX2 ,
			       RY2 ,
			       RZ2 ,
			       Delta2CorY ,
			       Delta2CorZ ,
			       R2CorZ ,
			       HeadingDev ,
			       SSpeed ,
			       SeaBedLHS1Dep ,
			       SeaBedLHS2Dep ,
			       SeaBedLHS3Dep ,
			       SeaBedLHS4Dep ,
			       FProc ,
			       FChart ,
			       FFProc ,
			       Discard ,
			       Depth ,
			       KPDifference ,
			       ScanExtraData1 ,
			       ScanExtraData2 ,
			       SeaBedRHS1Dep ,
			       SeaBedRHS2Dep ,
			       SeaBedRHS3Dep ,
			       SeaBedRHS4Dep ,
			       ROVID ,
			       SeaBedLHS1S ,
			       SeaBedLHS2S ,
			       SeaBedLHS3S ,
			       SeaBedLHS4S ,
			       SeaBedLHS1E ,
			       SeaBedLHS2E ,
			       SeaBedLHS3E ,
			       SeaBedLHS4E ,
			       SeaBedRHS1S ,
			       SeaBedRHS2S ,
			       SeaBedRHS3S ,
			       SeaBedRHS4S ,
			       SeaBedRHS1E ,
			       SeaBedRHS2E ,
			       SeaBedRHS3E ,
			       SeaBedRHS4E ,
			       SeaBed0Range ,
			       DoNotShowPipe ,
			       PTProc ,
			       SmoothedPipe ,
			       SmoothedSeabed ,
			       PTAdded ,
			       AverageSeabedDepth ,
			       PipeDiameter ,
			       DesignKp ,
			       OldKP ,
			       DesignDCC ,
			       OldDCC ,
			       DCC ,
			       CorrEast ,
			       CorrNorth ,
			       ScanDataProp1 ,
			       ScanDataProp2 ,
			       HeadEast ,
			       HeadNorth ,
				   IsPRC)
			SELECT DTime ,
			       ScanNum ,
			       East ,
			       North ,
			       KP ,
			       Pitch ,
			       Roll ,
			       Heading ,
			       MasterNumScans ,
			       SlaveNumScans ,
			       MasterAngStart ,
			       SlaveAngStart ,
			       MasterAngStep ,
			       SlaveAngStep ,
			       PipeDepth ,
			       SeaBedDep ,
			       PipeDeltaY ,
			       ScanData1 ,
			       DeltaX1 ,
			       DeltaY1 ,
			       DeltaZ1 ,
			       RX1 ,
			       RY1 ,
			       RZ1 ,
			       ScanData2 ,
			       DeltaX2 ,
			       DeltaY2 ,
			       DeltaZ2 ,
			       RX2 ,
			       RY2 ,
			       RZ2 ,
			       Delta2CorY ,
			       Delta2CorZ ,
			       R2CorZ ,
			       HeadingDev ,
			       SSpeed ,
			       SeaBedLHS1Dep ,
			       SeaBedLHS2Dep ,
			       SeaBedLHS3Dep ,
			       SeaBedLHS4Dep ,
			       FProc ,
			       FChart ,
			       FFProc ,
			       Discard ,
			       Depth ,
			       KPDifference ,
			       ScanExtraData1 ,
			       ScanExtraData2 ,
			       SeaBedRHS1Dep ,
			       SeaBedRHS2Dep ,
			       SeaBedRHS3Dep ,
			       SeaBedRHS4Dep ,
			       ROVID ,
			       SeaBedLHS1S ,
			       SeaBedLHS2S ,
			       SeaBedLHS3S ,
			       SeaBedLHS4S ,
			       SeaBedLHS1E ,
			       SeaBedLHS2E ,
			       SeaBedLHS3E ,
			       SeaBedLHS4E ,
			       SeaBedRHS1S ,
			       SeaBedRHS2S ,
			       SeaBedRHS3S ,
			       SeaBedRHS4S ,
			       SeaBedRHS1E ,
			       SeaBedRHS2E ,
			       SeaBedRHS3E ,
			       SeaBedRHS4E ,
			       SeaBed0Range ,
			       DoNotShowPipe ,
			       PTProc ,
			       SmoothedPipe ,
			       SmoothedSeabed ,
			       PTAdded ,
			       AverageSeabedDepth ,
			       PipeDiameter ,
			       DesignKp ,
			       OldKP ,
			       DesignDCC ,
			       OldDCC ,
			       DCC ,
			       CorrEast ,
			       CorrNorth ,
			       ScanDataProp1 ,
			       ScanDataProp2 ,
			       HeadEast ,
			       HeadNorth ,
				   IsPRC
				FROM @TableValue 
		END
		COMMIT TRANSACTION Trans
	END TRY
    BEGIN CATCH
		ROLLBACK TRANSACTION Trans
	END CATCH
END



GO
/****** Object:  StoredProcedure [dbo].[IMP_SonarLog_Insert]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IMP_SonarLog_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[IMP_SonarLog_Insert] AS' 
END
GO
ALTER PROCEDURE [dbo].[IMP_SonarLog_Insert]
	@TableValue AS [IMP_SonarLogType] READONLY ,
	@AllowDelete AS BIT ,
	@DateTimeFieldName AS NVARCHAR(250) = NULL ,
	@FromDate AS DATETIME = NULL ,
	@ToDate AS DATETIME = NULL
AS
BEGIN
	SET DATEFORMAT YMD;
	BEGIN TRANSACTION Trans
	BEGIN TRY
    	IF @AllowDelete = 1
		BEGIN
			DECLARE @SQLStatment AS NVARCHAR(1000)
			SET @SQLStatment = 'DELETE FROM [SonarLog] '
			IF @FromDate IS NOT NULL
            BEGIN
            	SET @SQLStatment = @SQLStatment + 'WHERE ' + @DateTimeFieldName + ' > ''' + CONVERT(NVARCHAR(25), @FromDate, 13) + ''''

				IF @ToDate IS NOT NULL               
					SET @SQLStatment = @SQLStatment + 'AND ' + @DateTimeFieldName + ' < ''' + CONVERT(NVARCHAR(25), @ToDate, 13) + ''''
            END
			EXECUTE (@SQLStatment)

			SET IDENTITY_INSERT [SonarLog] ON
			INSERT INTO dbo.SonarLog
			        ( ID ,
			       msg_length ,
			       CompTime ,
			       tx_nde ,
			       rx_nde ,
			       single_packet ,
			       mt_head_data ,
			       seq ,
			       nde ,
			       count ,
			       sonhd ,
			       status ,
			       sweep ,
			       hdctrl ,
			       range ,
			       txn ,
			       gain ,
			       slope ,
			       adspan ,
			       adlow ,
			       heading_offset ,
			       adinterval ,
			       llimit ,
			       rlimit ,
			       steps ,
			       bearing ,
			       dbytes_count ,
			       dbytes ,
			       ROVID
			        )
			SELECT ID ,
			       msg_length ,
			       CompTime ,
			       tx_nde ,
			       rx_nde ,
			       single_packet ,
			       mt_head_data ,
			       seq ,
			       nde ,
			       count ,
			       sonhd ,
			       status ,
			       sweep ,
			       hdctrl ,
			       range ,
			       txn ,
			       gain ,
			       slope ,
			       adspan ,
			       adlow ,
			       heading_offset ,
			       adinterval ,
			       llimit ,
			       rlimit ,
			       steps ,
			       bearing ,
			       dbytes_count ,
			       dbytes ,
			       ROVID
				FROM @TableValue 
			SET IDENTITY_INSERT [SonarLog] OFF
		END
		ELSE
		BEGIN
			INSERT INTO dbo.SonarLog
			        ( msg_length ,
	               CompTime ,
	               tx_nde ,
	               rx_nde ,
	               single_packet ,
	               mt_head_data ,
	               seq ,
	               nde ,
	               count ,
	               sonhd ,
	               status ,
	               sweep ,
	               hdctrl ,
	               range ,
	               txn ,
	               gain ,
	               slope ,
	               adspan ,
	               adlow ,
	               heading_offset ,
	               adinterval ,
	               llimit ,
	               rlimit ,
	               steps ,
	               bearing ,
	               dbytes_count ,
	               dbytes ,
	               ROVID
			        )
			SELECT msg_length ,
	               CompTime ,
	               tx_nde ,
	               rx_nde ,
	               single_packet ,
	               mt_head_data ,
	               seq ,
	               nde ,
	               count ,
	               sonhd ,
	               status ,
	               sweep ,
	               hdctrl ,
	               range ,
	               txn ,
	               gain ,
	               slope ,
	               adspan ,
	               adlow ,
	               heading_offset ,
	               adinterval ,
	               llimit ,
	               rlimit ,
	               steps ,
	               bearing ,
	               dbytes_count ,
	               dbytes ,
	               ROVID
				FROM @TableValue 
		END		   
		
		COMMIT TRANSACTION Trans
	END TRY
    BEGIN CATCH
		ROLLBACK TRANSACTION Trans
	END CATCH
END



GO
/****** Object:  StoredProcedure [dbo].[IMP_STCode_Insert]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IMP_STCode_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[IMP_STCode_Insert] AS' 
END
GO
ALTER PROCEDURE [dbo].[IMP_STCode_Insert]
	@TableValue AS [IMP_STCodeType] READONLY ,
	@AllowDelete AS BIT ,
	@DateTimeFieldName AS NVARCHAR(250) = NULL ,
	@FromDate AS DATETIME = NULL ,
	@ToDate AS DATETIME = NULL
AS
BEGIN
	SET DATEFORMAT YMD;
	BEGIN TRANSACTION Trans
	BEGIN TRY
    	IF @AllowDelete = 1
		BEGIN
			DECLARE @SQLStatment AS NVARCHAR(1000)
			SET @SQLStatment = 'DELETE FROM [STCode] '
			IF @FromDate IS NOT NULL
            BEGIN
            	SET @SQLStatment = @SQLStatment + 'WHERE ' + @DateTimeFieldName + ' > ''' + CONVERT(NVARCHAR(25), @FromDate, 13) + ''''

				IF @ToDate IS NOT NULL               
					SET @SQLStatment = @SQLStatment + 'AND ' + @DateTimeFieldName + ' < ''' + CONVERT(NVARCHAR(20), @ToDate, 13) + ''''
            END
			EXECUTE (@SQLStatment)

			SET IDENTITY_INSERT [STCode] ON
			INSERT INTO dbo.STCode
			        ( PrimaryID ,
			       ID ,
			       Code ,
			       Type ,
			       LayerName ,
			       LayerName_Side ,
			       Description ,
			       Comment ,
			       ProfileEventFlag ,
			       PlanEventFlag ,
			       AddPlanTextFlag ,
			       AddProfileTextFlag ,
			       Discard ,
			       imagecap ,
			       AnomalyCode
			        )
			SELECT PrimaryID ,
			       ID ,
			       Code ,
			       Type ,
			       LayerName ,
			       LayerName_Side ,
			       Description ,
			       Comment ,
			       ProfileEventFlag ,
			       PlanEventFlag ,
			       AddPlanTextFlag ,
			       AddProfileTextFlag ,
			       Discard ,
			       imagecap ,
			       AnomalyCode
				FROM @TableValue 
			SET IDENTITY_INSERT [STCode] OFF
		END
		ELSE
		BEGIN
			INSERT INTO dbo.STCode
			        ( PrimaryID ,
			       Code ,
			       Type ,
			       LayerName ,
			       LayerName_Side ,
			       Description ,
			       Comment ,
			       ProfileEventFlag ,
			       PlanEventFlag ,
			       AddPlanTextFlag ,
			       AddProfileTextFlag ,
			       Discard ,
			       imagecap ,
			       AnomalyCode
			        )
			SELECT PrimaryID ,
			       Code ,
			       Type ,
			       LayerName ,
			       LayerName_Side ,
			       Description ,
			       Comment ,
			       ProfileEventFlag ,
			       PlanEventFlag ,
			       AddPlanTextFlag ,
			       AddProfileTextFlag ,
			       Discard ,
			       imagecap ,
			       AnomalyCode
				FROM @TableValue 
		END   
		
		COMMIT TRANSACTION Trans
	END TRY
    BEGIN CATCH
		ROLLBACK TRANSACTION Trans
	END CATCH
END



GO
/****** Object:  StoredProcedure [dbo].[IMP_TableDatetimeColumns_GetLatestValue]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IMP_TableDatetimeColumns_GetLatestValue]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[IMP_TableDatetimeColumns_GetLatestValue] AS' 
END
GO
ALTER PROCEDURE [dbo].[IMP_TableDatetimeColumns_GetLatestValue]
(
	@TableName AS NVarChar(250),
	@ColumnName as NVarChar(250)
)
AS
BEGIN
	DECLARE @sqlStatment AS NVARCHAR(600)
	SET @sqlStatment = 'SELECT MAX( ' + @ColumnName + ' ) FROM ' + @TableName
	EXECUTE (@sqlStatment)
END

GO
/****** Object:  StoredProcedure [dbo].[IMP_TableDatetimeColumns_GetLatestValueWithInterval]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IMP_TableDatetimeColumns_GetLatestValueWithInterval]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[IMP_TableDatetimeColumns_GetLatestValueWithInterval] AS' 
END
GO



ALTER PROCEDURE [dbo].[IMP_TableDatetimeColumns_GetLatestValueWithInterval]
(
	@TableName AS NVARCHAR(250),
	@ColumnName AS NVARCHAR(250),
	@FromDate AS DATETIME,
	@ToDate AS DATETIME
)
AS
BEGIN
	DECLARE @sqlStatment AS NVARCHAR(600)
	SET @sqlStatment = 'SELECT MAX( ' + @ColumnName + ' ) FROM ' + @TableName + ' WHERE ' + @ColumnName + ' BETWEEN ''' + CONVERT(NVARCHAR(25), @FromDate, 13) + '''' + ' AND ''' + CONVERT(NVARCHAR(20), @ToDate, 120) + ''''
	EXECUTE (@sqlStatment)
END





GO
/****** Object:  StoredProcedure [dbo].[IMP_TableDatetimeColumns_Search]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IMP_TableDatetimeColumns_Search]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[IMP_TableDatetimeColumns_Search] AS' 
END
GO
ALTER PROCEDURE [dbo].[IMP_TableDatetimeColumns_Search]
(
	@TableName AS nvarchar(250)
)
AS
BEGIN
	SELECT DISTINCT tableColumn.COLUMN_NAME, tableColumn.ORDINAL_POSITION
	FROM INFORMATION_SCHEMA.COLUMNS as [tableColumn]
	WHERE tableColumn.DATA_TYPE = 'datetime'
		AND tableColumn.TABLE_NAME = @TableName
	ORDER BY tableColumn.ORDINAL_POSITION
END

GO
/****** Object:  StoredProcedure [dbo].[IMP_TableProcessing_Search]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IMP_TableProcessing_Search]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[IMP_TableProcessing_Search] AS' 
END
GO
ALTER Procedure [dbo].[IMP_TableProcessing_Search]
(
	@TableName AS NVARCHAR(250),
	@DateTimeColumnName AS NVARCHAR(250) = NULL,
	@FromDate AS DATETIME = NULL,
	@ToDate AS DATETIME
)
AS
BEGIN
	SET DATEFORMAT YMD;

	DECLARE @SQLStatment AS NVARCHAR(1000) 
	
	SET @SQLStatment = 'SELECT * '
	SET @SQLStatment = @SQLStatment + 'FROM ' + @TableName + ' '
	IF @FromDate IS NOT NULL
    BEGIN
    	SET @SQLStatment = @SQLStatment + 'WHERE ' 
		SET @SQLStatment = @SQLStatment + @DateTimeColumnName + ' > ''' + CONVERT(NVARCHAR(24), @FromDate, 13) + ''''

		IF @ToDate IS NOT NULL
		BEGIN
			SET @SQLStatment = @SQLStatment + 'AND ' + @DateTimeColumnName + ' < ''' + CONVERT(NVARCHAR(25), @ToDate, 13) + ''''
		END
    END
	
	EXECUTE(@SQLStatment)
END


GO
/****** Object:  StoredProcedure [dbo].[IMP_TablesList_Search]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IMP_TablesList_Search]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[IMP_TablesList_Search] AS' 
END
GO
ALTER Procedure [dbo].[IMP_TablesList_Search]
(
	@EndsWithExclude AS nvarchar(250) = NULL 
)
AS
BEGIN
	DECLARE @NotLikeTableName AS NVARCHAR(251) 

	IF @EndsWithExclude IS NOT NULL 
	BEGIN
		SET @NotLikeTableName = '%' + @EndsWithExclude
	END
	ELSE
    BEGIN
    	SET @NotLikeTableName = NULL
    END

	SELECT TABLE_NAME
	FROM INFORMATION_SCHEMA.TABLES
	WHERE TABLE_TYPE = 'BASE TABLE' 
		AND TABLE_NAME NOT LIKE ISNULL(@NotLikeTableName,'')
END

GO
/****** Object:  StoredProcedure [dbo].[IMP_Tide_Insert]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IMP_Tide_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[IMP_Tide_Insert] AS' 
END
GO
ALTER PROCEDURE [dbo].[IMP_Tide_Insert]
	@TableValue AS [IMP_TideType] READONLY ,
	@AllowDelete AS BIT ,
	@DateTimeFieldName AS NVARCHAR(250) = NULL ,
	@FromDate AS DATETIME = NULL ,
	@ToDate AS DATETIME = NULL
AS
BEGIN
	SET DATEFORMAT YMD;
	BEGIN TRANSACTION Trans
	BEGIN TRY
    	IF @AllowDelete = 1
		BEGIN
			DECLARE @SQLStatment AS NVARCHAR(1000)
			SET @SQLStatment = 'DELETE FROM [Tide] '
			IF @FromDate IS NOT NULL
            BEGIN
            	SET @SQLStatment = @SQLStatment + 'WHERE ' + @DateTimeFieldName + ' > ''' + CONVERT(NVARCHAR(25), @FromDate, 13) + ''''

				IF @ToDate IS NOT NULL               
					SET @SQLStatment = @SQLStatment + 'AND ' + @DateTimeFieldName + ' < ''' + CONVERT(NVARCHAR(25), @ToDate, 13) + ''''
            END
			EXECUTE (@SQLStatment)

			--SET IDENTITY_INSERT [Tide] ON
			INSERT INTO dbo.Tide
			        ( ID ,
			       DTime ,
			       KP ,
			       Tide
			        )
			SELECT ID ,
			       DTime ,
			       KP ,
			       Tide
				FROM @TableValue 
			--SET IDENTITY_INSERT [Tide] OFF
		END
		ELSE
        BEGIN
			INSERT INTO dbo.Tide
			        ( ID ,
			       DTime ,
			       KP ,
			       Tide
			        )
			SELECT ((SELECT ISNULL(MAX(ID), 0) FROM dbo.Tide) + 1) AS ID ,
			       DTime ,
			       KP ,
			       Tide
				FROM @TableValue 
        END
		
		COMMIT TRANSACTION Trans
	END TRY
    BEGIN CATCH
		ROLLBACK TRANSACTION Trans
	END CATCH
END



GO
/****** Object:  StoredProcedure [dbo].[IMP_TideTableDetails_Insert]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IMP_TideTableDetails_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[IMP_TideTableDetails_Insert] AS' 
END
GO
ALTER PROCEDURE [dbo].[IMP_TideTableDetails_Insert]
	@TableValue AS [IMP_TideTableDetailsType] READONLY ,
	@AllowDelete AS BIT ,
	@DateTimeFieldName AS NVARCHAR(250) = NULL ,
	@FromDate AS DATETIME = NULL ,
	@ToDate AS DATETIME = NULL
AS
BEGIN
	SET DATEFORMAT YMD;
	BEGIN TRANSACTION Trans
	BEGIN TRY
    	IF @AllowDelete = 1
		BEGIN
			DECLARE @SQLStatment AS NVARCHAR(1000)
			SET @SQLStatment = 'DELETE FROM [TideTableDetails] '
			IF @FromDate IS NOT NULL
            BEGIN
            	SET @SQLStatment = @SQLStatment + 'WHERE ' + @DateTimeFieldName + ' > ''' + CONVERT(NVARCHAR(25), @FromDate, 13) + ''''

				IF @ToDate IS NOT NULL               
					SET @SQLStatment = @SQLStatment + 'AND ' + @DateTimeFieldName + ' < ''' + CONVERT(NVARCHAR(25), @ToDate, 13) + ''''
            END
			EXECUTE (@SQLStatment)

			SET IDENTITY_INSERT [TideTableDetails] ON
			INSERT INTO dbo.TideTableDetails
			        ( ID ,
			       TideTableID ,
			       FromKP ,
			       ToKP
			        )
			SELECT ID ,
			       TideTableID ,
			       FromKP ,
			       ToKP
				FROM @TableValue 
	
			SET IDENTITY_INSERT [TideTableDetails] OFF
		END
		ELSE
        BEGIN
			INSERT INTO dbo.TideTableDetails
			        ( ID ,
			       TideTableID ,
			       FromKP ,
			       ToKP
			        )
			SELECT ID ,
			       TideTableID ,
			       FromKP ,
			       ToKP
				FROM @TableValue 	
        END
		COMMIT TRANSACTION Trans
	END TRY
    BEGIN CATCH
		ROLLBACK TRANSACTION Trans
	END CATCH
END



GO
/****** Object:  StoredProcedure [dbo].[IMP_VideoLog_Insert]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IMP_VideoLog_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[IMP_VideoLog_Insert] AS' 
END
GO
ALTER PROCEDURE [dbo].[IMP_VideoLog_Insert]
	@TableValue AS [IMP_VideoLogType] READONLY ,
	@AllowDelete AS BIT ,
	@DateTimeFieldName AS NVARCHAR(250) = NULL ,
	@FromDate AS DATETIME = NULL ,
	@ToDate AS DATETIME = NULL
AS
BEGIN
	SET DATEFORMAT YMD;
	BEGIN TRANSACTION Trans
	BEGIN TRY
    	IF @AllowDelete = 1
		BEGIN
			DECLARE @SQLStatment AS NVARCHAR(1000)
			SET @SQLStatment = 'DELETE FROM [VideoLog] '
			IF @FromDate IS NOT NULL
            BEGIN
            	SET @SQLStatment = @SQLStatment + 'WHERE ' + @DateTimeFieldName + ' > ''' + CONVERT(NVARCHAR(25), @FromDate, 13) + ''''

				IF @ToDate IS NOT NULL               
					SET @SQLStatment = @SQLStatment + 'AND ' + @DateTimeFieldName + ' < ''' + CONVERT(NVARCHAR(25), @ToDate, 13) + ''''
            END
			EXECUTE (@SQLStatment)
			SET IDENTITY_INSERT [VideoLog] ON
		
			INSERT INTO dbo.VideoLog
			        ( ID ,
			       SDTime ,
			       EDTime ,
			       FileName ,
			       FilePath ,
			       Discard ,
			       StartKP ,
			       EndKP ,
			       Combined ,
			       ROVID
			        )
			SELECT ID ,
			       SDTime ,
			       EDTime ,
			       FileName ,
			       FilePath ,
			       Discard ,
			       StartKP ,
			       EndKP ,
			       Combined ,
			       ROVID
				FROM @TableValue 
	
			SET IDENTITY_INSERT [VideoLog] OFF
		END
		ELSE
        BEGIN
			INSERT INTO dbo.VideoLog
			        ( SDTime ,
			       EDTime ,
			       FileName ,
			       FilePath ,
			       Discard ,
			       StartKP ,
			       EndKP ,
			       Combined ,
			       ROVID
			        )
			SELECT SDTime ,
			       EDTime ,
			       FileName ,
			       FilePath ,
			       Discard ,
			       StartKP ,
			       EndKP ,
			       Combined ,
			       ROVID
				FROM @TableValue 
        END
		COMMIT TRANSACTION Trans
	END TRY
    BEGIN CATCH
		ROLLBACK TRANSACTION Trans
	END CATCH
END



GO
/****** Object:  StoredProcedure [dbo].[InsertFreeSpanEvents]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InsertFreeSpanEvents]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InsertFreeSpanEvents] AS' 
END
GO

ALTER PROCEDURE [dbo].[InsertFreeSpanEvents]
	@FreeSpanEventData FreeSpanEvent readonly
AS
BEGIN


--Insert

Insert into EventProc(EventProc.KP,EventProc.DTime,EventProc.Discard,EventProc.ROVID,EventProc.PCode,EventProc.SCode,
EventProc.East,EventProc.North,EventProc.Comment) 

select InsertedEvent.KP,InsertedEvent.DTime,InsertedEvent.Discard,InsertedEvent.ROVID,InsertedEvent.PCode,InsertedEvent.SCode,
InsertedEvent.East,InsertedEvent.North,'Added'
 from @FreeSpanEventData as [InsertedEvent]

	
END




GO
/****** Object:  StoredProcedure [dbo].[InsertIntoEventProc]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InsertIntoEventProc]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InsertIntoEventProc] AS' 
END
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[InsertIntoEventProc]
	@EventProcResult NewEventProcType readonly
AS
BEGIN
    --====== Make a table variable to hold the new ID's
     Declare @IDList Table(ID int);
	 Insert into EventProc (DTime,KP,ROVId,PCode,SCode)
	 Output INSERTED.ID Into @IDList(ID)
	 select * from @EventProcResult
	 select * from @IDList
END 





GO
/****** Object:  StoredProcedure [dbo].[InterpolateCameraPositionFromPosProc]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InterpolateCameraPositionFromPosProc]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InterpolateCameraPositionFromPosProc] AS' 
END
GO

ALTER PROCEDURE [dbo].[InterpolateCameraPositionFromPosProc]
	@CameraData EastNorthType readonly
AS
BEGIN


	UPDATE PRCNavigationDetails SET  PRCNavigationDetails.East = UpdatedData.East, PRCNavigationDetails.North = UpdatedData.North
	                                
	FROM  @CameraData AS UpdatedData
	Where PRCNavigationDetails.ID = UpdatedData.ID 
END


GO
/****** Object:  StoredProcedure [dbo].[InterpolateCamerasDepthFromDepthLog]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InterpolateCamerasDepthFromDepthLog]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InterpolateCamerasDepthFromDepthLog] AS' 
END
GO

ALTER PROCEDURE [dbo].[InterpolateCamerasDepthFromDepthLog]
	@CameraData DepthType readonly
AS
BEGIN


	UPDATE PRCNavigationDetails SET  PRCNavigationDetails.Depth = UpdatedData.Depth
	                                
	FROM  @CameraData AS UpdatedData
	Where PRCNavigationDetails.ID = UpdatedData.ID 
END


GO
/****** Object:  StoredProcedure [dbo].[InterpolateCamerasHeadingDevKPFromPosProc]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InterpolateCamerasHeadingDevKPFromPosProc]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InterpolateCamerasHeadingDevKPFromPosProc] AS' 
END
GO

ALTER PROCEDURE [dbo].[InterpolateCamerasHeadingDevKPFromPosProc]
	@CameraData HeadingDevKPType readonly
AS
BEGIN


	UPDATE PRCNavigationDetails SET  PRCNavigationDetails.HeadingDev = UpdatedData.HeadingDev, PRCNavigationDetails.KP = UpdatedData.KP
	                                
	FROM  @CameraData AS UpdatedData
	Where PRCNavigationDetails.ID = UpdatedData.ID 
END


GO
/****** Object:  StoredProcedure [dbo].[InterpolateCamerasHeadingPitchRoll]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InterpolateCamerasHeadingPitchRoll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InterpolateCamerasHeadingPitchRoll] AS' 
END
GO

ALTER PROCEDURE [dbo].[InterpolateCamerasHeadingPitchRoll]
	@CameraData RollPitchHeadingType readonly
AS
BEGIN


	UPDATE PRCNavigationDetails SET  PRCNavigationDetails.Heading = UpdatedData.Heading, PRCNavigationDetails.Roll = UpdatedData.Roll,
	                                 PRCNavigationDetails.Pitch = UpdatedData.Pitch
	FROM  @CameraData AS UpdatedData
	Where PRCNavigationDetails.ID = UpdatedData.ID 
END


GO
/****** Object:  StoredProcedure [dbo].[InterpolateCPLogKPFromPosProc]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InterpolateCPLogKPFromPosProc]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InterpolateCPLogKPFromPosProc] AS' 
END
GO

ALTER PROCEDURE [dbo].[InterpolateCPLogKPFromPosProc]
	@CPLogData KpDataType readonly
AS
BEGIN


	UPDATE CPLog SET  CPLog.KP = UpdatedData.Kp
	FROM  @CPLogData AS UpdatedData
	Where CPLog.ID = UpdatedData.ID 
END


GO
/****** Object:  StoredProcedure [dbo].[InterpolateCPLogPositionFromDigitizedPipe]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InterpolateCPLogPositionFromDigitizedPipe]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InterpolateCPLogPositionFromDigitizedPipe] AS' 
END
GO

ALTER PROCEDURE [dbo].[InterpolateCPLogPositionFromDigitizedPipe]
	@CPLogData EastNorthType readonly
AS
BEGIN


	UPDATE CPLog SET  CPLog.East = UpdatedData.East, CPLog.North = UpdatedData.North
	FROM  @CPLogData AS UpdatedData
	Where CPLog.ID = UpdatedData.ID 
END


GO
/****** Object:  StoredProcedure [dbo].[InterpolateDigitizedDepthFromDepthLog]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InterpolateDigitizedDepthFromDepthLog]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InterpolateDigitizedDepthFromDepthLog] AS' 
END
GO

ALTER PROCEDURE [dbo].[InterpolateDigitizedDepthFromDepthLog]
	@DigitizedData DepthType readonly
AS
BEGIN


	UPDATE DigitizedPipe SET  DigitizedPipe.Depth = UpdatedDigitized.Depth
	FROM  @DigitizedData AS UpdatedDigitized
	Where DigitizedPipe.ID = UpdatedDigitized.ID
END


GO
/****** Object:  StoredProcedure [dbo].[InterpolateEventProcDateTimeFromPosProc]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InterpolateEventProcDateTimeFromPosProc]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InterpolateEventProcDateTimeFromPosProc] AS' 
END
GO

ALTER PROCEDURE [dbo].[InterpolateEventProcDateTimeFromPosProc]
	@EventProcData DateTimeType readonly
AS
BEGIN


	UPDATE EventProc SET  EventProc.DTime = UpdatedEventProc.DTime
	FROM  @EventProcData AS UpdatedEventProc
	Where EventProc.ID = UpdatedEventProc.ID
END


GO
/****** Object:  StoredProcedure [dbo].[InterpolateEventProcDepthFromDepthLog]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InterpolateEventProcDepthFromDepthLog]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InterpolateEventProcDepthFromDepthLog] AS' 
END
GO

ALTER PROCEDURE [dbo].[InterpolateEventProcDepthFromDepthLog]
	@EventProcData DepthType readonly
AS
BEGIN


	UPDATE EventProc SET  EventProc.Depth = UpdatedEventProc.Depth
	FROM  @EventProcData AS UpdatedEventProc
	Where EventProc.ID = UpdatedEventProc.ID
END


GO
/****** Object:  StoredProcedure [dbo].[InterpolateEventProcDepthFromDesignScanProc]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InterpolateEventProcDepthFromDesignScanProc]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InterpolateEventProcDepthFromDesignScanProc] AS' 
END
GO

ALTER PROCEDURE [dbo].[InterpolateEventProcDepthFromDesignScanProc]
	@EventProcData DepthType readonly
AS
BEGIN


	UPDATE EventProc SET  EventProc.Depth = UpdatedEventProc.Depth
	FROM  @EventProcData AS UpdatedEventProc
	Where EventProc.ID = UpdatedEventProc.ID
END


GO
/****** Object:  StoredProcedure [dbo].[InterpolateEventProcDepthFromScanProc]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InterpolateEventProcDepthFromScanProc]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InterpolateEventProcDepthFromScanProc] AS' 
END
GO

ALTER PROCEDURE [dbo].[InterpolateEventProcDepthFromScanProc]
	@EventProcData SeaPipeDepthType readonly
AS
BEGIN


	UPDATE EventProc SET  EventProc.Depth = UpdatedEventProc.Depth, EventProc.PipeDepth = UpdatedEventProc.PipeDepth, 
	EventProc.SeabedDepth = UpdatedEventProc.SeabedDepth
	
	FROM  @EventProcData AS UpdatedEventProc
	Where EventProc.ID = UpdatedEventProc.ID
END


GO
/****** Object:  StoredProcedure [dbo].[InterpolateEventProcDepthFromScanProc_DesignKP]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InterpolateEventProcDepthFromScanProc_DesignKP]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InterpolateEventProcDepthFromScanProc_DesignKP] AS' 
END
GO

ALTER PROCEDURE [dbo].[InterpolateEventProcDepthFromScanProc_DesignKP]
	@EventProcData SeaPipeDepthType readonly
AS
BEGIN


	UPDATE EventProc SET  EventProc.Depth = UpdatedEventProc.Depth, EventProc.PipeDepth = UpdatedEventProc.PipeDepth, 
	EventProc.SeabedDepth = UpdatedEventProc.SeabedDepth
	
	FROM  @EventProcData AS UpdatedEventProc
	Where EventProc.ID = UpdatedEventProc.ID
END


GO
/****** Object:  StoredProcedure [dbo].[InterpolateEventProcDepthFromTruePipe]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InterpolateEventProcDepthFromTruePipe]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InterpolateEventProcDepthFromTruePipe] AS' 
END
GO

ALTER PROCEDURE [dbo].[InterpolateEventProcDepthFromTruePipe]
	@EventProcData SeaPipeDepthType readonly
AS
BEGIN


	UPDATE EventProc SET  EventProc.Depth = UpdatedEventProc.Depth, EventProc.PipeDepth = UpdatedEventProc.PipeDepth, 
	EventProc.SeabedDepth = UpdatedEventProc.SeabedDepth
	
	FROM  @EventProcData AS UpdatedEventProc
	Where EventProc.ID = UpdatedEventProc.ID
END


GO
/****** Object:  StoredProcedure [dbo].[InterpolateEventProcHeading_HeadingDevFromPosProc]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InterpolateEventProcHeading_HeadingDevFromPosProc]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InterpolateEventProcHeading_HeadingDevFromPosProc] AS' 
END
GO

ALTER PROCEDURE [dbo].[InterpolateEventProcHeading_HeadingDevFromPosProc]
	@EventProcData Heading_HeadingDevType readonly
AS
BEGIN


	UPDATE EventProc SET  EventProc.Heading = UpdatedEventProc.Heading, EventProc.HeadingDev = UpdatedEventProc.HeadingDev
	FROM  @EventProcData AS UpdatedEventProc
	Where EventProc.ID = UpdatedEventProc.ID
END


GO
/****** Object:  StoredProcedure [dbo].[InterpolateEventProcHeading_HeadingDevFromScanProc]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InterpolateEventProcHeading_HeadingDevFromScanProc]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InterpolateEventProcHeading_HeadingDevFromScanProc] AS' 
END
GO

ALTER PROCEDURE [dbo].[InterpolateEventProcHeading_HeadingDevFromScanProc]
	@EventProcData Heading_HeadingDevType readonly
AS
BEGIN


	UPDATE EventProc SET  EventProc.Heading = UpdatedEventProc.Heading, EventProc.HeadingDev = UpdatedEventProc.HeadingDev
	FROM  @EventProcData AS UpdatedEventProc
	Where EventProc.ID = UpdatedEventProc.ID
END


GO
/****** Object:  StoredProcedure [dbo].[InterpolateEventProcKPFromPosProc]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InterpolateEventProcKPFromPosProc]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InterpolateEventProcKPFromPosProc] AS' 
END
GO

ALTER PROCEDURE [dbo].[InterpolateEventProcKPFromPosProc]
	@EventProcData KPDataType readonly
AS
BEGIN


	UPDATE EventProc SET EventProc.KP = UpdatedEventProc.KP
	FROM  @EventProcData AS UpdatedEventProc
	Where EventProc.ID = UpdatedEventProc.ID
END


GO
/****** Object:  StoredProcedure [dbo].[InterpolateEventProcPositionFromDesignPosProc]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InterpolateEventProcPositionFromDesignPosProc]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InterpolateEventProcPositionFromDesignPosProc] AS' 
END
GO

ALTER PROCEDURE [dbo].[InterpolateEventProcPositionFromDesignPosProc]
	@EventProcData EastNorthType readonly
AS
BEGIN


	UPDATE EventProc SET  EventProc.East = UpdatedEventProc.East, EventProc.North = UpdatedEventProc.North
	FROM  @EventProcData AS UpdatedEventProc
	Where EventProc.ID = UpdatedEventProc.ID
END


GO
/****** Object:  StoredProcedure [dbo].[InterpolateEventProcPositionFromDigitizedPipe]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InterpolateEventProcPositionFromDigitizedPipe]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InterpolateEventProcPositionFromDigitizedPipe] AS' 
END
GO

ALTER PROCEDURE [dbo].[InterpolateEventProcPositionFromDigitizedPipe]
	@EventProcData EastNorthType readonly
AS
BEGIN


	UPDATE EventProc SET  EventProc.East = UpdatedEventProc.East, EventProc.North = UpdatedEventProc.North
	FROM  @EventProcData AS UpdatedEventProc
	Where EventProc.ID = UpdatedEventProc.ID
END


GO
/****** Object:  StoredProcedure [dbo].[InterpolateEventProcPositionFromPosProc]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InterpolateEventProcPositionFromPosProc]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InterpolateEventProcPositionFromPosProc] AS' 
END
GO

ALTER PROCEDURE [dbo].[InterpolateEventProcPositionFromPosProc]
	@EventProcData EastNorthType readonly
AS
BEGIN


	UPDATE EventProc SET  EventProc.East = UpdatedEventProc.East, EventProc.North = UpdatedEventProc.North
	FROM  @EventProcData AS UpdatedEventProc
	Where EventProc.ID = UpdatedEventProc.ID
END


GO
/****** Object:  StoredProcedure [dbo].[InterpolateEventProcPositionFromScanProc_Head]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InterpolateEventProcPositionFromScanProc_Head]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InterpolateEventProcPositionFromScanProc_Head] AS' 
END
GO

ALTER PROCEDURE [dbo].[InterpolateEventProcPositionFromScanProc_Head]
	@EventProcData EastNorthType readonly
AS
BEGIN


	UPDATE EventProc SET  EventProc.East = UpdatedEventProc.East, EventProc.North = UpdatedEventProc.North
	FROM  @EventProcData AS UpdatedEventProc
	Where EventProc.ID = UpdatedEventProc.ID
END


GO
/****** Object:  StoredProcedure [dbo].[InterpolateEventProcPositionFromTruePipe]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InterpolateEventProcPositionFromTruePipe]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InterpolateEventProcPositionFromTruePipe] AS' 
END
GO

ALTER PROCEDURE [dbo].[InterpolateEventProcPositionFromTruePipe]
	@EventProcData EastNorthType readonly
AS
BEGIN


	UPDATE EventProc SET  EventProc.East = UpdatedEventProc.East, EventProc.North = UpdatedEventProc.North
	FROM  @EventProcData AS UpdatedEventProc
	Where EventProc.ID = UpdatedEventProc.ID
END


GO
/****** Object:  StoredProcedure [dbo].[InterpolatePosProcDepthFromDepthLog]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InterpolatePosProcDepthFromDepthLog]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InterpolatePosProcDepthFromDepthLog] AS' 
END
GO

ALTER PROCEDURE [dbo].[InterpolatePosProcDepthFromDepthLog]
	@PosProcData DepthType readonly
AS
BEGIN


	UPDATE PosProc SET  PosProc.Depth = UpdatedPosProcProc.Depth
	FROM  @PosProcData AS UpdatedPosProcProc
	Where PosProc.ID = UpdatedPosProcProc.ID
END


GO
/****** Object:  StoredProcedure [dbo].[InterpolatePosProcHeading]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InterpolatePosProcHeading]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InterpolatePosProcHeading] AS' 
END
GO

ALTER PROCEDURE [dbo].[InterpolatePosProcHeading]
	@PosData HeadingType readonly
AS
BEGIN


	UPDATE PosProc SET PosProc.Heading = UpdatedPos.Heading FROM  @PosData AS [UpdatedPos] 
	Where PosProc.ID = UpdatedPos.ID
END



GO
/****** Object:  StoredProcedure [dbo].[InterpolatePosProcHeadingDevFromDesignPosProc]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InterpolatePosProcHeadingDevFromDesignPosProc]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InterpolatePosProcHeadingDevFromDesignPosProc] AS' 
END
GO

ALTER PROCEDURE [dbo].[InterpolatePosProcHeadingDevFromDesignPosProc]
	@PosProcData HeadingDevType readonly
AS
BEGIN


	UPDATE PosProc SET  PosProc.HeadingDev = UpdatedPosProc.HeadingDev
	FROM  @PosProcData AS [UpdatedPosProc] 
	Where PosProc.ID = UpdatedPosProc.ID
END


GO
/****** Object:  StoredProcedure [dbo].[InterpolatePosProcHeadingDevFromDigitizedPipe]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InterpolatePosProcHeadingDevFromDigitizedPipe]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InterpolatePosProcHeadingDevFromDigitizedPipe] AS' 
END
GO

ALTER PROCEDURE [dbo].[InterpolatePosProcHeadingDevFromDigitizedPipe]
	@PosProcData HeadingDevType readonly
AS
BEGIN


	UPDATE PosProc SET PosProc.HeadingDev = UpdatedPosProc.HeadingDev
	FROM  @PosProcData AS [UpdatedPosProc] 
	Where PosProc.ID = UpdatedPosProc.ID
END


GO
/****** Object:  StoredProcedure [dbo].[InterpolatePosProcHeadingDevKPFromDesignPosProc]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InterpolatePosProcHeadingDevKPFromDesignPosProc]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InterpolatePosProcHeadingDevKPFromDesignPosProc] AS' 
END
GO

ALTER PROCEDURE [dbo].[InterpolatePosProcHeadingDevKPFromDesignPosProc]
	@PosProcData HeadingDevKPType readonly
AS
BEGIN


	UPDATE PosProc SET PosProc.Kp = UpdatedPosProc.KP, PosProc.HeadingDev = UpdatedPosProc.HeadingDev
	FROM  @PosProcData AS [UpdatedPosProc] 
	Where PosProc.ID = UpdatedPosProc.ID
END


GO
/****** Object:  StoredProcedure [dbo].[InterpolatePosProcHeadingDevKPFromDigitizedPipe]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InterpolatePosProcHeadingDevKPFromDigitizedPipe]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InterpolatePosProcHeadingDevKPFromDigitizedPipe] AS' 
END
GO

ALTER PROCEDURE [dbo].[InterpolatePosProcHeadingDevKPFromDigitizedPipe]
	@PosProcData HeadingDevKPType readonly
AS
BEGIN


	UPDATE PosProc SET PosProc.KP = UpdatedPosProc.KP , PosProc.HeadingDev = UpdatedPosProc.HeadingDev
	FROM  @PosProcData AS [UpdatedPosProc] 
	Where PosProc.ID = UpdatedPosProc.ID
END


GO
/****** Object:  StoredProcedure [dbo].[InterpolatePosProcHeadingPitchRoll]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InterpolatePosProcHeadingPitchRoll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InterpolatePosProcHeadingPitchRoll] AS' 
END
GO

ALTER PROCEDURE [dbo].[InterpolatePosProcHeadingPitchRoll]
	@PosData RollPitchHeadingType readonly
AS
BEGIN


	UPDATE PosProc SET PosProc.Roll = UpdatedPos.Roll, PosProc.Heading = UpdatedPos.Heading, PosProc.Pitch = UpdatedPos.Pitch
	FROM  @PosData AS [UpdatedPos] 
	Where PosProc.ID = UpdatedPos.ID
END


GO
/****** Object:  StoredProcedure [dbo].[InterpolatePosProcKPFromDesignPosProc]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InterpolatePosProcKPFromDesignPosProc]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InterpolatePosProcKPFromDesignPosProc] AS' 
END
GO

ALTER PROCEDURE [dbo].[InterpolatePosProcKPFromDesignPosProc]
	@PosProcData PosProcKPType readonly
AS
BEGIN


	UPDATE PosProc SET  PosProc.Kp = UpdatedPosProc.KP
	FROM  @PosProcData AS [UpdatedPosProc] 
	Where PosProc.ID = UpdatedPosProc.ID
END


GO
/****** Object:  StoredProcedure [dbo].[InterpolatePosProcKPFromDigitizedPipe]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InterpolatePosProcKPFromDigitizedPipe]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InterpolatePosProcKPFromDigitizedPipe] AS' 
END
GO

ALTER PROCEDURE [dbo].[InterpolatePosProcKPFromDigitizedPipe]
	@PosProcData PosProcKPType readonly
AS
BEGIN


	UPDATE PosProc SET PosProc.Kp = UpdatedPosProc.KP
	FROM  @PosProcData AS [UpdatedPosProc] 
	Where PosProc.ID = UpdatedPosProc.ID
END


GO
/****** Object:  StoredProcedure [dbo].[InterpolatePosProcPitch]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InterpolatePosProcPitch]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InterpolatePosProcPitch] AS' 
END
GO

ALTER PROCEDURE [dbo].[InterpolatePosProcPitch]
	@PosData PitchType readonly
AS
BEGIN


	UPDATE PosProc SET PosProc.Pitch = UpdatedPos.Pitch FROM  @PosData AS [UpdatedPos] 
	Where PosProc.ID = UpdatedPos.ID
END


GO
/****** Object:  StoredProcedure [dbo].[InterpolatePosProcRoll]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InterpolatePosProcRoll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InterpolatePosProcRoll] AS' 
END
GO

ALTER PROCEDURE [dbo].[InterpolatePosProcRoll]
	@PosData RollType readonly
AS
BEGIN


	UPDATE PosProc SET PosProc.Roll = UpdatedPos.Roll FROM  @PosData AS [UpdatedPos] 
	Where PosProc.ID = UpdatedPos.ID
END


GO
/****** Object:  StoredProcedure [dbo].[InterpolatePRCNavigationDepthFromDepthLog]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InterpolatePRCNavigationDepthFromDepthLog]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InterpolatePRCNavigationDepthFromDepthLog] AS' 
END
GO

ALTER PROCEDURE [dbo].[InterpolatePRCNavigationDepthFromDepthLog]
	@PRCData DepthType readonly
AS
BEGIN


	UPDATE PRCNavigationDetails SET PRCNavigationDetails.Depth = UpdatedPRC.Depth
	FROM  @PRCData AS [UpdatedPRC] 
	Where PRCNavigationDetails.ID = UpdatedPRC.ID
END



GO
/****** Object:  StoredProcedure [dbo].[InterpolatePRCNavigationHeadingDevKPFromPosProc]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InterpolatePRCNavigationHeadingDevKPFromPosProc]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InterpolatePRCNavigationHeadingDevKPFromPosProc] AS' 
END
GO

ALTER PROCEDURE [dbo].[InterpolatePRCNavigationHeadingDevKPFromPosProc]
	@PRCData headingDevKPType readonly
AS
BEGIN


	UPDATE PRCNavigationDetails SET PRCNavigationDetails.HeadingDev = UpdatedPRC.HeadingDev,
	PRCNavigationDetails.KP = UpdatedPRC.KP
	FROM  @PRCData AS [UpdatedPRC] 
	Where PRCNavigationDetails.ID = UpdatedPRC.ID
END




GO
/****** Object:  StoredProcedure [dbo].[InterpolatePRCNavigationPitchRollHeading]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InterpolatePRCNavigationPitchRollHeading]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InterpolatePRCNavigationPitchRollHeading] AS' 
END
GO

ALTER PROCEDURE [dbo].[InterpolatePRCNavigationPitchRollHeading]
	@PRCData RollPitchHeadingType readonly
AS
BEGIN


	UPDATE PRCNavigationDetails SET PRCNavigationDetails.Roll = UpdatedPRC.Roll, PRCNavigationDetails.Heading = UpdatedPRC.Heading, 
	PRCNavigationDetails.Pitch = UpdatedPRC.Pitch
	FROM  @PRCData AS [UpdatedPRC] 
	Where PRCNavigationDetails.ID = UpdatedPRC.ID
END


GO
/****** Object:  StoredProcedure [dbo].[InterpolatePTrackerProcDepthFromDepthLog]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InterpolatePTrackerProcDepthFromDepthLog]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InterpolatePTrackerProcDepthFromDepthLog] AS' 
END
GO

ALTER PROCEDURE [dbo].[InterpolatePTrackerProcDepthFromDepthLog]
	@PTrackerData DepthType readonly
AS
BEGIN


	UPDATE PTrackerProc SET PTrackerProc.Depth = UpdatedData.Depth
						
	                                
	FROM  @PTrackerData AS UpdatedData
	Where PTrackerProc.ID = UpdatedData.ID 
END


GO
/****** Object:  StoredProcedure [dbo].[InterpolatePTrackerProcDepthFromScanProc]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InterpolatePTrackerProcDepthFromScanProc]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InterpolatePTrackerProcDepthFromScanProc] AS' 
END
GO

ALTER PROCEDURE [dbo].[InterpolatePTrackerProcDepthFromScanProc]
	@PTrackerData DepthType readonly
AS
BEGIN


	UPDATE PTrackerProc SET PTrackerProc.Depth = UpdatedData.Depth
						
	                                
	FROM  @PTrackerData AS UpdatedData
	Where PTrackerProc.ID = UpdatedData.ID 
END


GO
/****** Object:  StoredProcedure [dbo].[InterpolatePTrackerProcHeading]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InterpolatePTrackerProcHeading]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InterpolatePTrackerProcHeading] AS' 
END
GO

ALTER PROCEDURE [dbo].[InterpolatePTrackerProcHeading]
	@PTrackerData HeadingType readonly
AS
BEGIN


	UPDATE PTrackerProc SET PTrackerProc.Heading = UpdatedData.Heading
						
	                                
	FROM  @PTrackerData AS UpdatedData
	Where PTrackerProc.ID = UpdatedData.ID 
END


GO
/****** Object:  StoredProcedure [dbo].[InterpolatePTrackerProcHeadingDevFromDesignPosProc]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InterpolatePTrackerProcHeadingDevFromDesignPosProc]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InterpolatePTrackerProcHeadingDevFromDesignPosProc] AS' 
END
GO

ALTER PROCEDURE [dbo].[InterpolatePTrackerProcHeadingDevFromDesignPosProc]
	@PTrackerData HeadingDevType readonly
AS
BEGIN


	UPDATE PTrackerProc SET PTrackerProc.HeadingDev = UpdatedData.HeadingDev
						
	                                
	FROM  @PTrackerData AS UpdatedData
	Where PTrackerProc.ID = UpdatedData.ID 
END


GO
/****** Object:  StoredProcedure [dbo].[InterpolatePTrackerProcHeadingDevFromDigitizedPipe]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InterpolatePTrackerProcHeadingDevFromDigitizedPipe]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InterpolatePTrackerProcHeadingDevFromDigitizedPipe] AS' 
END
GO

ALTER PROCEDURE [dbo].[InterpolatePTrackerProcHeadingDevFromDigitizedPipe]
	@PTrackerData HeadingDevType readonly
AS
BEGIN


	UPDATE PTrackerProc SET PTrackerProc.HeadingDev = UpdatedData.HeadingDev
						
	                                
	FROM  @PTrackerData AS UpdatedData
	Where PTrackerProc.ID = UpdatedData.ID 
END


GO
/****** Object:  StoredProcedure [dbo].[InterpolatePTrackerProcHeadingDevFromOldPosProc]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InterpolatePTrackerProcHeadingDevFromOldPosProc]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InterpolatePTrackerProcHeadingDevFromOldPosProc] AS' 
END
GO

ALTER PROCEDURE [dbo].[InterpolatePTrackerProcHeadingDevFromOldPosProc]
	@PTrackerData HeadingDevType readonly
AS
BEGIN


	UPDATE PTrackerProc SET PTrackerProc.HeadingDev = UpdatedData.HeadingDev
						
	                                
	FROM  @PTrackerData AS UpdatedData
	Where PTrackerProc.ID = UpdatedData.ID 
END


GO
/****** Object:  StoredProcedure [dbo].[InterpolatePTrackerProcHeadingDevFromPosProc]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InterpolatePTrackerProcHeadingDevFromPosProc]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InterpolatePTrackerProcHeadingDevFromPosProc] AS' 
END
GO

ALTER PROCEDURE [dbo].[InterpolatePTrackerProcHeadingDevFromPosProc]
	@PTrackerData HeadingDevType readonly
AS
BEGIN


	UPDATE PTrackerProc SET PTrackerProc.HeadingDev = UpdatedData.HeadingDev
						
	                                
	FROM  @PTrackerData AS UpdatedData
	Where PTrackerProc.ID = UpdatedData.ID 
END


GO
/****** Object:  StoredProcedure [dbo].[InterpolatePTrackerProcHeadingDevKPFromDesignPosProc]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InterpolatePTrackerProcHeadingDevKPFromDesignPosProc]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InterpolatePTrackerProcHeadingDevKPFromDesignPosProc] AS' 
END
GO

ALTER PROCEDURE [dbo].[InterpolatePTrackerProcHeadingDevKPFromDesignPosProc]
	@PTrackerData HeadingDevKpType readonly
AS
BEGIN


	UPDATE PTrackerProc SET PTrackerProc.HeadingDev = UpdatedData.HeadingDev,PTrackerProc.KP = UpdatedData.KP
						
	                                
	FROM  @PTrackerData AS UpdatedData
	Where PTrackerProc.ID = UpdatedData.ID 
END



GO
/****** Object:  StoredProcedure [dbo].[InterpolatePTrackerProcHeadingDevKPFromDigitizedPipe]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InterpolatePTrackerProcHeadingDevKPFromDigitizedPipe]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InterpolatePTrackerProcHeadingDevKPFromDigitizedPipe] AS' 
END
GO

ALTER PROCEDURE [dbo].[InterpolatePTrackerProcHeadingDevKPFromDigitizedPipe]
	@PTrackerData HeadingDevKpType readonly
AS
BEGIN


	UPDATE PTrackerProc SET PTrackerProc.HeadingDev = UpdatedData.HeadingDev,PTrackerProc.KP = UpdatedData.KP
						
	                                
	FROM  @PTrackerData AS UpdatedData
	Where PTrackerProc.ID = UpdatedData.ID 
END


GO
/****** Object:  StoredProcedure [dbo].[InterpolatePTrackerProcHeadingDevKPFromOldPosProc]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InterpolatePTrackerProcHeadingDevKPFromOldPosProc]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InterpolatePTrackerProcHeadingDevKPFromOldPosProc] AS' 
END
GO

ALTER PROCEDURE [dbo].[InterpolatePTrackerProcHeadingDevKPFromOldPosProc]
	@PTrackerData HeadingDevKpType readonly
AS
BEGIN


	UPDATE PTrackerProc SET PTrackerProc.HeadingDev = UpdatedData.HeadingDev, PTrackerProc.KP = UpdatedData.KP
						
	                                
	FROM  @PTrackerData AS UpdatedData
	Where PTrackerProc.ID = UpdatedData.ID 
END


GO
/****** Object:  StoredProcedure [dbo].[InterpolatePTrackerProcHeadingDevKPFromPosProc]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InterpolatePTrackerProcHeadingDevKPFromPosProc]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InterpolatePTrackerProcHeadingDevKPFromPosProc] AS' 
END
GO

ALTER PROCEDURE [dbo].[InterpolatePTrackerProcHeadingDevKPFromPosProc]
	@PTrackerData HeadingDevKpType readonly
AS
BEGIN


	UPDATE PTrackerProc SET PTrackerProc.HeadingDev = UpdatedData.HeadingDev, PTrackerProc.KP = UpdatedData.KP
						
	                                
	FROM  @PTrackerData AS UpdatedData
	Where PTrackerProc.ID = UpdatedData.ID 
END


GO
/****** Object:  StoredProcedure [dbo].[InterpolatePTrackerProcHeadingPitchRoll]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InterpolatePTrackerProcHeadingPitchRoll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InterpolatePTrackerProcHeadingPitchRoll] AS' 
END
GO

ALTER PROCEDURE [dbo].[InterpolatePTrackerProcHeadingPitchRoll]
	@PTrackerData RollPitchHeadingType readonly
AS
BEGIN


	UPDATE PTrackerProc SET  PTrackerProc.Roll = UpdatedData.Roll, PTrackerProc.Pitch = UpdatedData.Pitch,
							 PTrackerProc.Heading = UpdatedData.Heading
	                                
	FROM  @PTrackerData AS UpdatedData
	Where PTrackerProc.ID = UpdatedData.ID 
END


GO
/****** Object:  StoredProcedure [dbo].[InterpolatePTrackerProcKPFromDesignPosProc]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InterpolatePTrackerProcKPFromDesignPosProc]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InterpolatePTrackerProcKPFromDesignPosProc] AS' 
END
GO

ALTER PROCEDURE [dbo].[InterpolatePTrackerProcKPFromDesignPosProc]
	@PTrackerData KpDataType readonly
AS
BEGIN


	UPDATE PTrackerProc SET PTrackerProc.KP = UpdatedData.KP
						
	                                
	FROM  @PTrackerData AS UpdatedData
	Where PTrackerProc.ID = UpdatedData.ID 
END


GO
/****** Object:  StoredProcedure [dbo].[InterpolatePTrackerProcKPFromDigitizedPipe]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InterpolatePTrackerProcKPFromDigitizedPipe]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InterpolatePTrackerProcKPFromDigitizedPipe] AS' 
END
GO

ALTER PROCEDURE [dbo].[InterpolatePTrackerProcKPFromDigitizedPipe]
	@PTrackerData KPDataType readonly
AS
BEGIN


	UPDATE PTrackerProc SET PTrackerProc.KP = UpdatedData.KP
						
	                                
	FROM  @PTrackerData AS UpdatedData
	Where PTrackerProc.ID = UpdatedData.ID 
END


GO
/****** Object:  StoredProcedure [dbo].[InterpolatePTrackerProcKPFromOldPosProc]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InterpolatePTrackerProcKPFromOldPosProc]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InterpolatePTrackerProcKPFromOldPosProc] AS' 
END
GO

ALTER PROCEDURE [dbo].[InterpolatePTrackerProcKPFromOldPosProc]
	@PTrackerData KpDataType readonly
AS
BEGIN


	UPDATE PTrackerProc SET PTrackerProc.KP = UpdatedData.KP
						
	                                
	FROM  @PTrackerData AS UpdatedData
	Where PTrackerProc.ID = UpdatedData.ID 
END


GO
/****** Object:  StoredProcedure [dbo].[InterpolatePTrackerProcKPFromPosProc]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InterpolatePTrackerProcKPFromPosProc]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InterpolatePTrackerProcKPFromPosProc] AS' 
END
GO

ALTER PROCEDURE [dbo].[InterpolatePTrackerProcKPFromPosProc]
	@PTrackerData KpDataType readonly
AS
BEGIN


	UPDATE PTrackerProc SET PTrackerProc.KP = UpdatedData.KP
						
	                                
	FROM  @PTrackerData AS UpdatedData
	Where PTrackerProc.ID = UpdatedData.ID 
END


GO
/****** Object:  StoredProcedure [dbo].[InterpolatePTrackerProcPitch]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InterpolatePTrackerProcPitch]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InterpolatePTrackerProcPitch] AS' 
END
GO

ALTER PROCEDURE [dbo].[InterpolatePTrackerProcPitch]
	@PTrackerData PitchType readonly
AS
BEGIN


	UPDATE PTrackerProc SET PTrackerProc.Pitch = UpdatedData.Pitch
						
	                                
	FROM  @PTrackerData AS UpdatedData
	Where PTrackerProc.ID = UpdatedData.ID 
END


GO
/****** Object:  StoredProcedure [dbo].[InterpolatePTrackerProcPositionFromDesignPosProc]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InterpolatePTrackerProcPositionFromDesignPosProc]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InterpolatePTrackerProcPositionFromDesignPosProc] AS' 
END
GO

ALTER PROCEDURE [dbo].[InterpolatePTrackerProcPositionFromDesignPosProc]
	@PTrackerData EastNorthType readonly
AS
BEGIN


	UPDATE PTrackerProc SET PTrackerProc.East = UpdatedData.East,PTrackerProc.North = UpdatedData.North
						
	                                
	FROM  @PTrackerData AS UpdatedData
	Where PTrackerProc.ID = UpdatedData.ID 
END


GO
/****** Object:  StoredProcedure [dbo].[InterpolatePTrackerProcPositionFromDigitizedPipe]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InterpolatePTrackerProcPositionFromDigitizedPipe]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InterpolatePTrackerProcPositionFromDigitizedPipe] AS' 
END
GO

ALTER PROCEDURE [dbo].[InterpolatePTrackerProcPositionFromDigitizedPipe]
	@PTrackerData EastNorthType readonly
AS
BEGIN


	UPDATE PTrackerProc SET PTrackerProc.East = UpdatedData.East,PTrackerProc.North = UpdatedData.North
						
	                                
	FROM  @PTrackerData AS UpdatedData
	Where PTrackerProc.ID = UpdatedData.ID 
END


GO
/****** Object:  StoredProcedure [dbo].[InterpolatePTrackerProcPositionFromPosProc]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InterpolatePTrackerProcPositionFromPosProc]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InterpolatePTrackerProcPositionFromPosProc] AS' 
END
GO

ALTER PROCEDURE [dbo].[InterpolatePTrackerProcPositionFromPosProc]
	@PTrackerData EastNorthType readonly
AS
BEGIN


	UPDATE PTrackerProc SET PTrackerProc.East = UpdatedData.East, PTrackerProc.North = UpdatedData.North
						
	                                
	FROM  @PTrackerData AS UpdatedData
	Where PTrackerProc.ID = UpdatedData.ID 
END


GO
/****** Object:  StoredProcedure [dbo].[InterpolatePTrackerProcRoll]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InterpolatePTrackerProcRoll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InterpolatePTrackerProcRoll] AS' 
END
GO

ALTER PROCEDURE [dbo].[InterpolatePTrackerProcRoll]
	@PTrackerData RollType readonly
AS
BEGIN


	UPDATE PTrackerProc SET PTrackerProc.Roll = UpdatedData.Roll
						
	                                
	FROM  @PTrackerData AS UpdatedData
	Where PTrackerProc.ID = UpdatedData.ID 
END


GO
/****** Object:  StoredProcedure [dbo].[InterpolateScanProcDepthFromDepthLog]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InterpolateScanProcDepthFromDepthLog]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InterpolateScanProcDepthFromDepthLog] AS' 
END
GO

ALTER PROCEDURE [dbo].[InterpolateScanProcDepthFromDepthLog]
	@ScantProcData DepthType readonly
AS
BEGIN


	UPDATE ScanProc SET  ScanProc.Depth = UpdatedScanProc.Depth
	FROM  @ScantProcData AS UpdatedScanProc
	Where ScanProc.ID = UpdatedScanProc.ID
END


GO
/****** Object:  StoredProcedure [dbo].[InterpolateScanProcDepthFromDesignScanProc]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InterpolateScanProcDepthFromDesignScanProc]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InterpolateScanProcDepthFromDesignScanProc] AS' 
END
GO

ALTER PROCEDURE [dbo].[InterpolateScanProcDepthFromDesignScanProc]
	@ScantProcData DepthType readonly
AS
BEGIN


	UPDATE ScanProc SET  ScanProc.Depth = UpdatedScanProc.Depth
	FROM  @ScantProcData AS UpdatedScanProc
	Where ScanProc.ID = UpdatedScanProc.ID
END


GO
/****** Object:  StoredProcedure [dbo].[InterpolateScanProcHeading]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InterpolateScanProcHeading]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InterpolateScanProcHeading] AS' 
END
GO

ALTER PROCEDURE [dbo].[InterpolateScanProcHeading]
	@ScanData HeadingType readonly
AS
BEGIN


	UPDATE ScanProc SET ScanProc.Heading = UpdatedScan.Heading FROM  @ScanData AS [UpdatedScan] 
	Where ScanProc.ID = UpdatedScan.ID
END


GO
/****** Object:  StoredProcedure [dbo].[InterpolateScanProcHeadingDevFromDigitizedPipe]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InterpolateScanProcHeadingDevFromDigitizedPipe]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InterpolateScanProcHeadingDevFromDigitizedPipe] AS' 
END
GO

ALTER PROCEDURE [dbo].[InterpolateScanProcHeadingDevFromDigitizedPipe]
	@ScanProcData HeadingDevType readonly
AS
BEGIN


	UPDATE ScanProc SET ScanProc.HeadingDev = UpdatedScanProc.HeadingDev
	FROM  @ScanProcData AS UpdatedScanProc
	Where ScanProc.ID = UpdatedScanProc.ID
END


GO
/****** Object:  StoredProcedure [dbo].[InterpolateScanProcHeadingDevFromPosProc]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InterpolateScanProcHeadingDevFromPosProc]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InterpolateScanProcHeadingDevFromPosProc] AS' 
END
GO

ALTER PROCEDURE [dbo].[InterpolateScanProcHeadingDevFromPosProc]
	@ScanData HeadingDevType readonly
AS
BEGIN


	UPDATE ScanProc SET ScanProc.HeadingDev = UpdatedScan.HeadingDev FROM  @ScanData AS [UpdatedScan] 
	Where ScanProc.ID = UpdatedScan.ID
END



GO
/****** Object:  StoredProcedure [dbo].[InterpolateScanProcHeadingDevKPFromDigitizedPipe]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InterpolateScanProcHeadingDevKPFromDigitizedPipe]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InterpolateScanProcHeadingDevKPFromDigitizedPipe] AS' 
END
GO

ALTER PROCEDURE [dbo].[InterpolateScanProcHeadingDevKPFromDigitizedPipe]
	@ScanProcData HeadingDevKPType readonly
AS
BEGIN


	UPDATE ScanProc SET ScanProc.HeadingDev = UpdatedScanProc.HeadingDev, ScanProc.KP = UpdatedScanProc.KP
	FROM  @ScanProcData AS UpdatedScanProc
	Where ScanProc.ID = UpdatedScanProc.ID
END


GO
/****** Object:  StoredProcedure [dbo].[InterpolateScanProcHeadingDevKPFromPosProc]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InterpolateScanProcHeadingDevKPFromPosProc]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InterpolateScanProcHeadingDevKPFromPosProc] AS' 
END
GO

ALTER PROCEDURE [dbo].[InterpolateScanProcHeadingDevKPFromPosProc]
	@ScanProcData HeadingDevKPType readonly
AS
BEGIN


	UPDATE ScanProc SET ScanProc.KP = UpdatedScanProc.KP, ScanProc.HeadingDev = UpdatedScanProc.HeadingDev
	FROM  @ScanProcData AS UpdatedScanProc
	Where ScanProc.ID = UpdatedScanProc.ID
END


GO
/****** Object:  StoredProcedure [dbo].[InterpolateScanProcHeadingPitchRoll]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InterpolateScanProcHeadingPitchRoll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InterpolateScanProcHeadingPitchRoll] AS' 
END
GO

ALTER PROCEDURE [dbo].[InterpolateScanProcHeadingPitchRoll]
	@ScanData RollPitchHeadingType readonly
AS
BEGIN


	UPDATE ScanProc SET ScanProc.Roll = UpdatedScan.Roll, ScanProc.Heading = UpdatedScan.Heading, ScanProc.Pitch = UpdatedScan.Pitch
	FROM  @ScanData AS [UpdatedScan] 
	Where ScanProc.ID = UpdatedScan.ID
END


GO
/****** Object:  StoredProcedure [dbo].[InterpolateScanProcKPFromDigitizedPipe]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InterpolateScanProcKPFromDigitizedPipe]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InterpolateScanProcKPFromDigitizedPipe] AS' 
END
GO

ALTER PROCEDURE [dbo].[InterpolateScanProcKPFromDigitizedPipe]
	@ScanProcData KPDataType readonly
AS
BEGIN


	UPDATE ScanProc SET ScanProc.KP = UpdatedScanProc.KP
	FROM  @ScanProcData AS UpdatedScanProc
	Where ScanProc.ID = UpdatedScanProc.ID
END


GO
/****** Object:  StoredProcedure [dbo].[InterpolateScanProcKPFromPosProc]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InterpolateScanProcKPFromPosProc]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InterpolateScanProcKPFromPosProc] AS' 
END
GO

ALTER PROCEDURE [dbo].[InterpolateScanProcKPFromPosProc]
	@ScanData KPDataType readonly
AS
BEGIN


	UPDATE ScanProc SET ScanProc.KP = UpdatedScan.KP
	FROM  @ScanData AS [UpdatedScan] 
	Where ScanProc.ID = UpdatedScan.ID
END


GO
/****** Object:  StoredProcedure [dbo].[InterpolateScanProcPitch]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InterpolateScanProcPitch]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InterpolateScanProcPitch] AS' 
END
GO

ALTER PROCEDURE [dbo].[InterpolateScanProcPitch]
	@ScanData PitchType readonly
AS
BEGIN


	UPDATE ScanProc SET ScanProc.Pitch = UpdatedScan.Pitch FROM  @ScanData AS [UpdatedScan] 
	Where ScanProc.ID = UpdatedScan.ID
END


GO
/****** Object:  StoredProcedure [dbo].[InterpolateScanProcPositionFromDesignPosProc]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InterpolateScanProcPositionFromDesignPosProc]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InterpolateScanProcPositionFromDesignPosProc] AS' 
END
GO

ALTER PROCEDURE [dbo].[InterpolateScanProcPositionFromDesignPosProc]
	@ScanProcData EastNorthType readonly
AS
BEGIN


	UPDATE ScanProc SET  ScanProc.East = UpdatedScanProc.East, ScanProc.North = UpdatedScanProc.North
	FROM  @ScanProcData AS UpdatedScanProc
	Where ScanProc.ID = UpdatedScanProc.ID
END


GO
/****** Object:  StoredProcedure [dbo].[InterpolateScanProcPositionFromDigitizedPipe]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InterpolateScanProcPositionFromDigitizedPipe]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InterpolateScanProcPositionFromDigitizedPipe] AS' 
END
GO

ALTER PROCEDURE [dbo].[InterpolateScanProcPositionFromDigitizedPipe]
	@ScanProcData EastNorthDepthType readonly
AS
BEGIN


	UPDATE ScanProc SET  ScanProc.East = UpdatedScanProc.East, ScanProc.North = UpdatedScanProc.North, ScanProc.Depth = UpdatedScanProc.Depth
	FROM  @ScanProcData AS UpdatedScanProc
	Where ScanProc.ID = UpdatedScanProc.ID
END
GO
/****** Object:  StoredProcedure [dbo].[InterpolateScanProcPositionFromPosProc]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InterpolateScanProcPositionFromPosProc]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InterpolateScanProcPositionFromPosProc] AS' 
END
GO

ALTER PROCEDURE [dbo].[InterpolateScanProcPositionFromPosProc]
	@ScanProcData EastNorthType readonly
AS
BEGIN


	UPDATE ScanProc SET  ScanProc.East = UpdatedScanProc.East, ScanProc.North = UpdatedScanProc.North
	FROM  @ScanProcData AS UpdatedScanProc
	Where ScanProc.ID = UpdatedScanProc.ID
END


GO
/****** Object:  StoredProcedure [dbo].[InterpolateScanProcPositionFromPosProc_DontShowPipe]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InterpolateScanProcPositionFromPosProc_DontShowPipe]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InterpolateScanProcPositionFromPosProc_DontShowPipe] AS' 
END
GO

ALTER PROCEDURE [dbo].[InterpolateScanProcPositionFromPosProc_DontShowPipe]
	@ScanProcData EastNorthType readonly
AS
BEGIN


	UPDATE ScanProc SET  ScanProc.East = UpdatedScanProc.East, ScanProc.North = UpdatedScanProc.North
	FROM  @ScanProcData AS UpdatedScanProc
	Where ScanProc.ID = UpdatedScanProc.ID
END



GO
/****** Object:  StoredProcedure [dbo].[InterpolateScanProcRoll]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InterpolateScanProcRoll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InterpolateScanProcRoll] AS' 
END
GO

ALTER PROCEDURE [dbo].[InterpolateScanProcRoll]
	@ScanData RollType readonly
AS
BEGIN


	UPDATE ScanProc SET ScanProc.Roll = UpdatedScan.Roll FROM  @ScanData AS [UpdatedScan] 
	Where ScanProc.ID = UpdatedScan.ID
END


GO
/****** Object:  StoredProcedure [dbo].[PRC_PRCNavigationDetails_Insert]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PRC_PRCNavigationDetails_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PRC_PRCNavigationDetails_Insert] AS' 
END
GO



ALTER PROCEDURE [dbo].[PRC_PRCNavigationDetails_Insert]
	@TableValue AS [PRC_PRCNavigationDetailsType] READONLY,
	@AllowDelete AS BIT = 0
AS
BEGIN
	BEGIN TRANSACTION Trans
	BEGIN TRY
	IF @AllowDelete = 1
		BEGIN
			DELETE FROM [PRCNavigationDetails] WHERE FK_PRCNavID IN (
				SELECT FK_PRCNavID FROM @TableValue
			)
		End
    	SET IDENTITY_INSERT [PRCNavigationDetails] OFF
		INSERT INTO dbo.PRCNavigationDetails
		        ( FK_PRCNavID ,
		          DTime ,
		          East ,
		          North ,
		          KP ,
		          Depth ,
		          Heading ,
		          Pitch ,
		          Roll ,
				  HeadingDev ,
				  [Timestamp] ,
		          ImageNames ,
				  DroppedFrame ,
				  ROVID,
			      Discard,
			      INS_TimeTag,
			      INS_Latitude,
			      INS_Longitude,
			      INS_Depth,
			      INS_Altitude,
			      INS_Roll,
			      INS_Pitch,
			      INS_Heading,
			      INS_VelocityNorth,
			      INS_VelocityEast,
			      INS_VelocityDown,
			      INS_wFwd,
			      INS_wStbd,
			      INS_wDwn,
			      INS_AccelerationFwd,
			      INS_AccelerationStbd,
			      INS_AccelerationDwn,
			      INS_PosMajor,
			      INS_PosMinor,
			      INS_dirPMajor,
			      INS_StdDepth,
			      INS_StdLevN,
			      INS_StdLevE,
			      INS_stdHeading,
			      INS_velMajor,
			      INS_velMinor,
			      INS_dirVMajor,
			      INS_VelDown,
			      INS_Status,
				  INS_East,
				  INS_North,
				  INS_CurrentDTime,
				  INS_PreviousDTime
		        )
		SELECT FK_PRCNavID ,
               DTime ,
               East ,
               North ,
               KP ,
               Depth ,
               Heading ,
               Pitch ,
               Roll ,
               HeadingDev ,
			   [Timestamp] ,
               ImageNames ,
			   DroppedFrame ,
			   ROVID,
			   Discard,
			   INS_TimeTag,
			   INS_Latitude,
			   INS_Longitude,
			   INS_Depth,
			   INS_Altitude,
			   INS_Roll,
			   INS_Pitch,
			   INS_Heading,
			   INS_VelocityNorth,
			   INS_VelocityEast,
			   INS_VelocityDown,
			   INS_wFwd,
			   INS_wStbd,
			   INS_wDwn,
			   INS_AccelerationFwd,
			   INS_AccelerationStbd,
			   INS_AccelerationDwn,
			   INS_PosMajor,
			   INS_PosMinor,
			   INS_dirPMajor,
			   INS_StdDepth,
			   INS_StdLevN,
			   INS_StdLevE,
			   INS_stdHeading,
			   INS_velMajor,
			   INS_velMinor,
			   INS_dirVMajor,
			   INS_VelDown,
			   INS_Status,
			   INS_East,
			   INS_North,
			   INS_CurrentDTime,
			   INS_PreviousDTime
		FROM @TableValue 
		COMMIT TRANSACTION Trans
	END TRY
    BEGIN CATCH
		ROLLBACK TRANSACTION Trans
	END CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[PRC_PRCNavigationDetails_Update]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PRC_PRCNavigationDetails_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[PRC_PRCNavigationDetails_Update] AS' 
END
GO






ALTER PROCEDURE [dbo].[PRC_PRCNavigationDetails_Update]
(
	@TableValue AS [IMP_PRCNavigationDetailsType] READONLY,
	@UpdatePitchRollHeading AS BIT = 1,
	@UpdateHeadingDeviationKP AS BIT = 1,
    @UpdateDepth AS BIT = 1,
	@UpdateEastNorth AS BIT = 1
)
AS
BEGIN
	BEGIN TRANSACTION Trans
	BEGIN TRY
		UPDATE PRCNavigationDetails
		SET PRCNavigationDetails.Pitch = IIF(@UpdatePitchRollHeading = 1, UpdatedPRCNavigationDetails.Pitch, PRCNavigationDetails.Pitch)
		  , PRCNavigationDetails.[Roll] = IIF(@UpdatePitchRollHeading = 1, UpdatedPRCNavigationDetails.[Roll], PRCNavigationDetails.[Roll])
		  , PRCNavigationDetails.Heading = IIF(@UpdatePitchRollHeading = 1, UpdatedPRCNavigationDetails.Heading, PRCNavigationDetails.Heading)
		  , PRCNavigationDetails.HeadingDev = IIF(@UpdateHeadingDeviationKP = 1, UpdatedPRCNavigationDetails.HeadingDev, PRCNavigationDetails.HeadingDev)
		  , PRCNavigationDetails.KP = IIF(@UpdateHeadingDeviationKP = 1, UpdatedPRCNavigationDetails.KP, PRCNavigationDetails.KP)
		  , PRCNavigationDetails.Depth = IIF(@UpdateDepth = 1, UpdatedPRCNavigationDetails.Depth, PRCNavigationDetails.Depth)
		  , PRCNavigationDetails.East = IIF(@UpdateEastNorth = 1, UpdatedPRCNavigationDetails.East, PRCNavigationDetails.East)
		  , PRCNavigationDetails.North = IIF(@UpdateEastNorth = 1, UpdatedPRCNavigationDetails.North, PRCNavigationDetails.North)
		  , PRCNavigationDetails.INS_TimeTag          = UpdatedPRCNavigationDetails.INS_TimeTag       
		  , PRCNavigationDetails.INS_Latitude		  = UpdatedPRCNavigationDetails.INS_Latitude		 
		  , PRCNavigationDetails.INS_Longitude		  = UpdatedPRCNavigationDetails.INS_Longitude		 
		  , PRCNavigationDetails.INS_Depth			  = UpdatedPRCNavigationDetails.INS_Depth			 
		  , PRCNavigationDetails.INS_Altitude		  = UpdatedPRCNavigationDetails.INS_Altitude		 
		  , PRCNavigationDetails.INS_Roll			  = UpdatedPRCNavigationDetails.INS_Roll			 
		  , PRCNavigationDetails.INS_Pitch			  = UpdatedPRCNavigationDetails.INS_Pitch			 
		  , PRCNavigationDetails.INS_Heading		  = UpdatedPRCNavigationDetails.INS_Heading		 
		  , PRCNavigationDetails.INS_VelocityNorth	  = UpdatedPRCNavigationDetails.INS_VelocityNorth	 
		  , PRCNavigationDetails.INS_VelocityEast	  = UpdatedPRCNavigationDetails.INS_VelocityEast	 
		  , PRCNavigationDetails.INS_VelocityDown	  = UpdatedPRCNavigationDetails.INS_VelocityDown	 
		  , PRCNavigationDetails.INS_wFwd			  = UpdatedPRCNavigationDetails.INS_wFwd			 
		  , PRCNavigationDetails.INS_wStbd			  = UpdatedPRCNavigationDetails.INS_wStbd			 
		  , PRCNavigationDetails.INS_wDwn			  = UpdatedPRCNavigationDetails.INS_wDwn			 
		  , PRCNavigationDetails.INS_AccelerationFwd  = UpdatedPRCNavigationDetails.INS_AccelerationFwd 
		  , PRCNavigationDetails.INS_AccelerationStbd = UpdatedPRCNavigationDetails.INS_AccelerationStbd
		  , PRCNavigationDetails.INS_AccelerationDwn  = UpdatedPRCNavigationDetails.INS_AccelerationDwn 
		  , PRCNavigationDetails.INS_PosMajor		  = UpdatedPRCNavigationDetails.INS_PosMajor		 
		  , PRCNavigationDetails.INS_PosMinor		  = UpdatedPRCNavigationDetails.INS_PosMinor		 
		  , PRCNavigationDetails.INS_dirPMajor		  = UpdatedPRCNavigationDetails.INS_dirPMajor		 
		  , PRCNavigationDetails.INS_StdDepth		  = UpdatedPRCNavigationDetails.INS_StdDepth		 
		  , PRCNavigationDetails.INS_StdLevN		  = UpdatedPRCNavigationDetails.INS_StdLevN		 
		  , PRCNavigationDetails.INS_StdLevE		  = UpdatedPRCNavigationDetails.INS_StdLevE		 
		  , PRCNavigationDetails.INS_stdHeading		  = UpdatedPRCNavigationDetails.INS_stdHeading		 
		  , PRCNavigationDetails.INS_velMajor		  = UpdatedPRCNavigationDetails.INS_velMajor		 
		  , PRCNavigationDetails.INS_velMinor		  = UpdatedPRCNavigationDetails.INS_velMinor		 
		  , PRCNavigationDetails.INS_dirVMajor		  = UpdatedPRCNavigationDetails.INS_dirVMajor		 
		  , PRCNavigationDetails.INS_VelDown		  = UpdatedPRCNavigationDetails.INS_VelDown		 
		  , PRCNavigationDetails.INS_Status			  = UpdatedPRCNavigationDetails.INS_Status			 
		  , PRCNavigationDetails.INS_East			  = UpdatedPRCNavigationDetails.INS_East			 
		  , PRCNavigationDetails.INS_North			  = UpdatedPRCNavigationDetails.INS_North			 
		  , PRCNavigationDetails.INS_CurrentDTime	  = UpdatedPRCNavigationDetails.INS_CurrentDTime	 
		  , PRCNavigationDetails.INS_PreviousDTime	  = UpdatedPRCNavigationDetails.INS_PreviousDTime	 
		FROM @TableValue AS [UpdatedPRCNavigationDetails]
		WHERE PRCNavigationDetails.ID = UpdatedPRCNavigationDetails.ID
		COMMIT TRANSACTION Trans
	END TRY
    BEGIN CATCH
		ROLLBACK TRANSACTION Trans
	END CATCH
END







GO
/****** Object:  StoredProcedure [dbo].[SelectFromEventProc]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SelectFromEventProc]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[SelectFromEventProc] AS' 
END
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[SelectFromEventProc]
	@idlist idtype readonly
AS
BEGIN
    	 select * from EventProc where id in(select * from @idlist)
END 


GO
/****** Object:  StoredProcedure [dbo].[sp_InjectPRCNavDetailsTable]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_InjectPRCNavDetailsTable]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_InjectPRCNavDetailsTable] AS' 
END
GO

ALTER PROCEDURE [dbo].[sp_InjectPRCNavDetailsTable]
    @myPRCNavTableType [PRCNavDetailsType] READONLY
AS
BEGIN
	SET IDENTITY_INSERT [dbo].PRCNavigationDetails OFF
	INSERT INTO dbo.PRCNavigationDetails
	        ( FK_PRCNavID ,
	          DTime ,
	          East ,
	          North ,
	          KP ,
	          Depth ,
	          Heading ,
	          Pitch ,
	          Roll ,
	          HeadingDev ,
			  [Timestamp] ,
	          ImageNames
	        )
	SELECT FK_PRCNavID ,
            DTime ,
            East ,
            North ,
            KP ,
            Depth ,
            Heading ,
            Pitch ,
            Roll ,
            HeadingDev ,
			[Timestamp] ,
            ImageNames 
	FROM @myPRCNavTableType 
END


GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateDepthTable]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_UpdateDepthTable]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_UpdateDepthTable] AS' 
END
GO
ALTER PROCEDURE [dbo].[sp_UpdateDepthTable]
    @myDepthTableType [DepthType] READONLY
AS
BEGIN
	UPDATE b SET b.Depth = a.Depth
	FROM @myDepthTableType a
	JOIN [dbo].PRCNavigationDetails b ON b.ID=a.ID
END


GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateDigitizedPipe_Depth]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_UpdateDigitizedPipe_Depth]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_UpdateDigitizedPipe_Depth] AS' 
END
GO
ALTER PROCEDURE [dbo].[sp_UpdateDigitizedPipe_Depth]
    @UpdatedDepth [DepthType] READONLY
AS
BEGIN
	UPDATE DigitizedPipe SET DigitizedPipe.Depth = UpdatedDepth.Depth
	FROM @UpdatedDepth AS [UpdatedDepth]
	INNER JOIN DigitizedPipe ON DigitizedPipe.ID= UpdatedDepth.ID
END


GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateEastNorthTable]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_UpdateEastNorthTable]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_UpdateEastNorthTable] AS' 
END
GO
ALTER PROCEDURE [dbo].[sp_UpdateEastNorthTable]
    @myEastNorthTableType [EastNorthType] READONLY
AS
BEGIN
	UPDATE b SET b.East = a.East,
	b.North = a.North
	FROM @myEastNorthTableType a
	JOIN [dbo].PRCNavigationDetails b ON b.ID=a.ID
END


GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateHeadingDevKPTable]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_UpdateHeadingDevKPTable]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_UpdateHeadingDevKPTable] AS' 
END
GO

ALTER PROCEDURE [dbo].[sp_UpdateHeadingDevKPTable]
    @myHeadingDevKPTableType [HeadingDevKPType] READONLY
AS
BEGIN
	UPDATE b SET b.HeadingDev = a.HeadingDev,
	b.KP = a.KP
	FROM @myHeadingDevKPTableType a
	JOIN [dbo].PRCNavigationDetails b ON b.ID=a.ID
END


GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateHeadingDevKPTable_EventProc]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_UpdateHeadingDevKPTable_EventProc]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_UpdateHeadingDevKPTable_EventProc] AS' 
END
GO
ALTER PROCEDURE [dbo].[sp_UpdateHeadingDevKPTable_EventProc]
    @myHeadingDevKPTableType [HeadingDevKPType] readonly
AS
BEGIN

UPDATE b SET b.HeadingDev = a.HeadingDev,
b.KP = a.KP

FROM @myHeadingDevKPTableType a

JOIN [dbo].EventProc b ON b.ID=a.ID
END








GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateHeadingPitchRollTable]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_UpdateHeadingPitchRollTable]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_UpdateHeadingPitchRollTable] AS' 
END
GO

ALTER PROCEDURE [dbo].[sp_UpdateHeadingPitchRollTable]
    @myHeadingPitchRollType [RollPitchHeadingType] readonly
AS
BEGIN

UPDATE b SET b.Heading = a.Heading,
b.Pitch = a.Pitch,
b.Roll = a.Roll
                              
FROM @myHeadingPitchRollType a
                             
JOIN [dbo].PRCNavigationDetails b ON b.ID=a.ID
END


GO
/****** Object:  StoredProcedure [dbo].[sp_UpdatePosProc_Depth]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_UpdatePosProc_Depth]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_UpdatePosProc_Depth] AS' 
END
GO
ALTER PROCEDURE [dbo].[sp_UpdatePosProc_Depth]
    @myDepthTableType [DepthType] READONLY
AS
BEGIN
	UPDATE b SET b.Depth = a.Depth
	FROM @myDepthTableType a
	JOIN [dbo].PosProc b ON b.ID=a.ID
END


GO
/****** Object:  StoredProcedure [dbo].[Update_PitchRoleLog]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Update_PitchRoleLog]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[Update_PitchRoleLog] AS' 
END
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[Update_PitchRoleLog]
      @PitchRollLog IMP_PitchRollLogType READONLY
AS
BEGIN
      SET NOCOUNT OFF;
       --UPDATE EXISTING RECORDS
      UPDATE PitchRoleLog 
      SET PitchRoleLog.Pitch=p2.Pitch,PitchRoleLog.Role=p2.Roll,PitchRoleLog.CorrPitch=p2.CorrPitch,PitchRoleLog.CorrRole=p2.CorrRoll,PitchRoleLog.Discard=p2.Discard
      FROM PitchRoleLog p1
      INNER JOIN @PitchRollLog p2
      ON CONVERT(NVARCHAR(50),p1.DTime ,109) = CONVERT(NVARCHAR(50),p2.DTime ,109) AND  p1.Role != p2.Roll
 
      --INSERT NON-EXISTING RECORDS
      INSERT INTO PitchRoleLog
      SELECT p2.DTime,p2.Pitch,p2.Roll,p2.CorrPitch,p2.CorrRoll,p2.Discard,p2.ROVID
      FROM @PitchRollLog p2
      WHERE CONVERT(NVARCHAR(50),p2.DTime ,109) NOT IN(SELECT CONVERT(NVARCHAR(50),DTime ,109) FROM PitchRoleLog)
END


GO
/****** Object:  StoredProcedure [dbo].[Update_PitchRollLog]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Update_PitchRollLog]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[Update_PitchRollLog] AS' 
END
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[Update_PitchRollLog]
      @PitchRoleLog IMP_PitchRoleLogType READONLY
AS
BEGIN
       --UPDATE EXISTING RECORDS
      UPDATE PitchRollLog 
      SET PitchRollLog.Pitch=p2.Pitch,PitchRollLog.Roll=p2.Role,PitchRollLog.CorrPitch=p2.CorrPitch,PitchRollLog.CorrRoll=p2.CorrRole,PitchRollLog.Discard=p2.Discard
      FROM PitchRollLog p1
      INNER JOIN @PitchRoleLog p2
      ON CONVERT(NVARCHAR(50),p1.DTime ,109) = CONVERT(NVARCHAR(50),p2.DTime ,109) AND  p1.Roll != p2.Role
 
      --INSERT NON-EXISTING RECORDS
      INSERT INTO PitchRollLog 
      SELECT p2.DTime,p2.Pitch,p2.Role,p2.CorrPitch,p2.CorrRole,p2.Discard,p2.ROVID
      FROM @PitchRoleLog AS p2
      WHERE CONVERT(NVARCHAR(50),p2.DTime ,109) NOT IN(SELECT CONVERT(NVARCHAR(50),DTime ,109) FROM PitchRollLog)
END


GO
/****** Object:  StoredProcedure [dbo].[UpdateFreeSpanEvents]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UpdateFreeSpanEvents]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UpdateFreeSpanEvents] AS' 
END
GO

ALTER PROCEDURE [dbo].[UpdateFreeSpanEvents]
	@FreeSpanEventData FreeSpanEvent readonly
AS
BEGIN


--update
UPDATE EventProc SET EventProc.KP = UpdatedEvent.KP, EventProc.DTime = UpdatedEvent.DTime, EventProc.Discard = UpdatedEvent.Discard
 FROM  @FreeSpanEventData AS [UpdatedEvent] 
 Where EventProc.ID = UpdatedEvent.ID

END



GO
/****** Object:  StoredProcedure [dbo].[UpdatePTrackerProcFProcFromScanProcPTProc]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UpdatePTrackerProcFProcFromScanProcPTProc]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UpdatePTrackerProcFProcFromScanProcPTProc] AS' 
END
GO

ALTER PROCEDURE [dbo].[UpdatePTrackerProcFProcFromScanProcPTProc]
	@PTrackerData  KPsType readonly
AS
BEGIN


	UPDATE PTrackerProc SET PTrackerProc.FProc = '1', PTrackerProc.PTProc = '1'
						
	                                
	FROM  @PTrackerData AS UpdatedData
	Where PTrackerProc.KP >= UpdatedData.KP AND PTrackerProc.KP <= UpdatedData.KP2
END


GO
/****** Object:  StoredProcedure [dbo].[UpdateScanProcDeltaYFromCorrPosProc]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UpdateScanProcDeltaYFromCorrPosProc]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UpdateScanProcDeltaYFromCorrPosProc] AS' 
END
GO

ALTER PROCEDURE [dbo].[UpdateScanProcDeltaYFromCorrPosProc]
	@ScanProcData PipeDeltaType readonly
AS
BEGIN


	UPDATE ScanProc SET  ScanProc.PipeDeltaY = UpdatedData.PipeDelta
	FROM  @ScanProcData AS UpdatedData
	Where ScanProc.ID = UpdatedData.ID AND PTProc = '1' AND FProc = '1'
END


GO
/****** Object:  StoredProcedure [dbo].[UpdateScanProcDeltaYFromDigitizedPipe]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UpdateScanProcDeltaYFromDigitizedPipe]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UpdateScanProcDeltaYFromDigitizedPipe] AS' 
END
GO

ALTER PROCEDURE [dbo].[UpdateScanProcDeltaYFromDigitizedPipe]
	@ScanProcData PipeDeltaType readonly
AS
BEGIN


	UPDATE ScanProc SET  ScanProc.PipeDeltaY = UpdatedData.PipeDelta
	FROM  @ScanProcData AS UpdatedData
	Where ScanProc.ID = UpdatedData.ID AND PTProc = '1' AND FProc = '1'
END


GO
/****** Object:  StoredProcedure [dbo].[UpdateScanProcHeadEastHeadNorth]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UpdateScanProcHeadEastHeadNorth]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UpdateScanProcHeadEastHeadNorth] AS' 
END
GO

ALTER PROCEDURE [dbo].[UpdateScanProcHeadEastHeadNorth]
	@ScanProcData EastNorthType readonly
AS
BEGIN


	UPDATE ScanProc SET  ScanProc.HeadEast = UpdatedData.East, ScanProc.HeadNorth = UpdatedData.North
	FROM  @ScanProcData AS UpdatedData
	Where ScanProc.ID = UpdatedData.ID 
END


GO
/****** Object:  StoredProcedure [dbo].[UpdateScanProcPipeDepthFromPTrackerProcPipeDepth]    Script Date: 9/17/2018 02:19:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UpdateScanProcPipeDepthFromPTrackerProcPipeDepth]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UpdateScanProcPipeDepthFromPTrackerProcPipeDepth] AS' 
END
GO

ALTER PROCEDURE [dbo].[UpdateScanProcPipeDepthFromPTrackerProcPipeDepth]
	@ScanProcData DepthType readonly
AS
BEGIN


	UPDATE ScanProc SET  ScanProc.PipeDepth = UpdatedData.Depth, ScanProc.PTAdded ='1' 
	FROM  @ScanProcData AS UpdatedData
	Where ScanProc.ID = UpdatedData.ID AND ScanProc.PTProc = '1' AND ScanProc.FProc = '1'
END


GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DiagramPane1' , N'SCHEMA',N'dbo', N'VIEW',N'StoredNameType', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = '(H (1[40] 4[20] 2[20] 3) )'
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = '(H (1 [50] 4 [25] 3))'
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = '(H (1 [50] 2 [25] 3))'
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = '(H (4 [30] 2 [40] 3))'
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = '(H (1 [56] 3))'
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = '(H (2 [66] 3))'
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = '(H (4 [50] 3))'
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = '(V (3))'
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = '(H (1[56] 4[18] 2) )'
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = '(H (1 [75] 4))'
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = '(H (1[66] 2) )'
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = '(H (4 [60] 2))'
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = '(H (1) )'
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = '(V (4))'
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = '(V (2))'
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = 'StoredName'
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 261
               Right = 389
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = 'StoredType'
            Begin Extent = 
               Top = 19
               Left = 540
               Bottom = 217
               Right = 871
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ''
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'StoredNameType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DiagramPaneCount' , N'SCHEMA',N'dbo', N'VIEW',N'StoredNameType', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'StoredNameType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DiagramPane1' , N'SCHEMA',N'dbo', N'VIEW',N'Viewer_ClusterDetails_updated', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = '(H (1[40] 4[20] 2[20] 3) )'
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = '(H (1 [50] 4 [25] 3))'
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = '(H (1 [50] 2 [25] 3))'
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = '(H (4 [30] 2 [40] 3))'
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = '(H (1 [56] 3))'
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = '(H (2 [66] 3))'
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = '(H (4 [50] 3))'
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = '(V (3))'
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = '(H (1[56] 4[18] 2) )'
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = '(H (1 [75] 4))'
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = '(H (1[66] 2) )'
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = '(H (4 [60] 2))'
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = '(H (1) )'
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = '(V (4))'
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = '(V (2))'
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = -96
         Left = 0
      End
      Begin Tables = 
         Begin Table = 'BatchesDetails'
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 135
               Right = 269
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = 'Batches'
            Begin Extent = 
               Top = 6
               Left = 307
               Bottom = 135
               Right = 511
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = 'Clusters'
            Begin Extent = 
               Top = 6
               Left = 549
               Bottom = 135
               Right = 758
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = 'PRCNavigationDetails'
            Begin Extent = 
               Top = 6
               Left = 796
               Bottom = 135
               Right = 966
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ''
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or =' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Viewer_ClusterDetails_updated'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DiagramPane2' , N'SCHEMA',N'dbo', N'VIEW',N'Viewer_ClusterDetails_updated', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N' 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Viewer_ClusterDetails_updated'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DiagramPaneCount' , N'SCHEMA',N'dbo', N'VIEW',N'Viewer_ClusterDetails_updated', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Viewer_ClusterDetails_updated'
GO
USE [master]
GO
ALTER DATABASE [PipeBlank] SET  READ_WRITE 
GO
"
End Class
