﻿Public Class strctMCSProject

    Private _bId As Integer
    Public Property BId As Integer
        Get
            Return _bId
        End Get
        Set(ByVal value As Integer)
            _bId = value
        End Set
    End Property

    Private _server As strctServer
    Public Property Server As strctServer
        Get
            Return _server
        End Get
        Set(ByVal value As strctServer)
            _server = value
        End Set
    End Property

    Private _name As String
    Public Property Name As String
        Get
            Return _name
        End Get
        Set(ByVal value As String)
            _name = value
        End Set
    End Property

    Private _dbConnectionString As String
    Public Property DBConnectionString As String
        Get
            Return _dbConnectionString
        End Get
        Set(ByVal value As String)
            _dbConnectionString = value
        End Set
    End Property

    Private _MCSProjectVersion As String
    Public Property MCSProjectVersion() As String
        Get
            Return _MCSProjectVersion
        End Get
        Set(ByVal value As String)
            _MCSProjectVersion = value
        End Set
    End Property

End Class

