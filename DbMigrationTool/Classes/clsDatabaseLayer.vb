﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Xml.Serialization
Imports System.IO
Imports System.Drawing
Imports System.Globalization
Imports Microsoft.SqlServer.Management.Smo
Imports Microsoft.SqlServer.Management.Common
Imports System.Reflection
Imports System.Threading
Imports System.Reflection.Emit
Imports System.Runtime.Serialization.Formatters.Binary
Imports System.Text
Imports System.Xml
Imports System.Collections.ObjectModel
Imports System.ComponentModel

Public Class clsDatabaseLayer

    Private _conn As SqlConnection
    Private _trans As SqlTransaction
    Private _connStr As String = Nothing
    Private _sqlStmt As String = String.Empty

    Public _connPipeString As String
    Public _mcsProjectConnectionString As String


    Public Function ConnectionString(ByVal server As strctServer, Optional ByVal dataBase As String = Nothing) As String
        Dim connStr As String = String.Empty

        Try
            If Not String.IsNullOrEmpty(server.Name) AndAlso Not String.IsNullOrEmpty(server.UserName) AndAlso Not String.IsNullOrEmpty(server.PassWord) Then
                connStr = "Server=" & server.Name & ";" &
                           IIf(Not dataBase Is Nothing, "Initial Catalog=" & dataBase & ";", String.Empty) &
                           "User ID=" & server.UserName & ";" &
                           "Password=" & server.PassWord & ";" &
                           "Persist Security Info=True;"
            End If

            Return connStr
        Catch ex As Exception
            Return connStr
        End Try
    End Function


    Public Function PingServer(ByVal server As strctServer, Optional ByVal dataBase As String = Nothing) As Boolean
        Dim connected As Boolean = False

        Try
            If Not String.IsNullOrEmpty(server.Name) AndAlso Not String.IsNullOrEmpty(server.UserName) AndAlso Not String.IsNullOrEmpty(server.PassWord) Then
                _connStr = ConnectionString(server, dataBase)
                _conn = New SqlConnection(_connStr)
                _conn.Open()
                If _conn.State = ConnectionState.Open Then
                    connected = True
                    _conn.Close()
                End If
            End If

            Return connected
        Catch ex As Exception
            Return connected
        End Try
    End Function

    Public Function CutConnectionString(ByVal connectionStr As String, ByRef serverName As String, ByRef userName As String, ByRef password As String) As Boolean
        Dim connectionStringCut As Boolean = False
        Dim strCon As String = String.Empty
        Dim indexLen As Integer = 0
        Dim strSrc As String = String.Empty
        Try
            If Not connectionStr Is Nothing AndAlso connectionStr <> String.Empty Then
                strCon = connectionStr.ToLower
                'indexLen = strCon.IndexOf("data source=") + 12
                If strCon.IndexOf("server=") = -1 Then
                    indexLen = strCon.IndexOf("data source=") + 12
                Else
                    indexLen = strCon.IndexOf("server=") + 7
                End If

                If strCon.IndexOf(";", indexLen) > indexLen Then
                    strSrc = strCon.Substring(indexLen, strCon.IndexOf(";", indexLen) - indexLen)
                Else
                    strSrc = strCon.Substring(indexLen)
                End If
                serverName = strSrc

                indexLen = strCon.IndexOf("user id=") + 8
                If strCon.IndexOf(";", indexLen) > indexLen Then
                    strSrc = strCon.Substring(indexLen, strCon.IndexOf(";", indexLen) - indexLen)
                Else
                    strSrc = strCon.Substring(indexLen)
                End If
                userName = strSrc

                indexLen = strCon.IndexOf("password=") + 9
                If strCon.IndexOf(";", indexLen) > indexLen Then
                    strSrc = connectionStr.Substring(indexLen, connectionStr.IndexOf(";", indexLen) - indexLen)
                Else
                    strSrc = connectionStr.Substring(indexLen)
                End If
                password = strSrc

                connectionStringCut = True
            End If

            Return connectionStringCut
        Catch ex As Exception
            Return connectionStringCut
        End Try
    End Function


    ''This function is different from OFFLINE VERSION
    Public Function GetMCSProjects(ByVal server As strctServer) As List(Of strctMCSProject)
        Dim cmd As SqlCommand
        Dim mcsProject As strctMCSProject
        Dim lstAllMCSProjects As New List(Of strctMCSProject)
        Dim lstMCSProjects As New List(Of strctMCSProject)
        Try
            _connStr = server.ConnectionString
            _conn = New SqlConnection(_connStr)
            If _conn.State <> ConnectionState.Open Then _conn.Open()
            ' If _trans Is Nothing OrElse _trans.Connection Is Nothing Then _trans = _conn.BeginTransaction()

            _sqlStmt = "SELECT" + Space(1) +
                       "[Name]" + Space(1) +
                       "FROM [sys].[databases]" + Space(1) +
                       "WHERE [Name] LIKE 'MCSProject%'" + Space(1) +
                       "AND [DataBase_Id] >= 5"
            cmd = New SqlCommand(_sqlStmt, _conn)
            cmd.CommandType = CommandType.Text
            Dim reader As SqlDataReader = cmd.ExecuteReader()

            While reader.Read
                mcsProject = New strctMCSProject
                mcsProject.Name = IIf(Not IsDBNull(reader("Name")), reader("Name"), Nothing)
                mcsProject.DBConnectionString = ConnectionString(server, mcsProject.Name)
                lstAllMCSProjects.Add(mcsProject)
            End While
            reader.Close()
            cmd.Dispose()

            '   If Not _trans.Connection Is Nothing Then _trans.Commit()
            If _conn.State <> ConnectionState.Closed Then _conn.Close()

            For Each mcsProject In lstAllMCSProjects
                ' If ValidateMCSProjects(mcsProject.DBConnectionString) Then lstMCSProjects.Add(mcsProject)
                If CheckDBVersion(mcsProject.DBConnectionString) Then
                    mcsProject.MCSProjectVersion = "V5"
                Else
                    mcsProject.MCSProjectVersion = "V4"
                End If
                lstMCSProjects.Add(mcsProject)
            Next

            Return lstMCSProjects
        Catch ex As Exception
            '    If Not _trans.Connection Is Nothing Then _trans.Rollback()
            If _conn.State <> ConnectionState.Closed Then _conn.Close()
            AppController.ShowDXMessage(ex.Message & vbNewLine & ex.StackTrace, MessageBoxButton.OK, MessageBoxImage.Error)
            '  LogController.AddLogRecord("GETMCSPRojects", ex)
            Return Nothing
        End Try
    End Function


    Function CheckDBVersion(connectionstring As String) As Boolean
        Dim cmd As SqlCommand
        Try
            _connStr = connectionstring
            _conn = New SqlConnection(_connStr)
            If _conn.State <> ConnectionState.Open Then _conn.Open()
            'get tables
            _sqlStmt = "Select ISNULL(Count(TABLE_NAME), 0 ) FROM INFORMATION_SCHEMA.TABLES WHERE [TABLE_TYPE] = 'BASE TABLE' AND ([TABLE_NAME] = 'tblYears'
                           OR [TABLE_NAME] = 'tblPCode' OR [TABLE_NAME] = 'tblSTCode')"
            cmd = New SqlCommand(_sqlStmt, _conn)
            cmd.CommandType = CommandType.Text
            If cmd.ExecuteScalar > 0 Then
                Return True
            Else
                Return False
            End If
            If _conn.State <> ConnectionState.Closed Then _conn.Close()
        Catch ex As Exception
            If _conn.State <> ConnectionState.Closed Then _conn.Close()
        End Try
    End Function

    Public Function GetOldPipes(ByVal project As clsOfflineDbMigration) As List(Of clsOfflinePipe)
        Dim cmd As SqlCommand
        Dim pipe As clsOfflinePipe

        Dim lstPipes As New List(Of clsOfflinePipe)
        Dim userName As String = String.Empty
        Dim indexLen As Integer = 0

        Dim cmd2 As New SqlCommand
        Dim myReader As SqlDataReader

        Try
            _connStr = project.ConnectionString
            _conn = New SqlConnection(_connStr)
            If _conn.State <> ConnectionState.Open Then _conn.Open()
            '   If _trans Is Nothing OrElse _trans.Connection Is Nothing Then _trans = _conn.BeginTransaction()
            cmd2.Connection = _conn
            cmd2.CommandText = "Select Name, ProjectCode FROM " + Space(1) + "[" + project.MCSName + "].[dbo].[tblProjects] "
            cmd2.ExecuteNonQuery()
            myReader = cmd2.ExecuteReader()

            Dim projectCode As String = String.Empty
            Dim projectName As String = String.Empty
            While myReader.Read
                projectCode = myReader.GetValue(1)
                projectName = myReader.GetValue(0)
            End While
            _conn.Close()

            If _conn.State <> ConnectionState.Open Then _conn.Open()
            _sqlStmt = "SELECT" +
                       "[PK_PipeID],[Name], [LoggingDB], [ProcessingDB] FROM" + Space(1) + "[" + project.MCSName + "].[dbo].[tblPipes] "

            cmd = New SqlCommand(_sqlStmt, _conn)
            cmd.CommandType = CommandType.Text
            Dim reader As SqlDataReader = cmd.ExecuteReader()

            Dim shortName As String
            Dim id As Integer
            While reader.Read
                pipe = New clsOfflinePipe
                '  pipeProc = New clsOfflinePipe
                id = CInt(reader("PK_PipeID"))
                shortName = IIf(Not IsDBNull(reader("Name")), reader("Name"), Nothing) '.ToString.Split(";").Where(Function(pip) (pip.Contains("Initial Catalog="))).First.Replace("Initial Catalog=", "")
                pipe.PipeName = shortName
                pipe.Id = id
                pipe.LoggingDb = IIf(Not IsDBNull(reader("LoggingDB")), reader("LoggingDB"), Nothing)
                pipe.ProcessingDb = IIf(Not IsDBNull(reader("ProcessingDB")), reader("ProcessingDB"), Nothing)

                lstPipes.Add(pipe)
            End While
            reader.Close()
            cmd.Dispose()

            '  If Not _trans.Connection Is Nothing Then _trans.Commit()
            If _conn.State <> ConnectionState.Closed Then _conn.Close()

            Return lstPipes
        Catch ex As Exception
            '  If Not _trans.Connection Is Nothing Then _trans.Rollback()
            If _conn.State <> ConnectionState.Closed Then _conn.Close()
            AppController.ShowDXMessage(ex.Message & vbNewLine & ex.StackTrace, MessageBoxButton.OK, MessageBoxImage.Error)
            '   LogController.AddLogRecord("GetPipes", ex)
            Return Nothing
        End Try
    End Function


    Public Function ValidateMCSProjects(ByVal mcsProjectConnectionString As String) As Boolean
        Dim mcsProjectsValidated As Boolean = False
        Dim cmd As SqlCommand
        Dim count As Integer = 0
        Dim dataBaseName As String = String.Empty
        Dim indexLen As Integer = 0

        indexLen = mcsProjectConnectionString.ToLower.IndexOf("initial catalog=") + 16
        If mcsProjectConnectionString.IndexOf(";", indexLen) > indexLen Then
            dataBaseName = mcsProjectConnectionString.Substring(indexLen, mcsProjectConnectionString.ToLower.IndexOf(";", indexLen) - indexLen)
        Else
            dataBaseName = mcsProjectConnectionString.Substring(indexLen)
        End If

        Try
            _connStr = mcsProjectConnectionString
            _conn = New SqlConnection(_connStr)
            If _conn.State <> ConnectionState.Open Then _conn.Open()
            ' If _trans Is Nothing OrElse _trans.Connection Is Nothing Then _trans = _conn.BeginTransaction()

            _sqlStmt = "SELECT COUNT([TABLE_NAME]) AS [Tables] FROM" + Space(1) + "[" + dataBaseName + "].INFORMATION_SCHEMA.[TABLES] WHERE [TABLE_TYPE] = 'BASE TABLE'" _
                       + Space(1) + "AND ([TABLE_NAME] = 'tlbTeamAccounts' OR [TABLE_NAME] = 'tblDives' OR [TABLE_NAME] = 'tblDiveTypes' OR [TABLE_NAME] = 'tblProjectLogBook' OR [TABLE_NAME] = 'tblPCode' OR [TABLE_NAME] = 'tblPipes' OR [TABLE_NAME] = 'tblPipeRadii' OR [TABLE_NAME] = 'tblPipeStatus'" _
                       + Space(1) + "OR [TABLE_NAME] = 'tblProjects' OR [TABLE_NAME] = 'tblROVs' OR [TABLE_NAME] = 'tblROVSettings' OR [TABLE_NAME] = 'tblSTCode' OR [TABLE_NAME] = 'tblTide' OR [TABLE_NAME] = 'tblTideDetails' OR [TABLE_NAME] = 'tblYears' OR [TABLE_NAME] = 'tblCoordinateSystem')"
            cmd = New SqlCommand(_sqlStmt, _conn)
            cmd.CommandType = CommandType.Text
            count = Convert.ToByte(cmd.ExecuteScalar())
            If count = 16 Then
                _sqlStmt = "SELECT COUNT([COLUMN_NAME]) AS [Columns] FROM" + Space(1) + "[" + dataBaseName + "].INFORMATION_SCHEMA.[COLUMNS] WHERE [TABLE_NAME] = 'tlbTeamAccounts' AND ([COLUMN_NAME] IN ('FK_ProjectID', 'ID', 'UserName', 'Password', 'RoleID'))"
                cmd = New SqlCommand(_sqlStmt, _conn)
                cmd.CommandType = CommandType.Text
                count = Convert.ToByte(cmd.ExecuteScalar())
                If count <> 5 Then GoTo MCSProjectInvalid

                _sqlStmt = "SELECT COUNT([COLUMN_NAME]) AS [Columns] FROM" + Space(1) + "[" + dataBaseName + "].INFORMATION_SCHEMA.[COLUMNS] WHERE [TABLE_NAME] = 'tblDives' AND ([COLUMN_NAME] IN ('ID', 'FK_ProjectID', 'Dive_Number', 'StartKP', 'EndKP', 'StartDTime', 'EndDTime', 'DiveType', 'ROVID'))"
                cmd = New SqlCommand(_sqlStmt, _conn)
                cmd.CommandType = CommandType.Text
                count = Convert.ToByte(cmd.ExecuteScalar())
                If count <> 9 Then GoTo MCSProjectInvalid

                _sqlStmt = "SELECT COUNT([COLUMN_NAME]) AS [Columns] FROM" + Space(1) + "[" + dataBaseName + "].INFORMATION_SCHEMA.[COLUMNS] WHERE [TABLE_NAME] = 'tblDiveTypes' AND ([COLUMN_NAME] IN ('ID', 'FK_ProjectID', 'DiveType', 'DiveType_Code', 'Method_Used'))"
                cmd = New SqlCommand(_sqlStmt, _conn)
                cmd.CommandType = CommandType.Text
                count = Convert.ToByte(cmd.ExecuteScalar())
                If count <> 5 Then GoTo MCSProjectInvalid

                _sqlStmt = "SELECT COUNT([COLUMN_NAME]) AS [Columns] FROM" + Space(1) + "[" + dataBaseName + "].INFORMATION_SCHEMA.[COLUMNS] WHERE [TABLE_NAME] = 'tblProjectLogBook' AND ([COLUMN_NAME] IN ('FK_ProjectID', 'FK_PipeID', 'ID', 'DTime', 'UserID', 'Description', 'Activity', 'App'))"
                cmd = New SqlCommand(_sqlStmt, _conn)
                cmd.CommandType = CommandType.Text
                count = Convert.ToByte(cmd.ExecuteScalar())
                If count <> 8 Then GoTo MCSProjectInvalid

                _sqlStmt = "SELECT COUNT([COLUMN_NAME]) AS [Columns] FROM" + Space(1) + "[" + dataBaseName + "].INFORMATION_SCHEMA.[COLUMNS] WHERE [TABLE_NAME] = 'tblPCode' AND ([COLUMN_NAME] IN ('ID', 'Code', 'Name', 'Picture', 'PShortcut', 'Type', 'Layername'))"
                cmd = New SqlCommand(_sqlStmt, _conn)
                cmd.CommandType = CommandType.Text
                count = Convert.ToByte(cmd.ExecuteScalar())
                If count <> 7 Then GoTo MCSProjectInvalid

                _sqlStmt = "SELECT COUNT([COLUMN_NAME]) AS [Columns] FROM" + Space(1) + "[" + dataBaseName + "].INFORMATION_SCHEMA.[COLUMNS] WHERE [TABLE_NAME] = 'tblPipes' AND ([COLUMN_NAME] IN ('FK_ProjectID', 'PK_PipeID', 'FK_ProjectYearID', 'Name', 'LoggingDB', 'ProcessingDB', 'VideoFilePath1', 'VideoFilePath2', 'StartDate', 'EndDate', 'Duration', 'PatchSize', 'NumberOfPatchs', 'PipeType', 'LeftVideoFilePath', 'CenterVideoFilePath', 'RightVideoFilePath', 'CombinedVideoFilePath', 'StartKP', 'EndKP', 'StartDTime', 'EndDTime', 'SurveyType', 'KPUnit', 'UTCShift', 'Area', 'OfflineKP', 'ChecksKP', 'SmoothingKP', 'EventsKP', 'ChartingKP', 'Reported', 'PipeStatus', 'StoredProcedureSettings' , 'PipeDirection'))"
                cmd = New SqlCommand(_sqlStmt, _conn)
                cmd.CommandType = CommandType.Text
                count = Convert.ToByte(cmd.ExecuteScalar())
                If count <> 35 Then GoTo MCSProjectInvalid

                _sqlStmt = "SELECT COUNT([COLUMN_NAME]) AS [Columns] FROM" + Space(1) + "[" + dataBaseName + "].INFORMATION_SCHEMA.[COLUMNS] WHERE [TABLE_NAME] = 'tblPipeRadii' AND ([COLUMN_NAME] IN ('ID', 'FK_ProjectID', 'FK_PipeID', 'FromKP', 'ToKP', 'FromDTime', 'ToDTime', 'Radius', 'Value', 'IntervalType'))"
                cmd = New SqlCommand(_sqlStmt, _conn)
                cmd.CommandType = CommandType.Text
                count = Convert.ToByte(cmd.ExecuteScalar())
                If count <> 10 Then GoTo MCSProjectInvalid

                _sqlStmt = "SELECT COUNT([COLUMN_NAME]) AS [Columns] FROM" + Space(1) + "[" + dataBaseName + "].INFORMATION_SCHEMA.[COLUMNS] WHERE [TABLE_NAME] = 'tblPipeStatus' AND ([COLUMN_NAME] IN ('PK_PipeStatus', 'FK_ProjectID', 'FK_PipeID', 'FK_ProjectYearID', 'GeneralDetails_StartDtime', 'GeneralDetails_EndDtime', 'GeneralDetails_StartKp', 'GeneralDetails_EndKp', 'Importing_FromLogDBToProcDB', 'TrackProcessing_ImportFromPoslogToPosProc', 'TrackProcessing_DeSpikingNavigation', 'TrackProcessing_RouteDigitizing', 'TrackProcessing_KpCalc', 'TrackProcessing_HeadingDevCalc', 'TrackProcessing_DesignKpCalc', 'TrackProcessing_DccCalc', 'DepthProcessing_DepthAndTideCalc', 'ScansProcessing_ImportFromScanLogToScanProc', 'ScansProcessing_HPRInterpolation', 'ScansProcessing_KpCalc', 'ScansProcessing_HeadingDevCalc', 'ScansProcessing_PositionCalc', 'ScansProcessing_DesignKpCalc', 'ScansProcessing_DccCalc', 'ScansProcessing_DepthCalc', 'ScansProcessing_UpdateKpInterval', 'ScansProcessing_PipeAndSeaBedDetection', 'ScansProcessing_GapsChecks', 'ScansProcessing_ProfileSmoothing', 'ScansProcessing_SeabedSmoothing', 'PipeTrackerProcessing_ImportFromPTrackerLogToPTrackerProc', 'PipeTrackerProcessing_HPRInterpolation', 'PipeTrackerProcessing_PositionCalc', 'PipeTrackerProcessing_KpCalc', 'PipeTrackerProcessing_DepthCalc', 'PipeTrackerProcessing_UpdateKpInterval', 'PipeTrackerProcessing_ImplementingPipeTrackerInScanProc', 'EventsProcessing_VideoCombining', 'EventsProcessing_ImportFromEventLogToEventProc', 'EventsProcessing_VideoReview', 'EventsProcessing_KpCalc', 'EventsProcessing_EventChecks', 'EventsProcessing_PositionCalc', 'EventsProcessing_DepthCalc', 'EventsProcessing_DesignKpCalc', 'EventsProcessing_DccCalc', 'Deliverables_PreparingEventList', 'Deliverables_EventListQC', 'Deliverables_PreparingCharts', 'Deliverables_ChartsQC', 'Deliverables_PreparingReport', 'Deliverables_ReportQC', 'FinishedOnline', 'FinishedProcessing'))"
                cmd = New SqlCommand(_sqlStmt, _conn)
                cmd.CommandType = CommandType.Text
                count = Convert.ToByte(cmd.ExecuteScalar())
                If count <> 54 Then GoTo MCSProjectInvalid

                _sqlStmt = "SELECT COUNT([COLUMN_NAME]) AS [Columns] FROM" + Space(1) + "[" + dataBaseName + "].INFORMATION_SCHEMA.[COLUMNS] WHERE [TABLE_NAME] = 'tblProjects' AND ([COLUMN_NAME] IN ('PK_ProjectID', 'Name', 'Comment', 'ProjectCode', 'JobFolder', 'TimeShift'))"
                cmd = New SqlCommand(_sqlStmt, _conn)
                cmd.CommandType = CommandType.Text
                count = Convert.ToByte(cmd.ExecuteScalar())
                If count <> 6 Then GoTo MCSProjectInvalid

                _sqlStmt = "SELECT COUNT([COLUMN_NAME]) AS [Columns] FROM" + Space(1) + "[" + dataBaseName + "].INFORMATION_SCHEMA.[COLUMNS] WHERE [TABLE_NAME] = 'tblROVs' AND ([COLUMN_NAME] IN ('FK_ProjectID', 'PK_ROVID', 'Name', 'Description', 'Image', 'Settings'))"
                cmd = New SqlCommand(_sqlStmt, _conn)
                cmd.CommandType = CommandType.Text
                count = Convert.ToByte(cmd.ExecuteScalar())
                If count <> 6 Then GoTo MCSProjectInvalid

                _sqlStmt = "SELECT COUNT([COLUMN_NAME]) AS [Columns] FROM" + Space(1) + "[" + dataBaseName + "].INFORMATION_SCHEMA.[COLUMNS] WHERE [TABLE_NAME] = 'tblROVSettings' AND ([COLUMN_NAME] IN ('FK_ProjectID', 'PK_ROVID', 'PosX', 'PosY', 'PosZ', 'ScanX', 'ScanY', 'ScanZ', 'AltX', 'AltY', 'AltZ', 'EventX', 'EventY', 'EventZ', 'DepthX', 'DepthY', 'DepthZ', 'PTrackerX', 'PTrackerY', 'PTrackerZ', 'DopplerX', 'DopplerY', 'DopplerZ', 'MScanOffsetX', 'MScanOffsetY', 'MScanOffsetZ', 'SScanOffsetX', 'SScanOffsetY', 'SScanOffsetZ', 'MRX', 'MRY', 'MRZ', 'SRX', 'SRY', 'SRZ', 'DMSRX', 'DMSRY', 'DMSRZ', 'Distance', 'DatumHeight', 'PTrackerAltX', 'PTrackerAltY', 'PTrackerAltZ', 'Width', 'Height', 'Length', 'SettingsType'))"
                cmd = New SqlCommand(_sqlStmt, _conn)
                cmd.CommandType = CommandType.Text
                count = Convert.ToByte(cmd.ExecuteScalar())
                If count <> 47 Then GoTo MCSProjectInvalid

                _sqlStmt = "SELECT COUNT([COLUMN_NAME]) AS [Columns] FROM" + Space(1) + "[" + dataBaseName + "].INFORMATION_SCHEMA.[COLUMNS] WHERE [TABLE_NAME] = 'tblSTCode' AND ([COLUMN_NAME] IN ('PrimaryID', 'ID', 'Code', 'Type', 'LayerName', 'LayerName_Side', 'Description', 'Comment', 'ProfileEventFlag', 'PlanEventFlag', 'AddPlanTextFlag', 'AddProfileTextFlag', 'Discard', 'imagecap', 'AnomalyCode', 'GenerateAOC', 'EndCode'))"
                cmd = New SqlCommand(_sqlStmt, _conn)
                cmd.CommandType = CommandType.Text
                count = Convert.ToByte(cmd.ExecuteScalar())
                If count <> 17 Then GoTo MCSProjectInvalid

                _sqlStmt = "SELECT COUNT([COLUMN_NAME]) AS [Columns] FROM" + Space(1) + "[" + dataBaseName + "].INFORMATION_SCHEMA.[COLUMNS] WHERE [TABLE_NAME] = 'tblTide' AND ([COLUMN_NAME] IN ('FK_ProjectID', 'PK_TideID', 'Name', 'Details'))"
                cmd = New SqlCommand(_sqlStmt, _conn)
                cmd.CommandType = CommandType.Text
                count = Convert.ToByte(cmd.ExecuteScalar())
                If count <> 4 Then GoTo MCSProjectInvalid

                _sqlStmt = "SELECT COUNT([COLUMN_NAME]) AS [Columns] FROM" + Space(1) + "[" + dataBaseName + "].INFORMATION_SCHEMA.[COLUMNS] WHERE [TABLE_NAME] = 'tblTideDetails' AND ([COLUMN_NAME] IN ('FK_ProjectId', 'PK_TideID', 'ID', 'DTime', 'KP', 'Value'))"
                cmd = New SqlCommand(_sqlStmt, _conn)
                cmd.CommandType = CommandType.Text
                count = Convert.ToByte(cmd.ExecuteScalar())
                If count <> 6 Then GoTo MCSProjectInvalid

                _sqlStmt = "SELECT COUNT([COLUMN_NAME]) AS [Columns] FROM" + Space(1) + "[" + dataBaseName + "].INFORMATION_SCHEMA.[COLUMNS] WHERE [TABLE_NAME] = 'tblYears' AND ([COLUMN_NAME] IN ('PK_ProjectYearID', 'FK_ProjectID', 'Year'))"
                cmd = New SqlCommand(_sqlStmt, _conn)
                cmd.CommandType = CommandType.Text
                count = Convert.ToByte(cmd.ExecuteScalar())
                If count <> 3 Then GoTo MCSProjectInvalid

                _sqlStmt = "SELECT COUNT([COLUMN_NAME]) AS [Columns] FROM" + Space(1) + "[" + dataBaseName + "].INFORMATION_SCHEMA.[COLUMNS] WHERE [TABLE_NAME] = 'tblCoordinateSystem' AND ([COLUMN_NAME] IN ('Id', 'PipeID', 'ProjectID', 'CoordinateSystemSettings', 'StartKP', 'EndKP', 'StartDTime', 'EndDTime', 'SpheroidName', 'OriginParametersName'))"
                cmd = New SqlCommand(_sqlStmt, _conn)
                cmd.CommandType = CommandType.Text
                count = Convert.ToByte(cmd.ExecuteScalar())
                If count = 10 Then mcsProjectsValidated = True
            End If

MCSProjectInvalid:

            cmd.Dispose()

            '   If Not _trans.Connection Is Nothing Then _trans.Commit()
            If _conn.State <> ConnectionState.Closed Then _conn.Close()

            Return mcsProjectsValidated
        Catch ex As Exception
            ' If Not _trans.Connection Is Nothing Then _trans.Rollback()
            If _conn.State <> ConnectionState.Closed Then _conn.Close()
            AppController.ShowDXMessage(ex.Message & vbNewLine & ex.StackTrace, MessageBoxButton.OK, MessageBoxImage.Error)
            '   LogController.AddLogRecord("ValidateMCSProjects", ex)
            Return mcsProjectsValidated
        End Try
    End Function
End Class