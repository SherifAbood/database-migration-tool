﻿Imports System.ComponentModel
Imports System.Data
Imports System.Data.SqlClient


Public Class clsOfflineDbMigration

    Implements INotifyPropertyChanged

    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged

    Private Sub NotifyPropertyChanged(ByVal info As String)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(info))
    End Sub


    Private _id As Integer
    Public Property Id As Integer
        Get
            Return _id
        End Get
        Set(ByVal value As Integer)
            _id = value
        End Set
    End Property

    Private _MCSName As String
    Public Property MCSName() As String
        Get
            Return _MCSName
        End Get
        Set(ByVal value As String)
            _MCSName = value
        End Set
    End Property

    'Private _type As enumServersBinding
    'Public Property Type As enumServersBinding
    '    Get
    '        Return _type
    '    End Get
    '    Set(ByVal value As enumServersBinding)
    '        _type = value
    '    End Set
    'End Property

    Private _connectionString As String
    Public Property ConnectionString As String
        Get
            Return _connectionString
        End Get
        Set(ByVal value As String)
            _connectionString = value
        End Set
    End Property

    Private _lstOfPipes As New BindingList(Of clsOfflinePipe)
    Public Property LstOfPipes() As BindingList(Of clsOfflinePipe)
        Get
            Return _lstOfPipes
        End Get
        Set(ByVal value As BindingList(Of clsOfflinePipe))
            _lstOfPipes = value
            RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs("LstOfPipes"))
        End Set
    End Property


    Private _isMCSProjectChecked As Boolean
    Public Property IsMCSProjectChecked() As Boolean
        Get
            Return _isMCSProjectChecked
        End Get
        Set(ByVal value As Boolean)
            _isMCSProjectChecked = value
            If value = False Then
                For Each pipe In LstOfPipes
                    pipe.IsPipeChecked = False
                Next
            End If
            RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs("IsMCSProjectChecked"))
        End Set
    End Property

    Private _mcsProjectYear As String = String.Empty
    Public Property MCSProjectYear() As String
        Get
            Return _mcsProjectYear
        End Get
        Set(ByVal value As String)
            _mcsProjectYear = value
        End Set
    End Property

    Private _Version As String
    Public Property Version() As String
        Get
            Return _Version
        End Get
        Set(ByVal value As String)
            _Version = value
            If Version = "V5" Then
                Dim _connStr As String
                Dim _conn As SqlConnection
                Dim cmd As New SqlCommand
                Dim reader As SqlDataReader
                _connStr = ConnectionString
                _conn = New SqlConnection(_connStr)
                If _conn.State <> ConnectionState.Open Then _conn.Open()
                For Each pipe In LstOfPipes
                    cmd = New SqlCommand("select Year from tblYears as y, tblPipes as p where y.PK_ProjectYearID = p.FK_ProjectYearID and PK_PipeID =" & pipe.Id, _conn)
                    reader = cmd.ExecuteReader()
                    While reader.Read
                        pipe.PipeYear = reader("Year")
                    End While
                    reader.Close()
                Next
                If _conn.State <> ConnectionState.Closed Then _conn.Close()
            End If
            RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs("Version"))
        End Set
    End Property
End Class
