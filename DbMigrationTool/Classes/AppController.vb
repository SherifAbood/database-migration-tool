﻿Imports System.ComponentModel
Imports System.Security
Imports System.Security.Permissions
Imports DevExpress.Xpf.Core
Imports System.IO
Imports DevExpress.Xpf.Editors

Public Class AppController

    Public Const SettingsDirectory As String = "Settings"
    Public Const GeneralSettingsFileName As String = "Settings\GeneralSettings.xml"
    Public Const PRCPLYSettingsFileName As String = "Settings\PRCPLYGen.xml"
    Public Const DetectionPRCSettingsFileName As String = "Settings\DetectionPRC.xml"
    Public Const DetectionSCNSettingsFileName As String = "Settings\DetectionSCN.xml"
    Public Const CalcFarmSettingsFileName As String = "Settings\CalcFarm.xml"
    Public Const CalcFarmTasksFileName As String = "Settings\CalcFarmTasks.xml"
    Public Const AOCEventSelectionSettingsFileName As String = "Settings\AOC.xml"
    Public Const AOCEventsProcessingTempFileName As String = "Settings\AOCPETmp.xml"
    Public Const ClusterBatchProcessingTempFileName As String = "Settings\ClusterBatchTmp.xml"

    Public Const AOCForwardPLYSettingsFileName As String = "Settings\AOCFRWPLYGen.xml"
    Public Const AOC360PLYSettingsFileName As String = "Settings\AOC360PLYGen.xml"
    Public Const AOCScanPLYSettingsFileName As String = "Settings\AOCScanPLYGen.xml"
    Public Const AOCMainSettingsFileName As String = "Settings\AOCMain.xml"
    Public Const MCS3DSettingsFileName As String = "Settings\MCS3DSettings.xml"

    Public Const TrackSettingsFileName As String = "Settings\Track.xml"
    Public Const ProfileVSKPSettingsFileName As String = "Settings\ProfileVSKP.xml"
    Public Const ScanSettingsFileName As String = "Settings\Scan.xml"

    Public Const ScanAutoProcessingSettingsFileName As String = "Settings\ScanAutoProcessing.xml"

    Public Const DateFormalFormat As String = "dd/MM/yyyy HH:mm:ss"
    Public Const DateFormalFormatWithFraction As String = "dd/MM/yyyy HH:mm:ss.fff"

    Public Shared DetectionSettingsPRC As New Object
    Public Shared DetectionSettingsSCN As New Object


    Public Shared IsCurrentlySynching As Boolean = False
    Public Shared ArrOverlay As New ArrayList
    Public Shared VideoOverlay As Boolean
    Public Shared Opacity As Integer


    Public Shared UseImageAutoProcessing As Boolean = True
    Public Shared ScansAutoProcessingSettingsFilePath As String = String.Empty

    Public Shared DBlayer As clsDatabaseLayer

    Shared Property SplashMessage As String = "Initializing Components..."
    Shared Property MainWindowTitle As String
    Shared ReadOnly Property MainWindowTitleNoVersion As String
        Get
            If MainWindowTitle.Length > 0 Then
                Return MainWindowTitle.Substring(0, MainWindowTitle.IndexOf("."))
            Else
                Return ""
            End If
        End Get
    End Property



    Public Shared IsUsingCalcFarm As Boolean = True
    Public Shared IsLocalOfflineIncluded As Boolean = True



    Public Shared Function ShowDXMessage(MessageContent As String, MessageButton As MessageBoxButton, MessageImage As MessageBoxImage) As MessageBoxResult
        Return DXMessageBox.Show(MessageContent, "Database Migration Tool", MessageButton, MessageImage)
    End Function



    Public Shared Function ValidatePath(ByRef path As Object, ByVal pathType As enumPathType, ByVal pathText As String, Optional ByVal mandatory As Boolean = True, Optional extension As String = "") As Boolean
        Dim pathValidated As Boolean = True
        Dim pathContent As String = String.Empty

        If TypeOf path Is TextEdit Then
            If IsNothing(DirectCast(path, TextEdit).Text) = False Then
                pathContent = DirectCast(path, TextEdit).Text
            End If
        Else
            If IsNothing(path) = False Then
                pathContent = path
            End If
        End If

        If Not pathContent Is Nothing AndAlso pathContent <> String.Empty Then
            If pathType = enumPathType.File Then
                If Not System.IO.File.Exists(IIf(pathContent.Substring(pathContent.Length - 4) = extension, pathContent, pathContent & extension)) Then
                    AppController.ShowDXMessage(pathText & Space(1) & IIf(Not pathText.ToLower.Contains("file"), "file", String.Empty) & Space(1) & "not exist," & vbCrLf & "or Invalid file extension  ... !", MessageBoxButton.OK, MessageBoxImage.Error)
                    pathValidated = False
                Else
                    If TypeOf path Is TextEdit Then DirectCast(path, TextEdit).Text = IIf(DirectCast(path, TextEdit).Text.Substring(pathContent.Length - 4) = extension, DirectCast(path, TextEdit).Text, DirectCast(path, TextEdit).Text & extension)
                End If
            ElseIf pathType = enumPathType.Folder Then
                If Not System.IO.Directory.Exists(pathContent) Then
                    AppController.ShowDXMessage(pathText & Space(1) & IIf(Not pathText.ToLower.Contains("folder"), "folder", String.Empty) & Space(1) & "not exist ... !", MessageBoxButton.OK, MessageBoxImage.Error)
                    pathValidated = False
                End If
            End If
        ElseIf mandatory Then
            AppController.ShowDXMessage(pathText & Space(1) & IIf(Not pathText.ToLower.Contains("folder"), "folder", String.Empty) & Space(1) & "is empty ... !", MessageBoxButton.OK, MessageBoxImage.Error)
            pathValidated = False
        End If

        Return pathValidated
    End Function

    Public Enum enumPathType
        File = 0
        Folder = 1
    End Enum

End Class
