﻿Public Class Class1
    Public script As String = "USE [master]
GO
/****** Object:  Database [MCSProjects5]    Script Date: 10/4/2018 04:12:14 PM ******/

CREATE DATABASE [MCSProjects5]
-- CONTAINMENT = NONE
-- ON  PRIMARY 
--( NAME = N'MCSProjects5.10', FILENAME = N'C:\AbdullahDatabases\MCSProjects5' , SIZE = 289792KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
-- LOG ON 


GO
ALTER DATABASE [MCSProjects5] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [MCSProjects5].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [MCSProjects5] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [MCSProjects5] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [MCSProjects5] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [MCSProjects5] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [MCSProjects5] SET ARITHABORT OFF 
GO
ALTER DATABASE [MCSProjects5] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [MCSProjects5] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [MCSProjects5] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [MCSProjects5] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [MCSProjects5] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [MCSProjects5] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [MCSProjects5] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [MCSProjects5] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [MCSProjects5] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [MCSProjects5] SET  DISABLE_BROKER 
GO
ALTER DATABASE [MCSProjects5] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [MCSProjects5] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [MCSProjects5] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [MCSProjects5] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [MCSProjects5] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [MCSProjects5] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [MCSProjects5] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [MCSProjects5] SET RECOVERY FULL 
GO
ALTER DATABASE [MCSProjects5] SET  MULTI_USER 
GO
ALTER DATABASE [MCSProjects5] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [MCSProjects5] SET DB_CHAINING OFF 
GO
ALTER DATABASE [MCSProjects5] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [MCSProjects5] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [MCSProjects5] SET DELAYED_DURABILITY = DISABLED 
GO
USE [MCSProjects5]
GO
/****** Object:  Table [dbo].[Patchs]    Script Date: 10/4/2018 04:12:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Patchs]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Patchs](
	[FK_ProjectID] [int] NULL,
	[FK_PipeID] [int] NULL,
	[PK_PatchID] [int] NULL,
	[PatchStart] [datetime] NULL,
	[PathEnd] [datetime] NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tblAIMSComponentLocation]    Script Date: 10/4/2018 04:12:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblAIMSComponentLocation]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblAIMSComponentLocation](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Component_Location] [nvarchar](4000) NULL,
	[Component_Type] [nvarchar](4000) NULL,
	[AssetID] [int] NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tblAIMSHashTable]    Script Date: 10/4/2018 04:12:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblAIMSHashTable]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblAIMSHashTable](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Pcode] [nvarchar](50) NULL,
	[Scode] [nvarchar](50) NULL,
	[Primary_Event_Category] [nvarchar](100) NULL,
	[Secondary_Event_Category] [nvarchar](100) NULL,
	[Range_Point] [nvarchar](100) NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tblAutoBackup]    Script Date: 10/4/2018 04:12:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblAutoBackup]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblAutoBackup](
	[ID] [int] NOT NULL,
	[Name] [varchar](255) NOT NULL,
	[XMLSettings] [xml] NOT NULL,
	[Period] [int] NULL,
	[DayName] [int] NULL,
	[DayNumber] [int] NULL,
	[MonthNumber] [int] NULL,
	[DateTime] [datetime] NULL,
	[Type] [int] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblCoordinateSystem]    Script Date: 10/4/2018 04:12:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblCoordinateSystem]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblCoordinateSystem](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PipeID] [int] NULL,
	[ProjectID] [int] NULL,
	[CoordinateSystemSettings] [xml] NULL,
	[StartKP] [float] NULL,
	[EndKP] [float] NULL,
	[StartDTime] [datetime] NULL,
	[EndDTime] [datetime] NULL,
	[SpheroidName] [nvarchar](50) NULL,
	[OriginParametersName] [nvarchar](50) NULL,
 CONSTRAINT [PK_tblCoordinateSystem] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tblDesignRoutes]    Script Date: 10/4/2018 04:12:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblDesignRoutes]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblDesignRoutes](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FK_ProjectID] [int] NOT NULL,
	[Route_Data] [nvarchar](50) NULL,
	[East] [decimal](16, 6) NULL,
	[North] [decimal](16, 6) NULL,
	[KP] [decimal](16, 6) NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tblDives]    Script Date: 10/4/2018 04:12:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblDives]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblDives](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FK_ProjectID] [int] NOT NULL,
	[Dive_Number] [int] NOT NULL,
	[StartKP] [decimal](16, 6) NULL,
	[EndKP] [decimal](16, 6) NULL,
	[StartDTime] [datetime] NULL,
	[EndDTime] [datetime] NULL,
	[DiveType] [int] NULL,
	[ROVID] [int] NOT NULL,
 CONSTRAINT [PK_tblDives] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tblDiveTypes]    Script Date: 10/4/2018 04:12:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblDiveTypes]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblDiveTypes](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FK_ProjectID] [int] NOT NULL,
	[DiveType] [text] NOT NULL,
	[DiveType_Code] [nvarchar](50) NULL,
	[Method_Used] [nvarchar](255) NULL,
 CONSTRAINT [PK_tblDiveTypes] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tblIBISHashTable]    Script Date: 10/4/2018 04:12:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblIBISHashTable]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblIBISHashTable](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Pcode] [text] NULL,
	[Scode] [text] NULL,
	[IBIS_Code] [int] NULL,
	[Status] [text] NULL,
	[Primary_Description] [text] NULL,
	[Secondary_Description] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tblOperationCodes]    Script Date: 10/4/2018 04:12:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblOperationCodes]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblOperationCodes](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FK_ProjectID] [int] NOT NULL,
	[OperationType] [nvarchar](255) NOT NULL,
	[Operation_Code] [nvarchar](50) NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tblPCode]    Script Date: 10/4/2018 04:12:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblPCode]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblPCode](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [nvarchar](15) NULL,
	[Name] [nvarchar](50) NULL,
	[Picture] [image] NULL,
	[PShortcut] [int] NULL CONSTRAINT [DF_tblPCode_PShortcut]  DEFAULT ((0)),
	[Type] [int] NULL,
	[Layername] [nvarchar](50) NULL,
	[ProjectID] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tblPipeRadii]    Script Date: 10/4/2018 04:12:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblPipeRadii]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblPipeRadii](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FK_ProjectID] [int] NULL,
	[FK_PipeID] [int] NOT NULL,
	[FromKP] [decimal](16, 6) NULL,
	[ToKP] [decimal](16, 6) NULL,
	[FromDTime] [datetime] NULL,
	[ToDTime] [datetime] NULL,
	[Radius] [float] NULL,
	[Value] [float] NULL,
	[IntervalType] [int] NULL,
 CONSTRAINT [PK_PipeRadii] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tblPipes]    Script Date: 10/4/2018 04:12:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblPipes]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblPipes](
	[PK_PipeID] [int] NOT NULL,
	[FK_ProjectID] [int] NULL,
	[Name] [nvarchar](255) NULL,
	[LoggingDB] [text] NULL,
	[ProcessingDB] [text] NULL,
	[VideoFilePath1] [varchar](255) NULL,
	[VideoFilePath2] [varchar](255) NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[Duration] [real] NULL,
	[PatchSize] [int] NULL,
	[NumberOfPatchs] [int] NULL,
	[PipeType] [int] NULL,
	[LeftVideoFilePath] [varchar](255) NULL,
	[CenterVideoFilePath] [varchar](255) NULL,
	[RightVideoFilePath] [varchar](255) NULL,
	[CombinedVideoFilePath] [varchar](255) NULL,
	[StartKP] [decimal](16, 6) NULL,
	[EndKP] [decimal](16, 6) NULL,
	[StartDTime] [datetime] NULL,
	[EndDTime] [datetime] NULL,
	[SurveyType] [int] NULL,
	[KPUnit] [int] NULL,
	[UTCShift] [real] NULL,
	[Area] [nvarchar](50) NULL,
	[OfflineKP] [decimal](16, 6) NULL,
	[ChecksKP] [decimal](16, 6) NULL,
	[SmoothingKP] [decimal](16, 6) NULL,
	[EventsKP] [decimal](16, 6) NULL,
	[ChartingKP] [decimal](16, 6) NULL,
	[Reported] [bit] NULL,
	[PipeStatus] [xml] NULL,
	[StoredProcedureSettings] [xml] NULL,
	[PipeDirection] [nchar](15) NULL,
	[FK_ProjectYearID] [int] NULL,
	[DBVersion] [nvarchar](50) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblPipeStatus]    Script Date: 10/4/2018 04:12:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblPipeStatus]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblPipeStatus](
	[PK_PipeStatus] [int] IDENTITY(1,1) NOT NULL,
	[FK_ProjectID] [int] NOT NULL,
	[FK_PipeID] [int] NOT NULL,
	[FK_ProjectYearID] [int] NULL,
	[GeneralDetails_StartDtime] [datetime] NULL,
	[GeneralDetails_EndDtime] [datetime] NULL,
	[GeneralDetails_StartKp] [float] NULL,
	[GeneralDetails_EndKp] [float] NULL,
	[Importing_FromLogDBToProcDB] [float] NULL,
	[TrackProcessing_ImportFromPoslogToPosProc] [float] NULL,
	[TrackProcessing_DeSpikingNavigation] [float] NULL,
	[TrackProcessing_RouteDigitizing] [float] NULL,
	[TrackProcessing_KpCalc] [float] NULL,
	[TrackProcessing_HeadingDevCalc] [float] NULL,
	[TrackProcessing_DesignKpCalc] [float] NULL,
	[TrackProcessing_DccCalc] [float] NULL,
	[DepthProcessing_DepthAndTideCalc] [float] NULL,
	[ScansProcessing_ImportFromScanLogToScanProc] [float] NULL,
	[ScansProcessing_HPRInterpolation] [float] NULL,
	[ScansProcessing_KpCalc] [float] NULL,
	[ScansProcessing_HeadingDevCalc] [float] NULL,
	[ScansProcessing_PositionCalc] [float] NULL,
	[ScansProcessing_DesignKpCalc] [float] NULL,
	[ScansProcessing_DccCalc] [float] NULL,
	[ScansProcessing_DepthCalc] [float] NULL,
	[ScansProcessing_UpdateKpInterval] [float] NULL,
	[ScansProcessing_PipeAndSeaBedDetection] [float] NULL,
	[ScansProcessing_GapsChecks] [float] NULL,
	[ScansProcessing_ProfileSmoothing] [float] NULL,
	[ScansProcessing_SeabedSmoothing] [float] NULL,
	[PipeTrackerProcessing_ImportFromPTrackerLogToPTrackerProc] [float] NULL,
	[PipeTrackerProcessing_HPRInterpolation] [float] NULL,
	[PipeTrackerProcessing_PositionCalc] [float] NULL,
	[PipeTrackerProcessing_KpCalc] [float] NULL,
	[PipeTrackerProcessing_DepthCalc] [float] NULL,
	[PipeTrackerProcessing_UpdateKpInterval] [float] NULL,
	[PipeTrackerProcessing_ImplementingPipeTrackerInScanProc] [float] NULL,
	[EventsProcessing_VideoCombining] [float] NULL,
	[EventsProcessing_ImportFromEventLogToEventProc] [float] NULL,
	[EventsProcessing_VideoReview] [float] NULL,
	[EventsProcessing_KpCalc] [float] NULL,
	[EventsProcessing_EventChecks] [float] NULL,
	[EventsProcessing_PositionCalc] [float] NULL,
	[EventsProcessing_DepthCalc] [float] NULL,
	[EventsProcessing_DesignKpCalc] [float] NULL,
	[EventsProcessing_DccCalc] [float] NULL,
	[Deliverables_PreparingEventList] [float] NULL,
	[Deliverables_EventListQC] [float] NULL,
	[Deliverables_PreparingCharts] [float] NULL,
	[Deliverables_ChartsQC] [float] NULL,
	[Deliverables_PreparingReport] [float] NULL,
	[Deliverables_ReportQC] [float] NULL,
	[FinishedOnline] [bit] NULL,
	[FinishedProcessing] [bit] NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tblPipeWorkScope]    Script Date: 10/4/2018 04:12:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblPipeWorkScope]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblPipeWorkScope](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FK_ProjectID] [int] NOT NULL,
	[FK_PipeID] [int] NULL,
	[Line_Code] [nvarchar](200) NOT NULL,
	[FromKP] [decimal](16, 6) NOT NULL,
	[ToKP] [decimal](16, 6) NOT NULL,
	[Length] [decimal](16, 6) NULL,
	[Exclude] [bit] NULL,
	[IBIS_Number] [int] NULL,
	[Plan_Number] [text] NULL,
	[Base_Plan] [text] NULL,
	[Comments] [text] NULL,
	[Profiles_Required] [bit] NULL,
	[CP_Required] [bit] NULL,
	[Database_Column] [text] NULL,
	[Database_Complete] [bit] NULL,
	[Complete] [bit] NULL,
	[IN_Workplan] [bit] NULL,
	[Structure] [bit] NULL,
	[Open_Water] [bit] NULL,
	[Where_Exactly] [text] NULL,
	[Area] [text] NULL,
	[Route_Data] [text] NULL,
 CONSTRAINT [PK_tblPipeWorkScope] PRIMARY KEY CLUSTERED 
(
	[Line_Code] ASC,
	[FromKP] ASC,
	[ToKP] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tblProjectLogBook]    Script Date: 10/4/2018 04:12:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblProjectLogBook]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblProjectLogBook](
	[FK_ProjectID] [int] NULL,
	[FK_PipeID] [int] NULL,
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DTime] [datetime] NOT NULL,
	[UserID] [int] NULL,
	[Description] [text] NULL,
	[Activity] [int] NULL,
	[App] [int] NULL,
 CONSTRAINT [PK_LogBook] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tblProjects]    Script Date: 10/4/2018 04:12:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblProjects]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblProjects](
	[PK_ProjectID] [int] NOT NULL,
	[Name] [nvarchar](255) NULL,
	[Comment] [text] NULL,
	[ProjectCode] [nvarchar](50) NULL,
	[JobFolder] [text] NULL,
	[TimeShift] [decimal](16, 6) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tblReportRegister]    Script Date: 10/4/2018 04:12:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblReportRegister]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblReportRegister](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FK_ProjectID] [int] NOT NULL,
	[FK_SurveySectionID] [int] NULL,
	[StartKP] [decimal](16, 6) NULL,
	[EndKP] [decimal](16, 6) NULL,
	[Length] [decimal](16, 6) NULL,
	[ReportNo] [text] NULL,
	[Client_ReportNo] [text] NULL,
	[Start_DTime] [datetime] NULL,
	[End_DTime] [datetime] NULL,
	[Comment] [text] NULL,
	[RevisionNo] [int] NULL,
	[Submitted] [bit] NULL,
	[Submitted_Date] [datetime] NULL,
	[Returned] [bit] NULL,
	[Returned_Date] [datetime] NULL,
	[Corrected] [bit] NULL,
	[Corected_Date] [datetime] NULL,
	[Accepted] [bit] NULL,
	[Accepted_Date] [datetime] NULL,
	[Canceled] [bit] NULL,
	[Canceled_Date] [datetime] NULL,
	[IBIS_SequenceNumber] [int] NULL,
	[IBIS_IncidentFile_Revision] [int] NULL,
	[IBIS_IncidentFile_Submitted] [bit] NULL,
	[IBIS_IncidentFile_Submitted_Date] [datetime] NULL,
	[IBIS_IncidentFile_Corrected] [bit] NULL,
	[IBIS_IncidentFile_Corrected_Date] [datetime] NULL,
	[IBIS_IncidentFile_Returned] [bit] NULL,
	[IBIS_IncidentFile_Returned_Date] [datetime] NULL,
	[IBIS_IncidentFile_Accepted] [bit] NULL,
	[IBIS_IncidentFile_Accepted_Date] [datetime] NULL,
	[IBIS_IncidentFile_Canceled] [bit] NULL,
	[IBIS_IncidentFile_Canceled_Date] [datetime] NULL,
	[IBIS_LogFile_Revision] [int] NULL,
	[IBIS_LogFile_Submitted] [bit] NULL,
	[IBIS_LogFile_Submitted_Date] [datetime] NULL,
	[IBIS_LogFile_Corrected] [bit] NULL,
	[IBIS_LogFile_Corrected_Date] [datetime] NULL,
	[IBIS_LogFile_Returned] [bit] NULL,
	[IBIS_LogFile_Returned_Date] [datetime] NULL,
	[IBIS_LogFile_Accepted] [bit] NULL,
	[IBIS_LogFile_Accepted_Date] [datetime] NULL,
	[IBIS_LogFile_Canceled] [bit] NULL,
	[IBIS_LogFile_Canceled_Date] [datetime] NULL,
	[IBIS_DiveFile_Revision] [int] NULL,
	[IBIS_DiveFile_Submitted] [bit] NULL,
	[IBIS_DiveFile_Submitted_Date] [datetime] NULL,
	[IBIS_DiveFile_Corrected] [bit] NULL,
	[IBIS_DiveFile_Corrected_Date] [datetime] NULL,
	[IBIS_DiveFile_Returned] [bit] NULL,
	[IBIS_DiveFile_Returned_Date] [datetime] NULL,
	[IBIS_DiveFile_Accepted] [bit] NULL,
	[IBIS_DiveFile_Accepted_Date] [datetime] NULL,
	[IBIS_DiveFile_Canceled] [bit] NULL,
	[IBIS_DiveFile_Canceled_Date] [datetime] NULL,
	[IBIS_ImageFile_Revision] [int] NULL,
	[IBIS_ImageFile_Submitted] [bit] NULL,
	[IBIS_ImageFile_Submitted_Date] [datetime] NULL,
	[IBIS_ImageFile_Corrected] [bit] NULL,
	[IBIS_ImageFile_Corrected_Date] [datetime] NULL,
	[IBIS_ImageFile_Returned] [bit] NULL,
	[IBIS_ImageFile_Returned_Date] [datetime] NULL,
	[IBIS_ImageFile_Accepted] [bit] NULL,
	[IBIS_ImageFile_Accepted_Date] [datetime] NULL,
	[IBIS_ImageFile_Canceled] [bit] NULL,
	[IBIS_ImageFile_Canceled_Date] [datetime] NULL,
	[IBIS_ProfileFile_Revision] [int] NULL,
	[IBIS_ProfileFile_Submitted] [bit] NULL,
	[IBIS_ProfileFile_Submitted_Date] [datetime] NULL,
	[IBIS_ProfileFile_Corrected] [bit] NULL,
	[IBIS_ProfileFile_Corrected_Date] [datetime] NULL,
	[IBIS_ProfileFile_Returned] [bit] NULL,
	[IBIS_ProfileFile_Returned_Date] [datetime] NULL,
	[IBIS_ProfileFile_Accepted] [bit] NULL,
	[IBIS_ProfileFile_Accepted_Date] [datetime] NULL,
	[IBIS_ProfileFile_Canceled] [bit] NULL,
	[IBIS_ProfileFile_Canceled_Date] [datetime] NULL,
	[IncidentFile_Submit] [bit] NULL,
	[LogFile_Submit] [bit] NULL,
	[ImageFile_Submit] [bit] NULL,
	[DiveFile_Submit] [bit] NULL,
	[ProfileFile_Submit] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tblROVs]    Script Date: 10/4/2018 04:12:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblROVs]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblROVs](
	[FK_ProjectID] [int] NOT NULL,
	[PK_ROVID] [int] NOT NULL,
	[Name] [nvarchar](255) NULL,
	[Description] [text] NULL,
	[Image] [image] NULL,
	[Settings] [xml] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tblROVSettings]    Script Date: 10/4/2018 04:12:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblROVSettings]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblROVSettings](
	[FK_ProjectID] [int] NOT NULL,
	[PK_ROVID] [int] NOT NULL,
	[PosX] [real] NULL,
	[PosY] [real] NULL,
	[PosZ] [real] NULL,
	[ScanX] [real] NULL,
	[ScanY] [real] NULL,
	[ScanZ] [real] NULL,
	[AltX] [real] NULL,
	[AltY] [real] NULL,
	[AltZ] [real] NULL,
	[EventX] [real] NULL,
	[EventY] [real] NULL,
	[EventZ] [real] NULL,
	[DepthX] [real] NULL,
	[DepthY] [real] NULL,
	[DepthZ] [real] NULL,
	[PTrackerX] [real] NULL,
	[PTrackerY] [real] NULL,
	[PTrackerZ] [real] NULL,
	[DopplerX] [real] NULL,
	[DopplerY] [real] NULL,
	[DopplerZ] [real] NULL,
	[MScanOffsetX] [real] NULL,
	[MScanOffsetY] [real] NULL,
	[MScanOffsetZ] [real] NULL,
	[SScanOffsetX] [real] NULL,
	[SScanOffsetY] [real] NULL,
	[SScanOffsetZ] [real] NULL,
	[MRX] [real] NULL,
	[MRY] [real] NULL,
	[MRZ] [real] NULL,
	[SRX] [real] NULL,
	[SRY] [real] NULL,
	[SRZ] [real] NULL,
	[DMSRX] [real] NULL,
	[DMSRY] [real] NULL,
	[DMSRZ] [real] NULL,
	[Distance] [real] NULL,
	[DatumHeight] [real] NULL,
	[PTrackerAltX] [real] NULL,
	[PTrackerAltY] [real] NULL,
	[PTrackerAltZ] [real] NULL,
	[Width] [real] NULL,
	[Height] [real] NULL,
	[Length] [real] NULL,
	[SettingsType] [int] NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tblSettingsFolders]    Script Date: 10/4/2018 04:12:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblSettingsFolders]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblSettingsFolders](
	[FK_ProjectID] [int] NOT NULL,
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[OnlineProfilePath] [nvarchar](255) NULL,
	[OfflineProfilePath] [nvarchar](255) NULL,
	[StoredProceduresPath] [nvarchar](255) NULL,
	[AutoProcessingSettingsPath] [nvarchar](255) NULL,
	[ProcessGraphicalPath] [nvarchar](255) NULL,
	[MultipleScansPath] [nvarchar](255) NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tblSTCode]    Script Date: 10/4/2018 04:12:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblSTCode]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblSTCode](
	[PrimaryID] [int] NOT NULL,
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [nvarchar](15) NOT NULL,
	[Type] [int] NULL,
	[LayerName] [nvarchar](50) NOT NULL,
	[LayerName_Side] [nvarchar](50) NOT NULL,
	[Description] [text] NULL,
	[Comment] [text] NULL,
	[ProfileEventFlag] [bit] NULL,
	[PlanEventFlag] [bit] NULL,
	[AddPlanTextFlag] [bit] NULL,
	[AddProfileTextFlag] [bit] NULL,
	[Discard] [bit] NULL,
	[imagecap] [bit] NULL,
	[AnomalyCode] [nvarchar](15) NULL,
	[GenerateAOC] [bit] NULL,
	[EndCode] [nvarchar](15) NULL,
	[AutoGeneratedComment] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tblStructure]    Script Date: 10/4/2018 04:12:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblStructure]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblStructure](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FK_ProjectID] [int] NOT NULL,
	[Installation] [nvarchar](255) NULL,
	[Workpack] [nvarchar](255) NULL,
	[WorkpackYear] [int] NULL,
	[DBConnection] [nvarchar](255) NULL,
	[LeftVideoFilePath] [varchar](255) NULL,
	[CenterVideoFilePath] [varchar](255) NULL,
	[RightVideoFilePath] [varchar](255) NULL,
	[CombinedVideoFilePath] [varchar](255) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblSurveySections]    Script Date: 10/4/2018 04:12:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblSurveySections]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblSurveySections](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FK_ProjectID] [int] NOT NULL,
	[FK_PipeID] [int] NOT NULL,
	[Task_Number] [int] NULL,
	[Dive_Number] [int] NULL,
	[Plan_Number] [text] NULL,
	[Start_WorkScope_KP] [decimal](16, 6) NULL,
	[Start_Actual_KP] [decimal](16, 6) NULL,
	[Start_DTime] [datetime] NULL,
	[End_WorkScope_KP] [decimal](16, 6) NULL,
	[End_Actual_KP] [decimal](16, 6) NULL,
	[End_DTime] [datetime] NULL,
	[Comments] [text] NULL,
	[Report_Number] [text] NULL,
	[Operation_Code] [text] NULL,
	[ROVID] [int] NOT NULL,
	[WorkScopeID] [int] NOT NULL,
	[Line_Code] [text] NULL,
 CONSTRAINT [PK_tblSurveySections] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tblTasks]    Script Date: 10/4/2018 04:12:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblTasks]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblTasks](
	[ID] [int] NOT NULL,
	[GUID] [nvarchar](14) NULL,
	[FK_ProjectID] [int] NULL,
	[FK_PipeID] [int] NULL,
	[Title] [nvarchar](255) NULL,
	[Task] [ntext] NULL,
	[FK_TaskTypeID] [int] NULL,
	[TaskStatus] [int] NULL,
	[TaskPercentege] [float] NULL,
	[IssuingDTime] [datetime] NULL,
	[ReadDTime] [datetime] NULL,
	[ClosedDTime] [datetime] NULL,
	[IssuerID] [int] NULL,
	[IssuerName] [nvarchar](255) NULL,
	[UserID] [int] NULL,
	[UserName] [nvarchar](255) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tblTaskType]    Script Date: 10/4/2018 04:12:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblTaskType]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblTaskType](
	[ID] [int] NULL,
	[TaskType] [nvarchar](255) NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tblTide]    Script Date: 10/4/2018 04:12:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblTide]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblTide](
	[FK_ProjectID] [int] NOT NULL,
	[PK_TideID] [int] NOT NULL,
	[Name] [nvarchar](255) NULL,
	[Details] [xml] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tblTideDetails]    Script Date: 10/4/2018 04:12:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblTideDetails]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblTideDetails](
	[FK_ProjectId] [int] NOT NULL,
	[PK_TideID] [int] NOT NULL,
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DTime] [datetime] NULL,
	[KP] [float] NULL,
	[Value] [real] NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tblTitles]    Script Date: 10/4/2018 04:12:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblTitles]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblTitles](
	[FK_ProjectID] [int] NOT NULL,
	[ClientName] [text] NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[Duration] [real] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tblYears]    Script Date: 10/4/2018 04:12:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblYears]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblYears](
	[PK_ProjectYearID] [int] NOT NULL,
	[FK_ProjectID] [int] NOT NULL,
	[Year] [int] NOT NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tlbTeamAccounts]    Script Date: 10/4/2018 04:12:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tlbTeamAccounts]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tlbTeamAccounts](
	[FK_ProjectID] [int] NOT NULL,
	[ID] [int] NOT NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[Password] [nvarchar](50) NOT NULL,
	[RoleID] [int] NULL
) ON [PRIMARY]
END
GO
USE [master]
GO
ALTER DATABASE [MCSProjects5] SET  READ_WRITE 
GO
"
End Class
