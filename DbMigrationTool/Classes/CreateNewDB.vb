﻿Imports System.IO
Imports System.Text.RegularExpressions
Imports System.Data.SqlClient
Imports Microsoft.SqlServer.Management.Smo
Imports Microsoft.SqlServer.Management.Common
Imports System.Data
Public Class CreateNewDB
    Dim _connStr As String
    Dim _conn As SqlConnection
    Dim _trans As SqlTransaction
    Dim cmd As New SqlCommand
    Dim _sqlStmt As String = String.Empty
    Dim _server As New Server
    Public DBexists As Integer
    Public PipeExists As Integer

    Public Function CreatePipe(sqlScriptFilePath As String, ByVal DBConnectionString As String, pipeName As String) As Boolean
        Try


            _connStr = DBConnectionString
            _conn = New SqlConnection(_connStr)
            If _conn.State <> ConnectionState.Open Then _conn.Open()
            Dim cmd As New SqlCommand("select count(name) FROM sys.databases WHERE name = N'" + pipeName + "' ", _conn)
            PipeExists = cmd.ExecuteScalar()
            If PipeExists > 0 Then
                Exit Function
            End If
            Dim con As New ServerConnection(_conn)
            _server = New Server(con)

            Dim file As SciptFile = New SciptFile
            Dim sqlscript As String = file.scriptPipe
            'File.ReadAllText(sqlScriptFilePath)
            If sqlscript.IndexOf("PipeBlank") = -1 Then
                'DXMessage.show("Invalid Pipe Script File...", MessageBoxButton.OK, MessageBoxImage.Error)
                Return False
            Else
                sqlscript = sqlscript.Replace("PipeBlank", pipeName)
            End If
            _server.ConnectionContext.ExecuteNonQuery(sqlscript, ExecutionTypes.ContinueOnError)
            If _conn.State <> ConnectionState.Closed Then _conn.Close()

            Return True
        Catch ex As Exception
            'If Not _trans.Connection Is Nothing Then _trans.Rollback()
            If _conn.State <> ConnectionState.Closed Then _conn.Close()
            Return False
        End Try
    End Function


    Public Function CreateMCSProject(sqlScriptFilePath As String, mcsProjectConnectionString As String, projectName As String) As Boolean
        Try
            _connStr = mcsProjectConnectionString
            _conn = New SqlConnection(_connStr)
            If _conn.State <> ConnectionState.Open Then _conn.Open()
            Dim cmd As New SqlCommand("select count(name) FROM sys.databases WHERE name = N'" + projectName + "' ", _conn)
            DBexists = cmd.ExecuteScalar()
            If DBexists > 0 Then
                Exit Function
            End If

            Dim server As Server = New Server(New ServerConnection(_conn))
            Dim file As SciptFile = New SciptFile
            Dim sqlscript As String = file.scriptMCSProject
            'File.ReadAllText(sqlScriptFilePath)
            If sqlscript.Contains("MCSProjects5") Then
                sqlscript = sqlscript.Replace("MCSProjects5", projectName)

                server.ConnectionContext.ExecuteNonQuery(sqlscript, ExecutionTypes.Default)

            Else
                AppController.ShowDXMessage("Invalid MCSProject Script File...", MessageBoxButton.OK, MessageBoxImage.Error)
                Return False
            End If

            If _conn.State <> ConnectionState.Closed Then _conn.Close()

            Return True
        Catch ex As Exception
            If _conn.State <> ConnectionState.Closed Then _conn.Close()
            'ShowDXMessage(ex.Message & vbNewLine & ex.StackTrace, MessageBoxButton.OK, MessageBoxImage.Error)
            Return False
        End Try
    End Function

End Class
