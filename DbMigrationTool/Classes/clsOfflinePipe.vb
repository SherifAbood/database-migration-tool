﻿Imports System.ComponentModel
Imports System.Data
Imports System.Data.SqlClient
Public Class clsOfflinePipe
    Implements INotifyPropertyChanged

    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged

    Private Sub NotifyPropertyChanged(ByVal info As String)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(info))
    End Sub


    Private _Id As Integer = Nothing
    Public Property Id() As Integer
        Get
            Return _Id
        End Get
        Set(ByVal value As Integer)
            _Id = value
        End Set
    End Property

    Private _mcsProjectId As Integer = Nothing
    Public Property MCSProjectId() As Integer
        Get
            Return _mcsProjectId
        End Get
        Set(ByVal value As Integer)
            _mcsProjectId = value
        End Set
    End Property

    Private _pipeName As String
    Public Property PipeName() As String
        Get
            Return _pipeName
        End Get
        Set(ByVal value As String)
            _pipeName = value
        End Set
    End Property


    Private _pipeYear As String = String.Empty
    Public Property PipeYear() As String
        Get
            Return _pipeYear
        End Get
        Set(ByVal value As String)
            _pipeYear = value
            RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs("PipeYear"))
        End Set
    End Property


    Private _loggingDb As String
    Public Property LoggingDb() As String
        Get
            Return _loggingDb
        End Get
        Set(ByVal value As String)
            _loggingDb = value
        End Set
    End Property

    Private _processingDb As String
    Public Property ProcessingDb() As String
        Get
            Return _processingDb
        End Get
        Set(ByVal value As String)
            _processingDb = value
        End Set
    End Property

    Private _isPipeChecked As Boolean
    Public Property IsPipeChecked() As Boolean
        Get
            Return _isPipeChecked
        End Get
        Set(ByVal value As Boolean)
            _isPipeChecked = value
            If value = True Then
                Dim _connStr As String
                Dim _conn As SqlConnection
                Dim cmd As New SqlCommand
                _connStr = ProcessingDb
                _conn = New SqlConnection(_connStr)
                If _conn.State <> ConnectionState.Open Then _conn.Open()
                cmd = New SqlCommand("select count(name) FROM sys.databases WHERE name = N'" + "V5_" + ProcessingDb.ToString.Split(";").Where(Function(pip) (pip.Contains("Initial Catalog="))).First.Replace("Initial Catalog=", "") + "'", _conn)
                Dim PipeExists As Integer = cmd.ExecuteScalar()
                If PipeExists > 0 Then

                    AppController.ShowDXMessage("You've migrated data from this pipe: " & PipeName & " before.", MessageBoxButton.OK, MessageBoxImage.Information)
                    IsPipeChecked = False
                End If
            End If
            RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs("IsPipeChecked"))
        End Set
    End Property


    Private _mcsProjectName As String
    Public Property MCSProjectName() As String
        Get
            Return _mcsProjectName
        End Get
        Set(ByVal value As String)
            _mcsProjectName = value
        End Set
    End Property
End Class
