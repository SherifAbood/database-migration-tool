﻿Public Class DataTypes
    Structure Table
        Dim Name As String
        Dim Body As String
        Dim Column As List(Of Tuple(Of String, String, String, String))
    End Structure

    Structure Type
        Dim Name As String
        Dim Body As String
    End Structure

    Structure StoredProc
        Dim Name As String
        Dim Body As String
    End Structure

    Structure OtherCommands
        Dim Body As String
    End Structure
End Class
