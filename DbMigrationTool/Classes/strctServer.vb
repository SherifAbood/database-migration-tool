﻿
Public Class strctServer

    Private _bId As Integer
    Public Property BId As Integer
        Get
            Return _bId
        End Get
        Set(ByVal value As Integer)
            _bId = value
        End Set
    End Property

    Private _name As String
    Public Property Name As String
        Get
            Return _name
        End Get
        Set(ByVal value As String)
            _name = value
        End Set
    End Property

    Private _userName As String
    Public Property UserName As String
        Get
            Return _userName
        End Get
        Set(ByVal value As String)
            _userName = value
        End Set
    End Property

    Private _passWord As String
    Public Property PassWord As String
        Get
            Return _passWord
        End Get
        Set(ByVal value As String)
            _passWord = value
        End Set
    End Property

    Private _connectionString As String
    Public Property ConnectionString As String
        Get
            Return _connectionString
        End Get
        Set(ByVal value As String)
            _connectionString = value
        End Set
    End Property

End Class

