﻿Public Class ColumnEntity

    Private _name As String
    Public Property Name() As String
        Get
            Return _name
        End Get
        Set(ByVal value As String)
            _name = value
        End Set
    End Property

    Private _dataType As String
    Public Property DataType() As String
        Get
            Return _dataType
        End Get
        Set(ByVal value As String)
            _dataType = value
        End Set
    End Property


    Private _isNull As String
    Public Property IsNull() As String
        Get
            Return _isNull
        End Get
        Set(ByVal value As String)
            _isNull = value
        End Set
    End Property


    Private _maxLength As String = String.Empty
    Public Property MaxLength() As String
        Get
            Return _maxLength
        End Get
        Set(ByVal value As String)
            _maxLength = value
        End Set
    End Property

End Class
