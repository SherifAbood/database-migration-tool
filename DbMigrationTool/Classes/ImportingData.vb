﻿Imports System.Data
Imports System.Data.SqlClient
Public Class ImportingData
    Dim _sqlStmt As String = String.Empty
    Dim _connStr As String
    Dim srcConnectionstring As String
    Dim cmd As New SqlCommand
    Dim _conn As New SqlConnection
    Dim colNames As String
    Dim query As String
    Dim _trans As SqlTransaction
    Public appendCols As String = String.Empty
    Dim sqlobj As SQLObject
    Dim LstOfTables As New List(Of TableEntity)
    Dim destDBName As String
    Dim srcDBName As String
    Dim IdentityFlag As Boolean
    Dim TableOfIdentity As List(Of String) = New List(Of String)
    Dim queryYear As String


    Sub New(servername As String, destDBName As String, srcDBName As String, username As String, password As String)
        queryYear = String.Empty
        Me.destDBName = destDBName
        Me.srcDBName = srcDBName
        _connStr = "Server = " + servername + ";database = " & destDBName & " ;User Id=" & username & ";Password=" & password & ";Persist Security Info=True"
        srcConnectionstring = "Server = " + servername + ";database=" & srcDBName & ";User Id=" & username & ";Password=" & password & ";Persist Security Info=True"
        _conn = New SqlConnection(_connStr)
        _conn.Open()
        cmd.Connection = _conn
        cmd.CommandText = "Select TABLE_NAME
from INFORMATION_SCHEMA.COLUMNS
where COLUMNPROPERTY(object_id(TABLE_SCHEMA+'.'+TABLE_NAME), COLUMN_NAME, 'IsIdentity') = 1 
"
        Dim reader As SqlDataReader = cmd.ExecuteReader
        While reader.Read
            TableOfIdentity.Add(reader.GetValue(0))
        End While
        reader.Close()
        If _conn.State <> ConnectionState.Closed Then _conn.Close()
    End Sub




    Public Sub GetTablesAndColumnsName(ByRef tablelst As List(Of TableEntity), connectionstring As String, srcConnectionstring As String)
        Try
            _connStr = connectionstring
            _conn = New SqlConnection(_connStr)
            If _conn.State <> ConnectionState.Open Then _conn.Open()
            'get tables
            _sqlStmt = "Select TABLE_NAME FROM INFORMATION_SCHEMA.TABLES "
            cmd = New SqlCommand(_sqlStmt, _conn)
            cmd.CommandType = CommandType.Text

            Dim reader As SqlDataReader
            reader = cmd.ExecuteReader()
            While reader.Read()
                Dim newTable As New TableEntity
                newTable.TblName = reader.GetValue(0)
                tablelst.Add(newTable)
            End While
            reader.Close()
            If _conn.State <> ConnectionState.Closed Then _conn.Close()

            For Each tbl In tablelst
                Dim cmd2 As SqlCommand
                Dim colLst As New List(Of ColumnEntity)
                _conn = New SqlConnection(srcConnectionstring)
                If _conn.State <> ConnectionState.Open Then _conn.Open()
                _sqlStmt = "Select column_name,DATA_TYPE FROM information_schema.columns where table_name='" & tbl.TblName & "'"
                cmd2 = New SqlCommand(_sqlStmt, _conn)
                cmd2.CommandType = CommandType.Text
                Dim reader2 As SqlDataReader
                reader2 = cmd2.ExecuteReader()
                While reader2.Read()
                    Dim newCol As New ColumnEntity
                    newCol.Name = reader2.GetValue(0)
                    newCol.DataType = reader2.GetValue(1)
                    colLst.Add(newCol)
                End While
                reader2.Close()
                tbl.LstOfColumns = colLst
                If _conn.State <> ConnectionState.Closed Then _conn.Close()

            Next
        Catch ex As Exception
            If ex.Message.Contains("time out") Then

            End If
            If _conn.State <> ConnectionState.Closed Then _conn.Close()
        End Try

    End Sub



    Sub Import(ByVal connString As String, lstOfPipes As List(Of clsOfflinePipe), ByVal McsProjectExists As Boolean)
        LstOfTables.Clear()
        GetTablesAndColumnsName(LstOfTables, connString, srcConnectionstring)

        Dim proc As String
        Dim log As String
        Dim checkTbl As TableEntity = New TableEntity
        For Each tbl In LstOfTables
            queryYear = ""
            IdentityFlag = False
            colNames = String.Empty
            For i = 0 To tbl.LstOfColumns.Count - 1
                If i <> tbl.LstOfColumns.Count - 1 Then
                    colNames += "[" + tbl.LstOfColumns(i).Name + "],"
                Else
                    colNames += "[" + tbl.LstOfColumns(i).Name + "]" + Environment.NewLine
                End If

            Next
            Try
                For i = 0 To LstOfTables.Count - 1
                    If TableOfIdentity.Contains(tbl.TblName) Then
                        IdentityFlag = True
                    End If
                Next
                Dim heading As String = String.Empty

                Dim tail As String = String.Empty
                If IdentityFlag Then
                    heading = "SET IDENTITY_INSERT  [" & destDBName & "].dbo." & tbl.TblName & " ON " + Environment.NewLine + "GO" + Environment.NewLine
                    tail = " SET IDENTITY_INSERT  [" & destDBName & "].dbo." & tbl.TblName & " OFF " + Environment.NewLine + "GO" + Environment.NewLine
                End If

                If tbl.TblName <> "tblPipes" Then
                    If tbl.TblName = "tblYears" Then
                        Dim reader As SqlDataReader
                        For Each pipe In lstOfPipes

                            If _conn.State <> ConnectionState.Open Then _conn.Open()
                            cmd.Connection = _conn
                            cmd.CommandText = "Select count(*) from [" + destDBName + "].[dbo].[tblYears] as dst WHERE  dst.[Year] = " + pipe.PipeYear
                            If cmd.ExecuteScalar > 0 Then
                                cmd.CommandText = "Select * from [" + destDBName + "].[dbo].[tblYears] as dst WHERE  dst.[Year] = " + pipe.PipeYear
                                If _conn.State <> ConnectionState.Open Then _conn.Open()
                                cmd.Connection = _conn
                                reader = cmd.ExecuteReader
                                If reader.Read Then
                                    queryYear += "Update [" & destDBName & "].dbo.[tblPipes] set [FK_ProjectYearID] ='" & reader("PK_ProjectYearID") & "' where PK_PipeID=" + pipe.Id.ToString + Environment.NewLine + "GO" + Environment.NewLine

                                End If
                                If _conn.State <> ConnectionState.Closed Then _conn.Close()
                                Continue For
                            Else
                                If _conn.State <> ConnectionState.Closed Then _conn.Close()
                                queryYear += heading + "Insert Into  [" & destDBName & "].dbo." & tbl.TblName & "(PK_ProjectYearID,FK_ProjectID,Year)" & Environment.NewLine & "values(" + Environment.NewLine +
                                     "(select ISNULL(max(PK_ProjectYearID), 0 ) from tblYears)+1 , (select ISNULL(max(PK_ProjectID), 0 ) from tblProjects) , " & pipe.PipeYear & ")" + Environment.NewLine + tail + "GO" + Environment.NewLine
                                queryYear += "Update [" & destDBName & "].dbo.[tblPipes] Set [FK_ProjectYearID] = (select ISNULL(max(PK_ProjectYearID), 0 ) from tblYears) where PK_PipeID=" + pipe.Id.ToString + Environment.NewLine + "GO" + Environment.NewLine
                            End If

                        Next
                    Else
                        query = heading + "Insert Into  [" & destDBName & "].dbo." & tbl.TblName & "(" & colNames & ") " + Environment.NewLine + "Select " + Environment.NewLine +
                             colNames + Environment.NewLine + " FROM [" & srcDBName & "].dbo." & tbl.TblName + Environment.NewLine + "GO" + Environment.NewLine + tail

                        appendCols += query + Environment.NewLine
                    End If
                Else
                    For Each pipe In lstOfPipes
                        If pipe.MCSProjectName = srcDBName Then
                            proc = pipe.ProcessingDb.ToString.Split(";").Where(Function(pip) (pip.Contains("Initial Catalog="))).First.Replace("Initial Catalog=", "")
                            log = pipe.LoggingDb.ToString.Split(";").Where(Function(pip) (pip.Contains("Initial Catalog="))).First.Replace("Initial Catalog=", "")
                            query = "Insert Into  [" & destDBName & "].dbo.[tblPipes] (" & colNames & ") " + Environment.NewLine + "Select " + Environment.NewLine +
                             colNames + Environment.NewLine + " FROM [" & srcDBName & "].dbo.[tblPipes] where PK_PipeID=" + pipe.Id.ToString + Environment.NewLine + "GO" + Environment.NewLine
                            query += "Update [" & destDBName & "].dbo.[tblPipes] Set ProcessingDB ='" & pipe.ProcessingDb.Replace(proc, "V5_" + proc) & "' where PK_PipeID=" + pipe.Id.ToString + Environment.NewLine + "GO" + Environment.NewLine

                            query += "Update [" & destDBName & "].dbo.[tblPipes] set LoggingDB ='" & pipe.LoggingDb.Replace(log, "V5_" + log) & "' where PK_PipeID=" + pipe.Id.ToString + Environment.NewLine + "GO" + Environment.NewLine
                            appendCols += query + Environment.NewLine
                        End If
                    Next
                End If
                appendCols += queryYear
            Catch ex As Exception
            End Try
        Next
    End Sub





End Class
