﻿Imports System.ComponentModel
Public Class TableEntity

    Private _tblName As String
    Public Property TblName() As String
        Get
            Return _tblName
        End Get
        Set(ByVal value As String)
            _tblName = value
        End Set
    End Property

    Private _lstOfColumns As List(Of ColumnEntity)
    Public Property LstOfColumns() As List(Of ColumnEntity)

        Get
            Return _lstOfColumns
        End Get
        Set(ByVal value As List(Of ColumnEntity))

            _lstOfColumns = value
        End Set
    End Property

End Class
