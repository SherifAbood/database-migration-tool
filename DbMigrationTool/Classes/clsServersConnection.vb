﻿Imports System.Data
Imports System.ComponentModel

Public Class clsServersConnection
    Implements INotifyPropertyChanged

    Public Event PropertyChanged(sender As Object, e As PropertyChangedEventArgs) Implements INotifyPropertyChanged.PropertyChanged

    Public _dbLayer As New clsDatabaseLayer


    Public _filterFrom As Object = Nothing
    Public _filterTo As Object = Nothing
    'Public _definedServers As New List(Of clsServersBinding)
    'Public _recentServers As New List(Of strctRecentServers)


    Public Function GetServersNames() As List(Of String)
        Dim lstServersNames As New List(Of String)

        Try
            Dim servers As DataTable = System.Data.Sql.SqlDataSourceEnumerator.Instance.GetDataSources()
            For Each server In servers.Rows
                Dim serverName As String = server("ServerName").ToString() + IIf(server("InstanceName").ToString() <> String.Empty, "\" + server("InstanceName").ToString(), String.Empty)
                lstServersNames.Add(serverName)
            Next

            Return lstServersNames

        Catch ex As Exception
            Return Nothing
        End Try
    End Function


End Class
