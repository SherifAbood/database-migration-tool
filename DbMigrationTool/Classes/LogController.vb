﻿

Public Class LogController

    Private Shared eventViewerLog As New EventLog
    Private ReadOnly Property Log As EventLog
        Get
            If eventViewerLog Is Nothing Then
                eventViewerLog = New EventLog()
                If Not EventLog.SourceExists(AppController.MainWindowTitleNoVersion) Then
                    EventLog.CreateEventSource(AppController.MainWindowTitleNoVersion, AppController.MainWindowTitleNoVersion)
                End If
                eventViewerLog.Source = AppController.MainWindowTitleNoVersion
                eventViewerLog.Log = AppController.MainWindowTitleNoVersion
                eventViewerLog.EnableRaisingEvents = True
            End If
            Return eventViewerLog
        End Get
    End Property

    Private Shared Sub AddEventLogEntry(ByVal exception As Exception)
        Try
            Dim logMessage As String = String.Empty
            logMessage &= "Message :" & Environment.NewLine
            logMessage &= exception.Message & Environment.NewLine
            logMessage &= "Stack Trace :" & Environment.NewLine
            logMessage &= exception.StackTrace & Environment.NewLine
            eventViewerLog.WriteEntry(logMessage, EventLogEntryType.Error)
        Catch ex As Exception
        End Try
    End Sub

    '    Private Shared Sub AddEventLogEntry(LogAction As LogActions, Description As String)
    '        Try
    '            Dim logMessage As String = String.Empty
    '            logMessage &= "LogAction :" & Environment.NewLine
    '            logMessage &= LogAction.ToString & Environment.NewLine
    '            logMessage &= "Description :" & Environment.NewLine
    '            logMessage &= Description
    '            eventViewerLog.WriteEntry(logMessage, EventLogEntryType.Error)
    '        Catch ex As Exception
    '        End Try
    '    End Sub

    '    Public Shared Sub AddLogRecord(LogAction As LogActions, Description As String)
    '        Try
    '            If IsNothing(AppController.DBlayer) = False AndAlso AppController.DBlayer.AddLogRecord(LogAction, Description) Then
    '            Else
    '                AddEventLogEntry(LogAction, Description)
    '            End If
    '        Catch ex As Exception
    '            AddEventLogEntry(LogAction, Description)
    '        End Try
    '    End Sub

    '    Public Shared Sub AddLogRecord(SubFunctionName As String, ex As Exception)
    '        Try
    '            If AppController.DBlayer.AddLogRecord(LogActions.Error, SubFunctionName & vbNewLine & ex.StackTrace) Then
    '            Else
    '                AddEventLogEntry(ex)
    '            End If
    '        Catch ex2 As Exception
    '            AddEventLogEntry(ex)
    '        End Try
    '#If DEBUG Then
    '        Console.WriteLine(SubFunctionName & vbNewLine & ex.StackTrace)
    '#End If
    '    End Sub

    '    Public Shared Sub AddMCSProjectLogRecord(MCSProjectConnection As String, LogAction As LogActions, Description As String)
    '        Try
    '            If AppController.DBlayer.AddLogRecord(MCSProjectConnection, LogAction, Description) Then
    '            Else
    '                AddEventLogEntry(LogAction, Description)
    '            End If
    '        Catch ex As Exception
    '            AddEventLogEntry(LogAction, Description)
    '        End Try
    '    End Sub

    '    Public Shared Sub AddMCSProjectLogRecord(MCSProjectConnection As String, ex As Exception)
    '        Try
    '            If AppController.DBlayer.AddLogRecord(MCSProjectConnection, LogActions.Error, ex.StackTrace) Then
    '            Else
    '                AddEventLogEntry(ex)
    '            End If
    '        Catch ex2 As Exception
    '            AddEventLogEntry(ex)
    '        End Try
    '    End Sub

End Class
