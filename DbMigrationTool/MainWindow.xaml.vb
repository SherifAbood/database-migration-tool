﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.ComponentModel
Imports System.Windows.Forms
Imports DevExpress.Xpf.Grid
Imports DevExpress.Xpf.Core
Imports DevExpress.Xpf.Editors
Imports DevExpress.Xpf.Core.Native
Imports Microsoft.SqlServer.Management.Smo
Imports Microsoft.SqlServer.Management.Common
Class MainWindow
    Implements INotifyPropertyChanged

    'Public Event PropertyChanged(sender As Object, e As PropertyChangedEventArgs) Implements INotifyPropertyChanged.PropertyChanged
    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged

#Region "Attribute"

    Dim srcDBproc As String = String.Empty
    Dim srcDBlog As String = String.Empty
    Dim dstDBproc As String = String.Empty
    Dim dstDBlog As String = String.Empty
    Dim srcDBmcs As String = String.Empty
    Dim dstDBmcs As String = String.Empty
    Dim serverConnString = String.Empty

    Dim server As New strctServer
    Private _serverConn As New clsServersConnection
    Private _serverConnection As New clsServersConnection
    Private _serversNames As List(Of String)

    Private _lstOfMCSProjects As BindingList(Of clsOfflineDbMigration) = New BindingList(Of clsOfflineDbMigration)
    Private ValidYear As Boolean = False
    Private _dbLayer As New clsDatabaseLayer
    Private _createDB As New CreateNewDB
    Private _listOfPipes As New BindingList(Of clsOfflinePipe)
    Private _selectedServer As New strctServer
    Private SelectedLstOfMCSProjects As New List(Of clsOfflineDbMigration)
    Private SelectedLstOfPipes As New List(Of clsOfflinePipe)

    Private validatedText As Boolean = False
    Private dataSaved As Boolean = True
#End Region

#Region "Property"

    Private _WaitViewData As String
    Public Property WaitViewData() As String
        Get
            Return _WaitViewData
        End Get
        Set(ByVal value As String)
            _WaitViewData = value
        End Set
    End Property

    Private _mcsProjectScript As String = "./McsScript5.sql"
    Public Property MCSProjectScript() As String
        Get
            Return _mcsProjectScript
        End Get
        Set(ByVal value As String)
            _mcsProjectScript = value
        End Set
    End Property


    Private _pipeScript As String = "./PipeScript5.sql"
    Public Property PipeScript() As String
        Get
            Return _pipeScript
        End Get
        Set(ByVal value As String)
            _pipeScript = value
        End Set
    End Property

    Public Property LstOfMCSProjects() As BindingList(Of clsOfflineDbMigration)
        Get
            Return _lstOfMCSProjects
        End Get
        Set(ByVal value As BindingList(Of clsOfflineDbMigration))
            _lstOfMCSProjects = value
            RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs("LstOfMCSProjects"))
        End Set
    End Property

    Private _lstOfPipes As BindingList(Of clsOfflinePipe)
    Public Property LstOfPipes() As BindingList(Of clsOfflinePipe)
        Get
            Return _lstOfPipes
        End Get
        Set(ByVal value As BindingList(Of clsOfflinePipe))
            _lstOfPipes = value
            RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs("LstOfPipes"))
        End Set
    End Property

    Public Property ServersNames() As List(Of String)
        Get
            Return _serversNames
        End Get
        Set(ByVal value As List(Of String))
            _serversNames = value
            RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs("ServersNames"))
        End Set
    End Property

#End Region

    Sub New()

        ' This call is required by the designer.
        InitializeComponent()
        _serverConnection = New clsServersConnection
        _serversNames = _serverConnection.GetServersNames()
        Me.DataContext = Me
    End Sub


#Region "WindowEvents"

#Region "Connect"

    Private Sub imgConnect_MouseLeftButtonDown(sender As Object, e As MouseButtonEventArgs)
        Try

            LstOfMCSProjects.Clear()

            ShowWaitPanel()
            WaitViewData = "Wait, Connecting to Server..."

            ConnectServer()
            HideWaitPanel()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub pwdUser_KeyDown(sender As Object, e As Input.KeyEventArgs)
        If e.Key = Key.Enter Then
            LstOfMCSProjects.Clear()
            ShowWaitPanel()
            WaitViewData = "Connecting to Server..."
            ConnectServer()
            HideWaitPanel()
        End If
    End Sub

#End Region

    Private Sub imgMCSProjectScriptPath_MouseLeftButtonDown(sender As Object, e As MouseButtonEventArgs)
        Dim FileType = ".sql"
        txtMCSProjectScript.Text = OpenFile(FileType)
        MCSProjectScript = txtMCSProjectScript.Text
    End Sub

    Private Sub imgPipeScriptPath_MouseLeftButtonDown(sender As Object, e As MouseButtonEventArgs)
        Dim FileType = ".sql"
        txtPipeScript.Text = OpenFile(FileType)

        PipeScript = txtPipeScript.Text
    End Sub

    Private Sub chkPipes_Checked(sender As Object, e As RoutedEventArgs)
        Dim SelectedPipe As New clsOfflinePipe
        SelectedPipe = Me.tblPipes.DataControl.SelectedItem()
        Dim index As Integer

        Try
            index = SelectedPipe.MCSProjectId

        Catch ex As Exception
            Exit Sub
        End Try

        Dim mcsProject As New clsOfflineDbMigration
        mcsProject = LstOfMCSProjects.Where(Function(project) project.Id = index).First
        If mcsProject.IsMCSProjectChecked Then
            If String.IsNullOrWhiteSpace(SelectedPipe.PipeYear) Then
                AppController.ShowDXMessage("Please insert a year for your selected pipe first.", MessageBoxButton.OK, MessageBoxImage.Information)
                SelectedPipe.IsPipeChecked = False
            End If
        Else
            AppController.ShowDXMessage("Please select MCS Project first", MessageBoxButton.OK, MessageBoxImage.Information)
            SelectedPipe.IsPipeChecked = False
        End If

    End Sub

    Private Sub imgOk_Click(sender As Object, e As RoutedEventArgs)
        MigratingChanges()
    End Sub
    'Private Sub DoChanges()
    '    SelectedLstOfMCSProjects = LstOfMCSProjects.Where(Function(item) item.IsMCSProjectChecked = True).ToList()

    'End Sub
    Private Sub clmnYear_Validate(sender As Object, e As DevExpress.Xpf.Grid.GridCellValidationEventArgs)  ''working for starting row only !!! -_-
        If (Not ValidateYear(CStr(e.Value))) Then
            e.SetError("Year entry should be 4 digits only, starting with 1 or 2")
        Else
            e.IsValid = True
            grdPipes.RefreshData()

        End If
    End Sub

    Private Sub clmnMCSProjectYear_Validate(sender As Object, e As GridCellValidationEventArgs)
        If (Not ValidateYear(CStr(e.Value))) Then
            e.SetError("Year entry should be 4 digits only, starting with 1 or 2")
        Else
            e.IsValid = True
            Dim selectedRow As Object
            selectedRow = tblMCSProjects.FocusedRowData.Row
            Dim tempIndex As Integer
            tempIndex = selectedRow.Id
            If e.Value <> String.Empty Then
                Dim msgwindow As MessageWindow = New MessageWindow("Please specify your pipe edit scope... ", "Override All", "Set Blank")
                msgwindow.ShowDialog()

                If msgwindow.result = True Then
                    For Each pip In LstOfMCSProjects.Where(Function(project) project.Id = tempIndex).First.LstOfPipes
                        If pip.PipeYear = String.Empty Then
                            pip.PipeYear = e.Value

                        End If
                    Next
                ElseIf msgwindow.result = False Then
                    For Each pip In LstOfMCSProjects.Where(Function(project) project.Id = tempIndex).First.LstOfPipes
                        pip.PipeYear = e.Value
                    Next
                End If

            End If
            grdPipes.RefreshData()

        End If
    End Sub

#Region "Closing"
    Private Sub imgCancelAndClose_MouseLeftButtonDown(sender As Object, e As MouseButtonEventArgs)
        Me.Close()
    End Sub

    Private Sub MainWindow_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
        If LstOfMCSProjects.Count <> 0 Then
            If LstOfMCSProjects.Any(Function(proj) (proj.IsMCSProjectChecked = True)) Then
                dataSaved = False
            Else
                dataSaved = True
            End If
        End If
        If Not dataSaved Then
            If closingMsg() = MessageBoxResult.Yes Then
                e.Cancel = False
            Else
                e.Cancel = True
            End If
        Else
            e.Cancel = False
        End If
    End Sub

#End Region

#End Region

#Region "MainFunctions"
    Public Sub ConnectServer()
        If String.IsNullOrEmpty(pwdUser.Text) OrElse String.IsNullOrEmpty(txtUserName.Text) OrElse String.IsNullOrEmpty(cmbServerName.Text) Then
            AppController.ShowDXMessage("There are missing data !", MessageBoxButton.OK, MessageBoxImage.Information)
            Exit Sub
        End If

        server.Name = cmbServerName.Text
        server.UserName = txtUserName.Text
        server.PassWord = pwdUser.Text
        server.ConnectionString = "Server=" & cmbServerName.Text & ";User Id=" & txtUserName.Text & ";Password=" & pwdUser.Text & ";Persist Security Info=True"

        If TestConn(server.ConnectionString) Then
            AppController.ShowDXMessage("Server Connected", MessageBoxButton.OK, MessageBoxImage.Information)

            serverConnString = server.ConnectionString

            ShowWaitPanel()
            WaitViewData = "Loading MCS Projects and Pipes..."

            ''Enable controls
            imgOk.IsEnabled = True
            Me.lblMCSProject.IsEnabled = True
            Me.txtMCSProjectScript.IsEnabled = True
            Me.imgMCSProjectScriptPath.IsEnabled = True
            Me.lblPipe.IsEnabled = True
            Me.txtPipeScript.IsEnabled = True
            Me.imgPipeScriptPath.IsEnabled = True
            Me.grdProjects.IsEnabled = True
            Me.imgCancelAndClose.IsEnabled = True
            GetMCSProjects(server)
        Else
            Me.grdProjects.IsEnabled = False
            AppController.ShowDXMessage("Server Connection Failed!", MessageBoxButton.OK, MessageBoxImage.Error)
        End If
        HideWaitPanel()
    End Sub

    Private Function TestConn(ByVal strConn As String) As Boolean
        Dim connobj As New SqlConnection
        Try
            connobj.ConnectionString = strConn
            connobj.Open()
            connobj.Close()
            Return True
        Catch ex As Exception
            AppController.ShowDXMessage("Invalid Server Connection !", MessageBoxButton.OK, MessageBoxImage.Information)

            Return False
        End Try
    End Function

    Private Function GetMCSProjects(ByVal server As strctServer) As Boolean
        Dim successed As Boolean
        Dim counter As Integer
        counter = 1
        Try
            If _dbLayer.PingServer(server) Then
                For Each mcsProject In _dbLayer.GetMCSProjects(server)
                    Dim bindingMCSProject As clsOfflineDbMigration
                    bindingMCSProject = New clsOfflineDbMigration
                    bindingMCSProject.Id = counter
                    bindingMCSProject.MCSName = mcsProject.Name
                    bindingMCSProject.ConnectionString = mcsProject.DBConnectionString
                    bindingMCSProject.LstOfPipes = GetPipes(bindingMCSProject, counter)
                    bindingMCSProject.Version = mcsProject.MCSProjectVersion
                    LstOfMCSProjects.Add(bindingMCSProject)
                    counter += 1

                Next

            End If

        Catch ex As Exception
            successed = False
        End Try

        Return successed
    End Function

    Function GetPipes(ByRef mcsProject As clsOfflineDbMigration, ByVal mcsProjectId As Integer) As BindingList(Of clsOfflinePipe)
        Try
            For Each pipe In _dbLayer.GetOldPipes(mcsProject)
                Dim bindingPipe As clsOfflinePipe
                bindingPipe = New clsOfflinePipe
                bindingPipe.Id = pipe.Id
                bindingPipe.MCSProjectId = mcsProjectId
                bindingPipe.MCSProjectName = mcsProject.MCSName
                bindingPipe.PipeName = pipe.PipeName
                bindingPipe.ProcessingDb = pipe.ProcessingDb
                bindingPipe.LoggingDb = pipe.LoggingDb
                bindingPipe.PipeYear = If(pipe.PipeYear Is Nothing, String.Empty, pipe.PipeYear)
                mcsProject.LstOfPipes.Add(bindingPipe)
            Next

            Return mcsProject.LstOfPipes
        Catch ex As Exception
            'Return False
        End Try
    End Function

    Private Function OpenFile(fileType As String) As String
        Try
            'Create OpenFileDialog 
            Dim dlg As New OpenFileDialog()

            'Set filter for file extension And default file extension
            If fileType = ".sql" Then
                dlg.DefaultExt = ".sql"
                dlg.Filter = "Text documents (.sql)|*.sql"
            End If
            ' Display OpenFileDialog by calling ShowDialog method 
            Dim result As Nullable(Of Boolean) = dlg.ShowDialog()

            'Get the selected file name And display in a TextBox 
            If result = True Then
                'Open document 
                Dim filename As String = dlg.FileName
                Return filename
            End If
        Catch ex As Exception
            Return AppController.ShowDXMessage("File doesn't exist", MessageBoxButton.OK, MessageBoxImage.Information)
        End Try

    End Function

    Private Sub MigratingChanges()
        SelectedLstOfMCSProjects = LstOfMCSProjects.Where(Function(item) item.IsMCSProjectChecked = True).ToList()

        For Each project In SelectedLstOfMCSProjects
            For Each pipe In project.LstOfPipes.Where(Function(x) x.IsPipeChecked = True).ToList
                SelectedLstOfPipes.Add(pipe)
            Next

            If Not Validation() Then
                Exit Sub
            End If

            For Each newMCSProject In SelectedLstOfMCSProjects
                Dim _conn As New SqlConnection(serverConnString)
                Try
                    Dim selectedpipe As List(Of clsOfflinePipe) = SelectedLstOfPipes.Where(Function(pip) (pip.MCSProjectId = newMCSProject.Id)).ToList
                    ShowWaitPanel()
                    WaitViewData = "Importing Data from MCS Project... " + Environment.NewLine + "MCS Project Name: " + newMCSProject.MCSName

                    dstDBmcs = newMCSProject.MCSName + "_V5"
                    srcDBmcs = newMCSProject.MCSName
                    _createDB.CreateMCSProject(MCSProjectScript, serverConnString, dstDBmcs)
                    Dim importMCSData As ImportingData = New ImportingData(cmbServerName.Text, dstDBmcs, newMCSProject.MCSName, txtUserName.Text, pwdUser.Text)

                    If _createDB.DBexists > 0 Then
                        importMCSData.Import("Server=" & cmbServerName.Text & ";initial catalog=" & dstDBmcs & ";User Id=" & txtUserName.Text & ";Password=" & pwdUser.Text & ";Persist Security Info=True", selectedpipe, True)

                    Else
                        importMCSData.Import("Server=" & cmbServerName.Text & ";initial catalog=" & dstDBmcs & ";User Id=" & txtUserName.Text & ";Password=" & pwdUser.Text & ";Persist Security Info=True", selectedpipe, False)

                    End If
                    Dim script As String = "USE [" + dstDBmcs + "]" + Environment.NewLine + "GO " + Environment.NewLine + importMCSData.appendCols


                    If _conn.State <> ConnectionState.Open Then _conn.Open()
                    Dim con As New ServerConnection(_conn)
                    Dim _server As New Server(con)

                    _server.ConnectionContext.ExecuteNonQuery(script, ExecutionTypes.ContinueOnError)
                    If _conn.State <> ConnectionState.Closed Then _conn.Close()
                    HideWaitPanel()
                Catch ex As Exception
                    If _conn.State <> ConnectionState.Closed Then _conn.Close()
                End Try
            Next
        Next

        For Each newPipe In SelectedLstOfPipes
            Dim _conn As New SqlConnection(serverConnString)
            Try
                ShowWaitPanel()
                WaitViewData = "Importing Data from Pipe... " + Environment.NewLine + "Pipe Name: " + newPipe.PipeName

                dstDBproc = "V5_" + newPipe.ProcessingDb.ToString.Split(";").Where(Function(pip) (pip.Contains("Initial Catalog="))).First.Replace("Initial Catalog=", "")
                dstDBlog = "V5_" + newPipe.LoggingDb.ToString.Split(";").Where(Function(pip) (pip.Contains("Initial Catalog="))).First.Replace("Initial Catalog=", "")
                srcDBproc = newPipe.ProcessingDb.ToString.Split(";").Where(Function(pip) (pip.Contains("Initial Catalog="))).First.Replace("Initial Catalog=", "")
                srcDBlog = newPipe.LoggingDb.ToString.Split(";").Where(Function(pip) (pip.Contains("Initial Catalog="))).First.Replace("Initial Catalog=", "")
                _createDB.CreatePipe(PipeScript, serverConnString, dstDBproc)
                _createDB.CreatePipe(PipeScript, serverConnString, dstDBlog)

                '<<Importing Data for Proc Pipe =======================================================================================>>
                Dim importMCSData As ImportingData = New ImportingData(cmbServerName.Text, dstDBproc, srcDBproc, txtUserName.Text, pwdUser.Text)
                importMCSData.Import("Server=" & cmbServerName.Text & ";initial catalog=" & dstDBproc & ";User Id=" & txtUserName.Text & ";Password=" & pwdUser.Text & ";Persist Security Info=True", Nothing, False)
                Dim script As String = "USE [" + dstDBproc + "]" + Environment.NewLine + "GO " + Environment.NewLine + importMCSData.appendCols

                '  Dim _conn As New SqlConnection(serverConnString)
                If _conn.State <> ConnectionState.Open Then _conn.Open()
                Dim con As New ServerConnection(_conn)
                Dim _server As New Server(con)

                If _conn.State <> ConnectionState.Open Then _conn.Open()
                _server.ConnectionContext.ExecuteNonQuery(script, ExecutionTypes.ContinueOnError)
                If _conn.State <> ConnectionState.Closed Then _conn.Close()
                '<<Importing Data for Log Pipe =======================================================================================>>
                importMCSData = New ImportingData(cmbServerName.Text, dstDBlog, srcDBlog, txtUserName.Text, pwdUser.Text)
                importMCSData.Import("Server=" & cmbServerName.Text & ";initial catalog=" & dstDBlog & ";User Id=" & txtUserName.Text & ";Password=" & pwdUser.Text & ";Persist Security Info=True", Nothing, False)
                script = "USE [" + dstDBlog + "]" + Environment.NewLine + "GO " + Environment.NewLine + importMCSData.appendCols


                If _conn.State <> ConnectionState.Open Then _conn.Open()
                con = New ServerConnection(_conn)
                _server = New Server(con)

                If _conn.State <> ConnectionState.Open Then _conn.Open()
                _server.ConnectionContext.ExecuteNonQuery(script, ExecutionTypes.ContinueOnError)
                If _conn.State <> ConnectionState.Closed Then _conn.Close()

            Catch ex As Exception
                If _conn.State <> ConnectionState.Open Then _conn.Close()
            End Try
            HideWaitPanel()
        Next
        dataSaved = True
        AppController.ShowDXMessage("Database Migration has completed successfully.", MessageBoxButton.OK, MessageBoxImage.Information)
    End Sub

    Private Function closingMsg() As MessageBoxResult
        Dim result = AppController.ShowDXMessage("Changes you made are not saved." + Environment.NewLine + "Are you sure you want to close without migrating changes ?", MessageBoxButton.YesNo, MessageBoxImage.Warning)
        Return result
    End Function

    Private Sub ShowWaitPanel()
        waitview.IsSplashScreenShown = True
    End Sub

    Private Sub HideWaitPanel()
        waitview.IsSplashScreenShown = False
    End Sub

#End Region

#Region "Validation"
    Public Function Validation() As Boolean
        Dim result As New Object
        'If String.IsNullOrWhiteSpace(txtMCSProjectScript.Text) Or String.IsNullOrWhiteSpace(txtPipeScript.Text) Then
        '    AppController.ShowDXMessage("Please load MCS Project Script & Pipe Script first.", MessageBoxButton.OK, MessageBoxImage.Exclamation)
        '    Return False
        'If Not AppController.ValidatePath(txtMCSProjectScript.Text, AppController.enumPathType.File, lblMCSProject.Content, True, ".sql") Then
        '    txtMCSProjectScript.Text = String.Empty
        '    Return False
        'ElseIf Not AppController.ValidatePath(txtPipeScript.Text, AppController.enumPathType.File, lblPipe.Content, True, ".sql") Then
        '    txtPipeScript.Text = String.Empty
        '    Return False
        If SelectedLstOfMCSProjects.Count = 0 Then
            AppController.ShowDXMessage("You haven't selected any MCS projects, please select at least one MCS project.", MessageBoxButton.OK, MessageBoxImage.Information)
            Return False
        Else
            imgOk.IsEnabled = True
            Return True
        End If

    End Function

    Private Function ValidateYear(ByVal txtInput As String) As Boolean
        Try
            If txtInput.Length <> 4 Or txtInput Is Nothing Then
                ValidateYear = False

            Else
                Dim num As Boolean
                Dim start As Boolean
                If txtInput.StartsWith("1") Or txtInput.StartsWith("2") Then
                    start = True
                End If
                For Each number As Char In txtInput
                    Dim n As Integer
                    If Integer.TryParse(number, n) Then

                        num = True
                    End If
                Next
                ValidateYear = num AndAlso start
            End If
        Catch ex As Exception
            ValidateYear = True
        End Try

    End Function

#End Region
End Class
