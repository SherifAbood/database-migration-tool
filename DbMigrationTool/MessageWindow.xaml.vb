﻿Public Class MessageWindow
    Public Property result As Nullable(Of Boolean) = Nothing

    Sub New(message As String, button1content As String, button2content As String)
        InitializeComponent()

        Button1.Content = button1content
        Button2.Content = button2content
        lblmsg.Content = message
    End Sub

    Private Sub Cancel_Click(sender As Object, e As RoutedEventArgs)
        Me.DialogResult = False
        result = Nothing
        Close()
    End Sub

    Private Sub Button1_Click(sender As Object, e As RoutedEventArgs)
        Me.DialogResult = True
        result = False
        Close()
    End Sub

    Private Sub Button2_Clik(sender As Object, e As RoutedEventArgs)
        Me.DialogResult = True
        result = True
        Close()
    End Sub
End Class
